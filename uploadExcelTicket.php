<?php
// require_once dirname(__FILE__) . '/sessionLoginChecker.php';
if (session_id() == "")
{
    session_start();
}
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

$timestamp = time();

include 'selectFilecss.php';
// include 'js.php';

require_once('vendor/php-excel-reader/excel_reader2.php');
require_once('vendor/SpreadsheetReader.php');

if (isset($_POST["import"]))
{
  $allowedFileType = ['application/vnd.ms-excel','text/xls','text/xlsx','application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'];

  if(in_array($_FILES["file"]["type"],$allowedFileType))
  {
    // $targetPath = 'uploads/'.$_FILES['file']['name'];
    $targetPath = 'uploads/'.$timestamp.$_FILES['file']['name'];
    move_uploaded_file($_FILES['file']['tmp_name'], $targetPath);
    $Reader = new SpreadsheetReader($targetPath);
    $sheetCount = count($Reader->sheets());
    for($i=0;$i<$sheetCount;$i++)
    {
      $Reader->ChangeSheet($i);
      foreach ($Reader as $Row)
      {

        $country = "";
        if(isset($Row[0])) 
        {
          $country = mysqli_real_escape_string($conn,$Row[0]);
        }
        $state = "";
        if(isset($Row[0])) 
        {
          $state = mysqli_real_escape_string($conn,$Row[1]);
        }
        $photo = "";
        if(isset($Row[0])) 
        {
          $photo = mysqli_real_escape_string($conn,$Row[2]);
        }
        $title = "";
        if(isset($Row[0])) 
        {
          $title = mysqli_real_escape_string($conn,$Row[3]);
        }
        $merchant = "";
        if(isset($Row[0])) 
        {
          $merchant = mysqli_real_escape_string($conn,$Row[4]);
        }
        $description = "";
        if(isset($Row[0])) 
        {
          $description = mysqli_real_escape_string($conn,$Row[5]);
        }



        $packageA = "";
        if(isset($Row[0])) 
        {
          $packageA = mysqli_real_escape_string($conn,$Row[6]);
        }
        $unitA = "";
        if(isset($Row[0])) 
        {
          $unitA = mysqli_real_escape_string($conn,$Row[7]);
        }

        $packageB = "";
        if(isset($Row[0])) 
        {
          $packageB = mysqli_real_escape_string($conn,$Row[8]);
        }
        $unitB = "";
        if(isset($Row[0])) 
        {
          $unitB = mysqli_real_escape_string($conn,$Row[9]);
        }

        $packageC = "";
        if(isset($Row[0])) 
        {
          $packageC = mysqli_real_escape_string($conn,$Row[10]);
        }
        // $unitA = "";
        // if(isset($Row[0])) 
        // {
        //   $unitA = mysqli_real_escape_string($conn,$Row[9]);
        // }
        // $unitB = "";
        // if(isset($Row[0])) 
        // {
        //   $unitB = mysqli_real_escape_string($conn,$Row[10]);
        // }
        $unitC = "";
        if(isset($Row[0])) 
        {
          $unitC = mysqli_real_escape_string($conn,$Row[11]);
        }



        $notes = "";
        if(isset($Row[0])) 
        {
          $notes = mysqli_real_escape_string($conn,$Row[12]);
        }
        $terms = "";
        if(isset($Row[0])) 
        {
          $terms = mysqli_real_escape_string($conn,$Row[13]);
        }

        $uid = md5(uniqid());

        if (!empty($country) || !empty($state) || !empty($photo) || !empty($title) || !empty($merchant) || !empty($description) || !empty($packageA) || !empty($packageB) || !empty($packageC) || !empty($unitA) || !empty($unitB) || !empty($unitC) || !empty($notes) || !empty($terms))
        {
          $query = "INSERT INTO ticket (uid,country,state,photo,title,merchant_name,description,package_a,package_b,package_c,unit_a,unit_b,unit_c,notes,terms) 
                    VALUES ('".$uid."','".$country."','".$state."','".$photo."','".$title."','".$merchant."','".$description."','".$packageA."','".$packageB."','".$packageC."','".$unitA."','".$unitB."','".$unitC."','".$notes."','".$terms."') ";

          $result = mysqli_query($conn, $query);
          if (! empty($result))
          {
            // echo "<script>alert('Excel Uploaded !');window.location='../telemarketing/uploadExcel.php'</script>";       
            echo "Uploaded !";
          }
          else 
          {
            echo "Fail !";
          }
        }
      }
    }
  }
  else
  {
    echo "ERROR !";
  }
}
?>

<!DOCTYPE html>
<html>
<head>
  <?php include 'meta.php'; ?>
  <!-- <meta property="og:url" content="https://qlianmeng.asia/addReferee.php" /> -->
  <meta property="og:title" content="Import Ticket | Tobigo" />
  <title>Import Ticket | Tobigo</title>
  <!-- <link rel="canonical" href="https://qlianmeng.asia/addReferee.php" /> -->
  <?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'headerAdmin.php'; ?>
<div class="clear"></div>
<div class="width100 overflow menu-distance same-padding ow-sakura-height">
	<h1 class="title-h1 raleway red-text wow fadeIn" data-wow-delay="0.1s">Import Excel File for Ticket</a></h1>

  <div class="outer-container text-center ">
    <form action="" method="post" name="frmExcelImport" id="frmExcelImport" enctype="multipart/form-data">
      <label>Select File</label><br><input type="file" name="file" id="file" accept=".xls,.xlsx"><div class="clear"></div>
      <button type="submit" id="submit" name="import"  class="clean red-btn margin-top30 fix300-btn margin-left0">Submit</button>
      <div id="response" class="<?php if(!empty($type)) { echo $type . " display-block"; } ?>"><?php if(!empty($message)) { echo $message; } ?></div>
    </form>
  </div>
</div>
<div class="clear"></div>

<style>
.footer-div{
	display:none;}
</style>
<div class="clear"></div>	
<div class="footer-div" style="display:block !important;">
	<p class="footer-p wow fadeIn" data-wow-delay="0.1s">&copy;<span id="year"></span> Tabigo, All Rights Reserved.</p>
</div>
</body>
</html>