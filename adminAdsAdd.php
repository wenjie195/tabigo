<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Countries.php';
require_once dirname(__FILE__) . '/classes/Slider.php';
// require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

$allCountries = getCountries($conn);
$adsDetails = getSlider($conn, "WHERE status = 'Show' ");

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>
<!doctype html>
<html>
<head>
	<?php include 'meta.php'; ?>
<!--         <meta property="og:url" content="https://tabigo.holiday/" />
        <link rel="canonical" href="https://tabigo.holiday/" />-->
        <meta property="og:title" content="Add Advertisement | Tabigo" />
        <title>Add Advertisement | Tabigo</title>   
	<?php include 'css.php'; ?>


</head>

<body class="body">
<?php include 'headerAdmin.php'; ?>

<div class="clear"></div>

<div class="width100 overflow menu-distance same-padding ow-sakura-height">

	<!-- <h1 class="title-h1 raleway red-text ow-no-margin wow fadeIn" data-wow-delay="0.1s">Tour Package <a href="adminAddTour.php" class="pink-link hover-effect underline">(Add)</a></h1>	 -->
    <h1 class="title-h1 raleway red-text ow-no-margin">Add Advertisement </h1>	

    <!-- <form method="POST" action="utilities/adminSliderUpdateFunction.php" enctype="multipart/form-data"> -->
    <form method="POST" action="utilities/adminSliderAddFunction.php" enctype="multipart/form-data"> 

    	<div class="width100 overflow">
        	<p class="input-top-p admin-top-p">Link</p>
        	<input class="tele-input clean" type="text" placeholder="https://" id="link" name="link" required>  
        </div>
  
    	<div class="width100 overflow">
        	<p class="input-top-p admin-top-p">Country</p>
        	<!-- <input class="tele-input clean" type="text" placeholder="Country" id="country" name="country" required>   -->
            <select class="clean tele-input"  id="country" name="country" required>
                <option>Select Country</option>
                <?php
                for ($cntPro=0; $cntPro <count($allCountries) ; $cntPro++)
                {
                ?>
                  <option value="<?php echo $allCountries[$cntPro]->getEnName();?>">
                    <?php echo $allCountries[$cntPro]->getEnName();?>
                  </option>
                <?php
                }
                ?>
            </select>
        </div>

    	<div class="width100 overflow">
        	<p class="input-top-p admin-top-p">Upload New Image (Less than 1.8mb)</p>
        	<!-- <input class="clean"  type="file" >   -->
            <input class="clean" type="file" name="image_one" id="image_one" accept="image/*" required>
        </div>        

        <div class="width100 text-center">
            <button class="clean red-btn hover-effect middle-button-size below-forgot margin-bottom30" name="submit">Save</button>
        </div>
    </form>

</div>

<div class="clear"></div>

<?php include 'js.php'; ?>

</body>
</html>