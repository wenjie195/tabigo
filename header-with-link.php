<?php
if(isset($_SESSION['uid']))
{
?>
   <?php
    if($_SESSION['user_type'] == 0)
    //admin
    {
    ?>
<header id="header" class="header header--fixedcheader1 menu-color" role="banner" >
        <div class="big-container-size hidden-padding" id="top-menu">
			<div class="red-bg top-fix-row width100 same-padding overflow">
				<p class="left-top-p white-text"><img src="img/phone.png" alt="Contact Us" title="Contact Us" class="top-icon phone-img"> <a href="tel:+60379540196" class="call-a">+603-7954 0196</a> &nbsp;  &nbsp; | &nbsp;  &nbsp; <a href="tel:+60379540096" class="call-a">+603-7954 0096</a> &nbsp;  &nbsp;   &nbsp;
				<span class="email-span"><img src="img/email.png" alt="Contact Us" title="Contact Us" class="top-icon email-img"> outbound@golden-adv.com</span>
				</p>
				<p class="right-top-p"><span class="email-span2"><img src="img/email.png" alt="Contact Us" title="Contact Us" class="top-icon email-img"> outbound@golden-adv.com &nbsp;  &nbsp; </span><a href="https://www.instagram.com/tabigo_asia/" target="_blank"><img src="img/instagram.png" class="top-icon insta-icon"></a> &nbsp; <a href="https://www.facebook.com/Tabigo.Asia" target="_blank"><img src="img/facebook.png" class="top-icon fb-icon"></a></p>

			</div>			
			<div class="clear"></div>
			
			
			<div class="same-padding opacity-white-bg width100">
				<div class="float-left left-logo-div">
					<img src="img/logo2.png" class="logo-img" alt="Tabigo" title="Tabigo">
				</div>



				<div class="right-menu-div float-right before-header-div">
            
					<a href="adminOrder.php" class="menu-margin-right menu-item opacity-hover hover-effect user-header">
						Order
					</a>  
                        <div class="dropdown menu-margin-right menu-item opacity-hover hover-effect user-header">
                            Details
                            <img src="img/dropdown.png" class="dropdown-img" >
                            <div class="dropdown-content yellow-dropdown-content">
                                <p class="dropdown-p opacity-hover"><a href="adminTour.php" class="menu-item">Tour Package</a></p>
                                <p class="dropdown-p opacity-hover"><a href="adminRestaurant.php" class="menu-item">Restaurant</a></p>
                                <p class="dropdown-p opacity-hover"><a href="adminTransportation.php" class="menu-item">Transportation</a></p>
                                <p class="dropdown-p opacity-hover"><a href="adminPlace.php" class="menu-item">Place of Interest</a></p>
                                <p class="dropdown-p opacity-hover"><a href="adminTicket.php" class="menu-item">Ticket Booking</a></p>
                                <p class="dropdown-p opacity-hover"><a href="adminWifi.php" class="menu-item">Wifi</a></p>
                                <p class="dropdown-p opacity-hover"><a href="adminHotel.php" class="menu-item">Hotel & Flight</a></p>
                                <p class="dropdown-p opacity-hover"><a href="adminInsurance.php" class="menu-item">Insurance</a></p>
                            </div>
                        </div>  
					<a href="logout.php" class="menu-margin-right menu-item opacity-hover hover-effect user-header">
						Logout
					</a> 
                                                
                                    <div id="dl-menu" class="dl-menuwrapper before-dl">
                                        <button class="dl-trigger">Open Menu</button>
                                        <ul class="dl-menu">
                                        <li><a href="adminOrder.php" class="black-text opacity-hover">Order</a></li>
                                        <li><a href="adminTour.php" class="black-text opacity-hover">Tour Package</a></li>
                                        <li><a href="adminRestaurant.php" class="black-text opacity-hover">Restaurant</a></li>
                                        <li><a href="adminTransportation.php" class="black-text opacity-hover">Transportation</a></li>                                        <li><a href="adminPlace.php" class="black-text opacity-hover">Place of Interest</a></li>
                                        <li><a href="adminTicket.php" class="black-text opacity-hover">Ticket Booking</a></li>
                                        <li><a href="adminWifi.php" class="black-text opacity-hover">Wifi</a></li>                                        <li><a href="adminHotel.php" class="black-text opacity-hover">Hotel & Flight</a></li> 
                                        <li><a href="adminInsurance.php" class="black-text opacity-hover">Insurance</a></li> 
                                        <li><a  href="logout.php" class="black-text opacity-hover">Logout</a></li>
                                        </ul>
                                    </div>					



				</div>
			</div>
        </div>

</header>


    <?php
    }
    elseif($_SESSION['user_type'] == 1)
    //user
    {
    ?>
<header id="header" class="header header--fixedcheader1 menu-color" role="banner" >
        <div class="big-container-size hidden-padding" id="top-menu">
			<div class="red-bg top-fix-row width100 same-padding overflow">
				<p class="left-top-p white-text"><img src="img/phone.png" alt="Contact Us" title="Contact Us" class="top-icon phone-img"> <a href="tel:+60379540196" class="call-a">+603-7954 0196</a> &nbsp;  &nbsp; | &nbsp;  &nbsp; <a href="tel:+60379540096" class="call-a">+603-7954 0096</a> &nbsp;  &nbsp;   &nbsp;
				<span class="email-span"><img src="img/email.png" alt="Contact Us" title="Contact Us" class="top-icon email-img"> outbound@golden-adv.com</span>
				</p>
				<p class="right-top-p"><span class="email-span2"><img src="img/email.png" alt="Contact Us" title="Contact Us" class="top-icon email-img"> outbound@golden-adv.com &nbsp;  &nbsp; </span><a href="https://www.instagram.com/tabigo_asia/" target="_blank"><img src="img/instagram.png" class="top-icon insta-icon"></a> &nbsp; <a href="https://www.facebook.com/Tabigo.Asia" target="_blank"><img src="img/facebook.png" class="top-icon fb-icon"></a></p>

			</div>			
			<div class="clear"></div>
			
			
			<div class="same-padding opacity-white-bg width100">
				<div class="float-left left-logo-div">
					<img src="img/logo2.png" class="logo-img" alt="Tabigo" title="Tabigo">
				</div>



				<div class="right-menu-div float-right before-header-div">
            
					<a href="userHome.php" class="menu-margin-right menu-item opacity-hover hover-effect user-header">
						Home
					</a>  
                        <div class="dropdown menu-margin-right menu-item opacity-hover hover-effect user-header">
                            Tourism
                            <img src="img/dropdown.png" class="dropdown-img" >
                            <div class="dropdown-content yellow-dropdown-content">
                                <p class="dropdown-p opacity-hover"><a href="userTour.php" class="menu-item">Tour Package</a></p>
                                <p class="dropdown-p opacity-hover"><a href="userRestaurant.php" class="menu-item">Restaurant</a></p>
                                <p class="dropdown-p opacity-hover"><a href="userTransportation.php" class="menu-item">Transportation</a></p>
                                <p class="dropdown-p opacity-hover"><a href="userPlace.php" class="menu-item">Place of Interest</a></p>
                                <p class="dropdown-p opacity-hover"><a href="userTicket.php" class="menu-item">Ticket Booking</a></p>
                                <p class="dropdown-p opacity-hover"><a href="userWifi.php" class="menu-item">Wifi</a></p>
                                <p class="dropdown-p opacity-hover"><a href="userHotel.php" class="menu-item">Hotel & Flight</a></p>
                                <p class="dropdown-p opacity-hover"><a href="userInsurance.php" class="menu-item">Insurance</a></p>
                            </div>
                        </div>  
					<a href="userPurchase.php" class="menu-margin-right menu-item opacity-hover hover-effect user-header">
						Purchase History
					</a> 
                        <div class="dropdown menu-item opacity-hover hover-effect user-header"  style="vertical-align:bottom;">
                            Settings
                            <img src="img/dropdown.png" class="dropdown-img" >
                            <div class="dropdown-content yellow-dropdown-content">
                                <p class="dropdown-p opacity-hover"><a href="userEditProfile.php" class="menu-item">Edit Profile</a></p>
                                <p class="dropdown-p opacity-hover"><a href="userEditPassword.php" class="menu-item">Edit Password</a></p>								<p class="dropdown-p opacity-hover"><a href="index.php" class="menu-item">Change Country</a></p>
                                <p class="dropdown-p opacity-hover"><a href="about.php" class="menu-item">About Tabigo</a></p>
                                <p class="dropdown-p opacity-hover"><a href="logout.php" class="menu-item">Logout</a></p>
                            </div>
                        </div>                                                 
                                    <div id="dl-menu" class="dl-menuwrapper before-dl">
                                        <button class="dl-trigger">Open Menu</button>
                                        <ul class="dl-menu">
                                        <li><a href="userHome.php" class="black-text opacity-hover">Home</a></li>
                                        <li><a href="userTour.php" class="black-text opacity-hover">Tour Package</a></li>
                                        <li><a href="userRestaurant.php" class="black-text opacity-hover">Restaurant</a></li>
                                        <li><a href="userTransportation.php" class="black-text opacity-hover">Transportation</a></li>                                        <li><a href="userPlace.php" class="black-text opacity-hover">Place of Interest</a></li>
                                        <li><a href="userTicket.php" class="black-text opacity-hover">Ticket Booking</a></li>
                                        <li><a href="userWifi.php" class="black-text opacity-hover">Wifi</a></li>                                        <li><a href="userHotel.php" class="black-text opacity-hover">Hotel & Flight</a></li> 
                                        <li><a href="userInsurance.php" class="black-text opacity-hover">Insurance</a></li> 
                                        <li><a href="userPurchase.php" class="black-text opacity-hover">Purchase History</a></li> 
                                        <li><a href="userEditProfile.php" class="black-text opacity-hover">Edit Profile</a></li> 
                                        <li><a href="userEditPassword.php" class="black-text opacity-hover">Edit Password</a></li>                                        <li><a  href="index.php" class="black-text opacity-hover">Change Country</a></li> 
                                        <li><a  href="about.php" class="black-text opacity-hover">About Tabigo</a></li>   
                                        <li><a  href="logout.php" class="black-text opacity-hover">Logout</a></li>
                                        </ul>
                                    </div>					



				</div>
			</div>
        </div>

</header>
    <?php
    }
    ?>   
<?php
}
else
//no login
{
?>
<header id="header" class="header header--fixedcheader1 menu-color" role="banner" >
        <div class="big-container-size hidden-padding" id="top-menu">
			<div class="red-bg top-fix-row width100 same-padding overflow">
				<p class="left-top-p white-text"><img src="img/phone.png" alt="Contact Us" title="Contact Us" class="top-icon phone-img"> <a href="tel:+60379540196" class="call-a">+603-7954 0196</a> &nbsp;  &nbsp; | &nbsp;  &nbsp; <a href="tel:+60379540096" class="call-a">+603-7954 0096</a> &nbsp;  &nbsp;   &nbsp;
				<span class="email-span"><img src="img/email.png" alt="Contact Us" title="Contact Us" class="top-icon email-img"> outbound@golden-adv.com</span>
				</p>
				<p class="right-top-p"><span class="email-span2"><img src="img/email.png" alt="Contact Us" title="Contact Us" class="top-icon email-img"> outbound@golden-adv.com &nbsp;  &nbsp; </span><a href="https://www.instagram.com/tabigo_asia/" target="_blank"><img src="img/instagram.png" class="top-icon insta-icon"></a> &nbsp; <a href="https://www.facebook.com/Tabigo.Asia" target="_blank"><img src="img/facebook.png" class="top-icon fb-icon"></a></p>

			</div>			
			<div class="clear"></div>
			
			
			<div class="same-padding opacity-white-bg width100">
				<div class="float-left left-logo-div">
					<img src="img/logo2.png" class="logo-img" alt="Tabigo" title="Tabigo">
				</div>



				<div class="right-menu-div float-right before-header-div">
            
					<a class="menu-margin-right menu-item opacity-hover open-signup hover-effect">
						Register
					</a>  
					<a class="white-text menu-item opacity-hover open-login hover-effect" style="vertical-align:baseline;">
						Login
					</a>             




				</div>
			</div>
        </div>

</header>
<?php
}
?>