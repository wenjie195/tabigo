<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

// require_once dirname(__FILE__) . '/classes/TourPackage.php';
require_once dirname(__FILE__) . '/classes/Transport.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>
<!doctype html>
<html>

<head>
    <?php include 'meta.php'; ?>
    <!-- <meta property="og:url" content="https://tabigo.holiday/" />
    <link rel="canonical" href="https://tabigo.holiday/" /> -->
    <meta property="og:title" content="Edit Transportation Package Details | Tabigo" />
    <title>Edit Transportation Package Details | Tabigo</title>   
    <?php include 'css.php'; ?>
    <script src="//cdn.ckeditor.com/4.14.0/full/ckeditor.js"></script>
</head>

<body class="body">
<?php include 'headerAdmin.php'; ?>

<div class="clear"></div>

<div class="width100 overflow menu-distance same-padding ow-sakura-height">
	<h1 class="title-h1 raleway red-text ow-no-margin wow fadeIn" data-wow-delay="0.1s">Edit Transportation Package Details</a></h1>

    <?php
    if(isset($_POST['package_uid']))
    {
    $conn = connDB();
    $itemDetails = getTransportation($conn,"WHERE uid = ? ", array("uid") ,array($_POST['package_uid']),"s");
    ?>

        <form action="utilities/adminTransportationEditFunction.php" method="POST">

        <input class="tele-input clean" type="hidden" value="<?php echo $itemDetails[0]->getUid(); ?>" name="item_uid" id="item_uid" readonly> 

            <div class="dual-input">
                <p class="input-top-p admin-top-p">Country*</p>
                <!-- <select class="tele-input clean"><option>Malaysia</option></select> -->
                <input class="tele-input clean" type="text" value="<?php echo $itemDetails[0]->getCountry(); ?>" placeholder="Country" name="country" id="country" required>  
            </div>

            <div class="dual-input second-dual-input">
                <p class="input-top-p admin-top-p">Departure State*</p>
                <input class="tele-input clean" type="text" value="<?php echo $itemDetails[0]->getState(); ?>" placeholder="State" name="state" id="state" required>  
            </div>

            <div class="clean"></div>

            <div class="dual-input">
                <p class="input-top-p admin-top-p">Title*</p>
                <!-- <select class="tele-input clean"><option>Penang</option></select>   -->
                <input class="tele-input clean" type="text" value="<?php echo $itemDetails[0]->getTitle(); ?>" placeholder="Title" name="title" id="title" required>        
            </div>

            <div class="dual-input second-dual-input">
                <p class="input-top-p admin-top-p">Company Name*</p>
                <input class="tele-input clean" type="text" value="<?php echo $itemDetails[0]->getCompanyName(); ?>" placeholder="Company Name" name="company_name" id="company_name" required>  
            </div>

            <div class="clean"></div>

            <div class="width100 overflow">
                <p class="input-top-p admin-top-p">Depart Route*</p>
                <input class="tele-input clean" type="text" value="<?php echo $itemDetails[0]->getRoute(); ?>" placeholder="Depart Route" name="route" id="route" required>         
            </div>

            <div class="clear"></div>

            <div class="width100 overflow">
                <p class="input-top-p admin-top-p">Description</p>
                <textarea class="tele-input clean lato input-textarea admin-input editor-input" type="text" placeholder="Description" name="description" id="description"><?php echo $itemDetails[0]->getDescription(); ?></textarea>  	
            </div>   

            <div class="clear"></div>

            <!-- Vehicle A -->
            <div class="width100 overflow">
                <p class="input-top-p admin-top-p">Vehicle A Photo 1 Link in Google Drive <img src="img/drive-link.jpg" class="tutorial-img"></p>
                <input class="tele-input clean" type="text" value="<?php echo $itemDetails[0]->getPhotoOneA(); ?>" placeholder="Vehicle A Photo 1" name="photo_a1" id="photo_a1">         
            </div>
            <div class="clear"></div>
            <div class="width100 overflow">
            <p class="input-top-p admin-top-p">Vehicle A Photo 2 Link in Google Drive <img src="img/drive-link.jpg" class="tutorial-img"></p>
                <input class="tele-input clean" type="text" value="<?php echo $itemDetails[0]->getPhotoTwoA(); ?>" placeholder="Vehicle A Photo 2" name="photo_a2" id="photo_a2">         
            </div>
            <div class="clear"></div>
            <div class="width100 overflow">
            <p class="input-top-p admin-top-p">Vehicle A Photo 3 Link in Google Drive <img src="img/drive-link.jpg" class="tutorial-img"></p>
                <input class="tele-input clean" type="text" value="<?php echo $itemDetails[0]->getPhotoThreeA(); ?>" placeholder="Vehicle A Photo 3" name="photo_a3" id="photo_a3">         
            </div>
            <div class="clear"></div>
            <!-- <div class="width100 overflow"> -->
            <div class="dual-input">
            <p class="input-top-p admin-top-p">Vehicle A*</p>
                <input class="tele-input clean" type="text" value="<?php echo $itemDetails[0]->getVehicleA(); ?>" placeholder="Vehicle A" name="vehicle_a" id="vehicle_a" required>         
            </div>
            <!-- <div class="dual-input"> -->
            <div class="dual-input second-dual-input">
            <p class="input-top-p admin-top-p">Vehicle A Price*</p>
                <input class="tele-input clean" type="text" value="<?php echo $itemDetails[0]->getPriceA(); ?>" placeholder="Vehicle A Price" name="vehicle_a_price" id="vehicle_a_price" required>  
            </div>
            <div class="clear"></div>  

            <!-- Vehicle B -->
            <div class="width100 overflow">
                <p class="input-top-p admin-top-p">Vehicle B Photo 1  Link in Google Drive <img src="img/drive-link.jpg" class="tutorial-img"></p>
                <input class="tele-input clean" type="text" value="<?php echo $itemDetails[0]->getPhotoOneB(); ?>" placeholder="Vehicle B Photo 1" name="photo_b1" id="photo_b1">         
            </div>
            <div class="clear"></div>
            <div class="width100 overflow">
            <p class="input-top-p admin-top-p">Vehicle B Photo 2  Link in Google Drive <img src="img/drive-link.jpg" class="tutorial-img"></p>
                <input class="tele-input clean" type="text" value="<?php echo $itemDetails[0]->getPhotoTwoB(); ?>" placeholder="Vehicle B Photo 2" name="photo_b2" id="photo_b2">         
            </div>
            <div class="clear"></div>
            <div class="width100 overflow">
            <p class="input-top-p admin-top-p">Vehicle B Photo 3  Link in Google Drive <img src="img/drive-link.jpg" class="tutorial-img"></p>
                <input class="tele-input clean" type="text" value="<?php echo $itemDetails[0]->getPhotoThreeB(); ?>" placeholder="Vehicle B Photo 3" name="photo_b3" id="photo_b3">         
            </div>
            <div class="clear"></div>
            <!-- <div class="width100 overflow"> -->
            <div class="dual-input">
            <p class="input-top-p admin-top-p">Vehicle B*</p>
                <input class="tele-input clean" type="text" value="<?php echo $itemDetails[0]->getVehicleB(); ?>" placeholder="Vehicle B" name="vehicle_b" id="vehicle_b">         
            </div>
            <!-- <div class="dual-input"> -->
            <div class="dual-input second-dual-input">
            <p class="input-top-p admin-top-p">Vehicle B Price*</p>
                <input class="tele-input clean" type="text" value="<?php echo $itemDetails[0]->getPriceB(); ?>" placeholder="Vehicle B Price" name="vehicle_b_price" id="vehicle_b_price">  
            </div>
            <div class="clear"></div>  

            <!-- Vehicle C -->
            <div class="width100 overflow">
                <p class="input-top-p admin-top-p">Vehicle C Photo 1  Link in Google Drive <img src="img/drive-link.jpg" class="tutorial-img"></p>
                <input class="tele-input clean" type="text" value="<?php echo $itemDetails[0]->getPhotoOneC(); ?>" placeholder="Vehicle C Photo 1" name="photo_c1" id="photo_c1">         
            </div>
            <div class="clear"></div>
            <div class="width100 overflow">
            <p class="input-top-p admin-top-p">Vehicle C Photo 2  Link in Google Drive <img src="img/drive-link.jpg" class="tutorial-img"></p>
                <input class="tele-input clean" type="text" value="<?php echo $itemDetails[0]->getPhotoTwoC(); ?>" placeholder="Vehicle C Photo 2" name="photo_c2" id="photo_c2">         
            </div>
            <div class="clear"></div>
            <div class="width100 overflow">
            <p class="input-top-p admin-top-p">Vehicle C Photo 3  Link in Google Drive <img src="img/drive-link.jpg" class="tutorial-img"></p>
                <input class="tele-input clean" type="text" value="<?php echo $itemDetails[0]->getPhotoThreeC(); ?>" placeholder="Vehicle C Photo 3" name="photo_c3" id="photo_c3">         
            </div>
            <div class="clear"></div>
            <!-- <div class="width100 overflow"> -->
            <div class="dual-input">
            <p class="input-top-p admin-top-p">Vehicle C*</p>
                <input class="tele-input clean" type="text" value="<?php echo $itemDetails[0]->getVehicleC(); ?>" placeholder="Vehicle C" name="vehicle_c" id="vehicle_c">         
            </div>
            <!-- <div class="dual-input"> -->
            <div class="dual-input second-dual-input">
            <p class="input-top-p admin-top-p">Vehicle C Price*</p>
                <input class="tele-input clean" type="text" value="<?php echo $itemDetails[0]->getPriceC(); ?>" placeholder="Vehicle C Price" name="vehicle_c_price" id="vehicle_c_price">  
            </div>
            <div class="clear"></div>  

            <!-- Vehicle D -->
            <div class="width100 overflow">
                <p class="input-top-p admin-top-p">Vehicle D Photo 1  Link in Google Drive <img src="img/drive-link.jpg" class="tutorial-img"></p>
                <input class="tele-input clean" type="text" value="<?php echo $itemDetails[0]->getPhotoOneD(); ?>" placeholder="Vehicle D Photo 1" name="photo_d1" id="photo_d1">         
            </div>
            <div class="clear"></div>
            <div class="width100 overflow">
            <p class="input-top-p admin-top-p">Vehicle D Photo 2  Link in Google Drive <img src="img/drive-link.jpg" class="tutorial-img"></p>
                <input class="tele-input clean" type="text" value="<?php echo $itemDetails[0]->getPhotoTwoD(); ?>" placeholder="Vehicle D Photo 2" name="photo_d2" id="photo_d3">         
            </div>
            <div class="clear"></div>
            <div class="width100 overflow">
            <p class="input-top-p admin-top-p">Vehicle D Photo 3  Link in Google Drive <img src="img/drive-link.jpg" class="tutorial-img"></p>
                <input class="tele-input clean" type="text" value="<?php echo $itemDetails[0]->getPhotoThreeD(); ?>" placeholder="Vehicle D Photo 3" name="photo_d3" id="photo_d3">         
            </div>
            <div class="clear"></div>
            <!-- <div class="width100 overflow"> -->
            <div class="dual-input">
            <p class="input-top-p admin-top-p">Vehicle D*</p>
                <input class="tele-input clean" type="text" value="<?php echo $itemDetails[0]->getVehicleD(); ?>" placeholder="Vehicle D" name="vehicle_d" id="vehicle_d">         
            </div>
            <!-- <div class="dual-input"> -->
            <div class="dual-input second-dual-input">
            <p class="input-top-p admin-top-p">Vehicle D Price*</p>
                <input class="tele-input clean" type="text" value="<?php echo $itemDetails[0]->getPriceD(); ?>" placeholder="Vehicle D Price" name="vehicle_d_price" id="vehicle_d_price">  
            </div>
            <div class="clear"></div>  

            <div class="clear"></div>  

            <div class="width100 text-center">
                <button class="clean red-btn hover-effect middle-button-size below-forgot margin-bottom30" name="submit">Submit</button>
            </div>

        </form>

    <?php
    }
    ?>

</div>

<div class="clear"></div>

<?php include 'js.php'; ?>

<script>
    CKEDITOR.replace('editor');
</script>

</body>
</html>