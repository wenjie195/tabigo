<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
<meta property="og:type" content="website" />
<meta property="og:image" content="https://vidatechft.com/hosting-picture/fb-meta-tabigo.jpg" />
<meta name="author" content="Tabigo">

<meta property="og:description" content="Tabigo provides travelers with the experiences they want and need.The only platform you will need for travel activities and more in Southeast Asia. " />
<meta name="description" content="Tabigo provides travelers with the experiences they want and need.The only platform you will need for travel activities and more in Southeast Asia. " />
<meta name="keywords" content="Tabigo, app, travel app, traveller, japan,tokyo, korea, taiwan, travelling app, adventure, experience traveller, etc">
<!-- Global site tag (gtag.js) - Google Analytics -->
<!--<script async src="https://www.googletagmanager.com/gtag/js?id=UA-178461364-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-178461364-1');
</script>-->