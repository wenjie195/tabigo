<?php
require_once dirname(__FILE__) . '/../adminAccess.php';
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/TourPackage.php';
require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/allNoticeModals.php';
require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
     $conn = connDB();

     $packageUid = rewrite($_POST['package_uid']);

     $country = rewrite($_POST['country']);
     $state = rewrite($_POST['state']);
     $merchantName = rewrite($_POST['merchant_name']);
     $price = rewrite($_POST['price']);

     $terms = rewrite($_POST['terms']);
     $title = rewrite($_POST['title']);
     $photoOne = rewrite($_POST['photo_one']);
     $photoTwo = rewrite($_POST['photo_two']);  
     $googleLink = rewrite($_POST['google_link']);
     // $description = rewrite($_POST['description']);
     $description = ($_POST['description']);
     $paragraphOne = ($_POST['editor']);
     // $display = "Yes";

     // //   FOR DEBUGGING 
     // echo "<br>";
     // echo $uid."<br>";
     // echo $title."<br>";
     // echo $paragraphOne."<br>";
     // echo $type."<br>";

     if(isset($_POST['submit']))
     {
          $tableName = array();
          $tableValue =  array();
          $stringType =  "";
          // //echo "save to database";
          if($country)
          {
               array_push($tableName,"country");
               array_push($tableValue,$country);
               $stringType .=  "s";
          }
          if($state)
          {
               array_push($tableName,"state");
               array_push($tableValue,$state);
               $stringType .=  "s";
          }
          if($merchantName)
          {
               array_push($tableName,"merchant_name");
               array_push($tableValue,$merchantName);
               $stringType .=  "s";
          }
          if($price)
          {
               array_push($tableName,"price");
               array_push($tableValue,$price);
               // $stringType .=  "s";
               $stringType .=  "d";
          }


          if($terms)
          {
               array_push($tableName,"terms");
               array_push($tableValue,$terms);
               $stringType .=  "s";
          }
          if($title)
          {
               array_push($tableName,"title");
               array_push($tableValue,$title);
               $stringType .=  "s";
          }
          if($photoOne)
          {
               array_push($tableName,"photo_one");
               array_push($tableValue,$photoOne);
               $stringType .=  "s";
          }
          if($photoTwo)
          {
               array_push($tableName,"photo_two");
               array_push($tableValue,$photoTwo);
               $stringType .=  "s";
          }
          if($googleLink)
          {
               array_push($tableName,"links");
               array_push($tableValue,$googleLink);
               $stringType .=  "s";
          }
          if($description)
          {
               array_push($tableName,"description");
               array_push($tableValue,$description);
               $stringType .=  "s";
          }
          if($paragraphOne)
          {
               array_push($tableName,"paragraph_one");
               array_push($tableValue,$paragraphOne);
               $stringType .=  "s";
          }
          
          array_push($tableValue,$packageUid);
          $stringType .=  "s";
          $updateArticles = updateDynamicData($conn,"tour_package"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
          if($updateArticles)
          {
               $_SESSION['messageType'] = 1;
               header('Location: ../adminTour.php?type=3');
          }
          else
          {    
               echo "FAIL";
               // header('Location: ../adminBlogView.php?type=1');
          }
     }
     else
     {
          echo "ERROR";
          // header('Location: ../viewArticles.php?type=3');
     }
  
}
else 
{
     header('Location: ../index.php');
}

?>