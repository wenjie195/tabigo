<?php
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';

require_once dirname(__FILE__) . '/../classes/Restaurant.php';
require_once dirname(__FILE__) . '/../classes/User.php';
require_once dirname(__FILE__) . '/../classes/Wifi.php';
require_once dirname(__FILE__) . '/../classes/Ticket.php';
// require_once dirname(__FILE__) . '/../classes/Variation.php';

require_once dirname(__FILE__) . '/allNoticeModals.php';
require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

$userUid = $_SESSION['uid'];


function addPreOrder($conn,$userUid,$productName,$originalPrice,$quantity,$totalPrice,$status,$mainProductUid,$serviceType,$serviceDate,$productUid)
{
     if(insertDynamicData($conn,"preorder_list",array("user_uid","product_name","original_price","quantity","totalPrice","status","main_product_uid","service_type","service_date","product_uid"),
          array($userUid,$productName,$originalPrice,$quantity,$totalPrice,$status,$mainProductUid,$serviceType,$serviceDate,$productUid),"ssssssssss") === null)
     {
          echo "gg";
     }
     else{    }
     return true;
}

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
     $conn = connDB();

     // $uid = md5(uniqid());
     $mainProductUid = rewrite($_POST['package_uid']);
     $serviceType = rewrite($_POST['service_type']);
     // $serviceDate = rewrite($_POST['service_date']);
     $serviceDate = NULL;
     // $serviceType = NULL;

     if($serviceType == 'Restaurant')
     {
          $packageRows = getRestaurant($conn,"WHERE uid = ? ", array("uid") ,array($mainProductUid),"s");
          $productUid = $packageRows[0]->getUid();
          $productName = $packageRows[0]->getName();
          $originalPrice = $packageRows[0]->getPrice();
     }
     elseif($serviceType == 'Wifi')
     {
          $wifiRows = getWifi($conn,"WHERE uid = ? ", array("uid") ,array($mainProductUid),"s");
          $productUid = $wifiRows[0]->getUid();
          $productName = $wifiRows[0]->getTitle();
          $originalPrice = $wifiRows[0]->getPrice();
     }
     elseif($serviceType == 'Ticket')
     {
          $serviceOption = rewrite($_POST['tracsport_package']);

          $ticketRows = getTicket($conn,"WHERE uid = ? ", array("uid") ,array($mainProductUid),"s");
          $productUid = $ticketRows[0]->getUid();
          $productMainName = $ticketRows[0]->getTitle();
          $spacing = "  |  ";
          // $originalPrice = $packageRows[0]->getPrice();

          if($serviceOption == "A")
          {
               $originalPrice = $ticketRows[0]->getPackageA();
               $packageName = $ticketRows[0]->getUnitA();
               $productName = $productMainName.$spacing.$packageName;
          }
          elseif($serviceOption == "B")
          {
               $originalPrice = $ticketRows[0]->getPackageB();
               $packageName = $ticketRows[0]->getUnitB();
               $productName = $productMainName.$spacing.$packageName;
          }
          elseif($serviceOption == "C")
          {
               $originalPrice = $ticketRows[0]->getPackageC();
               $packageName = $ticketRows[0]->getUnitC();
               $productName = $productMainName.$spacing.$packageName;
          }

     }

     // $packageRows = getRestaurant($conn,"WHERE uid = ? ", array("uid") ,array($mainProductUid),"s");
     // $productUid = $packageRows[0]->getUid();
     // $productName = $packageRows[0]->getName();
     // $originalPrice = $packageRows[0]->getPrice();

     $quantity = 1;
     // $totalPrice = $originalPrice * $quantity;
     $totalPrice = $originalPrice;
     $status = 'Pending';

     // //   FOR DEBUGGING 
     // echo "<br>";
     // echo $uid."<br>";
     // echo $name."<br>";

     if(addPreOrder($conn,$userUid,$productName,$originalPrice,$quantity,$totalPrice,$status,$mainProductUid,$serviceType,$serviceDate,$productUid))
     {
          // echo "product added to cart";
          header('Location: ../viewShoppingCart.php');
     }
     else
     {
          echo "fail to add product into cart";
     }

}
else 
{
     header('Location: ../index.php');
}
?>