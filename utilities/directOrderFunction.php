<?php
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';


require_once dirname(__FILE__) . '/../classes/Orders.php';
require_once dirname(__FILE__) . '/../classes/OrderList.php';
require_once dirname(__FILE__) . '/../classes/TourPackage.php';
require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/allNoticeModals.php';
require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

$userUid = $_SESSION['uid'];
$timestamp = time();

function addOrderList($conn,$orderUid,$userUid,$productUid,$productName,$originalPrice,$quantity,$totalPrice,$status,$mainProductUid)
{
     if(insertDynamicData($conn,"order_list",array("order_id","user_uid","product_uid","product_name","original_price","quantity","totalPrice","status","main_product_uid"),
          array($orderUid,$userUid,$productUid,$productName,$originalPrice,$quantity,$totalPrice,$status,$mainProductUid),"sssssssss") === null)
     {
          echo "gg";
     }
     else{    }
     return true;
}

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
     $conn = connDB();

     // $uid = md5(uniqid());

     // echo $productUid = rewrite($_POST['product_uid']);
     // echo "<br>";
     // $productUid = $_POST['product_uid'];

     // $productName = rewrite($_POST['product_name']);
     // $originalPrice = rewrite($_POST['product_price']);
     // $mainProductUid = rewrite($_POST['main_product_uid']);

     // $productVariation = getVariation($conn,"WHERE uid = ? ", array("uid") ,array($productUid),"s");
     // echo $productName = $productVariation[0]->getName();
     // echo "<br>";
     // echo $originalPrice = $productVariation[0]->getPrice();
     // echo "<br>";
     // echo $mainProductUid = $productVariation[0]->getProductUid();
     // echo "<br>";
     // $quantity = rewrite($_POST['product_quantity']);
     // $totalPrice = $originalPrice * $quantity;
     // // $status = 'Pending';

     $tourUid = rewrite($_POST['tour_uid']);

     $tourRows = getTourPackage($conn,"WHERE uid = ? ", array("uid") ,array($tourUid),"s");
     $productName = $tourRows[0]->getTitle();
     $originalPrice = $tourRows[0]->getPrice();
     $mainProductUid = $tourRows[0]->getUid();
     $productUid = $mainProductUid;
     $quantity = 1;
     $totalPrice = $originalPrice;
     // $status = 'Pending';

     $orderUid = $userUid.$timestamp;
     $_SESSION['order_uid'] = $orderUid;
     $status = "Pending";


     // //   FOR DEBUGGING 
     // echo "<br>";
     // echo $uid."<br>";
     // echo $name."<br>";

     if($quantity < 1)
     {
          header('Location: ../shopping.php');
     }
     else
     {
          if(addOrderList($conn,$orderUid,$userUid,$productUid,$productName,$originalPrice,$quantity,$totalPrice,$status,$mainProductUid))
          {
               header('Location: ../viewCheckoutList.php');
          }
          else
          {
               echo "fail to add product into order list";
          }
     }

}
else 
{
     header('Location: ../index.php');
}
?>