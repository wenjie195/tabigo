<?php
require_once dirname(__FILE__) . '/../adminAccess.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';

// require_once dirname(__FILE__) . '/../classes/TourPackage.php';
require_once dirname(__FILE__) . '/../classes/Transport.php';
require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/allNoticeModals.php';
require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
    $conn = connDB();

    $package_uid = rewrite($_POST["package_uid"]);
    $display = "No";

    // // //   FOR DEBUGGING 
    // echo "<br>";
    // echo $transfer_uid."<br>";

    $upgradeMembership = getTransportation($conn," WHERE uid = ? ",array("uid"),array($package_uid),"s");   

    if($upgradeMembership)
    {   
        $tableName = array();
        $tableValue =  array();
        $stringType =  "";
        //echo "save to database";
        if($display)
        {
            array_push($tableName,"display");
            array_push($tableValue,$display);
            $stringType .=  "s";
        }

        array_push($tableValue,$package_uid);
        $stringType .=  "s";
        $statusUpdated = updateDynamicData($conn,"transportation"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
        if($statusUpdated)
        {
            // echo "<script>alert('membership rejected successfully !!');window.location='../adminTour_Renew.php'</script>";  
            $_SESSION['messageType'] = 1;
            header('Location: ../adminTransportation.php?type=1');
        }
        else
        {
            echo "<script>alert('fail to reject membership !!');window.location='../adminTour_Renew.php'</script>";  
        }
    }
    else
    {
        echo "<script>alert('ERROR !!');window.location='../adminTour_Renew.php'</script>";  
    }
}
else 
{
    header('Location: ../index.php');
}
?>