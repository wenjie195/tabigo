<?php
require_once dirname(__FILE__) . '/../adminAccess.php';
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/Restaurant.php';
require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/allNoticeModals.php';
require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
     $conn = connDB();

     $packageUid = rewrite($_POST['package_uid']);

     $country = rewrite($_POST['country']);
     $state = rewrite($_POST['state']);
     $name = rewrite($_POST['name']);
     $photo = rewrite($_POST['photo']);
     $title = rewrite($_POST['title']);
     $price = rewrite($_POST['price']);
     $address = rewrite($_POST['address']);
     $latlong = rewrite($_POST['latlong']);
     // $description = rewrite($_POST['description']);
     $description = ($_POST['description']);
     $openingHrs = rewrite($_POST['opening_hrs']);  
     $website = rewrite($_POST['website']);
     // $description = ($_POST['contact']);
     $contact = rewrite($_POST['contact']);

     // //   FOR DEBUGGING 
     // echo "<br>";
     // echo $uid."<br>";
     // echo $title."<br>";

     if(isset($_POST['submit']))
     {
          $tableName = array();
          $tableValue =  array();
          $stringType =  "";
          // //echo "save to database";

          if($country)
          {
               array_push($tableName,"country");
               array_push($tableValue,$country);
               $stringType .=  "s";
          }

          if($state)
          {
               array_push($tableName,"state");
               array_push($tableValue,$state);
               $stringType .=  "s";
          }
          if($name)
          {
               array_push($tableName,"name");
               array_push($tableValue,$name);
               $stringType .=  "s";
          }
          if($photo)
          {
               array_push($tableName,"photo");
               array_push($tableValue,$photo);
               $stringType .=  "s";
          }
          if($title)
          {
               array_push($tableName,"title");
               array_push($tableValue,$title);
               $stringType .=  "s";
          }
          if($price)
          {
               array_push($tableName,"price");
               array_push($tableValue,$price);
               // $stringType .=  "s";
               $stringType .=  "d";
          }
          if($address)
          {
               array_push($tableName,"address");
               array_push($tableValue,$address);
               $stringType .=  "s";
          }
          if($latlong)
          {
               array_push($tableName,"lat_long");
               array_push($tableValue,$latlong);
               $stringType .=  "s";
          }
          if($description)
          {
               array_push($tableName,"description");
               array_push($tableValue,$description);
               $stringType .=  "s";
          }
          if($openingHrs)
          {
               array_push($tableName,"opening_hrs");
               array_push($tableValue,$openingHrs);
               $stringType .=  "s";
          }
          if($website)
          {
               array_push($tableName,"website");
               array_push($tableValue,$website);
               $stringType .=  "s";
          }
          if($contact)
          {
               array_push($tableName,"contact");
               array_push($tableValue,$contact);
               $stringType .=  "s";
          }
          
          array_push($tableValue,$packageUid);
          $stringType .=  "s";
          $updateArticles = updateDynamicData($conn,"restaurant"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
          if($updateArticles)
          {
               $_SESSION['messageType'] = 1;
               header('Location: ../adminRestaurant.php?type=3');
          }
          else
          {    
               $_SESSION['messageType'] = 1;
               header('Location: ../adminRestaurant.php?type=6');
          }
     }
     else
     {
          $_SESSION['messageType'] = 1;
          header('Location: ../adminRestaurant.php?type=5');
     }
  
}
else 
{
     header('Location: ../index.php');
}

?>