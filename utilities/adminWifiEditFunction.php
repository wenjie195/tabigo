<?php
require_once dirname(__FILE__) . '/../adminAccess.php';
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/Wifi.php';
require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/allNoticeModals.php';
require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
     $conn = connDB();

     $itemUid = rewrite($_POST['item_uid']);

     $country = rewrite($_POST['country']);
     $state = rewrite($_POST['state']);
     $title = rewrite($_POST['title']);
     $name = rewrite($_POST['name']);
     $photo = rewrite($_POST['photo']);
     $price = rewrite($_POST['price']);
     $details = ($_POST['details']);
     $latlong = rewrite($_POST['lat_long']);

     // //   FOR DEBUGGING 
     // echo "<br>";
     // echo $uid."<br>";
     // echo $title."<br>";

     $wifiDetails = getWifi($conn," WHERE uid = ? ",array("uid"),array($itemUid),"s");   

     if($wifiDetails)
     {
          $tableName = array();
          $tableValue =  array();
          $stringType =  "";
          // //echo "save to database";
          if($country)
          {
               array_push($tableName,"country");
               array_push($tableValue,$country);
               $stringType .=  "s";
          }
          if($state)
          {
               array_push($tableName,"state");
               array_push($tableValue,$state);
               $stringType .=  "s";
          }
          if($title)
          {
               array_push($tableName,"title");
               array_push($tableValue,$title);
               $stringType .=  "s";
          }
          if($name)
          {
               array_push($tableName,"name");
               array_push($tableValue,$name);
               $stringType .=  "s";
          }
          if($photo)
          {
               array_push($tableName,"photo");
               array_push($tableValue,$photo);
               $stringType .=  "s";
          }
          if($price)
          {
               array_push($tableName,"price");
               array_push($tableValue,$price);
               $stringType .=  "s";
          }
          if($details)
          {
               array_push($tableName,"details");
               array_push($tableValue,$details);
               $stringType .=  "s";
          }
          if($latlong)
          {
               array_push($tableName,"lat_long");
               array_push($tableValue,$latlong);
               $stringType .=  "s";
          }
          
          array_push($tableValue,$itemUid);
          $stringType .=  "s";
          $updateArticles = updateDynamicData($conn,"wifi"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
          if($updateArticles)
          {
               $_SESSION['messageType'] = 1;
               header('Location: ../adminWifi.php?type=3');
          }
          else
          {    
               $_SESSION['messageType'] = 1;
               header('Location: ../adminWifi.php?type=6');
          }
     }
     else
     {
          $_SESSION['messageType'] = 1;
          header('Location: ../adminWifi.php?type=5');
     }
  
}
else 
{
     header('Location: ../index.php');
}

?>