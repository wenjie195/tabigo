<?php
require_once dirname(__FILE__) . '/../adminAccess.php';
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/Ticket.php';
require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/allNoticeModals.php';
require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
     $conn = connDB();

     $itemUid = rewrite($_POST['item_uid']);

     $country = rewrite($_POST['country']);
     $state = rewrite($_POST['state']);
     $photo = rewrite($_POST['photo']);
     $title = rewrite($_POST['title']);
     $merchantName = rewrite($_POST['merchant_name']);
     $description = ($_POST['editor']);

     $unitA = rewrite($_POST['unit_a']);
     $packageA = rewrite($_POST['package_a']);
     $unitB = rewrite($_POST['unit_b']);
     $packageB = rewrite($_POST['package_b']);
     $unitC = rewrite($_POST['unit_c']);
     $packageC = rewrite($_POST['package_c']);

     $notes = rewrite($_POST['notes']);
     $terms = rewrite($_POST['terms']);


     // //   FOR DEBUGGING 
     // echo "<br>";
     // echo $uid."<br>";
     // echo $title."<br>";

     $ticketsDetails = getTicket($conn," WHERE uid = ? ",array("uid"),array($itemUid),"s");   

     if($ticketsDetails)
     {
          $tableName = array();
          $tableValue =  array();
          $stringType =  "";
          // //echo "save to database";
          if($country)
          {
               array_push($tableName,"country");
               array_push($tableValue,$country);
               $stringType .=  "s";
          }
          if($state)
          {
               array_push($tableName,"state");
               array_push($tableValue,$state);
               $stringType .=  "s";
          }
          if($photo)
          {
               array_push($tableName,"photo");
               array_push($tableValue,$photo);
               $stringType .=  "s";
          }
          if($title)
          {
               array_push($tableName,"title");
               array_push($tableValue,$title);
               $stringType .=  "s";
          }
          if($merchantName)
          {
               array_push($tableName,"merchant_name");
               array_push($tableValue,$merchantName);
               $stringType .=  "s";
          }
          if($description)
          {
               array_push($tableName,"description");
               array_push($tableValue,$description);
               $stringType .=  "s";
          }

          if($unitA)
          {
               array_push($tableName,"unit_a");
               array_push($tableValue,$unitA);
               $stringType .=  "s";
          }
          if($packageA)
          {
               array_push($tableName,"package_a");
               array_push($tableValue,$packageA);
               $stringType .=  "s";
          }
          if($unitB)
          {
               array_push($tableName,"unit_b");
               array_push($tableValue,$unitB);
               $stringType .=  "s";
          }
          if($packageB)
          {
               array_push($tableName,"package_b");
               array_push($tableValue,$packageB);
               $stringType .=  "s";
          }
          if($unitC)
          {
               array_push($tableName,"unit_c");
               array_push($tableValue,$unitC);
               $stringType .=  "s";
          }
          if($packageC)
          {
               array_push($tableName,"package_c");
               array_push($tableValue,$packageC);
               $stringType .=  "s";
          }
          if($notes)
          {
               array_push($tableName,"notes");
               array_push($tableValue,$notes);
               $stringType .=  "s";
          }
          if($terms)
          {
               array_push($tableName,"terms");
               array_push($tableValue,$terms);
               $stringType .=  "s";
          }

          array_push($tableValue,$itemUid);
          $stringType .=  "s";
          $updateArticles = updateDynamicData($conn,"ticket"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
          if($updateArticles)
          {
               $_SESSION['messageType'] = 1;
               header('Location: ../adminTicket.php?type=3');
          }
          else
          {    
               $_SESSION['messageType'] = 1;
               header('Location: ../adminTicket.php?type=6');
          }
     }
     else
     {
          $_SESSION['messageType'] = 1;
          header('Location: ../adminTicket.php?type=5');
     }
  
}
else 
{
     header('Location: ../index.php');
}

?>