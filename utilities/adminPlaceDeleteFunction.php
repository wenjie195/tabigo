<?php
require_once dirname(__FILE__) . '/../adminAccess.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';

require_once dirname(__FILE__) . '/../classes/InterestingPlace.php';
require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
    $conn = connDB();

    $package_uid = rewrite($_POST["package_uid"]);
    $display = "No";

    // // //   FOR DEBUGGING 
    // echo "<br>";
    // echo $transfer_uid."<br>";

    $upgradeMembership = getInterestingPlace($conn," WHERE uid = ? ",array("uid"),array($package_uid),"s");   

    if($upgradeMembership)
    {   
        $tableName = array();
        $tableValue =  array();
        $stringType =  "";
        //echo "save to database";
        if($display)
        {
            array_push($tableName,"display");
            array_push($tableValue,$display);
            $stringType .=  "s";
        }

        array_push($tableValue,$package_uid);
        $stringType .=  "s";
        $statusUpdated = updateDynamicData($conn,"interesting_place"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
        if($statusUpdated)
        {
            // echo "<script>alert('membership rejected successfully !!');window.location='../adminTour_Renew.php'</script>";  
            $_SESSION['messageType'] = 1;
            header('Location: ../adminPlace.php?type=1');
        }
        else
        {
            $_SESSION['messageType'] = 1;
            header('Location: ../adminPlace.php?type=4');
        }
    }
    else
    {
        $_SESSION['messageType'] = 1;
        header('Location: ../adminPlace.php?type=5');
    }
}
else 
{
    header('Location: ../index.php');
}
?>