<?php
require_once dirname(__FILE__) . '/../adminAccess.php';
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/InterestingPlace.php';
require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/allNoticeModals.php';
require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
     $conn = connDB();

     $itemUid = rewrite($_POST['item_uid']);

     $country = rewrite($_POST['country']);
     $state = rewrite($_POST['state']);
     $title = rewrite($_POST['title']);

     $descriptionOne = ($_POST['editor_one']);
     $descriptionTwo = ($_POST['editor_two']);
     $descriptionThree = ($_POST['editor_three']);

     $photoOne = rewrite($_POST['photo_one']);
     $photoTwo = rewrite($_POST['photo_two']);
     $address = rewrite($_POST['address']);
     $latlong = rewrite($_POST['lat_long']);
     $openingHrs = rewrite($_POST['opening_hours']);
     $website = rewrite($_POST['website']);

     // //   FOR DEBUGGING 
     // echo "<br>";
     // echo $uid."<br>";
     // echo $title."<br>";

     $poiDetails = getInterestingPlace($conn," WHERE uid = ? ",array("uid"),array($itemUid),"s");   

     if($poiDetails)
     {
          $tableName = array();
          $tableValue =  array();
          $stringType =  "";
          // //echo "save to database";
          if($country)
          {
               array_push($tableName,"country");
               array_push($tableValue,$country);
               $stringType .=  "s";
          }
          if($state)
          {
               array_push($tableName,"state");
               array_push($tableValue,$state);
               $stringType .=  "s";
          }
          if($title)
          {
               array_push($tableName,"title");
               array_push($tableValue,$title);
               $stringType .=  "s";
          }

          if($descriptionOne)
          {
               array_push($tableName,"description_one");
               array_push($tableValue,$descriptionOne);
               $stringType .=  "s";
          }
          if($descriptionTwo)
          {
               array_push($tableName,"description_two");
               array_push($tableValue,$descriptionTwo);
               $stringType .=  "s";
          }
          if($descriptionThree)
          {
               array_push($tableName,"description_three");
               array_push($tableValue,$descriptionThree);
               $stringType .=  "s";
          }

          if($photoOne)
          {
               array_push($tableName,"photo_one");
               array_push($tableValue,$photoOne);
               $stringType .=  "s";
          }
          if($photoTwo)
          {
               array_push($tableName,"photo_two");
               array_push($tableValue,$photoTwo);
               $stringType .=  "s";
          }

          if($address)
          {
               array_push($tableName,"address");
               array_push($tableValue,$address);
               $stringType .=  "s";
          }
          if($latlong)
          {
               array_push($tableName,"lat_long");
               array_push($tableValue,$latlong);
               $stringType .=  "s";
          }
          if($openingHrs)
          {
               array_push($tableName,"opening_hrs");
               array_push($tableValue,$openingHrs);
               $stringType .=  "s";
          }
          if($website)
          {
               array_push($tableName,"website");
               array_push($tableValue,$website);
               $stringType .=  "s";
          }

          array_push($tableValue,$itemUid);
          $stringType .=  "s";
          $updateArticles = updateDynamicData($conn,"interesting_place"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
          if($updateArticles)
          {
               $_SESSION['messageType'] = 1;
               header('Location: ../adminPlace.php?type=3');
          }
          else
          {    
               $_SESSION['messageType'] = 1;
               header('Location: ../adminPlace.php?type=6');
          }
     }
     else
     {
          $_SESSION['messageType'] = 1;
          header('Location: ../adminPlace.php?type=5');
     }
  
}
else 
{
     header('Location: ../index.php');
}

?>