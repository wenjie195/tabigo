<?php
require_once dirname(__FILE__) . '/../adminAccess.php';
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/User.php';
require_once dirname(__FILE__) . '/../classes/OrderList.php';

require_once dirname(__FILE__) . '/allNoticeModals.php';
require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

$uid = $_SESSION['uid'];

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
     $conn = connDB();

     // $withdrawalUid = md5(uniqid());

     $productUid = rewrite($_POST["product_uid"]);
     $status = "Claimed";

     // //   FOR DEBUGGING
     // echo "<br>";
     // echo $withdrawalUid."<br>";

     if(isset($_POST['product_uid']))
     {
          $tableName = array();
          $tableValue =  array();
          $stringType =  "";
          // //echo "save to database";

          if($status)
          {
               array_push($tableName,"status");
               array_push($tableValue,$status);
               $stringType .=  "s";
          }

          array_push($tableValue,$productUid);
          $stringType .=  "s";
          $updateOrderStatus = updateDynamicData($conn,"order_list"," WHERE id = ? ",$tableName,$tableValue,$stringType);
          // $updateOrderStatus = updateDynamicData($conn,"order_list"," WHERE product_uid = ? ",$tableName,$tableValue,$stringType);
          if($updateOrderStatus)
          {
               // $_SESSION['messageType'] = 1;
               // header('Location: ../adminOrderCompleted.php?type=3');
               header('Location: ../adminOrderCompleted.php');
          }    
          else
          {
               echo "fail";
          }
     }
     else
     {
          echo "error level 222";
     }
}
else
{
     header('Location: ../index.php');
}
?>