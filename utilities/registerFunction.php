<?php
// require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
if (session_id() == "")
{
    session_start();
}
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/allNoticeModals.php';
require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

// $timestamp = time();

// function addStaff($conn,$uid,$username,$fullname,$icno,$phone,$address,$emergencyContact,$emergencyPpl,$salary,$allowance,$userType,$epfNo,$finalPassword,$salt,$joinDate)
// {
//      if(insertDynamicData($conn,"user",array("uid","username","fullname","ic_no","phone","address","emergency_contact","emergency_ppl","salary","allowance","user_type","epf_no","sosco","lhdn","password","salt","join_date"),
//           array($uid,$username,$fullname,$icno,$phone,$address,$emergencyContact,$emergencyPpl,$salary,$allowance,$userType,$epfNo,$finalPassword,$salt,$joinDate),"ssssssssssissss") === null)
//      {
//           echo "gg";
//      }
//      else{    }
//      return true;
// }

// function registerFunction($conn,$uid,$username,$fullname,$phone,$email,$country,$finalPassword,$salt)
// {
//      if(insertDynamicData($conn,"user",array("uid","username","fullname","phone","email","country","password","salt"),
//           array($uid,$username,$fullname,$phone,$email,$country,$finalPassword,$salt),"ssssssss") === null)
//      {
//           echo "gg";
//      }
//      else{    }
//      return true;
// }

function registerFunction($conn,$uid,$username,$fullname,$phone,$email,$country,$icno,$finalPassword,$salt)
{
     if(insertDynamicData($conn,"user",array("uid","username","fullname","phone","email","country","ic_no","password","salt"),
          array($uid,$username,$fullname,$phone,$email,$country,$icno,$finalPassword,$salt),"sssssssss") === null)
     {
          echo "gg";
     }
     else{    }
     return true;
}

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
     $conn = connDB();

     $uid = md5(uniqid());

     $username = rewrite($_POST['username']);
     $fullname = rewrite($_POST['fullname']);
     $phone = rewrite($_POST['phone']);
     $email = rewrite($_POST['email']);
     $country = rewrite($_POST['country']);
     $icno = rewrite($_POST['ic_no']);

     $register_password = rewrite($_POST['password']);
     $register_password_validation = strlen($register_password);
     $register_retype_password = rewrite($_POST['retype_password']);
     $password = hash('sha256',$register_password);
     $salt = substr(sha1(mt_rand()), 0, 100);
     $finalPassword = hash('sha256', $salt.$password);

     // //   FOR DEBUGGING 
     // echo "<br>";
     // echo $username."<br>";
     // echo $fullname."<br>";
     // echo $phone."<br>";
     // echo $email."<br>";

     $usernameRows = getUser($conn," WHERE username = ? ",array("username"),array($username),"s");
     $existingUsername = $usernameRows[0];

     $emailRows = getUser($conn," WHERE email = ? ",array("email"),array($email),"s");
     $existingEmail = $emailRows[0];

     $phoneRows = getUser($conn," WHERE phone = ? ",array("phone"),array($phone),"s");
     $existingPhone = $phoneRows[0];

     $icRows = getUser($conn," WHERE ic_no = ? ",array("ic_no"),array($icno),"s");
     $existingIc = $icRows[0];

     if($register_password == $register_retype_password)
     {
          if($register_password_validation >= 6)
          {
               if (!$existingUsername)
               {
                    if (!$existingEmail)
                    {
                         if (!$existingPhone)
                         {
                              if (!$existingIc)
                              {
                                   // if(registerFunction($conn,$uid,$username,$fullname,$phone,$email,$country,$finalPassword,$salt))
                                   if(registerFunction($conn,$uid,$username,$fullname,$phone,$email,$country,$icno,$finalPassword,$salt))
                                   {
                                        // echo "register successfully";
                                        // header('Location: ../adminDashboard.php');     
                                        $_SESSION['messageType'] = 1;
                                        header('Location: ../index.php?type=1');
                                   }
                                   else
                                   {
                                        // echo "fail";
                                        $_SESSION['messageType'] = 2;
                                        header('Location: ../index.php?type=1');
                                   }
                              }
                              else
                              {
                                   $_SESSION['messageType'] = 2;
                                   header('Location: ../index.php?type=7');
                              }
                         }
                         else
                         {
                              // echo "repeat phone";
                              $_SESSION['messageType'] = 2;
                              header('Location: ../index.php?type=2');
                         }
                    }
                    else
                    {
                         // echo "repeat email";
                         $_SESSION['messageType'] = 2;
                         header('Location: ../index.php?type=3');
                    }
               }
               else
               {
                    // echo "repeat username";
                    $_SESSION['messageType'] = 2;
                    header('Location: ../index.php?type=4');
               }
          }
          else 
          {
               // echo "password must more than 6";
               $_SESSION['messageType'] = 2;
               header('Location: ../index.php?type=5');
          }
     }
     else 
     {
          // echo "password different with retype password";
          $_SESSION['messageType'] = 2;
          header('Location: ../index.php?type=6');
     } 

     // if (!$existingFullname)
     // {
     //      if(registerFunction($conn,$uid,$username,$fullname,$phone,$email,$country,$finalPassword,$salt))
     //      {
     //           echo "success";  
     //           // $_SESSION['messageType'] = 1;
     //           // header('Location: ../adminDashboard.php?type=1');
     //      }
     //      else
     //      {
     //           echo "fail";
     //      }
     // }
     // else
     // {
     //      echo "repeat fullname";
     // }
}
else 
{
     header('Location: ../index.php');
     // header('Location: /../index.php');
}
?>