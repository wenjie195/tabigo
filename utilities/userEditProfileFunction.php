<?php
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/allNoticeModals.php';
require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
    $conn = connDB();

    $uid = $_SESSION['uid'];

    $username = rewrite($_POST['username']);
    // $fullname = rewrite($_POST['fullname']);
    $icNo = rewrite($_POST['icno']);
    $email = rewrite($_POST['email']);
    $phone = rewrite($_POST['phone']);
    $address = rewrite($_POST['address']);
    $emergencyContact = rewrite($_POST['emergency_contact']);
    $emergencyPpl = rewrite($_POST['emergency_ppl']);
  
    $userDetails = getUser($conn," WHERE uid = ? ",array("uid"),array($uid),"s");

    if($userDetails)
    {
        $tableName = array();
        $tableValue =  array();
        $stringType =  "";
        //echo "save to database";
        if($username)
        {
            array_push($tableName,"username");
            array_push($tableValue,$username);
            $stringType .=  "s";
        }
        // if($fullname)
        // {
        //     array_push($tableName,"fullname");
        //     array_push($tableValue,$fullname);
        //     $stringType .=  "s";
        // }
        if($icNo)
        {
            array_push($tableName,"ic_no");
            array_push($tableValue,$icNo);
            $stringType .=  "s";
        }
        if($phone)
        {
            array_push($tableName,"phone");
            array_push($tableValue,$phone);
            $stringType .=  "s";
        }

        if($email)
        {
            array_push($tableName,"email");
            array_push($tableValue,$email);
            $stringType .=  "s";
        }

        if($address)
        {
            array_push($tableName,"address");
            array_push($tableValue,$address);
            $stringType .=  "s";
        }
        if($emergencyContact)
        {
            array_push($tableName,"emergency_contact");
            array_push($tableValue,$emergencyContact);
            $stringType .=  "s";
        }
        if($emergencyPpl)
        {
            array_push($tableName,"emergency_ppl");
            array_push($tableValue,$emergencyPpl);
            $stringType .=  "s";
        }

        array_push($tableValue,$uid);
        $stringType .=  "s";
        $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
        if($passwordUpdated)
        {
            $_SESSION['messageType'] = 1;
            header('Location: ../userEditProfile.php?type=1');
        }
        else
        {
            $_SESSION['messageType'] = 1;
            header('Location: ../userEditProfile.php?type=2');
        }
    }
    else
    {
        $_SESSION['messageType'] = 1;
        header('Location: ../userEditProfile.php?type=3');
    }
}
else 
{
    header('Location: ../index.php');
}
?>