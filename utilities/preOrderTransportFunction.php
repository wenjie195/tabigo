<?php
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';

require_once dirname(__FILE__) . '/../classes/Transport.php';
require_once dirname(__FILE__) . '/../classes/User.php';
// require_once dirname(__FILE__) . '/../classes/Variation.php';

require_once dirname(__FILE__) . '/allNoticeModals.php';
require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

$userUid = $_SESSION['uid'];

// function addPreOrder($conn,$userUid,$productUid,$productName,$originalPrice,$quantity,$totalPrice,$status,$mainProductUid)
// {
//      if(insertDynamicData($conn,"preorder_list",array("user_uid","product_uid","product_name","original_price","quantity","totalPrice","status","main_product_uid"),
//           array($userUid,$productUid,$productName,$originalPrice,$quantity,$totalPrice,$status,$mainProductUid),"ssssssss") === null)
//      {
//           echo "gg";
//      }
//      else{    }
//      return true;
// }

function addPreOrder($conn,$userUid,$productName,$originalPrice,$quantity,$totalPrice,$status,$mainProductUid,$serviceType,$serviceDate,$productUid)
{
     if(insertDynamicData($conn,"preorder_list",array("user_uid","product_name","original_price","quantity","totalPrice","status","main_product_uid","service_type","service_date","product_uid"),
          array($userUid,$productName,$originalPrice,$quantity,$totalPrice,$status,$mainProductUid,$serviceType,$serviceDate,$productUid),"ssssssssss") === null)
     {
          echo "gg";
     }
     else{    }
     return true;
}

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
     $conn = connDB();

     // $uid = md5(uniqid());
     $productUid = md5(uniqid());

     $serviceType = rewrite($_POST['service_type']);

     // echo "<br>";

     $serviceOption = rewrite($_POST['tracsport_package']);
     
     $packageUid = rewrite($_POST['package_uid']);
     $transportRows = getTransportation($conn,"WHERE uid = ? ", array("uid") ,array($packageUid),"s");
     $packageRoute = $transportRows[0]->getRoute();
     // $productName = $tourRows[0]->getTitle();
     // $originalPrice = $tourRows[0]->getPrice();
     $spacing = "  |  ";

     if($serviceOption == "A")
     {
          $vehicleType = $transportRows[0]->getVehicleA();
          $productName = $packageRoute.$spacing.$vehicleType;
          $originalPrice = $transportRows[0]->getPriceA();
     }
     elseif($serviceOption == "B")
     {
          $vehicleType = $transportRows[0]->getVehicleB();
          $productName = $packageRoute.$spacing.$vehicleType;
          $originalPrice = $transportRows[0]->getPriceB();
     }
     elseif($serviceOption == "C")
     {
          $vehicleType = $transportRows[0]->getVehicleC();
          $productName = $packageRoute.$spacing.$vehicleType;
          $originalPrice = $transportRows[0]->getPriceC();
     }
     elseif($serviceOption == "D")
     {
          $vehicleType = $transportRows[0]->getVehicleD();
          $productName = $packageRoute.$spacing.$vehicleType;
          $originalPrice = $transportRows[0]->getPriceD();
     }

     $serviceDate = rewrite($_POST['service_date']);



     // $tourUid = rewrite($_POST['tour_uid']);

     // $tourRows = getTourPackage($conn,"WHERE uid = ? ", array("uid") ,array($tourUid),"s");
     // $productName = $tourRows[0]->getTitle();
     // $originalPrice = $tourRows[0]->getPrice();

     $mainProductUid = rewrite($_POST['package_uid']);
     // $mainProductUid = $tourRows[0]->getUid();

     // $quantity = rewrite($_POST['product_quantity']);
     $quantity = 1;
     // $totalPrice = $originalPrice * $quantity;
     $totalPrice = $originalPrice;
     $status = 'Pending';

     // //   FOR DEBUGGING 
     // echo "<br>";
     // echo $uid."<br>";
     // echo $name."<br>";

     if(addPreOrder($conn,$userUid,$productName,$originalPrice,$quantity,$totalPrice,$status,$mainProductUid,$serviceType,$serviceDate,$productUid))
     {
          // echo "product added to cart";
          header('Location: ../viewShoppingCart.php');
     }
     else
     {
          echo "fail to add product into cart";
     }

}
else 
{
     header('Location: ../index.php');
}
?>