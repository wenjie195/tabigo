<?php
require_once dirname(__FILE__) . '/../adminAccess.php';
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/Transport.php';
require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/allNoticeModals.php';
require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
     $conn = connDB();

     $itemUid = rewrite($_POST["item_uid"]);

     $country = rewrite($_POST['country']);

     $state = rewrite($_POST['state']);

     $title = rewrite($_POST['title']);
     $companyName = rewrite($_POST['company_name']);
     $route = rewrite($_POST['route']);
     $description = ($_POST['description']);

     $photoA1 = rewrite($_POST['photo_a1']);
     $photoA2 = rewrite($_POST['photo_a2']);
     $photoA3 = rewrite($_POST['photo_a3']);
     $vehicleA = rewrite($_POST['vehicle_a']);
     $vehicleAPrice = rewrite($_POST['vehicle_a_price']);

     $photoB1 = rewrite($_POST['photo_b1']);
     $photoB2 = rewrite($_POST['photo_b2']);
     $photoB3 = rewrite($_POST['photo_b3']);
     $vehicleB = rewrite($_POST['vehicle_b']);
     $vehicleBPrice = rewrite($_POST['vehicle_b_price']);

     $photoC1 = rewrite($_POST['photo_c1']);
     $photoC2 = rewrite($_POST['photo_c2']);
     $photoC3 = rewrite($_POST['photo_c3']);
     $vehicleC = rewrite($_POST['vehicle_c']);
     $vehicleCPrice = rewrite($_POST['vehicle_c_price']);

     $photoD1 = rewrite($_POST['photo_d1']);
     $photoD2 = rewrite($_POST['photo_d2']);
     $photoD3 = rewrite($_POST['photo_d3']);
     $vehicleD = rewrite($_POST['vehicle_d']);
     $vehicleDPrice = rewrite($_POST['vehicle_d_price']);

     // //   FOR DEBUGGING 
     // echo "<br>";
     // echo $itemUid."<br>";
     // echo $country."<br>";
     // echo $title."<br>";
     // echo $companyName."<br>";
     // echo $route."<br>";
     // echo $description."<br>";

     // $package_uid = rewrite($_POST["package_uid"]);
     $updateDetails = getTransportation($conn," WHERE uid = ? ",array("uid"),array($itemUid),"s");   

     if($updateDetails)
     { 
          $tableName = array();
          $tableValue =  array();
          $stringType =  "";
          // //echo "save to database";
          if($country)
          {
               array_push($tableName,"country");
               array_push($tableValue,$country);
               $stringType .=  "s";
          }

          if($state)
          {
               array_push($tableName,"state");
               array_push($tableValue,$state);
               $stringType .=  "s";
          }

          if($title)
          {
               array_push($tableName,"title");
               array_push($tableValue,$title);
               $stringType .=  "s";
          }
          if($companyName)
          {
               array_push($tableName,"company_name");
               array_push($tableValue,$companyName);
               $stringType .=  "s";
          }
          if($route)
          {
               array_push($tableName,"departure_route");
               array_push($tableValue,$route);
               $stringType .=  "s";
          }
          if($description)
          {
               array_push($tableName,"description");
               array_push($tableValue,$description);
               $stringType .=  "s";
          }


          if($photoA1)
          {
               array_push($tableName,"photo_aone");
               array_push($tableValue,$photoA1);
               $stringType .=  "s";
          }
          if($photoA2)
          {
               array_push($tableName,"photo_atwo");
               array_push($tableValue,$photoA2);
               $stringType .=  "s";
          }
          if($photoA3)
          {
               array_push($tableName,"photo_athree");
               array_push($tableValue,$photoA3);
               $stringType .=  "s";
          }
          if($vehicleA)
          {
               array_push($tableName,"vehicle_a");
               array_push($tableValue,$vehicleA);
               $stringType .=  "s";
          }
          if($vehicleAPrice)
          {
               array_push($tableName,"price_a");
               array_push($tableValue,$vehicleAPrice);
               $stringType .=  "s";
          }

          if($photoB1)
          {
               array_push($tableName,"photo_bone");
               array_push($tableValue,$photoB1);
               $stringType .=  "s";
          }
          if($photoB2)
          {
               array_push($tableName,"photo_btwo");
               array_push($tableValue,$photoB2);
               $stringType .=  "s";
          }
          if($photoB3)
          {
               array_push($tableName,"photo_bthree");
               array_push($tableValue,$photoB3);
               $stringType .=  "s";
          }
          if($vehicleB)
          {
               array_push($tableName,"vehicle_b");
               array_push($tableValue,$vehicleB);
               $stringType .=  "s";
          }
          if($vehicleBPrice)
          {
               array_push($tableName,"price_b");
               array_push($tableValue,$vehicleBPrice);
               $stringType .=  "s";
          }

          if($photoC1)
          {
               array_push($tableName,"photo_cone");
               array_push($tableValue,$photoC1);
               $stringType .=  "s";
          }
          if($photoC2)
          {
               array_push($tableName,"photo_ctwo");
               array_push($tableValue,$photoC2);
               $stringType .=  "s";
          }
          if($photoC3)
          {
               array_push($tableName,"photo_cthree");
               array_push($tableValue,$photoC3);
               $stringType .=  "s";
          }
          if($vehicleC)
          {
               array_push($tableName,"vehicle_c");
               array_push($tableValue,$vehicleC);
               $stringType .=  "s";
          }
          if($vehicleCPrice)
          {
               array_push($tableName,"price_c");
               array_push($tableValue,$vehicleCPrice);
               $stringType .=  "s";
          }

          if($photoD1)
          {
               array_push($tableName,"photo_done");
               array_push($tableValue,$photoD1);
               $stringType .=  "s";
          }
          if($photoD2)
          {
               array_push($tableName,"photo_dtwo");
               array_push($tableValue,$photoD2);
               $stringType .=  "s";
          }
          if($photoD3)
          {
               array_push($tableName,"photo_dthree");
               array_push($tableValue,$photoD3);
               $stringType .=  "s";
          }
          if($vehicleD)
          {
               array_push($tableName,"vehicle_d");
               array_push($tableValue,$vehicleD);
               $stringType .=  "s";
          }
          if($vehicleDPrice)
          {
               array_push($tableName,"price_d");
               array_push($tableValue,$vehicleDPrice);
               $stringType .=  "s";
          }
          
          array_push($tableValue,$itemUid);
          $stringType .=  "s";
          $updateArticles = updateDynamicData($conn,"transportation"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
          if($updateArticles)
          {
               $_SESSION['messageType'] = 1;
               header('Location: ../adminTransportation.php?type=3');
          }
          else
          {    
               $_SESSION['messageType'] = 1;
               header('Location: ../adminTransportation.php?type=4');
          }
     }
     else
     {
          $_SESSION['messageType'] = 1;
          header('Location: ../adminTransportation.php?type=5');
     }
  
}
else 
{
     header('Location: ../index.php');
}

?>