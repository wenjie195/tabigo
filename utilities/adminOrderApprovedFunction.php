<?php
require_once dirname(__FILE__) . '/../adminAccess.php';
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/User.php';
require_once dirname(__FILE__) . '/../classes/Orders.php';

require_once dirname(__FILE__) . '/allNoticeModals.php';
require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

$uid = $_SESSION['uid'];

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
     $conn = connDB();

     // $withdrawalUid = md5(uniqid());

     $orderUid = rewrite($_POST["order_uid"]);
     $status = "APPROVED";
     // $status = "REJECTED";

     $paymentStatus = "COMPLETED";

     // //   FOR DEBUGGING
     // echo "<br>";
     // echo $withdrawalUid."<br>";

     if(isset($_POST['order_uid']))
     {
          $tableName = array();
          $tableValue =  array();
          $stringType =  "";
          // //echo "save to database";

          if($status)
          {
               array_push($tableName,"shipping_status");
               array_push($tableValue,$status);
               $stringType .=  "s";
          }
          if($paymentStatus)
          {
               array_push($tableName,"payment_status");
               array_push($tableValue,$paymentStatus);
               $stringType .=  "s";
          }
          // if($method)
          // {
          //      array_push($tableName,"method");
          //      array_push($tableValue,$method);
          //      $stringType .=  "s";
          // }
          // if($reference)
          // {
          //      array_push($tableName,"note");
          //      array_push($tableValue,$reference);
          //      $stringType .=  "s";
          // }
          array_push($tableValue,$orderUid);
          $stringType .=  "s";
          $updateWithdrawalStatus = updateDynamicData($conn,"orders"," WHERE order_id = ? ",$tableName,$tableValue,$stringType);
          if($updateWithdrawalStatus)
          {
               // $_SESSION['messageType'] = 1;
               // header('Location: ../adminOrderCompleted.php?type=1');
               header('Location: ../adminOrderCompleted.php');
          }    
          else
          {
               echo "fail";
          }
     }
     else
     {
          echo "error level 222";
     }
}
else
{
     header('Location: ../index.php');
}
?>