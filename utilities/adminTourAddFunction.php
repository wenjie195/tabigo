<?php
require_once dirname(__FILE__) . '/../adminAccess.php';
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

// require_once dirname(__FILE__) . '/../classes/Article.php';
require_once dirname(__FILE__) . '/../classes/TourPackage.php';
require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/allNoticeModals.php';
require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

// function addTourPackage($conn,$uid,$country,$state,$merchantName,$price,$terms,$title,$photoOne,$photoTwo,$googleLink,$description,$paragraphOne,$display)
// {
//      if(insertDynamicData($conn,"tour_package",array("uid","country","state","merchant_name","price","terms","title","photo_one","photo_two","links","description","paragraph_one","display"),
//           array($uid,$country,$state,$merchantName,$price,$terms,$title,$photoOne,$photoTwo,$googleLink,$description,$paragraphOne,$display),"sssssssssssss") === null)
//      {
//           echo "GG";
//      }
//      else{    }
//      return true;
// }

function addTourPackage($conn,$uid,$country,$state,$merchantName,$price,$terms,$title,$photoOne,$photoTwo,$googleLink,$description,$paragraphOne,$display)
{
     if(insertDynamicData($conn,"tour_package",array("uid","country","state","merchant_name","price","terms","title","photo_one","photo_two","links","description","paragraph_one","display"),
          array($uid,$country,$state,$merchantName,$price,$terms,$title,$photoOne,$photoTwo,$googleLink,$description,$paragraphOne,$display),"ssssdssssssss") === null)
     {
          echo "GG";
     }
     else{    }
     return true;
}

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
     $conn = connDB();

     $uid = md5(uniqid());

     $country = rewrite($_POST['country']);
     $state = rewrite($_POST['state']);
     $merchantName = rewrite($_POST['merchant_name']);
     $price = rewrite($_POST['price']);
     $terms = rewrite($_POST['terms']);
     $title = rewrite($_POST['title']);

     $photoOne = rewrite($_POST['photo_one']);

     // $photoTwo = rewrite($_POST['photo_two']);
     $checkPhotoTwo = rewrite($_POST['photo_two']);
     if($checkPhotoTwo != '')
     {
          $photoTwo = $checkPhotoTwo;
     }
     else
     {
          $photoTwo = NULL;
     }
     
     $googleLink = rewrite($_POST['google_link']);
     $description = rewrite($_POST['description']);
     //no rewrite, cause error in db
     $paragraphOne = ($_POST['editor']);
     $display = "Yes";

     // //   FOR DEBUGGING 
     // echo "<br>";
     // echo $uid."<br>";
     // echo $title."<br>";
     // echo $paragraphOne."<br>";
     // echo $type."<br>";

     if(addTourPackage($conn,$uid,$country,$state,$merchantName,$price,$terms,$title,$photoOne,$photoTwo,$googleLink,$description,$paragraphOne,$display))
     {
          // echo "success";
          $_SESSION['messageType'] = 1;
          header('Location: ../adminTour.php?type=2');
     }
     else
     {
          echo "fail";
          // $_SESSION['messageType'] = 1;
          // header('Location: ../blogSummary.php?type=2');
     }
  
}
else 
{
     header('Location: ../index.php');
}

?>