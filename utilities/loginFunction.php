<?php
if (session_id() == "")
{
    session_start();
}
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/allNoticeModals.php';
require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';
// require_once dirname(__FILE__) . '/mailerFunction.php';

$conn = connDB();

$conn->close();

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
    //todo validation on server side
    //TODO change login with email to use username instead
    //TODO add username field to register's backend
    $conn = connDB();

    if(isset($_POST['login'])){
        // $email = rewrite($_POST['email']);

        $username = rewrite($_POST['username']);
        $password = $_POST['password'];

        $userRows = getUser($conn," WHERE username = ? ",array("username"),array($username),"s");
        if($userRows)
        {
            $user = $userRows[0];

                $tempPass = hash('sha256',$password);
                $finalPassword = hash('sha256', $user->getSalt() . $tempPass);
    
                if($finalPassword == $user->getPassword()) 
                {
                    $_SESSION['uid'] = $user->getUid();
                    $_SESSION['user_type'] = $user->getUserType();
                    
                    if($user->getUserType() == 0)
                    {
                        // echo "admin page";
                        // header('Location: ../adminTour_Renew.php');
                        // header('Location: ../adminTour.php');
                        header('Location: ../adminOrderPending.php');
                    }
                    elseif($user->getUserType() == 1)
                    {
                        // echo "normal user";
                        // header('Location: ../userHome_Renew2.php');
                        // header('Location: ../userHome.php');
                        if(isset($_SESSION['url'])) 
                        {
                            $url = $_SESSION['url']; 
                            header("location: $url");
                        }
                        else 
                        {
                            // header('Location: ../userPurchase.php');
                            header('Location: ../userOrders.php');
                        }
                    }
                    else
                    {
                        // echo "unknown user type";
                        $_SESSION['messageType'] = 3;
                        header('Location: ../index.php?type=1');
                    }
                }
                else 
                {
                    $_SESSION['messageType'] = 3;
                    header('Location: ../index.php?type=2');
                }

        }
        else
        {
            $_SESSION['messageType'] = 3;
            header('Location: ../index.php?type=3');
        }
    }

    $conn->close();
}
?>