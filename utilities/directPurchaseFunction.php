<?php
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';


require_once dirname(__FILE__) . '/../classes/Orders.php';
require_once dirname(__FILE__) . '/../classes/OrderList.php';
require_once dirname(__FILE__) . '/../classes/TourPackage.php';
require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/allNoticeModals.php';
require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

$userUid = $_SESSION['uid'];
$timestamp = time();

function addOrderList($conn,$orderUid,$userUid,$productUid,$productName,$originalPrice,$quantity,$totalPrice,$status,$mainProductUid)
{
     if(insertDynamicData($conn,"order_list",array("order_id","user_uid","product_uid","product_name","original_price","quantity","totalPrice","status","main_product_uid"),
          array($orderUid,$userUid,$productUid,$productName,$originalPrice,$quantity,$totalPrice,$status,$mainProductUid),"sssssssss") === null)
     {
          echo "gg";
     }
     else{    }
     return true;
}

function createOrder($conn,$orderUid,$uid,$name,$contact,$address,$subotal,$paymentMethod,$paymentStatus)
{
     if(insertDynamicData($conn,"orders",array("order_id","uid","name","contactNo","address_line_1","subtotal","payment_method","payment_status"),
          array($orderUid,$uid,$name,$contact,$address,$subotal,$paymentMethod,$paymentStatus),"ssssssss") === null)
     {
          echo "gg";
     }
     else{    }
     return true;
}

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
     $conn = connDB();

     $tourUid = rewrite($_POST['tour_uid']);

     $tourRows = getTourPackage($conn,"WHERE uid = ? ", array("uid") ,array($tourUid),"s");
     $productName = $tourRows[0]->getTitle();
     $originalPrice = $tourRows[0]->getPrice();
     $mainProductUid = $tourRows[0]->getUid();
     $productUid = $mainProductUid;
     $quantity = 1;
     $totalPrice = $originalPrice;

     $orderUid = $userUid.$timestamp;
     $_SESSION['order_uid'] = $orderUid;
     $status = "Pending";

     $userDetails = getUser($conn, "WHERE uid =?",array("uid"),array($uid),"s");
     $username = $userDetails[0]->getUsername();
     $contact = $userDetails[0]->getPhone();
     $orderUid = $_SESSION['order_uid'];
     $subotal = $totalPrice;

     $paymentMethod = 'BILLPLZ';
     $paymentStatus = 'WAITING';
     $orderListStatus = "Sold";

     // //   FOR DEBUGGING 
     // echo "<br>";
     // echo $uid."<br>";
     // echo $name."<br>";

     if($quantity < 1)
     {
          header('Location: ../shopping.php');
     }
     else
     {
          if(addOrderList($conn,$orderUid,$userUid,$productUid,$productName,$originalPrice,$quantity,$totalPrice,$status,$mainProductUid))
          {
               // header('Location: ../viewCheckoutList.php');
               $tableName = array();
               $tableValue =  array();
               $stringType =  "";
               //echo "save to database";
               if($orderListStatus)
               {
                   array_push($tableName,"status");
                   array_push($tableValue,$orderListStatus);
                   $stringType .=  "s";
               }    
               array_push($tableValue,$orderUid);
               $stringType .=  "s";
               $updateBonusPool = updateDynamicData($conn,"order_list"," WHERE order_id = ? ",$tableName,$tableValue,$stringType);
               if($updateBonusPool)
               {
                    if(createOrder($conn,$orderUid,$uid,$name,$contact,$address,$subotal,$paymentMethod,$paymentStatus))
                    {
                         // echo "order created";
                         header('Location: ../payAndShip.php');
                         // header('Location: ../userPayment.php');
                    }
                    else
                    {
                         echo "fail to create order";
                    }
               }
               else
               {
                    echo "FAIL TO CLEAR  ORDER LIST";
               }
          }
          else
          {
               echo "fail to add product into order list";
          }
     }

}
else 
{
     header('Location: ../index.php');
}
?>