<?php
if (session_id() == "")
{
    session_start();
}
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';
// require_once dirname(__FILE__) . '/sessionLoginChecker.php';

require_once dirname(__FILE__) . '/classes/Countries.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

$allCountry = getCountries($conn);

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>
<!doctype html>
<html>
<head>
	<?php include 'meta.php'; ?>
<!--         <meta property="og:url" content="https://tabigo.holiday/" />
        <link rel="canonical" href="https://tabigo.holiday/" />-->
        <meta property="og:title" content="Traveler's Choice | Tabigo" />
        <title>Traveler's Choice | Tabigo</title>   
	<?php include 'css.php'; ?>


</head>

<body class="body">

<!-- <?php //include 'header.php'; ?> -->
<?php include 'headerBeforeLogin.php'; ?>

<div class="clear"></div>

<div class="width100 overflow menu-distance same-padding sakura-bg ow-sakura-height">
	<h1 class="title-h1 raleway red-text ow-no-margin wow fadeIn" data-wow-delay="0.1s">Welcome to Tabigo</h1>	
    <div class="text-center"><p class="mid-content-p wow fadeIn" data-wow-delay="0.2s">Kindly choose a country.</p></div>
    <div class="clear"></div>

    <div class="ta-big-three">
        <?php
        if($allCountry)
        {
            for($cnt = 0;$cnt < count($allCountry) ;$cnt++)
            {
            ?>    
                <!-- <a href='countryDetails.php?id=<?php //echo $allCountry[$cnt]->getId();?>'> -->
                <a href='countryDetails.php?id=<?php echo $allCountry[$cnt]->getId();?>'>
                    <button class="country-button hover-effect clean">
                        <div class="ta-three-div">
                            <p><?php echo $allCountry[$cnt]->getEnName();?></p>
                        </div>
                    </button>
                </a>
            <?php
            }
        }
        ?>  
    </div>		
		
	
</div>

<?php include 'js.php'; ?>
<?php unset($_SESSION['country_id']);unset($_SESSION['country_name']); ?>

<style>
.before-login{
	display:none;}
</style>
<?php
if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Register Successfully !"; 
        }
        elseif($_GET['type'] == 2)
        {
            $messageType = "Please Register"; 
        }
        elseif($_GET['type'] == 3)
        {
            $messageType = "Incorrect Password";
        }
        echo '
        <script>
            putNoticeJavascript("Notice !! ","'.$messageType.'");  
        </script>
        ';   
        $_SESSION['messageType'] = 0;
    }
    elseif($_SESSION['messageType'] == 2)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Fail to register!"; 
        }
        elseif($_GET['type'] == 2)
        {
            $messageType = "Phone number has been registered by others !"; 
        }
        elseif($_GET['type'] == 3)
        {
            $messageType = "Email has been registered by others !"; 
        }
        elseif($_GET['type'] == 4)
        {
            $messageType = "Username has been registered by others !"; 
        }
        elseif($_GET['type'] == 5)
        {
            $messageType = "Password length must more than 6 !"; 
        }
        elseif($_GET['type'] == 6)
        {
            $messageType = "Password is different with Retype Passowrd !"; 
        }
        elseif($_GET['type'] == 7)
        {
            $messageType = "IC Number has been registered by others !"; 
        }
        echo '
        <script>
            putNoticeJavascript("Notice !! ","'.$messageType.'");  
        </script>
        ';   
        $_SESSION['messageType'] = 0;
    }
    elseif($_SESSION['messageType'] == 3)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Unknown Usertype !"; 
        }
        elseif($_GET['type'] == 2)
        {
            $messageType = "Incorrect Password";
        }
        elseif($_GET['type'] == 3)
        {
            $messageType = "Cannot found this username <br> Please Register"; 
        }
        echo '
        <script>
            putNoticeJavascript("Notice !! ","'.$messageType.'");  
        </script>
        ';   
        $_SESSION['messageType'] = 0;
    }
}
?>
</body>
</html>