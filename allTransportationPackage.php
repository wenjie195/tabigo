<?php
if (session_id() == "")
{
    session_start();
}
// require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

// require_once dirname(__FILE__) . '/classes/TourPackage.php';
require_once dirname(__FILE__) . '/classes/Transport.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$_SESSION['url'] = $_SERVER['REQUEST_URI'];

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
    $conn = connDB();

    $country = rewrite($_POST['country']);
    $state = rewrite($_POST['state']);

    $allTransport = getTransportation($conn, "WHERE country = ? AND state = ? ",array("country","remark"),array($country,$state),"ss");

    $conn->close();
}

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>


<!doctype html>
<html>

<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://tabigo.holiday/" />
<link rel="canonical" href="https://tabigo.holiday/" />-->
<meta property="og:title" content="All Transport | Tabigo" />
<title>All Transport | Tabigo</title>   
<?php include 'css.php'; ?>
</head>


<body class="body">
<?php include 'header.php'; ?>

<div class="width100 overflow menu-distance same-padding min-height padding-bottom50">


     <div class="width100 overflow">

        <h1 class="second-line red-text ow-margin-top0 title-css" style="margin-bottom:10px !important;">Transport (<?php echo $state;?>, <?php echo $country;?>)</h1>
		</div>
        <div class="clear"></div>
 		<div class="width103">


        <?php
        if($allTransport)
        {
            for($cnt = 0;$cnt < count($allTransport) ;$cnt++)
            {
            ?>

		    <a href='userDetailsTransportation.php?id=<?php echo $allTransport[$cnt]->getUid();?>'>
                <div class="whitebox-redshadow showall-box opacity-hover">

                    <?php 
                        // $linkTour = $allTransport[$cnt]->getPhotoOne();
                        // $stringTourA = $linkTour;
                        // $stringTourB = str_replace('https://drive.google.com/file/d/', '', $stringTourA);
                        // $googleLinkTour = str_replace('/view?usp=sharing', '', $stringTourB);
                    ?>
                    <!-- <img src='https://drive.google.com/uc?export=view&id=<?php echo $googleLinkTour;?>' class="width100"> -->

                    <div class="width100 product-details-div">
                        <p class="width100 slider-product-name"><?php echo $allTransport[$cnt]->getRoute();?></p>
                        <p class="width100 slider-product-price text-overflow"><?php echo $allTransport[$cnt]->getCompanyName();?></p>
                    </div>

                </div>
            </a>

            <?php
            }
            ?>
        <?php
        }
        ?>
                          
        </div>
    </div>



<div class="clear"></div>
<?php include 'js.php'; ?>

</body>
</html>