<?php
if (session_id() == "")
{
    session_start();
}
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';
// require_once dirname(__FILE__) . '/sessionLoginChecker.php';

require_once dirname(__FILE__) . '/classes/Countries.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
    $link = $_POST['link'];
    $_SESSION['url'] = $link;
    // $countryId = $_SESSION['country_id'];
    // $countryName = $_SESSION['country_name'];
}

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>
<!doctype html>
<html>
<head>
	<?php include 'meta.php'; ?>
<!--         <meta property="og:url" content="https://tabigo.holiday/" />
        <link rel="canonical" href="https://tabigo.holiday/" />-->
        <meta property="og:title" content="Login | Tabigo" />
        <title>Login | Tabigo</title>   
	<?php include 'css.php'; ?>


</head>

<body class="body">

<header id="header" class="header header--fixed header1 menu-color" role="banner">
    <div class="big-container-size hidden-padding" id="top-menu">
        <div class="red-bg top-fix-row width100 same-padding overflow">
            <p class="left-top-p white-text"><img src="img/phone.png" alt="Contact Us" title="Contact Us" class="top-icon phone-img"> <a href="tel:+60379540196" class="call-a">+603-7954 0196</a> &nbsp;  &nbsp; | &nbsp;  &nbsp; <a href="tel:+60379540096" class="call-a">+603-7954 0096</a> &nbsp;  &nbsp;   &nbsp;
            <span class="email-span"><img src="img/email.png" alt="Contact Us" title="Contact Us" class="top-icon email-img"> outbound@golden-adv.com</span>
            </p>
            <p class="right-top-p"><span class="email-span2"><img src="img/email.png" alt="Contact Us" title="Contact Us" class="top-icon email-img"> outbound@golden-adv.com &nbsp;  &nbsp; </span><a href="https://www.instagram.com/tabigo_asia/" target="_blank"><img src="img/instagram.png" class="top-icon insta-icon"></a> &nbsp; <a href="https://www.facebook.com/Tabigo.Asia" target="_blank"><img src="img/facebook.png" class="top-icon fb-icon"></a></p>
        </div>		

        <div class="clear"></div>

        <div class="same-padding opacity-white-bg width100">
            <div class="float-left left-logo-div">
                <img src="img/logo2.png" class="logo-img" alt="Tabigo" title="Tabigo">
            </div>
            <div class="right-menu-div float-right before-header-div">
                <a class="menu-margin-right menu-item opacity-hover open-signup hover-effect" style="vertical-align:baseline;">
                Register
                </a>            
            </div>
        </div>
    </div>
</header> 

<div class="clear"></div>

<div class="width100 overflow menu-distance same-padding sakura-bg ow-sakura-height">
	<div class="login-page-width">
    <h1 class="title-h1 darkpink-text login-h1 red-text">Login</h1>	   

        <div class="login-div">
            <!-- <form  class="tele-form" action="utilities/loginFunction.php" method="POST"> -->
            <form  class="tele-form" action="utilities/extraLoginFunction.php" method="POST">
            <div class="login-input-div">
                <p class="input-top-text">Username</p>
                <input class="clean tele-input" type="text" placeholder="Username" id="username" name="username" required>
            </div>  
            <div class="login-input-div">
                <p class="input-top-text">Password</p>
                <input class="clean tele-input" type="password" placeholder="Password" id="password" name="password" required>
            </div>   

            <!-- <input class="clean tele-input" type="hidden" value="<?php //echo $countryId;?>" id="country_id" name="country_id" readonly>
            <input class="clean tele-input" type="hidden" value="<?php //echo $countryName;?>" id="country_name" name="country_name" readonly> -->

            <button class="clean red-btn" name="login">Login</button>
            <p class="text-center margin-bottom0"><a class="open-forgot red-link font-400 forgot-size">Forgot Password?</a></p>
            <p class="text-center margin-bottom0"><a class="open-signup red-link font-400 forgot-size">Register Account</a></p>
        </form>
    </div>  
	</div>
</div>  

<?php include 'js.php'; ?>

</body>
</html>