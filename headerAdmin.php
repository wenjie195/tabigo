<header id="header" class="header header--fixed header1 menu-color" role="banner">
    <div class="big-container-size hidden-padding" id="top-menu">
        <div class="red-bg top-fix-row width100 same-padding overflow">
            <p class="left-top-p white-text"><img src="img/phone.png" alt="Contact Us" title="Contact Us" class="top-icon phone-img"> <a href="tel:+60379540196" class="call-a">+603-7954 0196</a> &nbsp;  &nbsp; | &nbsp;  &nbsp; <a href="tel:+60379540096" class="call-a">+603-7954 0096</a> &nbsp;  &nbsp;   &nbsp;
            <span class="email-span"><img src="img/email.png" alt="Contact Us" title="Contact Us" class="top-icon email-img"> outbound@golden-adv.com</span>
            </p>
            <p class="right-top-p"><span class="email-span2"><img src="img/email.png" alt="Contact Us" title="Contact Us" class="top-icon email-img"> outbound@golden-adv.com &nbsp;  &nbsp; </span><a href="https://www.instagram.com/tabigo_asia/" target="_blank"><img src="img/instagram.png" class="top-icon insta-icon"></a> &nbsp; <a href="https://www.facebook.com/Tabigo.Asia" target="_blank"><img src="img/facebook.png" class="top-icon fb-icon"></a></p>
        </div>			

        <div class="clear"></div>

        <div class="same-padding opacity-white-bg width100">
            <div class="float-left left-logo-div">
                <img src="img/logo2.png" class="logo-img" alt="Tabigo" title="Tabigo">
            </div>

            <div class="right-menu-div float-right before-header-div">
                <a href="adminOrderPending.php" class="menu-margin-right menu-item opacity-hover hover-effect user-header">
                    Order
                </a>  

                <!-- <a href="adminAds.php" class="menu-margin-right menu-item opacity-hover hover-effect user-header">
                    Ads
                </a>   -->

                <a href="adminAdsAll.php" class="menu-margin-right menu-item opacity-hover hover-effect user-header">
                    Ads
                </a>  

               <!-- <div class="dropdown menu-margin-right menu-item opacity-hover hover-effect user-header">
                    Ads
                    <img src="img/dropdown.png" class="dropdown-img" >
                    <div class="dropdown-content yellow-dropdown-content">
                        <p class="dropdown-p opacity-hover"><a href="adminAds.php" class="menu-item">All Ads</a></p>
                    </div>
                </div>  -->

                <div class="dropdown menu-margin-right menu-item opacity-hover hover-effect user-header">
                    Details
                    <img src="img/dropdown.png" class="dropdown-img" >
                    <div class="dropdown-content yellow-dropdown-content">
                        <p class="dropdown-p opacity-hover"><a href="adminTour.php" class="menu-item">Tour Package</a></p>
                        <p class="dropdown-p opacity-hover"><a href="adminRestaurant.php" class="menu-item">Restaurant</a></p>
                        <p class="dropdown-p opacity-hover"><a href="adminTransportation.php" class="menu-item">Transportation</a></p>
                        <p class="dropdown-p opacity-hover"><a href="adminPlace.php" class="menu-item">Place of Interest</a></p>
                        <p class="dropdown-p opacity-hover"><a href="adminTicket.php" class="menu-item">Ticket Booking</a></p>
                        <p class="dropdown-p opacity-hover"><a href="adminWifi.php" class="menu-item">Wifi</a></p>
                        <!--<p class="dropdown-p opacity-hover">
                        <a href="https://c121.travelpayouts.com/click?shmarker=297571&promo_id=3770&source_type=link&type=click&trs=137433" class="menu-item">Hotel & Flight</a>
                        </p>
                        <p class="dropdown-p opacity-hover">
                        <a href="https://www.tokionow.com/easc/online/Travel-Step-1-Single.jsp?aid=BORYITLING" class="menu-item">Insurance</a>
                        </p>-->
                    </div>
                </div>  

                <a href="logout.php" class="menu-margin-right menu-item opacity-hover hover-effect user-header">
                Logout
                </a> 

                <div id="dl-menu" class="dl-menuwrapper before-dl">
                    <button class="dl-trigger">Open Menu</button>
                    <ul class="dl-menu">
                        <li><a href="adminOrder.php" class="black-text opacity-hover">Order</a></li>
                        <li><a href="adminTour.php" class="black-text opacity-hover">Tour Package</a></li>
                        <li><a href="adminRestaurant.php" class="black-text opacity-hover">Restaurant</a></li>
                        <li><a href="adminTransportation.php" class="black-text opacity-hover">Transportation</a></li>                                        
                        <li><a href="adminPlace.php" class="black-text opacity-hover">Place of Interest</a></li>
                        <li><a href="adminTicket.php" class="black-text opacity-hover">Ticket Booking</a></li>
                        <li><a href="adminWifi.php" class="black-text opacity-hover">Wifi</a></li>  
                        <!-- <li>
                        <a href="https://c121.travelpayouts.com/click?shmarker=297571&promo_id=3770&source_type=link&type=click&trs=137433" class="black-text opacity-hover">Hotel & Flight</a>
                        </li> 
                        <li>
                        <a href="https://www.tokionow.com/easc/online/Travel-Step-1-Single.jsp?aid=BORYITLING" class="black-text opacity-hover">Insurance</a>
                        </li> -->
                        <li><a  href="logout.php" class="black-text opacity-hover">Logout</a></li>
                    </ul>
                </div>	
            </div>
        </div>
    </div>
</header> 