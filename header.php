<?php
if (!empty($_SERVER['HTTP_CLIENT_IP']))   
{
    $ip_address = $_SERVER['HTTP_CLIENT_IP'];
}
//whether ip is from proxy
elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))  
{
    $ip_address = $_SERVER['HTTP_X_FORWARDED_FOR'];
}
//whether ip is from remote address
else
{
    $ip_address = $_SERVER['REMOTE_ADDR'];
}
$ip_address;

require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Session.php';

$conn = connDB();

$countryRow = getSession($conn, "WHERE ip = ? ORDER BY date_created DESC LIMIT 1",array("ip"),array($ip_address),"s");
$countrySession = $countryRow[0]->getCountry();

$conn->close();
?>

<?php
if(isset($_SESSION['uid']))
{
?>
   <?php
    if($_SESSION['user_type'] == 0)
    //admin
    {
    ?>
    <header id="header" class="header header--fixed header1 menu-color" role="banner">
                <div class="big-container-size hidden-padding" id="top-menu">
            <div class="red-bg top-fix-row width100 same-padding overflow">
				<p class="left-top-p white-text"><img src="img/phone.png" alt="Contact Us" title="Contact Us" class="top-icon phone-img"> <a href="tel:+60379540196" class="call-a">+603-7954 0196</a> &nbsp;  &nbsp; | &nbsp;  &nbsp; <a href="tel:+60379540096" class="call-a">+603-7954 0096</a> &nbsp;  &nbsp;   &nbsp;
				<span class="email-span"><img src="img/email.png" alt="Contact Us" title="Contact Us" class="top-icon email-img"> outbound@golden-adv.com</span>
				</p>
				<p class="right-top-p"><span class="email-span2"><img src="img/email.png" alt="Contact Us" title="Contact Us" class="top-icon email-img"> outbound@golden-adv.com &nbsp;  &nbsp; </span><a href="https://www.instagram.com/tabigo_asia/" target="_blank"><img src="img/instagram.png" class="top-icon insta-icon"></a> &nbsp; <a href="https://www.facebook.com/Tabigo.Asia" target="_blank"><img src="img/facebook.png" class="top-icon fb-icon"></a></p>

			</div>			
			<div class="clear"></div>
<div class="same-padding opacity-white-bg width100">
				<div class="float-left left-logo-div">
					<img src="img/logo2.png" class="logo-img" alt="Tabigo" title="Tabigo">
				</div>



				<div class="right-menu-div float-right before-header-div">
            
					<a href="adminOrderPending.php" class="menu-margin-right menu-item opacity-hover hover-effect user-header">
						Order
					</a>  
                        <div class="dropdown menu-margin-right menu-item opacity-hover hover-effect user-header">
                            Details
                            <img src="img/dropdown.png" class="dropdown-img" >
                            <div class="dropdown-content yellow-dropdown-content">
                                <p class="dropdown-p opacity-hover"><a href="adminTour.php" class="menu-item">Tour Package</a></p>
                                <p class="dropdown-p opacity-hover"><a href="adminRestaurant.php" class="menu-item">Restaurant</a></p>
                                <p class="dropdown-p opacity-hover"><a href="adminTransportation.php" class="menu-item">Transportation</a></p>
                                <p class="dropdown-p opacity-hover"><a href="adminPlace.php" class="menu-item">Place of Interest</a></p>
                                <p class="dropdown-p opacity-hover"><a href="adminTicket.php" class="menu-item">Ticket Booking</a></p>
                                <p class="dropdown-p opacity-hover"><a href="adminWifi.php" class="menu-item">Wifi</a></p>

                                <!--<p class="dropdown-p opacity-hover">
                                    <a href="https://c121.travelpayouts.com/click?shmarker=297571&promo_id=3770&source_type=link&type=click&trs=137433" class="menu-item">Hotel & Flight</a>
                                </p>

                                <p class="dropdown-p opacity-hover">
                                    <a href="https://www.tokionow.com/easc/online/Travel-Step-1-Single.jsp?aid=BORYITLING" class="menu-item">Insurance</a>
                                </p>-->

                            </div>
                        </div>  
					<a href="logout.php" class="menu-margin-right menu-item opacity-hover hover-effect user-header">
						Logout
					</a> 
                                                
                                    <div id="dl-menu" class="dl-menuwrapper before-dl">
                                        <button class="dl-trigger">Open Menu</button>
                                        <ul class="dl-menu">
                                        <li><a href="adminOrder.php" class="black-text opacity-hover">Order</a></li>
                                        <li><a href="adminTour.php" class="black-text opacity-hover">Tour Package</a></li>
                                        <li><a href="adminRestaurant.php" class="black-text opacity-hover">Restaurant</a></li>
                                        <li><a href="adminTransportation.php" class="black-text opacity-hover">Transportation</a></li>                                        
                                        <li><a href="adminPlace.php" class="black-text opacity-hover">Place of Interest</a></li>
                                        <li><a href="adminTicket.php" class="black-text opacity-hover">Ticket Booking</a></li>
                                        <li><a href="adminWifi.php" class="black-text opacity-hover">Wifi</a></li>  

                                       <!-- <li>
                                            <a href="https://c121.travelpayouts.com/click?shmarker=297571&promo_id=3770&source_type=link&type=click&trs=137433" class="black-text opacity-hover">Hotel & Flight</a>
                                        </li> 

                                        <li>
                                            <a href="https://www.tokionow.com/easc/online/Travel-Step-1-Single.jsp?aid=BORYITLING" class="black-text opacity-hover">Insurance</a>
                                        </li> -->

                                        <li><a  href="logout.php" class="black-text opacity-hover">Logout</a></li>
                                        </ul>
                                    </div>					



				</div>
			</div></div>

        </header> 
    
 


    <?php
    }
    elseif($_SESSION['user_type'] == 1)
    //user
    {
    ?>
        <header id="header" class="header header--fixed header1 menu-color" role="banner">
            <div class="big-container-size hidden-padding" id="top-menu">

                <div class="red-bg top-fix-row width100 same-padding overflow">
                    <p class="left-top-p white-text"><img src="img/phone.png" alt="Contact Us" title="Contact Us" class="top-icon phone-img"> <a href="tel:+60379540196" class="call-a">+603-7954 0196</a> &nbsp;  &nbsp; | &nbsp;  &nbsp; <a href="tel:+60379540096" class="call-a">+603-7954 0096</a> &nbsp;  &nbsp;   &nbsp;
                    <span class="email-span"><img src="img/email.png" alt="Contact Us" title="Contact Us" class="top-icon email-img"> outbound@golden-adv.com</span>
                    </p>
                    <p class="right-top-p"><span class="email-span2"><img src="img/email.png" alt="Contact Us" title="Contact Us" class="top-icon email-img"> outbound@golden-adv.com &nbsp;  &nbsp; </span><a href="https://www.instagram.com/tabigo_asia/" target="_blank"><img src="img/instagram.png" class="top-icon insta-icon"></a> &nbsp; <a href="https://www.facebook.com/Tabigo.Asia" target="_blank"><img src="img/facebook.png" class="top-icon fb-icon"></a></p>
                </div>		

                <div class="clear"></div>

			    <div class="same-padding opacity-white-bg width100">

                    <div class="float-left left-logo-div">
                        <img src="img/logo2.png" class="logo-img" alt="Tabigo" title="Tabigo">
                    </div>

                    <div class="right-menu-div float-right before-header-div">

                       

                        <div class="dropdown menu-margin-right menu-item opacity-hover hover-effect user-header">
                            Tourism
                            <img src="img/dropdown.png" class="dropdown-img" >
                            <div class="dropdown-content yellow-dropdown-content">

                                <!-- <p class="dropdown-p opacity-hover"><a href="userTourism.php" class="menu-item">Tour Package</a></p>
                                <p class="dropdown-p opacity-hover"><a href="#" class="menu-item">Restaurant</a></p>
                                <p class="dropdown-p opacity-hover"><a href="#" class="menu-item">Transportation</a></p>
                                <p class="dropdown-p opacity-hover"><a href="#" class="menu-item">Place of Interest</a></p>
                                <p class="dropdown-p opacity-hover"><a href="#" class="menu-item">Ticket Booking</a></p>
                                <p class="dropdown-p opacity-hover"><a href="#" class="menu-item">Wifi</a></p> -->

                                <p class="dropdown-p opacity-hover">
                                    <form action="selectState.php" method="POST" class="menu-item">
                                       <button class="clean transparent border0 width100 hover-effect text-left padding0"> Tour Package
                                        <input type="hidden" value="<?php echo $countrySession;?>" name="country" id="country" readonly> 
                                        <input type="hidden" value="Tour" name="option" id="option" readonly>
                                        </button> 
                                    </form>
                                </p>

                                <p class="dropdown-p opacity-hover">
                                    <form action="selectState.php" method="POST" class="menu-item">
                                        <button class="clean transparent border0 width100 hover-effect text-left padding0">Restaurant
                                        <input type="hidden" value="<?php echo $countrySession;?>" name="country" id="country" readonly> 
                                        <input type="hidden" value="Restaurant" name="option" id="option" readonly>
                                        </button>  
                                    </form>
                                </p>

                                <p class="dropdown-p opacity-hover">
                                    <form action="selectState.php" method="POST" class="menu-item">
                                         <button class="clean transparent border0 width100 hover-effect text-left padding0">Transportation
                                        <input type="hidden" value="<?php echo $countrySession;?>" name="country" id="country" readonly> 
                                        <input type="hidden" value="Transport" name="option" id="option" readonly> 
                                        </button>
                                    </form>
                                </p>

                                <p class="dropdown-p opacity-hover">
                                    <form action="selectState.php" method="POST" class="menu-item">
                                         <button class="clean transparent border0 width100 hover-effect text-left padding0">Place of Interest
                                        <input type="hidden" value="<?php echo $countrySession;?>" name="country" id="country" readonly> 
                                        <input type="hidden" value="POI" name="option" id="option" readonly> 
                                        </button>
                                    </form>
                                </p>

                                <p class="dropdown-p opacity-hover">
                                    <form action="selectState.php" method="POST" class="menu-item">
                                         <button class="clean transparent border0 width100 hover-effect text-left padding0">Ticket Booking
                                        <input type="hidden" value="<?php echo $countrySession;?>" name="country" id="country" readonly> 
                                        <input type="hidden" value="Ticket" name="option" id="option" readonly> 
                                        </button>
                                    </form>
                                </p>

                                <p class="dropdown-p opacity-hover">
                                    <form action="selectState.php" method="POST" class="menu-item">
                                         <button class="clean transparent border0 width100 hover-effect text-left padding0">Wifi
                                        <input type="hidden" value="<?php echo $countrySession;?>" name="country" id="country" readonly> 
                                        <input type="hidden" value="Wifi" name="option" id="option" readonly> 
                                        </button>
                                    </form>
                                </p>

                             

                                <p class="dropdown-p opacity-hover">
                                    <a href="https://c121.travelpayouts.com/click?shmarker=297571&promo_id=3770&source_type=link&type=click&trs=137433" target="_blank" class="menu-item">Hotel & Flight</a>
                                </p>

                                <p class="dropdown-p opacity-hover">
                                    <a href="https://www.tokionow.com/easc/online/Travel-Step-1-Single.jsp?aid=BORYITLING" class="menu-item">Insurance</a>
                                </p>
                                <p class="dropdown-p opacity-hover"><a href="index.php" class="menu-item">Oversea Tour</a></p>
                            </div>
                        </div>  

					    <a href="userOrders.php" class="menu-margin-right menu-item opacity-hover hover-effect user-header">Purchase History</a> 

                        <div class="dropdown menu-item opacity-hover hover-effect user-header"  style="vertical-align:bottom;">
                            Settings
                            <img src="img/dropdown.png" class="dropdown-img" >
                            <div class="dropdown-content yellow-dropdown-content">
                                <p class="dropdown-p opacity-hover"><a href="userEditProfile.php" class="menu-item">Edit Profile</a></p>
                                <p class="dropdown-p opacity-hover"><a href="userEditPassword.php" class="menu-item">Edit Password</a></p>								
                                <p class="dropdown-p opacity-hover"><a href="index.php" class="menu-item">Change Country</a></p>
                                <p class="dropdown-p opacity-hover"><a href="about.php" class="menu-item">About Tabigo</a></p>
                                <p class="dropdown-p opacity-hover"><a href="logout.php" class="menu-item">Logout</a></p>
                            </div>
                        </div>                   

                        <div id="dl-menu" class="dl-menuwrapper before-dl">
                            <button class="dl-trigger">Open Menu</button>
                            <ul class="dl-menu">
                            


                            <li>
                                
                                <form action="selectState.php" method="POST" class="opacity-hover">
                                    
                                    <button class="clean transparent border0 width100 hover-effect text-left padding0 black-text margin0 follow-a-font-size">Tour Package
                                    <input type="hidden" value="<?php echo $countrySession;?>" name="country" id="country" readonly> 
                                    <input type="hidden" value="Tour" name="option" id="option" readonly>
                                    </button> 
                                </form>
                            </li>
                            <li>
                                
                                <form action="selectState.php" method="POST" class="opacity-hover">
                                    <button class="clean transparent border0 width100 hover-effect text-left padding0 black-text margin0 follow-a-font-size">Restaurant
                                    <input type="hidden" value="<?php echo $countrySession;?>" name="country" id="country" readonly> 
                                    <input type="hidden" value="Restaurant" name="option" id="option" readonly> 
                                    </button>
                                </form>
                            </li>
                            <li>
                                
                                <form action="selectState.php" method="POST" class="opacity-hover">
                                    <button class="clean transparent border0 width100 hover-effect text-left padding0 black-text margin0 follow-a-font-size">Transportation
                                    <input type="hidden" value="<?php echo $countrySession;?>" name="country" id="country" readonly> 
                                    <input type="hidden" value="Transport" name="option" id="option" readonly> 
                                    </button>
                                </form>
                            </li>                                        
                            <li>
                                
                                <form action="selectState.php" method="POST"  class="opacity-hover">
                                    <button class="clean transparent border0 width100 hover-effect text-left padding0 black-text margin0 follow-a-font-size">Place of Interest
                                    <input type="hidden" value="<?php echo $countrySession;?>" name="country" id="country" readonly> 
                                    <input type="hidden" value="POI" name="option" id="option" readonly> 
                                    </button>
                                </form>
                            </li>
                            <li>
                                
                                <form action="selectState.php" method="POST"  class="opacity-hover">
                                    <button class="clean transparent border0 width100 hover-effect text-left padding0 black-text margin0 follow-a-font-size">Ticket Booking
                                    <input type="hidden" value="<?php echo $countrySession;?>" name="country" id="country" readonly> 
                                    <input type="hidden" value="Ticket" name="option" id="option" readonly> 
                                    </button>
                                </form>
                            </li>
                            <li>
                               
                                <form action="selectState.php" method="POST"  class="opacity-hover">
                                    <button class="clean transparent border0 width100 hover-effect text-left padding0 black-text margin0 follow-a-font-size">Wifi
                                    <input type="hidden" value="<?php echo $countrySession;?>" name="country" id="country" readonly> 
                                    <input type="hidden" value="Wifi" name="option" id="option" readonly> 
                                    </button>
                                </form>
                            </li>  
                                                                  


                            <li>
                                <a href="https://c121.travelpayouts.com/click?shmarker=297571&promo_id=3770&source_type=link&type=click&trs=137433" target="_blank" class="black-text opacity-hover">Hotel & Flight</a>
                            </li> 

                            <li>
                                <a href="https://www.tokionow.com/easc/online/Travel-Step-1-Single.jsp?aid=BORYITLING" class="black-text opacity-hover">Insurance</a>
                            </li> 

                            <li><a href="index.php" class="black-text opacity-hover">Oversea Tour</a></li> 
                            
                            <li><a href="userOrders.php" class="black-text opacity-hover">Purchase History</a></li> 
                            <li><a href="userEditProfile.php" class="black-text opacity-hover">Edit Profile</a></li> 
                            <li><a href="userEditPassword.php" class="black-text opacity-hover">Edit Password</a></li>                                        
                            <li><a  href="index.php" class="black-text opacity-hover">Change Country</a></li> 
                            <li><a  href="about.php" class="black-text opacity-hover">About Tabigo</a></li>   
                            <li><a  href="logout.php" class="black-text opacity-hover">Logout</a></li>
                            </ul>
                        </div>					

				    </div>
                </div>
            </div>

        </header> 

    <?php
    }
    ?>   
<?php
}
else
//no login
{
?>

    <header id="header" class="header header--fixed header1 menu-color" role="banner">
            <div class="big-container-size hidden-padding" id="top-menu">
        <div class="red-bg top-fix-row width100 same-padding overflow">
            <p class="left-top-p white-text"><img src="img/phone.png" alt="Contact Us" title="Contact Us" class="top-icon phone-img"> <a href="tel:+60379540196" class="call-a">+603-7954 0196</a> &nbsp;  &nbsp; | &nbsp;  &nbsp; <a href="tel:+60379540096" class="call-a">+603-7954 0096</a> &nbsp;  &nbsp;   &nbsp;
            <span class="email-span"><img src="img/email.png" alt="Contact Us" title="Contact Us" class="top-icon email-img"> outbound@golden-adv.com</span>
            </p>
            <p class="right-top-p"><span class="email-span2"><img src="img/email.png" alt="Contact Us" title="Contact Us" class="top-icon email-img"> outbound@golden-adv.com &nbsp;  &nbsp; </span><a href="https://www.instagram.com/tabigo_asia/" target="_blank"><img src="img/instagram.png" class="top-icon insta-icon"></a> &nbsp; <a href="https://www.facebook.com/Tabigo.Asia" target="_blank"><img src="img/facebook.png" class="top-icon fb-icon"></a></p>

        </div>			
        <div class="clear"></div>
        <div class="same-padding opacity-white-bg width100">
            <div class="float-left left-logo-div">
                <img src="img/logo2.png" class="logo-img" alt="Tabigo" title="Tabigo">
            </div>

            <div class="right-menu-div float-right before-header-div">
 
                <a class="menu-margin-right menu-item opacity-hover open-signup hover-effect before-login before-login1">
                    Register
                </a>  
                <a class="menu-margin-right white-text menu-item opacity-hover open-login hover-effect before-login before-login1" style="vertical-align:baseline;">Login</a>             
          
            
                             <div class="dropdown menu-margin-right menu-item opacity-hover hover-effect user-header before-login">
                                    Tourism
                                    <img src="img/dropdown.png" class="dropdown-img" >
                                    <div class="dropdown-content yellow-dropdown-content">

                                      <p class="dropdown-p opacity-hover">
                                    <form action="selectState.php" method="POST" class="menu-item">
                                       <button class="clean transparent border0 width100 hover-effect text-left padding0"> Tour Package
                                        <input type="hidden" value="<?php echo $countrySession;?>" name="country" id="country" readonly> 
                                        <input type="hidden" value="Tour" name="option" id="option" readonly>
                                        </button> 
                                    </form>
                                        </p>
                                <p class="dropdown-p opacity-hover">
                                    <form action="selectState.php" method="POST" class="menu-item">
                                        <button class="clean transparent border0 width100 hover-effect text-left padding0">Restaurant
                                        <input type="hidden" value="<?php echo $countrySession;?>" name="country" id="country" readonly> 
                                        <input type="hidden" value="Restaurant" name="option" id="option" readonly>
                                        </button>  
                                    </form>
                                </p>

                                <p class="dropdown-p opacity-hover">
                                    <form action="selectState.php" method="POST" class="menu-item">
                                         <button class="clean transparent border0 width100 hover-effect text-left padding0">Transportation
                                        <input type="hidden" value="<?php echo $countrySession;?>" name="country" id="country" readonly> 
                                        <input type="hidden" value="Transport" name="option" id="option" readonly> 
                                        </button>
                                    </form>
                                </p>

                                <p class="dropdown-p opacity-hover">
                                    <form action="selectState.php" method="POST" class="menu-item">
                                         <button class="clean transparent border0 width100 hover-effect text-left padding0">Place of Interest
                                        <input type="hidden" value="<?php echo $countrySession;?>" name="country" id="country" readonly> 
                                        <input type="hidden" value="POI" name="option" id="option" readonly> 
                                        </button>
                                    </form>
                                </p>

                                <p class="dropdown-p opacity-hover">
                                    <form action="selectState.php" method="POST" class="menu-item">
                                         <button class="clean transparent border0 width100 hover-effect text-left padding0">Ticket Booking
                                        <input type="hidden" value="<?php echo $countrySession;?>" name="country" id="country" readonly> 
                                        <input type="hidden" value="Ticket" name="option" id="option" readonly> 
                                        </button>
                                    </form>
                                </p>

                                <p class="dropdown-p opacity-hover">
                                    <form action="selectState.php" method="POST" class="menu-item">
                                         <button class="clean transparent border0 width100 hover-effect text-left padding0">Wifi
                                        <input type="hidden" value="<?php echo $countrySession;?>" name="country" id="country" readonly> 
                                        <input type="hidden" value="Wifi" name="option" id="option" readonly> 
                                        </button>
                                    </form>
                                </p>
                                        <p class="dropdown-p opacity-hover"><a href="https://c121.travelpayouts.com/click?shmarker=297571&promo_id=3770&source_type=link&type=click&trs=137433" target="_blank" class="menu-item">Hotel & Flight</a></p>
                                        <p class="dropdown-p opacity-hover"><a href="https://www.tokionow.com/easc/online/Travel-Step-1-Single.jsp?aid=BORYITLING" target="_blank" class="menu-item">Insurance</a></p>
                                                                                <p class="dropdown-p opacity-hover"><a href="index.php" class="menu-item">Oversea Tour</a></p>
                                    </div></div>
<div class="dropdown menu-item opacity-hover hover-effect user-header"  style="vertical-align:bottom;">
                                    Settings
                                    <img src="img/dropdown.png" class="dropdown-img" >
                                    <div class="dropdown-content yellow-dropdown-content">						
                                        <p class="dropdown-p opacity-hover"><a href="index.php" class="menu-item">Change Country</a></p>
                                        <p class="dropdown-p opacity-hover"><a href="about.php" class="menu-item">About Tabigo</a></p>
                                    </div>
                                </div>      

                                </div>  

                                <div id="dl-menu" class="dl-menuwrapper before-dl before-login">
                                    <button class="dl-trigger">Open Menu</button>
                                    <ul class="dl-menu">
                            <li>
                                
                                <form action="selectState.php" method="POST" class="opacity-hover">
                                    
                                    <button class="clean transparent border0 width100 hover-effect text-left padding0 black-text margin0 follow-a-font-size">Tour Package
                                    <input type="hidden" value="<?php echo $countrySession;?>" name="country" id="country" readonly> 
                                    <input type="hidden" value="Tour" name="option" id="option" readonly>
                                    </button> 
                                </form>
                            </li>
                            <li>
                                
                                <form action="selectState.php" method="POST" class="opacity-hover">
                                    <button class="clean transparent border0 width100 hover-effect text-left padding0 black-text margin0 follow-a-font-size">Restaurant
                                    <input type="hidden" value="<?php echo $countrySession;?>" name="country" id="country" readonly> 
                                    <input type="hidden" value="Restaurant" name="option" id="option" readonly> 
                                    </button>
                                </form>
                            </li>
                            <li>
                                
                                <form action="selectState.php" method="POST" class="opacity-hover">
                                    <button class="clean transparent border0 width100 hover-effect text-left padding0 black-text margin0 follow-a-font-size">Transportation
                                    <input type="hidden" value="<?php echo $countrySession;?>" name="country" id="country" readonly> 
                                    <input type="hidden" value="Transport" name="option" id="option" readonly> 
                                    </button>
                                </form>
                            </li>                                        
                            <li>
                                
                                <form action="selectState.php" method="POST"  class="opacity-hover">
                                    <button class="clean transparent border0 width100 hover-effect text-left padding0 black-text margin0 follow-a-font-size">Place of Interest
                                    <input type="hidden" value="<?php echo $countrySession;?>" name="country" id="country" readonly> 
                                    <input type="hidden" value="POI" name="option" id="option" readonly> 
                                    </button>
                                </form>
                            </li>
                            <li>
                                
                                <form action="selectState.php" method="POST"  class="opacity-hover">
                                    <button class="clean transparent border0 width100 hover-effect text-left padding0 black-text margin0 follow-a-font-size">Ticket Booking
                                    <input type="hidden" value="<?php echo $countrySession;?>" name="country" id="country" readonly> 
                                    <input type="hidden" value="Ticket" name="option" id="option" readonly> 
                                    </button>
                                </form>
                            </li>
                            <li>
                               
                                <form action="selectState.php" method="POST"  class="opacity-hover">
                                    <button class="clean transparent border0 width100 hover-effect text-left padding0 black-text margin0 follow-a-font-size">Wifi
                                    <input type="hidden" value="<?php echo $countrySession;?>" name="country" id="country" readonly> 
                                    <input type="hidden" value="Wifi" name="option" id="option" readonly> 
                                    </button>
                                </form>
                            </li>                                          
                                    <li><a href="https://c121.travelpayouts.com/click?shmarker=297571&promo_id=3770&source_type=link&type=click&trs=137433" target="_blank" class="black-text opacity-hover">Hotel & Flight</a></li> 
                                    <li><a href="https://www.tokionow.com/easc/online/Travel-Step-1-Single.jsp?aid=BORYITLING" target="_blank" class="black-text opacity-hover">Insurance</a></li>        
                                    <li><a  href="index.php" class="black-text opacity-hover">Oversea Tour</a></li>                            
                                    <li><a  href="index.php" class="black-text opacity-hover">Change Country</a></li> 
                                    <li><a  href="about.php" class="black-text opacity-hover">About Tabigo</a></li>  
                                    <li><a   class="black-text opacity-hover open-signup">Register</a></li> 
                                    <li><a   class="black-text opacity-hover open-login">Login</a></li>                                       
                                    </ul>
                                </div>	
                              </div>    
        </div></div>
    </header> 

<?php
}
?>