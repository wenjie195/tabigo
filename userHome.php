<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/OrderList.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

// $userDetails = getUser($conn, "WHERE uid =?",array("uid"),array($uid),"s");
// $userData = $userDetails[0];

// $products = getOrderList($conn, "WHERE user_uid = ? ORDER BY date_created DESC ",array("user_uid"),array($uid),"s");
$products = getOrderList($conn, "WHERE user_uid = ? AND status != 'Claimed' ORDER BY date_created DESC ",array("user_uid"),array($uid),"s");

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>
<!doctype html>
<html>
<head>
	<?php include 'meta.php'; ?>
<!--         <meta property="og:url" content="https://tabigo.holiday/" />
        <link rel="canonical" href="https://tabigo.holiday/" />-->
        <meta property="og:title" content="Home | Tabigo" />
        <title>Home | Tabigo</title>   
	<?php include 'css.php'; ?>


</head>

<body class="body">
<?php include 'header.php'; ?>

<div class="clear"></div>

<div class="width100 overflow menu-distance same-padding sakura-bg ow-sakura-height">
	<h1 class="title-h1 raleway red-text ow-no-margin wow fadeIn" data-wow-delay="0.1s">Upcoming Tour / Package</h1>	
    
        <div class="overflow-div">

            <table class="white-table">
                <thead>
                    <th>No.</th>
                    <th>Date</th>
                    <th>Order ID</th>
                    <th>Title</th>
                    <th>Price</th>
                    <th>Service Date</th>
                    <th>Status</th>
                    <th>Details</th>
                </thead>

                <tbody>
                    <?php
                        if($products)
                        {
                            for($cnt = 0;$cnt < count($products) ;$cnt++)
                            {
                            ?>
                                <tr>
                                    <td><?php echo ($cnt+1)?></td>
                                    <td><?php echo $date = date("Y-m-d",strtotime($products[0]->getDateUpdated()));?></td>

                                    <td>
                                        <?php $productUid = $products[$cnt]->getProductUid();?>
                                        <?php echo $newstring = substr($productUid, 0, 10);?>
                                    </td>

                                    <td><?php echo $products[$cnt]->getProductName();?></td>
                                    <td><?php echo $products[$cnt]->getTotalPrice();?></td>
                                    <td><?php echo $products[$cnt]->getServiceDate();?></td>

                                    <td>
                                        <?php 
                                            $status = $products[$cnt]->getStatus();
                                            if($status == "Sold")
                                            {
                                                echo "Available";
                                            }
                                            else
                                            {
                                                // echo "Claimed";
                                                echo $status;
                                            }
                                        ?>
                                    </td>

                                    <td>
                                        <?php 
                                            $serviceType = $products[$cnt]->getServiceType();
                                            if($serviceType == "")
                                            {
                                            ?>
                                                <a href='userPurchaseDetails.php?id=<?php echo $products[$cnt]->getMainProductUid();?>' class="hover-effect red-link ow-font400">
                                                    Details
                                                </a>
                                            <?php
                                            }
                                            else
                                            {}

                                        ?>
                                    </td>
                                </tr>
                            <?php
                            }
                        }
                    ?>                                 
                </tbody>
            </table>

        </div>
</div>

<div class="clear"></div>
<?php include 'js.php'; ?>


</body>
</html>