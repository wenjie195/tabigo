<?php
if (session_id() == "")
{
    session_start();
}
// require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Countries.php';
require_once dirname(__FILE__) . '/classes/Wifi.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$uid = $_SESSION['uid'];
// if($_SESSION['uid'] != "")
// {
//     $uid = $_SESSION['uid'];
// }
// else
// {
//     $uid = NULL;
// }

$_SESSION['url'] = $_SERVER['REQUEST_URI'];

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>
<!doctype html>
<html>

<?php 
    // Program to display URL of current page. 
    if(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on') 
    $link = "https"; 
    else
    $link = "http"; 

    // Here append the common URL characters. 
    $link .= "://"; 

    // Append the host(domain name, ip) to the URL. 
    $link .= $_SERVER['HTTP_HOST']; 

    // Append the requested resource location to the URL 
    $link .= $_SERVER['REQUEST_URI']; 

    // Print the link 
    // echo $link; 
?>

<?php
if(isset($_GET['id']))
{
$conn = connDB();
$allPackage = getWifi($conn,"WHERE uid = ? AND display = 'YES' ",array("uid"),array($_GET['id']), "s");
// $TourDetails = getTourPackage($conn,"WHERE uid = ? AND display = 'YES' ",array("uid"),array($_GET['id']), "s");
// $allTransport = getTransportation($conn, "WHERE display = 'Yes' ORDER BY date_created DESC LIMIT 8");
?>

    <?php
    if($allPackage)
    {
        for($cnt = 0;$cnt < count($allPackage) ;$cnt++)
        {
        ?>

            <head>
            <?php include 'meta.php'; ?>
<!--            <meta property="og:url" content="https://tabigo.holiday/" />
            <link rel="canonical" href="https://tabigo.holiday/" />-->
            <meta property="og:title" content="<?php echo $allPackage[$cnt]->getTitle();?> | Tabigo" />
            <title><?php echo $allPackage[$cnt]->getTitle();?> | Tabigo</title>   
            <?php include 'css.php'; ?>
            </head>
            <body class="body">
            <?php include 'header.php'; ?>

        <?php
        }
        ?>
    <?php
    }
    ?>

    <div class="clear"></div>

    <?php
    if($allPackage)
    {
        for($cnt = 0;$cnt < count($allPackage) ;$cnt++)
        {
        ?>
        
            <div class="width100 overflow same-padding background-div">

                <div class="cover-gap article-content min-height2 blog-content-div1 with-price-height  ow-menu-distance">
                    <div class="print-area" id="printarea">

                        <div class="clear"></div>

                        <h1 class="red-h1 slab darkpink-text red-text"><?php echo $allPackage[$cnt]->getTitle();?></h1>

                        <?php $tourUid = $allPackage[$cnt]->getUid();?>
                        <?php $tourState = $allPackage[$cnt]->getState();?>

                        <table class="article-info-table">
                            <tr>
                                <td class="info-td1"><img src="img/location.png" class="info-img"></td>
                                <td class="info-td2"><?php echo $allPackage[$cnt]->getLocation();?></td>
                            </tr>
                            <tr>
                                <td class="info-td1"><img src="img/company.png" class="info-img"></td>
                                <td class="info-td2"><?php echo $allPackage[$cnt]->getName();?></td>
                            </tr>
                            <tr>
                                <td class="info-td1"><img src="img/compass.png" class="info-img"></td>
                                <td class="info-td2"><?php echo $allPackage[$cnt]->getLatLong();?></td>
                            </tr>
                            <tr>
                                <td class="info-td1"><img src="img/money.png" class="info-img"></td>
                                <td class="info-td2">RM <?php echo $allPackage[$cnt]->getPrice();?></td>
                            </tr>
                        </table>

                        <div class="test" data-allowfullscreen="true">
                            <div class="article-css-control"  data-allowfullscreen="true">
                                <p class="article-p"><?php echo $allPackage[$cnt]->getDetails();?></p> 
                            </div>
                        </div>




                            <?php
                            if($uid != "")
                            {
                            ?>

                                <!-- <form method="POST" action="utilities/preOrderFunction.php">
                                    <input type="hidden" value="<?php //echo $allPackage[$cnt]->getUid();?>" id="tour_uid" name="tour_uid" readonly>
                                    <div class="clear"></div>
                                    <button class="clean red-btn hover-effect border0" name="submit">Purchase</button>
                                </form> -->

                                <form method="POST" action="utilities/preOrderRestaurantFunction.php" class="text-center">

                                    <input type="hidden" value="Wifi" id="service_type" name="service_type" readonly>
                                    <input type="hidden" value="<?php echo $allPackage[$cnt]->getUid();?>" id="package_uid" name="package_uid" readonly>

                                    <!-- <div class="login-input-div">
                                        <p class="input-top-text">Date</p>
                                        <input class="clean tele-input" type="date" placeholder="Date" id="service_date" name="service_date" required>
                                    </div>  -->

                                    <button class="clean red-btn hover-effect border0 center-button" name="submit">Purchase</button>

                                </form>

                            <?php
                            }
                            else
                            {
                            ?>
                                <!-- <div class="text-center">
                                    <button class="clean red-btn hover-effect border0 center-button open-login">Purchase</button>
                                </div> -->

                                <div class="text-center">
                                    <form action="login.php" method="POST" >
                                        <?php
                                        $countryName = $allPackage[0]->getCountry();
                                        $_SESSION['country_name'] = $countryName;
                                        $conn = connDB();

                                        $countryDetails = getCountries($conn,"WHERE en_name = ? ",array("en_name"),array($countryName), "s");
                                        $_SESSION['country_id'] = $countryDetails[0]->getId();
                                        ?>
                                        <input type="hidden" value="<?php echo $_SESSION['url'];?>" id="link" name="link" readonly>
                                        <button class="clean red-btn hover-effect border0 center-button">Purchase</button>
                                    </form>
                                </div>
                            <?php
                            }
                            ?>
                        
                        <div class="hide-print">
                            <script async src="https://static.addtoany.com/menu/page.js"></script>
                            <!-- AddToAny END -->

                            <div class="clear"></div>

                            <div class="width100 overflow padding-bottom50">
                                <!-- AddToAny BEGIN -->
                                <div class="border-div"></div>
                                <h3 class="dark-blue-text share-h3">Share:</h3>
                                <div class="a2a_kit a2a_kit_size_32 a2a_default_style">
                                <a class="a2a_button_copy_link"></a>
                                <a class="a2a_button_facebook"></a>
                                <a class="a2a_button_twitter"></a>
                                <a class="a2a_button_linkedin"></a>
                                <a class="a2a_button_blogger"></a>
                                <a class="a2a_button_facebook_messenger"></a>
                                <a class="a2a_button_whatsapp"></a>
                                <a class="a2a_button_wechat"></a>
                                <a class="a2a_button_line"></a>
                                <a class="a2a_button_telegram"></a>
                                <!--<a class="a2a_button_print"></a>-->
                                </div> 
                            </div>
                        </div>


                        <div class="clear"></div>

                        <div class="border-div"></div>
                            <h3 class="dark-blue-text share-h3">Recommended Wifi</h3>
                            <div class="width100 glider-contain">
                                <div class="glider glider1">

                                    <?php
                                    $conn = connDB();
                                    $recommend = getWifi($conn," WHERE uid != '$tourUid' AND state = '$tourState' AND display = 'YES' ORDER BY date_created DESC LIMIT 8 ");
                                    if($recommend)
                                    {   
                                        for($cntAA = 0;$cntAA < count($recommend) ;$cntAA++)
                                        {
                                        ?>
                                            <a href='userDetailsWifi.php?id=<?php echo $recommend[$cnt]->getUid();?>'>
                                            <div class="shadow-white-box product-box opacity-hover">
                                                <div class="width100 product-details-div">
                                                    <p class="width100 text-overflow slider-product-name"><?php echo $recommend[$cntAA]->getTitle();?></p>
                                                    <p class="width100 slider-product-price text-overflow">RM<?php echo $recommend[$cntAA]->getPrice();?></p>
                                                </div>
                                             </div>
                                            </a>
                                        <?php
                                        }
                                        ?>
                                    <?php
                                    }
                                    ?>

                                </div>
                            </div>
                        </div>


                    </div>
                </div>

            </div>

            <!-- <div class="price-div width100 same-padding">
            </div> -->

        <?php
        }
        ?>
    <?php
    }
    ?>

<?php
}
?>

<?php include 'js.php'; ?>
<style>
*{
   
	vertical-align:middle;
	word-break:keep-all !important;
}
a{
	opacity:0.8;
	transition:.15s ease-in-out;
	cursor:pointer;}
a:hover{
	opacity:1;}
.langz{
	display:none;}
.footer-div{
	display:none;}
</style>

</body>
</html>