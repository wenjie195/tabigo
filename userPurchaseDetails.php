<?php
if (session_id() == "")
{
    session_start();
}
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/InterestingPlace.php';
require_once dirname(__FILE__) . '/classes/TourPackage.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$uid = $_SESSION['uid'];
// $articles = getInterestingPlace($conn,"WHERE uid = ? AND display = 'YES' ",array("articluide_link"),array($_GET['id']), "s");

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>
<!doctype html>
<html>

<?php 
    // Program to display URL of current page. 
    if(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on') 
    $link = "https"; 
    else
    $link = "http"; 

    // Here append the common URL characters. 
    $link .= "://"; 

    // Append the host(domain name, ip) to the URL. 
    $link .= $_SERVER['HTTP_HOST']; 

    // Append the requested resource location to the URL 
    $link .= $_SERVER['REQUEST_URI']; 

    // Print the link 
    // echo $link; 
?>

<?php
if(isset($_GET['id']))
{
$conn = connDB();
// $TourDetails = getInterestingPlace($conn,"WHERE uid = ? AND display = 'YES' ",array("uid"),array($_GET['id']), "s");
$TourDetails = getTourPackage($conn,"WHERE uid = ? AND display = 'YES' ",array("uid"),array($_GET['id']), "s");
?>

    <?php
    if($TourDetails)
    {
        for($cnt = 0;$cnt < count($TourDetails) ;$cnt++)
        {
        ?>

            <head>
            <?php include 'meta.php'; ?>
            <meta property="og:url" content="https://tabigo.holiday/" />
            <link rel="canonical" href="https://tabigo.holiday/" />
            <meta property="og:title" content="<?php echo $TourDetails[$cnt]->getTitle();?> | Tabigo" />
            <title><?php echo $TourDetails[$cnt]->getTitle();?> | Tabigo</title>   
            <?php include 'css.php'; ?>
            </head>
            <body class="body">
            <?php include 'header.php'; ?>

        <?php
        }
        ?>
    <?php
    }
    ?>

    <div class="clear"></div>

    <?php
    if($TourDetails)
    {
        for($cnt = 0;$cnt < count($TourDetails) ;$cnt++)
        {
        ?>
        
            <div class="width100 overflow same-padding background-div">

                <div class="cover-gap article-content ow-menu-distance min-height2 blog-content-div1 with-price-height">
                    <div class="print-area" id="printarea">

                        <div class="clear"></div>

                        <h1 class="red-h1 slab darkpink-text red-text"><?php echo $TourDetails[$cnt]->getTitle();?></h1>
                        <table class="article-info-table">
                            <tr>
                                <td class="info-td1"><img src="img/location.png" class="info-img"></td>
                                <td class="info-td2"><?php echo $TourDetails[$cnt]->getState();?>, <?php echo $TourDetails[$cnt]->getCountry();?></td>
                            </tr>
                            <tr>
                                <td class="info-td1"><img src="img/company.png" class="info-img"></td>
                                <td class="info-td2"><?php echo $TourDetails[$cnt]->getMerchantName();?></td>
                            </tr>                            
                            <tr>
                                <td class="info-td1"><img src="img/money.png" class="info-img"></td>
                                <td class="info-td2">RM<?php echo $TourDetails[$cnt]->getPrice();?></td>
                            </tr>                             
                        </table>

                        <div class="test" data-allowfullscreen="true">
                            <div class="article-css-control"  data-allowfullscreen="true">
                                <p class="article-p"><?php echo $TourDetails[$cnt]->getDescription();?></p> 
                                <p class="article-p"><?php echo $TourDetails[$cnt]->getParagraphOne();?></p> 
                            </div>
                        </div>

                        <div class="test" data-allowfullscreen="true">
                            <div class="article-css-control"  data-allowfullscreen="true">
                                <p class="article-p">Visit most of the famous Heritage sightseeing spot at Georgetown area, start from morning 9am to 2pm ,Transportation, lunch and all the entrance fee are included.</p> 
                                <p class="article-p">Schedule</p> 
                                <!-- <table>
                                	<tr>
                                    	<td>09:00</td>
                                        <td>Depart Hotel</td>
                                    </tr>
                                	<tr>
                                    	<td>09:30 - 11:00</td>
                                        <td>......</td>
                                    </tr>
                                	<tr>
                                    	<td>11:40 - 11:55</td>
                                        <td>......</td>
                                    </tr>                                    
                                	<tr>
                                    	<td>12:05 - 12:30</td>
                                        <td>......</td>
                                    </tr>      
                                	<tr>
                                    	<td>12:45 - 13:30</td>
                                        <td>......</td>
                                    </tr>                                     
                                	<tr>
                                    	<td>13:45 - 14:15</td>
                                        <td>......</td>
                                    </tr>                                	
                                    <tr>
                                    	<td>14:30</td>
                                        <td>Arrive Hotel</td>
                                    </tr>                                                                                                  
                                </table>   -->

                                <table>
                                	<tr>
                                    	<td>09:00</td>
                                        <td>Depart Hotel</td>
                                    </tr>
                                	<tr>
                                    	<td>09:30 - 11:00</td>
                                        <td>Georgetown Heritage Tour<br>
                                            ->St. George Church<br> 
                                            ->Kuan Yin Temple or The Goddess of Mercy Temple (Penangss Oldest Chinese Temple) <br> 
                                            ->Sri Mahamariamman Indian Temple <br> 
                                            ->Kapitan Keling Mosque <br> 
                                            ->Yap Kongsi Temple <br> 
                                            ->Khoo Kongsi <br> 
                                            ->Lebuh Aceh Mosque <br> 
                                            ->Penang Street Art <br> 
                                        </td>
                                    </tr>
                                	<tr>
                                    	<td>11:40 - 11:55</td>
                                        <td>Fort Cornwallis (built by the British East India Company in the late 18th century) </td>
                                    </tr>                                    
                                	<tr>
                                    	<td>12:05 - 12:30</td>
                                        <td>Houses on water stilts</td>
                                    </tr>      
                                	<tr>
                                    	<td>12:45 - 13:30</td>
                                        <td>Lunch: Local City restaurant </td>
                                    </tr>                                     
                                	<tr>
                                    	<td>13:45 - 14:15</td>
                                        <td>Shopping(•Sourviner and Latex shops)</td>
                                    </tr>                                	
                                    <tr>
                                    	<td>14:30</td>
                                        <td>Arrive Hotel</td>
                                    </tr>                                                                                                  
                                </table> 

                                <!-- <img src="img/p1.jpg"> -->
                                <!-- <p class="article-p text-center">Sri Mahamariamman Indian Temple</p> -->

                                <?php 
                                $linkTour = $TourDetails[$cnt]->getPhotoOne();
                                $stringTourA = $linkTour;
                                $stringTourB = str_replace('https://drive.google.com/file/d/', '', $stringTourA);
                                $googleLinkTour = str_replace('/view?usp=sharing', '', $stringTourB);
                                ?>
                                <img src='https://drive.google.com/uc?export=view&id=<?php echo $googleLinkTour;?>' >

                                <?php 
                                $linkTour2 = $TourDetails[$cnt]->getPhotoTwo();
                                $stringTourA2 = $linkTour2;
                                $stringTourB2 = str_replace('https://drive.google.com/file/d/', '', $stringTourA2);
                                $googleLinkTour2 = str_replace('/view?usp=sharing', '', $stringTourB2);
                                ?>
                                <img src='https://drive.google.com/uc?export=view&id=<?php echo $googleLinkTour2;?>' >
                                                              
                                <p class="article-p text-center">
                                    * The above itinerary is subject to change depending on the weather and traffic condition on that day.<br>
                                    *The photos stated above are for internal use only, not for advertising.
                                </p>
                                
                            </div>
                        </div>

                        <div class="hide-print">
                            <script async src="https://static.addtoany.com/menu/page.js"></script>
                            <!-- AddToAny END -->

                            <div class="clear"></div>

                            <div class="width100 overflow padding-bottom50">
                                <!-- AddToAny BEGIN -->
                                <div class="border-div"></div>
                                <h3 class="dark-blue-text share-h3">Share:</h3>
                                <div class="a2a_kit a2a_kit_size_32 a2a_default_style">
                                <a class="a2a_button_copy_link"></a>
                                <a class="a2a_button_facebook"></a>
                                <a class="a2a_button_twitter"></a>
                                <a class="a2a_button_linkedin"></a>
                                <a class="a2a_button_blogger"></a>
                                <a class="a2a_button_facebook_messenger"></a>
                                <a class="a2a_button_whatsapp"></a>
                                <a class="a2a_button_wechat"></a>
                                <a class="a2a_button_line"></a>
                                <a class="a2a_button_telegram"></a>
                                <!--<a class="a2a_button_print"></a>-->
                                </div> 
                            </div>
                        </div>



                    </div>
                </div>

            </div>

            <div class="price-div width100 same-padding">
                <p class="price-desc"><?php echo $TourDetails[$cnt]->getTerms();?></p>
                <p class="price-pp">RM<?php echo $TourDetails[$cnt]->getPrice();?></p>

                <?php
                if($uid != "")
                {
                ?>
                    <!-- <a href="userPayment.php"><div class="purchase-div hover-effect">Purchase</div></a> -->
                    <!-- <form method="POST" action="utilities/preOrderFunction.php"> -->
                    <!-- <form method="POST" action="utilities/directOrderFunction.php"> -->
                    <form method="POST" action="utilities/directPurchaseFunction.php">
                        <input type="hidden" value="<?php echo $TourDetails[$cnt]->getUid();?>" id="tour_uid" name="tour_uid" readonly>
                        <div class="clear"></div>
                        <button class="purchase-div hover-effect border0" name="submit">Purchase</button>
                    </form>
                
                <?php
                }
                else
                {
                    echo "Please login to purchase";
                }
                ?>

                <!-- <a href="userPayment.php"><div class="purchase-div hover-effect">Purchase</div></a> -->
            </div>

        <?php
        }
        ?>
    <?php
    }
    ?>

<?php
}
?>

<?php include 'js.php'; ?>
<style>
*{
   
	vertical-align:middle;
	word-break:keep-all !important;
}
a{
	opacity:0.8;
	transition:.15s ease-in-out;
	cursor:pointer;}
a:hover{
	opacity:1;}
.langz{
	display:none;}
.footer-div{
	display:none;}
</style>

</body>
</html>