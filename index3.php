<?php
// require_once dirname(__FILE__) . '/sessionLoginChecker.php';
if (session_id() == "")
{
    session_start();
}
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>
<!doctype html>
<html>
<head>
	<?php include 'meta.php'; ?>
         <meta property="og:url" content="https://tabigo.holiday/" />
        <link rel="canonical" href="https://tabigo.holiday/" />
        <meta property="og:title" content="Traveler's Choice | Tabigo" />
        <title>Traveler's Choice | Tabigo</title>   
	<?php include 'css.php'; ?>


</head>

<body class="body">
<?php include 'header.php'; ?>


<div class="clear"></div>
<div class="width100 overflow menu-distance same-padding banner-bg">
	<div class="red-opacity-bg">
		<p class="justify-p white-text banner-p raleway wow fadeIn" data-wow-delay="0.1s"><b>Tabigo</b> is the everything <b>travel app</b> that provides travelers with the <b>experiences they want</b> and <b>need</b>.</p>
		<a href="#download"><div class="white-button width100 raleway wow fadeIn" data-wow-delay="0.2s">Download Now</div></a>
	
	</div>	
	
</div>
<div class="clear"></div>
<div class="width100 overflow same-padding pink-bg" id="why">
	<h1 class="title-h1 raleway red-text wow fadeIn" data-wow-delay="0.1s">Why Tabigo App?</h1>			
	<div class="four-div wow fadeIn" data-wow-delay="0.3s">
		<img src="img/why1.png" class="why-png" alt="Smart Itinerary" title="Smart Itinerary">
		<p class="four-div-title four-min-height"><b>Smart Itinerary</b></p>
		<p class="four-div-content">Hassle- Free itinerary planning with Tabigo App.</p>
	</div>	
	<div class="four-div second-four-div mob-second wow fadeIn" data-wow-delay="0.4s">
		<img src="img/why2.png" class="why-png" alt="First Online Travel Platform for Tour Package" title="First Online Travel Platform for Tour Package">
		<p class="four-div-title four-min-height"><b>First Online Travel Platform for Tour Package</b></p>
		<p class="four-div-content">Discover and explore the best rate for Tour Package from varies Tour Agency.</p>
	</div>	
	<div class="tempo-mob-clear"></div>
	<div class="four-div third-four-div wow fadeIn" data-wow-delay="0.5s">
		<img src="img/why3.png" class="why-png" alt="Save on Travel with Tabigo App" title="Save on Travel with Tabigo App">
		<p class="four-div-title four-min-height"><b>Save on Travel with Tabigo App</b></p>
		<p class="four-div-content">Get credit rewards while you make transactions in the apps.</p>
	</div>
	<div class="four-div mob-second wow fadeIn" data-wow-delay="0.6s">
		<img src="img/why4.png" class="why-png" alt="Worry Free Travel" title="Worry Free Travel">
		<p class="four-div-title four-min-height"><b>Worry Free Travel</b></p>
		<p class="four-div-content">Get connected to local authorities such as Police Station, Hospital and Consulate for emergencies.</p>
	</div>		
	
	
</div>
<div class="clear"></div>
<div class="width100 text-center padding-top-bottom same-padding overflow">
	<h1 class="title-h1 raleway red-text ow-no-margin wow fadeIn" data-wow-delay="0.1s">Attractive Discount Rate</h1>	
	<p class="mid-content-p wow fadeIn" data-wow-delay="0.3s">We cover from Theme Park Ticket, Hotels, Restaurants, Transportation and Travel Kits at Discount.</p>
	<img src="img/sunway-lagoon.png" class="partner-png partner-margin-right wow fadeIn" data-wow-delay="0.4s" alt="Sunway Lagoon" title="Sunway Lagoon">
	<img src="img/cameron-lavender -garden.png" class="partner-png partner-margin-right wow fadeIn" data-wow-delay="0.5s" alt="Cameron Lavender Garden" title="Cameron Lavender Garden">
	<img src="img/zoo-negara.png" class="partner-png partner-margin-right wow fadeIn" data-wow-delay="0.6s" alt="Zoo Negara" title="Zoo Negara">
	<img src="img/petronas-twin-towers.png" class="partner-png partner-margin-right wow fadeIn" data-wow-delay="0.7s" alt="PETRONAS Twin Towers" title="PETRONAS Twin Towers">
	<img src="img/rasa-sayang-resort.png" class="partner-png partner-margin-right wow fadeIn" data-wow-delay="0.8s" alt="Shangri-La's Rasa Sayang Resort & Spa" title="Shangri-La's Rasa Sayang Resort & Spa">
	<img src="img/parkroyal-hotel.png" class="partner-png partner-margin-right wow fadeIn" data-wow-delay="0.9s" alt="Parkroyal Hotel & Resorts" title="Parkroyal Hotel & Resorts">
	<img src="img/sheraton-imperial.png" class="partner-png wow fadeIn" data-wow-delay="1s" alt="Sheraton Imperial" title="Sheraton Imperial">

	
</div>	
	
<div class="clear"></div>
<div class="width100 overflow same-padding sakura-bg" id="how">
	<h1 class="title-h1 raleway red-text wow fadeIn" data-wow-delay="0.1s">How Tabigo Works?</h1>			
	<div class="four-div wow fadeIn" data-wow-delay="0.3s">
		<img src="img/icon-01.png" class="why-png" >
		<p class="four-div-title"><b>1</b></p>
		<p class="four-div-content">Explore and Discover the Discounted deals within the destination you desire such as Tour Package or 
Theme Park.</p>
	</div>	
	<div class="four-div second-four-div mob-second wow fadeIn" data-wow-delay="0.4s">
		<img src="img/icon-02.png" class="why-png">
		<p class="four-div-title"><b>2</b></p>
		<p class="four-div-content">Plan and Organize your perfect Itineraries with detailed time and destination.</p>
	</div>
	<div class="tempo-mob-clear"></div>
	<div class="four-div third-four-div wow fadeIn" data-wow-delay="0.5s">
		<img src="img/icon-03.png" class="why-png" >
		<p class="four-div-title"><b>3</b></p>
		<p class="four-div-content">Confirm the attractive deals and Make Payment.</p>
	</div>
	<div class="four-div mob-second wow fadeIn" data-wow-delay="0.6s">
		<img src="img/icon-04.png" class="why-png">
		<p class="four-div-title"><b>4</b></p>
		<p class="four-div-content">Redeem the deals at the counter and Enjoy the Fun with credit for the next travel.</p>
	</div>		
	
	
</div>	
<div class="clear"></div>

<div class="width100 text-center padding-top-bottom same-padding overflow">
	<h1 class="title-h1 raleway red-text ow-no-margin wow fadeIn" data-wow-delay="0.1s">Keep in Touch</h1>	
	<p class="mid-content-p wow fadeIn" data-wow-delay="0.2s">Check us out on <a href="https://www.instagram.com/tabigo_asia/" target="_blank" class="red-link underline-link">Instagram</a> and <a href="https://www.facebook.com/Tabigo.Asia" target="_blank" class="red-link underline-link">Facebook</a> for more exclusive deals!</p>	
	<div class="clear"></div>
	<a href="https://www.instagram.com/tabigo_asia/" target="_blank" class="opacity-hover wow fadeIn" data-wow-delay="0.3s"><img src="img/tabigo-01.jpg" alt="Tabigo Instagram" title="Tabigo Instagram" class="insta-css insta-margin-right"></a>
	<a href="https://www.instagram.com/tabigo_asia/" target="_blank" class="opacity-hover wow fadeIn" data-wow-delay="0.4s"><img src="img/tabigo-02.jpg" alt="Tabigo Instagram" title="Tabigo Instagram" class="insta-css insta-margin-right mob-second"></a>	
	<a href="https://www.instagram.com/tabigo_asia/" target="_blank" class="opacity-hover wow fadeIn" data-wow-delay="0.5s"><img src="img/tabigo-03.jpg" alt="Tabigo Instagram" title="Tabigo Instagram" class="insta-css insta-margin-right"></a>	
	<div class="mob-tempo-three-clear2"></div>
	<a href="https://www.instagram.com/tabigo_asia/" target="_blank" class="opacity-hover wow fadeIn" data-wow-delay="0.6s"><img src="img/tabigo-04.jpg" alt="Tabigo Instagram" title="Tabigo Instagram" class="insta-css insta-margin-right"></a>	
	<a href="https://www.instagram.com/tabigo_asia/" target="_blank" class="opacity-hover wow fadeIn" data-wow-delay="0.7s"><img src="img/tabigo-05.jpg" alt="Tabigo Instagram" title="Tabigo Instagram" class="insta-css insta-margin-right mob-second"></a>	
	<a href="https://www.instagram.com/tabigo_asia/" target="_blank" class="opacity-hover wow fadeIn" data-wow-delay="0.8s"><img src="img/tabigo-06.jpg" alt="Tabigo Instagram" title="Tabigo Instagram" class="insta-css"></a>	
	
</div>
<div class="clear"></div>
<div class="width100 same-padding sea-background overflow" id="download">
	<h1 class="title-h1 raleway red-text wow fadeIn" data-wow-delay="0.1s">A Peek into Tabigo App</h1>
	<div class="overflow width100 app-container">
		<a href=""><img src="img/app-01.png" class="four-img1 wow fadeIn" data-wow-delay="0.2s" alt="Tabigo App" title="Tabigo App"></a>
		<a href=""><img src="img/app-02.png" class="four-img1 second-four-img1 wow fadeIn" data-wow-delay="0.3s" alt="Tabigo App" title="Tabigo App"></a>
		<a href=""><img src="img/app-03.png" class="four-img1 third-four-img1 wow fadeIn" data-wow-delay="0.4s" alt="Tabigo App" title="Tabigo App"></a>
		<a href=""><img src="img/app-04.png" class="four-img1 wow fadeIn" data-wow-delay="0.5s" alt="Tabigo App" title="Tabigo App"></a>
	</div>
	<div class="clear"></div>
	<div class="left50 text-center">
		<h1 class="title-h1 raleway red-text bottom-title wow fadeIn" data-wow-delay="0.1s">Download Now</h1>	
		<a href="" class="opacity-hover"><img src="img/google-play.png" class="download-css wow fadeIn" data-wow-delay="0.2s" alt="Download" title="Download"></a>
		<a href="" class="opacity-hover"><img src="img/app-store.png" class="download-css wow fadeIn" data-wow-delay="0.3s" alt="Download" title="Download"></a>
	</div>
	<div class="left50 text-center">
		<h1 class="title-h1 raleway red-text bottom-title wow fadeIn" data-wow-delay="0.1s">Payment Channel</h1>	
		<img src="img/payment.png" class="download-css wow fadeIn" data-wow-delay="0.2s" alt="Download" title="Download">
	</div>	
</div>


<?php include 'js.php'; ?>

<?php
if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Register Successfully !"; 
        }
        elseif($_GET['type'] == 2)
        {
            $messageType = "Please Register"; 
        }
        elseif($_GET['type'] == 3)
        {
            $messageType = "Incorrect Password";
        }
        elseif($_GET['type'] == 4)
        {
            $messageType = "Resign <br> Please Contact Admin !";
        }
        echo '
        <script>
            putNoticeJavascript("Notice !! ","'.$messageType.'");  
        </script>
        ';   
        $_SESSION['messageType'] = 0;
    }
}
?>

</body>
</html>