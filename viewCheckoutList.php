<?php
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';

require_once dirname(__FILE__) . '/classes/OrderList.php';
require_once dirname(__FILE__) . '/classes/PreOrderList.php';
// require_once dirname(__FILE__) . '/classes/States.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$uid = $_SESSION['uid'];
$orderUid = $_SESSION['order_uid'];

$conn = connDB();

$userDetails = getUser($conn, "WHERE uid =?",array("uid"),array($uid),"s");
// $userData = $userDetails[0];

// $products = getPreOrderList($conn, "WHERE status = 'Pending' ");
// $products = getOrderList($conn, "WHERE user_uid = ? AND status = 'Pending' ",array("user_uid"),array($uid),"s");
$products = getOrderList($conn, "WHERE user_uid = ? AND order_id = ? ",array("user_uid","order_id"),array($uid,$orderUid),"ss");

// $states = getStates($conn);

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>

	<?php include 'meta.php'; ?>
<!--         <meta property="og:url" content="https://tabigo.holiday/" />
        <link rel="canonical" href="https://tabigo.holiday/" />-->
        <meta property="og:title" content="Confirm Purchase | Tabigo" />
        <title>Confirm Purchase | Tabigo</title>   
	<?php include 'css.php'; ?>

</head>

<body class="body">
<?php include 'header.php'; ?>
                    
<div class="width100 overflow menu-distance same-padding sakura-bg ow-sakura-height">
	<h1 class="title-h1 raleway red-text ow-no-margin wow fadeIn" data-wow-delay="0.1s">Confirm Purchase</h1>	
  
        <div class="overflow-scroll-div margin-top30 same-padding-tdh">
            <table class="white-table">
                <thead>
                    <tr>
                        <th>No.</th>
                        <th>Product Name</th>
                        <th>Unit Price (RM)</th>
                        <th>Quantity</th>
                        <th>Subtotal (RM)</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                        if($products)
                        {
                            for($cnt = 0;$cnt < count($products) ;$cnt++)
                            {
                            ?>
                                <tr>
                                    <td><?php echo ($cnt+1)?></td>
                                    <td><?php echo $products[$cnt]->getProductName();?></td>
                                    <td><?php echo $products[$cnt]->getOriginalPrice();?></td>
                                    <td><?php echo $products[$cnt]->getQuantity();?></td>
                                    <td><?php echo $products[$cnt]->getTotalPrice();?></td>
                                </tr>
                            <?php
                            }
                        }
                    ?> 
                </tbody>
            </table>

                <?php
                if($products)
                {
                $totalOrderAmount = 0;
                for ($cnt=0; $cnt <count($products) ; $cnt++)
                {
                    $totalOrderAmount += $products[$cnt]->getTotalPrice();
                }
                }
                else
                {
                    $totalOrderAmount = 0 ;
                }
                ?>

                <div class="clear"></div>

                <div class="width100">
                    <p class="total-amount" >Total Amount : RM<?php echo $totalOrderAmount;?></p>	
                </div>

        <!-- </div> -->


			<div class="clear"></div>
            <!-- <form method="POST" action="payAndShip.php"> -->
            <form method="POST" action="utilities/createOrderFunction.php">             

                <input class="input-name clean" type="hidden" id="insert_name" name="insert_name" placeholder="Name" value="<?php echo $userDetails[0]->getUsername();?>" required>
                <input class="input-name clean" type="hidden" id="insert_contact" name="insert_contact" placeholder="Phone No." value="<?php echo $userDetails[0]->getPhone();?>" required> 

                <input type="hidden" id="order_uid" name="order_uid" value="<?php echo $orderUid ?>" readonly>
                <input type="hidden" id="subtotal" name="subtotal" value="<?php echo $totalOrderAmount;?>" readonly> 

                <div class="width100 text-center margin-top50">
                    <button class="red-btn center-button clean" name="submit">Continue To Payment</button>
                </div> 
                
            </form>

        </div>
    
</div>

<?php include 'js.php'; ?>


</body>
</html>