<?php
// require_once dirname(__FILE__) . '/sessionLoginChecker.php';
if (session_id() == "")
{
    session_start();
}
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>
<!doctype html>
<html>
<head>
	<?php include 'meta.php'; ?>
<!--         <meta property="og:url" content="https://tabigo.holiday/" />
        <link rel="canonical" href="https://tabigo.holiday/" />-->
        <meta property="og:title" content="Payment | Tabigo" />
        <title>Payment | Tabigo</title>   
	<?php include 'css.php'; ?>


</head>

<body class="body">
<?php include 'header.php'; ?>


<div class="clear"></div>
<!-- <div class="width100 overflow menu-distance same-padding min-height padding-bottom50"> -->
<div class="width100 overflow menu-distance same-padding sakura-bg ow-sakura-height">
	<div class="text-center">
    	<img src="img/checked.png" class="success-img">
    	<!-- <p class="success-p green-text">Payment Success</p> -->
        <h1 class="title-h1 raleway black-text ow-no-margin wow fadeIn">Payment Success</h1>
		<!-- <h1 class="red-h1 slab darkpink-text black-text">Half Day Georgetown Heritage tour</h1>
    	<p class="price-pp price-pp2">RM150</p> -->

		<h1 class="red-h1 slab darkpink-text black-text"> </h1>
    	<p class="price-pp price-pp2"> </p>

    	<a href="userPurchase.php"><div class="purchase-div hover-effect">View Purchase History</div></a>
        <!-- <a href="userPurchase_Renew.php"><div class="purchase-div hover-effect">View Purchase History</div></a> -->
    </div>
</div>


<div class="clear"></div>
<?php include 'js.php'; ?>


</body>
</html>