<?php
// if (session_id() == "")
// {
//     session_start();
// }
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>
<!doctype html>
<html>

<head>
    <?php include 'meta.php'; ?>
    <!-- <meta property="og:url" content="https://tabigo.holiday/" />
    <link rel="canonical" href="https://tabigo.holiday/" /> -->
    <meta property="og:title" content="Add Tour Package | Tabigo" />
    <title>Add Tour Package | Tabigo</title>   
    <?php include 'css.php'; ?>
    <script src="//cdn.ckeditor.com/4.14.0/full/ckeditor.js"></script>
</head>

<body class="body">
<?php include 'headerAdmin.php'; ?>

<div class="clear"></div>

<div class="width100 overflow menu-distance same-padding ow-sakura-height">
	<h1 class="title-h1 raleway red-text ow-no-margin wow fadeIn" data-wow-delay="0.1s">Add Tour Package</a></h1>
    <!-- Status default as Available -->	
    <!-- <form> -->
    <!-- <form action="utilities/adminTourAddFunction.php" method="POST" enctype="multipart/form-data"> -->
    <form action="utilities/adminTourAddFunction.php" method="POST">
        <!-- <div class="dual-input"> -->
        <div class="dual-input">
        	<p class="input-top-p admin-top-p">Country*</p>
            <!-- <select class="tele-input clean"><option>Malaysia</option></select> -->
            <input class="tele-input clean" type="text" placeholder="Country" name="country" id="country" required>  
        </div>

        <div class="dual-input second-dual-input">
        	<p class="input-top-p admin-top-p">State*</p>
            <!-- <select class="tele-input clean"><option>Penang</option></select>   -->
            <input class="tele-input clean" type="text" placeholder="State" name="state" id="state" required>        
        </div>

        <div class="clean"></div>

        <div class="dual-input">
        	<p class="input-top-p admin-top-p">Merchant Name*</p>
            <input class="tele-input clean" type="text" placeholder="Merchant Name" name="merchant_name" id="merchant_name" required>  
        </div>

        <div class="dual-input second-dual-input">
        	<p class="input-top-p admin-top-p">Price*</p>
            <input class="tele-input clean" type="text" placeholder="Price" name="price" id="price" required>         
        </div>

        <div class="clean"></div>

        <div class="width100 overflow">
            <p class="input-top-p admin-top-p">Term and Condition (Less than 100 words)*</p>
            <input class="tele-input clean" type="text" placeholder="Conditions" name="terms" id="terms" required>      
        </div>       

        <div class="width100 overflow">
            <p class="input-top-p admin-top-p">Title*</p>
            <input class="tele-input clean" type="text" placeholder="Title" name="title" id="title" required>      
        </div>

        <div class="clear"></div>

        <div class="width100 overflow">
            <p class="input-top-p admin-top-p">Photo 1 Link in Google Drive <img src="img/drive-link.jpg" class="tutorial-img"></p>
             <input class="tele-input clean" type="text" placeholder="Photo 1 Link in Google Drive" name="photo_one" id="photo_one" required>    
        </div>      

        <div class="clear"></div>

        <div class="width100 overflow">
            <p class="input-top-p admin-top-p">Photo 2 Link in Google Drive <img src="img/drive-link.jpg" class="tutorial-img"></p>
            <input class="tele-input clean" type="text" placeholder="Photo 2 Link in Google Drive" name="photo_two" id="photo_two">    
        </div>      

        <div class="clear"></div>

        <div class="width100 overflow">
            <!-- <p class="input-top-p admin-top-p">Profile Photo Link in Google Drive <img src="img/drive-link.jpg" class="tutorial-img"></p> -->
            <p class="input-top-p admin-top-p">External Link (If Any) <img src="img/drive-link.jpg" class="tutorial-img"></p>
             <input class="tele-input clean" type="text" placeholder="Link" name="google_link" id="google_link" >    
        </div>        

        <div class="clear"></div>

        <div class="width100 overflow">
            <p class="input-top-p admin-top-p">Description</p>
            <textarea class="tele-input clean lato blue-text input-textarea admin-input editor-input" type="text" placeholder="Description" name="descprition" id="descprition" required></textarea>  	
        </div>   

        <div class="form-group publish-border input-div width100 overflow">
            <p class="input-top-p admin-top-p">Main Content (Avoid "' and don't copy paste image inside, click the icon for upload image/video/link tutorial)
                <img src="img/refer.png" class="refer-png pointer opacity-hover open-refertexteditor" alt="Click Me" title="Click Me">
                <!-- <img src="img/attention2.png" class="attention-png opacity-hover open-tutorial" alt="Click Me!" title="Click Me!"> -->
            </p>
            <textarea name="editor" id="editor" rows="10" cols="80"  class="tele-input clean lato blue-text input-textarea admin-input editor-input" ></textarea>
        </div>    

        <div class="clear"></div>  

		<div class="width100 text-center">
        	<button class="clean red-btn hover-effect middle-button-size below-forgot margin-bottom30" name="submit">Submit</button>
		</div>

        <!-- <div class="width100 overflow text-center">     
            <button class="green-button white-text clean2 edit-1-btn margin-auto">Submit</button>
        </div> -->

    </form>

</div>

<div class="clear"></div>

<?php include 'js.php'; ?>

<script>
    CKEDITOR.replace('editor');
</script>

</body>
</html>