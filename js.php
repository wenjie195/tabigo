<?php
// if (session_id() == "")
// {
//     session_start();
// }
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Country.php';

$conn = connDB();

$allCountries = getCountry($conn);

$conn->close();
?>

<!-- Status Modal -->
<div id="company-modal" class="modal-css">

  <!-- Modal content -->
  <div class="modal-content-css status-content">
    <span class="close-css close-company">&times;</span>
    <h2 class="h2-title text-center">Add Company</h2>
    
    <!-- <form> -->
    <form action="utilities/addNewCompanyFunction.php" method="POST">
    	<p class="input-p">New Company Name</p>
        <input class="input-style clean" type="text" placeholder="Company Name" id="company_name" name="company_name" required>           
        
        <div class="clear"></div>
        <div class="width100 text-center">
        	<button class="clean red-btn text-center modal-submit">Submit</button>
        </div>
    </form>
  </div>

</div>
<div class="clear"></div>	
<div class="footer-div">
	<p class="footer-p">&copy;<span id="year"></span> Tabigo, All Rights Reserved.</p>
</div>
<!-- Login Modal -->
<div id="login-modal" class="modal-css">
	<div class="modal-content-css login-modal-content">
        <span class="close-css close-login">&times;</span>
        <div class="clear"></div>
   <h1 class="title-h1 darkpink-text login-h1 red-text">Login</h1>	   
   <div class="big-white-div">
		<div class="login-div">
         <form  class="tele-form" action="utilities/loginFunction.php" method="POST">
            <div class="login-input-div">
                <p class="input-top-text">Username</p>
                <input class="clean tele-input" type="text" placeholder="Username" id="username" name="username" required>
            </div>  
            <div class="login-input-div">
                <p class="input-top-text">Password</p>
                <input class="clean tele-input" type="password" placeholder="Password" id="password" name="password" required>
            </div>   

            <button class="clean red-btn" name="login">Login</button>
            <p class="text-center margin-bottom0"><a class="open-forgot red-link font-400 forgot-size">Forgot Password?</a></p>
            <p class="text-center margin-bottom0"><a class="open-signup red-link font-400 forgot-size">Register Account</a></p>
        </form>
          
      </div>                 
 </div>
        
                
	</div>

</div>
<!-- Forgot Password Modal -->
<div id="forgot-modal" class="modal-css">
	<div class="modal-content-css login-modal-content">
        <span class="close-css close-forgot">&times;</span>
        <div class="clear"></div>
   <h1 class="title-h1 darkpink-text login-h1 red-text">Forgot Password</h1>	   
   <div class="big-white-div">
		<div class="login-div">
         <form action="utilities/forgotPasswordFunction.php" method="POST" >
            <div class="login-input-div">
                <p class="input-top-text">Email</p>
                <input class="clean tele-input"  type="email" placeholder="Email" name="email" id="email" required>
            </div>  
             

            <button class="clean red-btn" name="submit">Submit</button>
            <p class="text-center margin-bottom0"><a class="open-login red-link font-400 forgot-size">Login</a></p>
            <p class="text-center margin-bottom0"><a class="open-signup red-link font-400 forgot-size">Register Account</a></p>
        </form>
          
      </div>                 
 </div>
        
                
	</div>

</div>
<!-- Sign Up Modal -->
<div id="signup-modal" class="modal-css">
	<div class="modal-content-css login-modal-content">
        <span class="close-css close-signup">&times;</span>
        <div class="clear"></div>
   <h1 class="title-h1 darkpink-text login-h1 red-text">Sign Up</h1>	   
   <div class="big-white-div">
		<div class="login-div">
         <!-- <form action="utilities/adminStaffAddNewFunction.php" method="POST"> -->
         <form action="utilities/registerFunction.php" method="POST">
            <div class="login-input-div">
                <p class="input-top-text">Username</p><!-- cannot repeat with other user-->
                <input class="clean tele-input" type="text" placeholder="Username" id="username" name="username" required>
            </div>  
            <div class="login-input-div">
                <p class="input-top-text">Email</p><!-- cannot repeat with other user-->
                <input class="clean tele-input" type="email" placeholder="Email" id="email" name="email" required>
            </div>           
            
            <div class="login-input-div">
                <p class="input-top-text">IC Number</p>
                <input class="clean tele-input" type="text" placeholder="IC Number" id="ic_no" name="ic_no" required>
            </div>      

            <div class="login-input-div">
                <p class="input-top-text">Password</p>
                <input class="clean tele-input" type="password" placeholder="Password" id="password" name="password" required>
            </div>   
            <div class="login-input-div">
                <p class="input-top-text">Retype Password</p>
                <input class="clean tele-input" type="password" placeholder="Retype Password" id="retype_password" name="retype_password" required>
            </div> 
            <div class="login-input-div">
                <p class="input-top-text">Fullname</p><!-- CAN repeat with other user-->
                <input class="clean tele-input" type="text" placeholder="Fullname" id="fullname" name="fullname" required>
            </div>    
            <div class="login-input-div">
                <p class="input-top-text">Phone No.</p><!-- cannot repeat with other user-->
                <input class="clean tele-input" type="text" placeholder="Phone No." id="phone" name="phone" required>
            </div>                       
            <div class="login-input-div">
                <p class="input-top-text">Country</p>
                <select class="clean tele-input"  id="country" name="country" required>
                	<!-- <option>Malaysia</option>
                  <option>Thailand</option> -->

                <option>Select Country</option>
                <?php
                for ($cntPro=0; $cntPro <count($allCountries) ; $cntPro++)
                {
                ?>
                  <option value="<?php echo $allCountries[$cntPro]->getEnName();?>">
                    <?php echo $allCountries[$cntPro]->getEnName();?>
                  </option>
                <?php
                }
                ?>

                </select>
            </div>               
            
            
            <button class="clean red-btn" name="submit">Register</button>
            <p class="text-center margin-bottom0"><a class="open-login red-link font-400 forgot-size">Login</a></p>
        </form>
          
      </div>                 
 </div>
        
                
	</div>

</div>
<div id="refertexteditor-modal" class="modal-css">
	<div class="modal-content-css refer-modal-content">
        <span class="close-css close-refertexteditor">&times;</span>
        <div class="clear"></div>
  
	<div class="clear"></div>
    <div class="width100 overflow tutorial-div">
    	<p class="step-p step-p2">How to place image? Click the image icon. Please don't directly copy paste image inside.</p>
        <img src="img/image-icon.jpg" class="width100" alt="How to place image?" title="How to place image?">
    	<p class="step-p step-p2">Right click the image and choose copy image address.</p>
        <img src="img/right-click.jpg" class="width100" alt="How to place image?" title="How to place image?">        
    	<p class="step-p step-p2">Paste the image address inside and click ok.</p>
        <img src="img/paste-image.jpg" class="width100" alt="How to place image?" title="How to place image?"> 
        <h3 class="step-h3">How to Embed Youtube Video?</h3>  
    	<p class="step-p step-p2">Click iframe (the Earth icon) from the tool list.</p>
        <img src="img/earth-icon.jpg" class="width100" alt="How to Embed Youtube Video?" title="How to Embed Youtube Video?">    	
        <p class="step-p step-p2">Click share below the Youtube video.</p>
        <img src="img/share.jpg" class="width100" alt="How to Embed Youtube Video?" title="How to Embed Youtube Video?"> 
        <p class="step-p step-p2">Click embed.</p>
        <img src="img/embed.jpg" class="width100" alt="How to Embed Youtube Video?" title="How to Embed Youtube Video?">        
        <p class="step-p step-p2">Copy the link ONLY, start from https</p>
        <img src="img/youtube-embed.jpg" class="width100" alt="How to Embed Youtube Video?" title="How to Embed Youtube Video?">        <p class="step-p step-p2">Paste the link and click ok.</p>
        <img src="img/youtube-embed2.jpg" class="width100" alt="How to Embed Youtube Video?" title="How to Embed Youtube Video?">        <h3 class="step-h3">How to Embed Facebook Video?</h3>     
        <p class="step-p step-p2">Make sure the video settings is public. Click share and then click embed.</p>
        <img src="img/fb-embed.png" class="width100" alt="How to Embed Facebook Video?" title="How to Embed Facebook Video?">        
         <p class="step-p step-p2">Copy the link inside the after src (the highlighted part inside the picture).</p>
        <img src="img/fb-embed2.png" class="width100" alt="How to Embed Facebook Video?" title="How to Embed Facebook Video?">         				
        <p class="step-p step-p2">Click iframe (the Earth icon) from the tool list.</p>
        <img src="img/earth-icon.jpg" class="width100" alt="How to Embed Facebook Video?" title="How to Embed Facebook Video?">        	
        <p class="step-p step-p2">Paste the link and click ok.</p>
        <img src="img/youtube-embed.jpg" class="width100" alt="How to Embed Facebook Video?" title="How to Embed Facebook Video?">        <h3 class="step-h3"><h3 class="step-h3">How to insert link? Click the link/chain icon.</h3>
        <img src="img/paste-link.jpg" class="width100" alt="How to insert link? Click the link/chain icon." title="How to insert link? Click the link/chain icon.">
         <p class="step-p step-p2">Type the display text for the link. Paste the link inside URL column.</p>
        <img src="img/paste-link2.png" class="width100" alt="How to insert link? Click the link/chain icon." title="How to insert link? Click the link/chain icon.">                   
    </div>        
     <div class="width100 overflow">
     	<div class="clean-button clean close-btn pink-button close-refertexteditor opacity-hover pointer">Close</div>
     </div>           
	</div>
</div>
<script src="js/jquery-3.3.1.min.js" type="text/javascript"></script>
<script>
var d = new Date();
document.getElementById("year").innerHTML = d.getFullYear();
</script>
<script src="js/bootstrap.min.js" type="text/javascript"></script>  
	
<script src="js/wow.min.js" type="text/javascript"></script>
    <script>
     new WOW().init();
    </script>	
<script src="js/headroom.js"></script>


<script>
    (function() {
        var header = new Headroom(document.querySelector("#header"), {
            tolerance: 5,
            offset : 205,
            classes: {
              initial: "animated",
              pinned: "slideDown",
              unpinned: "slideUp"
            }
        });
        header.init();
    
    }());
</script>
 
	<script>
    // Cache selectors
    var lastId,
        topMenu = $("#top-menu"),
        topMenuHeight = topMenu.outerHeight(),
        // All list items
        menuItems = topMenu.find("a"),
        // Anchors corresponding to menu items
        scrollItems = menuItems.map(function(){
          var item = $($(this).attr("href"));
          if (item.length) { return item; }
        });
    
    // Bind click handler to menu items
    // so we can get a fancy scroll animation
    menuItems.click(function(e){
      var href = $(this).attr("href"),
          offsetTop = href === "#" ? 0 : $(href).offset().top-topMenuHeight+1;
      $('html, body').stop().animate({ 
          scrollTop: offsetTop
      }, 500);
      e.preventDefault();
    });
    
    // Bind to scroll
    $(window).scroll(function(){
       // Get container scroll position
       var fromTop = $(this).scrollTop()+topMenuHeight;
       
       // Get id of current scroll item
       var cur = scrollItems.map(function(){
         if ($(this).offset().top < fromTop)
           return this;
       });
       // Get the id of the current element
       cur = cur[cur.length-1];
       var id = cur && cur.length ? cur[0].id : "";
                      
    });
    </script>
 <script type="text/javascript" src="js/modernizr.custom.js"></script>
<script type="text/javascript" src="js/jquery.dlmenu.js"></script>
		<script>
			$(function() {
				$( '#dl-menu' ).dlmenu({
					animationClasses : { classin : 'dl-animate-in-2', classout : 'dl-animate-out-2' }
				});
			});
		</script>



<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert-dev.js"></script>
<script src="js/notification.js"></script>
<script src="js/notiflix-aio-1.9.1.js"></script>
<script src="js/notiflix-aio-1.9.1.min.js"></script>
<script src="js/rrpowered_notification_script.js"></script>

<!--- Modal Box --->
<script>

var statusmodal = document.getElementById("status-modal");
var companymodal = document.getElementById("company-modal");
var loginmodal = document.getElementById("login-modal");
var signupmodal = document.getElementById("signup-modal");
var forgotmodal = document.getElementById("forgot-modal");
var refermodal = document.getElementById("refertexteditor-modal");
var filtermodal = document.getElementById("filter-modal");

var openstatus = document.getElementsByClassName("open-status")[0];
var opensignup = document.getElementsByClassName("open-signup")[0];
var opensignup1 = document.getElementsByClassName("open-signup")[1];
var opensignup2 = document.getElementsByClassName("open-signup")[2];
var opensignup3 = document.getElementsByClassName("open-signup")[3];
var openlogin = document.getElementsByClassName("open-login")[0];
var openlogin1 = document.getElementsByClassName("open-login")[1];
var openlogin2 = document.getElementsByClassName("open-login")[2];
var openlogin3 = document.getElementsByClassName("open-login")[3];
var openlogin4 = document.getElementsByClassName("open-login")[4];
var openforgot = document.getElementsByClassName("open-forgot")[0];
var openrefer = document.getElementsByClassName("open-refertexteditor")[0];
var openfilter = document.getElementsByClassName("open-filter")[0];

var closestatus = document.getElementsByClassName("close-status")[0];
var closerefer = document.getElementsByClassName("close-refertexteditor")[0];
var closerefer1 = document.getElementsByClassName("close-refertexteditor")[1];
var closefilter = document.getElementsByClassName("close-filter")[0];
var closefilter1 = document.getElementsByClassName("close-filter")[1];

var opencompany = document.getElementsByClassName("open-company")[0];

var closecompany = document.getElementsByClassName("close-company")[0];
var closesignup = document.getElementsByClassName("close-signup")[0];
var closelogin = document.getElementsByClassName("close-login")[0];
var closeforgot = document.getElementsByClassName("close-forgot")[0];



if(openstatus){
openstatus.onclick = function() {
  statusmodal.style.display = "block";
}
}

if(closestatus){
closestatus.onclick = function() {
  statusmodal.style.display = "none";
}
}

if(opencompany){
  opencompany.onclick = function() {
  companymodal.style.display = "block";
}
}

if(closecompany){
  closecompany.onclick = function() {
  companymodal.style.display = "none";
}
}


if(openlogin){
openlogin.onclick = function() {


  loginmodal.style.display = "block";  
}
}
if(openlogin1){
openlogin1.onclick = function() {

  forgotmodal.style.display = "none";  	
  loginmodal.style.display = "block";
  signupmodal.style.display = "none";
}
}
if(openlogin2){
openlogin2.onclick = function() {
  signupmodal.style.display = "none";
  forgotmodal.style.display = "none";  	
  loginmodal.style.display = "block";

}
}
if(openlogin3){
openlogin3.onclick = function() {
  signupmodal.style.display = "none";
  forgotmodal.style.display = "none";  	
  loginmodal.style.display = "block";

}
}
if(openlogin4){
openlogin4.onclick = function() {
  signupmodal.style.display = "none";
  forgotmodal.style.display = "none";  	
  loginmodal.style.display = "block";

}
}
if(opensignup){
opensignup.onclick = function() {
  loginmodal.style.display = "none";

  forgotmodal.style.display = "none";
  signupmodal.style.display = "block";  
}
}
if(opensignup1){
opensignup1.onclick = function() {
  loginmodal.style.display = "none";

  forgotmodal.style.display = "none";  
  signupmodal.style.display = "block";  
}
}
if(opensignup2){
opensignup2.onclick = function() {
  loginmodal.style.display = "none";

  forgotmodal.style.display = "none"; 
  signupmodal.style.display = "block";   
}
}
if(opensignup3){
opensignup3.onclick = function() {
  loginmodal.style.display = "none";

  forgotmodal.style.display = "none"; 
  signupmodal.style.display = "block";   
}
}
if(openforgot){
openforgot.onclick = function() {
  forgotmodal.style.display = "block";  
  loginmodal.style.display = "none";

}
}
if(openrefer){
openrefer.onclick = function() {
  refermodal.style.display = "block"; 

}
}
if(openfilter){
openfilter.onclick = function() {
  filtermodal.style.display = "block"; 

}
}


if(closelogin){
closelogin.onclick = function() {
  loginmodal.style.display = "none";
}
}
if(closesignup){
closesignup.onclick = function() {
  signupmodal.style.display = "none";
}
}
if(closeforgot){
closeforgot.onclick = function() {
  forgotmodal.style.display = "none";
}
}
if(closerefer){
closerefer.onclick = function() {
  refermodal.style.display = "none";
}
}
if(closerefer1){
closerefer1.onclick = function() {
  refermodal.style.display = "none";
}
}
window.onclick = function(event) {
  if (event.target == statusmodal) {
    statusmodal.style.display = "none";
  }       

  if (event.target == companymodal) {
    companymodal.style.display = "none";
  }       

  if (event.target == loginmodal) {
    loginmodal.style.display = "none";
  }       

  if (event.target == forgotmodal) {
    forgotmodal.style.display = "none";
  }       

  if (event.target == signupmodal) {
    signupmodal.style.display = "none";
  }
  if (event.target == refermodal) {
    refermodal.style.display = "none";
  }         
}
</script>
<script>
function goBack() {
  window.history.back();
}
</script>
<script src="js/jquery.fancybox.js" type="text/javascript"></script> 

<!-- JS Notice Modal Start-->
<script type="text/javascript" src="js/main.js?version=1.0.1"></script>

<!-- Notice Modal -->
<div id="notice-modal" class="modal-css">

  <!-- Modal content -->
  <div class="modal-content-css notice-modal-content">
    <span class="close-css close-notice" id="close-notice">&times;</span>
    <h1 class="menu-h1" id="noticeTitle">Title Here</h1>
	<div class="menu-link-container" id="noticeMessage">Message Here</div>
  </div>

</div>
<!-- JS Notice Modal Start End-->
    <script src="js/glider.js"></script>
    <script>
      window.addEventListener('load',function(){
        document.querySelector('.glider1').addEventListener('glider-slide-visible', function(event){
            var glider = Glider(this);
            console.log('Slide Visible %s', event.detail.slide)
        });
        document.querySelector('.glider1').addEventListener('glider-slide-hidden', function(event){
            console.log('Slide Hidden %s', event.detail.slide)
        });
        document.querySelector('.glider1').addEventListener('glider-refresh', function(event){
            console.log('Refresh')
        });
        document.querySelector('.glider1').addEventListener('glider-loaded', function(event){
            console.log('Loaded')
        });

        window._ = new Glider(document.querySelector('.glider1'), {
            slidesToShow: 'auto', //'auto',
			slidesToScroll: 1,
            draggable: true,
            scrollLock: false,
            rewind: true,





        });
      });
    </script>
    <script>
      window.addEventListener('load',function(){
        document.querySelector('.glider2').addEventListener('glider-slide-visible', function(event){
            var glider = Glider(this);
            console.log('Slide Visible %s', event.detail.slide)
        });
        document.querySelector('.glider2').addEventListener('glider-slide-hidden', function(event){
            console.log('Slide Hidden %s', event.detail.slide)
        });
        document.querySelector('.glider2').addEventListener('glider-refresh', function(event){
            console.log('Refresh')
        });
        document.querySelector('.glider2').addEventListener('glider-loaded', function(event){
            console.log('Loaded')
        });

        window._ = new Glider(document.querySelector('.glider2'), {
            slidesToShow: 'auto', //'auto',
			slidesToScroll: 1,
            draggable: true,
            scrollLock: false,
            rewind: true,





        });
      });
    </script>    
    <script>
      window.addEventListener('load',function(){
        document.querySelector('.glider3').addEventListener('glider-slide-visible', function(event){
            var glider = Glider(this);
            console.log('Slide Visible %s', event.detail.slide)
        });
        document.querySelector('.glider3').addEventListener('glider-slide-hidden', function(event){
            console.log('Slide Hidden %s', event.detail.slide)
        });
        document.querySelector('.glider3').addEventListener('glider-refresh', function(event){
            console.log('Refresh')
        });
        document.querySelector('.glider3').addEventListener('glider-loaded', function(event){
            console.log('Loaded')
        });

        window._ = new Glider(document.querySelector('.glider3'), {
            slidesToShow: 'auto', //'auto',
			slidesToScroll: 1,
            draggable: true,
            scrollLock: false,
            rewind: true,





        });
      });
    </script>  
        <script>
      window.addEventListener('load',function(){
        document.querySelector('.glider4').addEventListener('glider-slide-visible', function(event){
            var glider = Glider(this);
            console.log('Slide Visible %s', event.detail.slide)
        });
        document.querySelector('.glider4').addEventListener('glider-slide-hidden', function(event){
            console.log('Slide Hidden %s', event.detail.slide)
        });
        document.querySelector('.glider4').addEventListener('glider-refresh', function(event){
            console.log('Refresh')
        });
        document.querySelector('.glider4').addEventListener('glider-loaded', function(event){
            console.log('Loaded')
        });

        window._ = new Glider(document.querySelector('.glider4'), {
            slidesToShow: 'auto', //'auto',
			slidesToScroll: 1,
            draggable: true,
            scrollLock: false,
            rewind: true,





        });
      });
    </script>  
        <script>
      window.addEventListener('load',function(){
        document.querySelector('.glider5').addEventListener('glider-slide-visible', function(event){
            var glider = Glider(this);
            console.log('Slide Visible %s', event.detail.slide)
        });
        document.querySelector('.glider5').addEventListener('glider-slide-hidden', function(event){
            console.log('Slide Hidden %s', event.detail.slide)
        });
        document.querySelector('.glider5').addEventListener('glider-refresh', function(event){
            console.log('Refresh')
        });
        document.querySelector('.glider5').addEventListener('glider-loaded', function(event){
            console.log('Loaded')
        });

        window._ = new Glider(document.querySelector('.glider5'), {
            slidesToShow: 'auto', //'auto',
			slidesToScroll: 1,
            draggable: true,
            scrollLock: false,
            rewind: true,





        });
      });
    </script>  
         <script>
      window.addEventListener('load',function(){
        document.querySelector('.glider6').addEventListener('glider-slide-visible', function(event){
            var glider = Glider(this);
            console.log('Slide Visible %s', event.detail.slide)
        });
        document.querySelector('.glider6').addEventListener('glider-slide-hidden', function(event){
            console.log('Slide Hidden %s', event.detail.slide)
        });
        document.querySelector('.glider6').addEventListener('glider-refresh', function(event){
            console.log('Refresh')
        });
        document.querySelector('.glider6').addEventListener('glider-loaded', function(event){
            console.log('Loaded')
        });

        window._ = new Glider(document.querySelector('.glider6'), {
            slidesToShow: 'auto', //'auto',
			slidesToScroll: 1,
            draggable: true,
            scrollLock: false,
            rewind: true,





        });
      });
    </script>     

