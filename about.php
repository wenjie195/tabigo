<?php
// require_once dirname(__FILE__) . '/sessionLoginChecker.php';
if (session_id() == "")
{
    session_start();
}
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>
<!doctype html>
<html>
<head>
	<?php include 'meta.php'; ?>
<!--         <meta property="og:url" content="https://tabigo.holiday/" />
        <link rel="canonical" href="https://tabigo.holiday/" />-->
        <meta property="og:title" content="About Tabigo | Tabigo" />
        <title>About Tabigo | Tabigo</title>   
	<?php include 'css.php'; ?>


</head>

<body class="body">
<?php include 'header.php'; ?>


<div class="clear"></div>

<div class="width100 overflow same-padding pink-bg menu-distance" >
	
	<h1 class="title-h1 raleway red-text wow fadeIn" data-wow-delay="0.1s">Why Tabigo?</h1>			
	<div class="four-div wow fadeIn no-background-color" data-wow-delay="0.3s">
		<img src="img/why1.png" class="why-png" alt="Smart Itinerary" title="Smart Itinerary">
		<p class="four-div-title four-min-height"><b>Smart Itinerary</b></p>
		<p class="four-div-content">Hassle- Free itinerary planning with Tabigo.</p>
	</div>	
	<div class="four-div second-four-div mob-second wow fadeIn no-background-color" data-wow-delay="0.4s">
		<img src="img/why2.png" class="why-png" alt="First Online Travel Platform for Tour Package" title="First Online Travel Platform for Tour Package">
		<p class="four-div-title four-min-height"><b>First Online Travel Platform for Tour Package</b></p>
		<p class="four-div-content">Discover and explore the best rate for Tour Package from varies Tour Agency.</p>
	</div>	
	
	<div class="four-div third-four-div wow fadeIn no-background-color" data-wow-delay="0.5s">
		<img src="img/why3.png" class="why-png" alt="Save on Travel with Tabigo App" title="Save on Travel with Tabigo App">
		<p class="four-div-title four-min-height"><b>Save on Travel with Tabigo</b></p>
		<p class="four-div-content">Get credit rewards while you make transactions with Tabigo.</p>
	</div>
	<div class="four-div mob-second wow fadeIn no-background-color" data-wow-delay="0.6s">
		<img src="img/why4.png" class="why-png" alt="Worry Free Travel" title="Worry Free Travel">
		<p class="four-div-title four-min-height"><b>Worry Free Travel</b></p>
		<p class="four-div-content">Get connected to local authorities such as Police Station, Hospital and Consulate for emergencies.</p>
	</div>	
    </div>	
<div class="clear"></div>
<div class="width100 text-center padding-top-bottom same-padding overflow">
	<h1 class="title-h1 raleway red-text ow-no-margin wow fadeIn" data-wow-delay="0.1s">Attractive Discount Rate</h1>	
	<p class="mid-content-p wow fadeIn" data-wow-delay="0.3s">We cover from Theme Park Ticket, Hotels, Restaurants, Transportation and Travel Kits at Discount.</p>
	<img src="img/sunway-lagoon.png" class="partner-png partner-margin-right wow fadeIn" data-wow-delay="0.4s" alt="Sunway Lagoon" title="Sunway Lagoon">
	<img src="img/cameron-lavender -garden.png" class="partner-png partner-margin-right wow fadeIn" data-wow-delay="0.5s" alt="Cameron Lavender Garden" title="Cameron Lavender Garden">
	<img src="img/zoo-negara.png" class="partner-png partner-margin-right wow fadeIn" data-wow-delay="0.6s" alt="Zoo Negara" title="Zoo Negara">
	<img src="img/petronas-twin-towers.png" class="partner-png partner-margin-right wow fadeIn" data-wow-delay="0.7s" alt="PETRONAS Twin Towers" title="PETRONAS Twin Towers">
	<img src="img/rasa-sayang-resort.png" class="partner-png partner-margin-right wow fadeIn" data-wow-delay="0.8s" alt="Shangri-La's Rasa Sayang Resort & Spa" title="Shangri-La's Rasa Sayang Resort & Spa">
	<img src="img/parkroyal-hotel.png" class="partner-png partner-margin-right wow fadeIn" data-wow-delay="0.9s" alt="Parkroyal Hotel & Resorts" title="Parkroyal Hotel & Resorts">
	<img src="img/sheraton-imperial.png" class="partner-png wow fadeIn" data-wow-delay="1s" alt="Sheraton Imperial" title="Sheraton Imperial">

	
</div>	
	
<div class="clear"></div>
<div class="width100 overflow same-padding sakura-bg" id="how">
	<h1 class="title-h1 raleway red-text wow fadeIn" data-wow-delay="0.1s">How Tabigo Works?</h1>			
	<div class="four-div wow fadeIn no-background-color" data-wow-delay="0.3s">
		<img src="img/icon-01.png" class="why-png" >
		<p class="four-div-title"><b>1</b></p>
		<p class="four-div-content">Explore and Discover the Discounted deals within the destination you desire such as Tour Package or 
Theme Park.</p>
	</div>	
	<div class="four-div second-four-div mob-second wow fadeIn no-background-color" data-wow-delay="0.4s">
		<img src="img/icon-02.png" class="why-png">
		<p class="four-div-title"><b>2</b></p>
		<p class="four-div-content">Plan and Organize your perfect Itineraries with detailed time and destination.</p>
	</div>
	
	<div class="four-div third-four-div wow fadeIn no-background-color" data-wow-delay="0.5s">
		<img src="img/icon-03.png" class="why-png" >
		<p class="four-div-title"><b>3</b></p>
		<p class="four-div-content">Confirm the attractive deals and Make Payment.</p>
	</div>
	<div class="four-div mob-second wow fadeIn no-background-color" data-wow-delay="0.6s">
		<img src="img/icon-04.png" class="why-png">
		<p class="four-div-title"><b>4</b></p>
		<p class="four-div-content">Redeem the deals at the counter and Enjoy the Fun with credit for the next travel.</p>
	</div>		
	
	
</div>	
<div class="clear"></div>

<div class="width100 text-center padding-top-bottom same-padding overflow">
	<h1 class="title-h1 raleway red-text ow-no-margin wow fadeIn" data-wow-delay="0.1s">Keep in Touch</h1>	
	<p class="mid-content-p wow fadeIn" data-wow-delay="0.2s">Check us out on <a href="https://www.instagram.com/tabigo_asia/" target="_blank" class="red-link underline-link">Instagram</a> and <a href="https://www.facebook.com/Tabigo.Asia" target="_blank" class="red-link underline-link">Facebook</a> for more exclusive deals!</p>	
	<div class="clear"></div>
	<a href="https://www.instagram.com/tabigo_asia/" target="_blank" class="opacity-hover wow fadeIn" data-wow-delay="0.3s"><img src="img/tabigo-01.jpg" alt="Tabigo Instagram" title="Tabigo Instagram" class="insta-css insta-margin-right"></a>
	<a href="https://www.instagram.com/tabigo_asia/" target="_blank" class="opacity-hover wow fadeIn" data-wow-delay="0.4s"><img src="img/tabigo-02.jpg" alt="Tabigo Instagram" title="Tabigo Instagram" class="insta-css insta-margin-right mob-second"></a>	
	<a href="https://www.instagram.com/tabigo_asia/" target="_blank" class="opacity-hover wow fadeIn" data-wow-delay="0.5s"><img src="img/tabigo-03.jpg" alt="Tabigo Instagram" title="Tabigo Instagram" class="insta-css insta-margin-right"></a>	
	<div class="mob-tempo-three-clear2"></div>
	<a href="https://www.instagram.com/tabigo_asia/" target="_blank" class="opacity-hover wow fadeIn" data-wow-delay="0.6s"><img src="img/tabigo-04.jpg" alt="Tabigo Instagram" title="Tabigo Instagram" class="insta-css insta-margin-right"></a>	
	<a href="https://www.instagram.com/tabigo_asia/" target="_blank" class="opacity-hover wow fadeIn" data-wow-delay="0.7s"><img src="img/tabigo-05.jpg" alt="Tabigo Instagram" title="Tabigo Instagram" class="insta-css insta-margin-right mob-second"></a>	
	<a href="https://www.instagram.com/tabigo_asia/" target="_blank" class="opacity-hover wow fadeIn" data-wow-delay="0.8s"><img src="img/tabigo-06.jpg" alt="Tabigo Instagram" title="Tabigo Instagram" class="insta-css"></a>	
	
</div>
<div class="clear"></div>	
	

		



<?php include 'js.php'; ?>


</body>
</html>