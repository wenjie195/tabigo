<?php
if (session_id() == "")
{
    session_start();
}
// require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Countries.php';
require_once dirname(__FILE__) . '/classes/Ticket.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$uid = $_SESSION['uid'];
// if($_SESSION['uid'] != "")
// {
//     $uid = $_SESSION['uid'];
// }
// else
// {
//     $uid = NULL;
// }

$_SESSION['url'] = $_SERVER['REQUEST_URI'];

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>
<!doctype html>
<html>

<?php 
    // Program to display URL of current page. 
    if(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on') 
    $link = "https"; 
    else
    $link = "http"; 

    // Here append the common URL characters. 
    $link .= "://"; 

    // Append the host(domain name, ip) to the URL. 
    $link .= $_SERVER['HTTP_HOST']; 

    // Append the requested resource location to the URL 
    $link .= $_SERVER['REQUEST_URI']; 

    // Print the link 
    // echo $link; 
?>

<?php
if(isset($_GET['id']))
{
$conn = connDB();
$allPackage = getTicket($conn,"WHERE uid = ? AND display = 'YES' ",array("uid"),array($_GET['id']), "s");
// $TourDetails = getTourPackage($conn,"WHERE uid = ? AND display = 'YES' ",array("uid"),array($_GET['id']), "s");
// $allTransport = getTransportation($conn, "WHERE display = 'Yes' ORDER BY date_created DESC LIMIT 8");
?>

    <?php
    if($allPackage)
    {
        for($cnt = 0;$cnt < count($allPackage) ;$cnt++)
        {
        ?>

            <head>
            <?php include 'meta.php'; ?>
<!--            <meta property="og:url" content="https://tabigo.holiday/" />
            <link rel="canonical" href="https://tabigo.holiday/" />-->
            <meta property="og:title" content="<?php echo $allPackage[$cnt]->getTitle();?> | Tabigo" />
            <title><?php echo $allPackage[$cnt]->getTitle();?> | Tabigo</title>   
            <?php include 'css.php'; ?>
            </head>
            <body class="body">
            <?php include 'header.php'; ?>

        <?php
        }
        ?>
    <?php
    }
    ?>

    <div class="clear"></div>

    <?php
    if($allPackage)
    {
        for($cnt = 0;$cnt < count($allPackage) ;$cnt++)
        {
        ?>
        
            <div class="width100 overflow same-padding background-div">

                <div class="cover-gap article-content min-height2 blog-content-div1 with-price-height ow-menu-distance">
                    <div class="print-area" id="printarea">

                        <div class="clear"></div>

                        <h1 class="red-h1 slab darkpink-text red-text"><?php echo $allPackage[$cnt]->getTitle();?>, <?php echo $allPackage[$cnt]->getCountry();?></h1>

                        <?php $tourUid = $allPackage[$cnt]->getUid();?>
                        <?php $tourState = $allPackage[$cnt]->getState();?>

                        <table class="article-info-table">
                            <tr>
                                <td class="info-td1"><img src="img/location.png" class="info-img"></td>
                                <td class="info-td2"><?php echo $allPackage[$cnt]->getState();?></td>
                            </tr>
                            <tr>
                                <td class="info-td1"><img src="img/company.png" class="info-img"></td>
                                <td class="info-td2"><?php echo $allPackage[$cnt]->getMerchantName();?></td>
                            </tr>    
                        </table>
                        <div class="test" data-allowfullscreen="true">
                            <div class="article-css-control"  data-allowfullscreen="true">
                            <?php 
                            $linkTour = $allPackage[$cnt]->getPhoto();
                            $stringTourA = $linkTour;
                            $stringTourB = str_replace('https://drive.google.com/file/d/', '', $stringTourA);
                            $googleLinkTour = str_replace('/view?usp=sharing', '', $stringTourB);
                            ?>
                            <img src='https://drive.google.com/uc?export=view&id=<?php echo $googleLinkTour;?>' >


                                <p class="article-p"><?php echo $allPackage[$cnt]->getDescription();?></p> 
                            </div>
                        </div>

                        <div class="test" data-allowfullscreen="true">
                            <div class="article-css-control"  data-allowfullscreen="true">
                                <p class="article-p"><?php echo $allPackage[$cnt]->getUnitA();?> | RM<?php echo $allPackage[$cnt]->getPackageA();?></p> 
                                <p class="article-p"><?php echo $allPackage[$cnt]->getUnitB();?> | RM<?php echo $allPackage[$cnt]->getPackageB();?></p>                          </div>
                        </div>

<!--                        <div class="test" data-allowfullscreen="true">
                            <div class="article-css-control"  data-allowfullscreen="true">
                                <p class="article-p text-center">
                                    * The above itinerary is subject to change depending on the weather and traffic condition on that day.<br>
                                    *The photos stated above are for internal use only, not for advertising.
                                </p>
                            </div>
                        </div>-->

                        <p class="article-p">Terms : <?php echo $allPackage[$cnt]->getTerms();?></p>

                        
                            <?php
                            if($uid != "")
                            {
                            ?>

                                <!-- <form method="POST" action="utilities/preOrderTransportFunction.php"> -->
                                <form method="POST" action="utilities/preOrderRestaurantFunction.php">

                                    <input type="hidden" value="Ticket" id="service_type" name="service_type" readonly>

                                    <input type="hidden" value="<?php echo $allPackage[$cnt]->getUid();?>" id="package_uid" name="package_uid" readonly>

                                    <div class="login-input-div margin-top30">
                                        <p class="input-top-text">Package</p>
                                        <select class="clean tele-input"  id="tracsport_package" name="tracsport_package" required>
                                            
                                            <option value="A"><?php echo $allPackage[$cnt]->getUnitA();?> | RM<?php echo $allPackage[$cnt]->getPackageA();?></option>

                                            <?php 
                                                $checkUnitB = $allPackage[$cnt]->getUnitB();
                                                if($checkUnitB != "")
                                                {
                                                ?>
                                                    <option value="B"><?php echo $allPackage[$cnt]->getUnitB();?> | RM<?php echo $allPackage[$cnt]->getPackageB();?></option>
                                                <?php
                                                }
                                                else
                                                {}
                                            ?>

                                            <?php 
                                                $checkUnitC = $allPackage[$cnt]->getUnitC();
                                                if($checkUnitC != "")
                                                {
                                                ?>
                                                    <option value="C"><?php echo $allPackage[$cnt]->getUnitC();?> | RM<?php echo $allPackage[$cnt]->getPackageC();?></option>
                                                <?php
                                                }
                                                else
                                                {}
                                            ?>

                                        </select>
                                    </div> 

                                    <!-- <div class="login-input-div">
                                        <p class="input-top-text">Date</p>
                                        <input class="clean tele-input" type="date" placeholder="Date" id="service_date" name="service_date" required>
                                    </div>  -->

                                    <div class="clear"></div>
									<div class="text-center">
                                    	<button class="clean red-btn hover-effect border0 center-button" name="submit">Purchase</button>
									</div>
                                </form>

                            <?php
                            }
                            else
                            {
                            ?>
                                <!-- <div class="text-center">
                                    <button class="clean red-btn hover-effect border0 open-login center-button">Purchase</button>
                                </div> -->

                                <div class="text-center">
                                    <form action="login.php" method="POST" >
                                        <?php
                                        $countryName = $allPackage[0]->getCountry();
                                        $_SESSION['country_name'] = $countryName;
                                        $conn = connDB();

                                        $countryDetails = getCountries($conn,"WHERE en_name = ? ",array("en_name"),array($countryName), "s");
                                        $_SESSION['country_id'] = $countryDetails[0]->getId();
                                        ?>
                                        <input type="hidden" value="<?php echo $_SESSION['url'];?>" id="link" name="link" readonly>
                                        <button class="clean red-btn hover-effect border0 center-button">Purchase</button>
                                    </form>
                                </div>
                            <?php
                            }
                            ?>
                        
                        <div class="hide-print">
                            <script async src="https://static.addtoany.com/menu/page.js"></script>
                            <!-- AddToAny END -->

                            <div class="clear"></div>

                            <div class="width100 overflow padding-bottom50">
                                <!-- AddToAny BEGIN -->
                                <div class="border-div"></div>
                                <h3 class="dark-blue-text share-h3">Share:</h3>
                                <div class="a2a_kit a2a_kit_size_32 a2a_default_style">
                                <a class="a2a_button_copy_link"></a>
                                <a class="a2a_button_facebook"></a>
                                <a class="a2a_button_twitter"></a>
                                <a class="a2a_button_linkedin"></a>
                                <a class="a2a_button_blogger"></a>
                                <a class="a2a_button_facebook_messenger"></a>
                                <a class="a2a_button_whatsapp"></a>
                                <a class="a2a_button_wechat"></a>
                                <a class="a2a_button_line"></a>
                                <a class="a2a_button_telegram"></a>
                                <!--<a class="a2a_button_print"></a>-->
                                </div> 
                            </div>
                        </div>


                        <div class="clear"></div>

                        <div class="border-div"></div>
                            <h3 class="dark-blue-text share-h3">Recommended Ticket</h3>
                            <div class="width100 glider-contain">
                                <div class="glider glider1">

                                    <?php
                                    $conn = connDB();
                                    $recommend = getTicket($conn," WHERE uid != '$tourUid' AND state = '$tourState' AND display = 'YES' ORDER BY date_created DESC LIMIT 8 ");
                                    if($recommend)
                                    {   
                                        for($cntAA = 0;$cntAA < count($recommend) ;$cntAA++)
                                        {
                                        ?>

                                            <a href='userDetailsTour.php?id=<?php echo $recommend[$cntAA]->getUid();?>'>
                                                <div class="shadow-white-box product-box opacity-hover">
                                                    <div class=" square">
                                                        <div class="width100 white-bg image-box-size content2">
                                                        <?php $tourImage = $recommend[$cntAA]->getPhotoOne();?>
                                                            <?php 
                                                                $stringTourA = $tourImage;
                                                                $stringTourB = str_replace('https://drive.google.com/file/d/', '', $stringTourA);
                                                                $googleLinkTour = str_replace('/view?usp=sharing', '', $stringTourB);
                                                            ?>
                                                            <img src='https://drive.google.com/uc?export=view&id=<?php echo $googleLinkTour;?>' >
                                                        </div>
                                                    </div>
                                                    <div class="width100 product-details-div">
                                                        <p class="width100 text-overflow slider-product-name"><?php echo $recommend[$cntAA]->getTitle();?></p>
                                                        <p class="width100 slider-product-price text-overflow"><?php echo $recommend[$cntAA]->getState();?></p>
                                                    </div>
                                                </div>
                                            </a>

                                        <?php
                                        }
                                        ?>
                                    <?php
                                    }
                                    ?>

                                </div>
                            </div>
                        </div>


                    </div>
                </div>

            </div>

            <!-- <div class="price-div width100 same-padding">
            </div> -->

        <?php
        }
        ?>
    <?php
    }
    ?>

<?php
}
?>

<?php include 'js.php'; ?>
<style>
*{
   
	vertical-align:middle;
	word-break:keep-all !important;
}
a{
	opacity:0.8;
	transition:.15s ease-in-out;
	cursor:pointer;}
a:hover{
	opacity:1;}
.langz{
	display:none;}
.footer-div{
	display:none;}
</style>

</body>
</html>