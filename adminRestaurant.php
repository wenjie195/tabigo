<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Restaurant.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

$allTour = getRestaurant($conn, "WHERE display = 'Yes' ORDER BY date_created DESC");

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>
<!doctype html>
<html>
<head>
	<?php include 'meta.php'; ?>
<!--         <meta property="og:url" content="https://tabigo.holiday/" />
        <link rel="canonical" href="https://tabigo.holiday/" />-->
        <meta property="og:title" content="Restaurant Package | Tabigo" />
        <title>Restaurant Package | Tabigo</title>   
	<?php include 'css.php'; ?>


</head>

<body class="body">
<?php include 'headerAdmin.php'; ?>


<div class="clear"></div>
<div class="width100 overflow menu-distance same-padding sakura-bg ow-sakura-height">
    <h1 class="title-h1 raleway red-text ow-no-margin">Restaurant Package 
        <a href="uploadExcelRestaurant.php" class="pink-link hover-effect underline add-link">(Add by Excel)</a></h1>
        <!-- <a href="#" class="pink-link hover-effect underline">(Add 1 by 1)</a> | <a href="uploadExcelRestaurant.php" class="pink-link hover-effect underline">(Add by Excel)</a></h1> -->
        <!-- <a href="adminTourAdd.php" class="pink-link hover-effect underline">(Add 1 by 1)</a> | <a href="uploadExcelRestaurant.php" class="pink-link hover-effect underline">(Add by Excel)</a></h1>	 -->
    <div class="overflow-div">
            <table class="white-table">
                <thead>
                    <th>No.</th>
                    <th>Country</th>
                    <th>Location</th>
                    <th>Name</th>
                    <th>Title</th>
                    <th>Price</th>

                    <!-- <th>Address</th>
                    <th>Lat/ Long</th> -->
                    
                    <!-- <th>Status</th> -->
                    <th>Edit</th>
                    <th>Delete</th>
                </thead>

                <?php
                if($allTour)
                {
                    for($cnt = 0;$cnt < count($allTour) ;$cnt++)
                    {
                    ?>    
                        <tr>
                            <td><?php echo ($cnt+1)?></td>
                            <td><?php echo $allTour[$cnt]->getCountry();?></td>
                            <td><?php echo $allTour[$cnt]->getState();?></td>
                            <td><?php echo $allTour[$cnt]->getName();?></td>
                            <td><?php echo $allTour[$cnt]->getTitle();?></td>

                            <td>
                                <?php 
                                    $country = $allTour[$cnt]->getCountry();
                                    if($country == "Malaysia")
                                    {
                                        echo "RM";

                                    }
                                    elseif($country == "Japan")
                                    {
                                        echo "JPY";
                                    }
                                    else
                                    {}
                                ?>
                                <?php echo $allTour[$cnt]->getPrice();?>
                            </td>

                            <!-- <td><?php //echo $allTour[$cnt]->getAddress();?></td>
                            <td><?php //echo $allTour[$cnt]->getLatLong();?></td> -->
                            
                            <td>
                                <form action="adminRestaurantEdit.php" method="POST">
                                    <button class="transparent-button clean orange-link hover-effect" type="submit" name="package_uid" value="<?php echo $allTour[$cnt]->getUid();?>">
                                        Edit
                                    </button>
                                </form>
                            </td>

                            <td>
                                <form method="POST" action="utilities/adminRestaurantDeleteFunction.php" class="hover1">
                                    <button class="transparent-button clean red-link hover-effect" type="submit" name="package_uid" value="<?php echo $allTour[$cnt]->getUid();?>">
                                        Delete
                                    </button>
                                </form>
                            </td>

                        </tr>
                    <?php
                    }
                }
                ?>  

            </table>
        </div>
</div>
<div class="clear"></div>
<?php include 'js.php'; ?>

<?php
if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Item Deleted !"; 
        }
        elseif($_GET['type'] == 2)
        {
            $messageType = "New Item Added !"; 
        }
        elseif($_GET['type'] == 3)
        {
            $messageType = "Details Updated !";
        }
        elseif($_GET['type'] == 4)
        {
            $messageType = "Fail To Delete !";
        }
        elseif($_GET['type'] == 5)
        {
            $messageType = "ERROR !";
        }
        elseif($_GET['type'] == 6)
        {
            $messageType = "Fail To Update !";
        }
        echo '
        <script>
            putNoticeJavascript("Notice !! ","'.$messageType.'");  
        </script>
        ';   
        $_SESSION['messageType'] = 0;
    }
}
?>

</body>
</html>