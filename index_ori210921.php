<?php
// require_once dirname(__FILE__) . '/sessionLoginChecker.php';
if (session_id() == "")
{
    session_start();
}
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>
<!doctype html>
<html>
<head>
	<?php include 'meta.php'; ?>
         <meta property="og:url" content="https://tabigo.holiday/" />
        <link rel="canonical" href="https://tabigo.holiday/" />
        <meta property="og:title" content="Traveler's Choice | Tabigo" />
        <title>Traveler's Choice | Tabigo</title>   
	<?php include 'css.php'; ?>


</head>

<body class="body">
<?php include 'header.php'; ?>


<div class="clear"></div>
<div class="width100 overflow menu-distance same-padding sakura-bg ow-sakura-height">
	<h1 class="title-h1 raleway red-text ow-no-margin wow fadeIn" data-wow-delay="0.1s">Welcome to Tabigo</h1>	
    <div class="text-center"><p class="mid-content-p wow fadeIn" data-wow-delay="0.2s">Kindly choose a country.</p></div>
    <div class="clear"></div>
    <div class="ta-big-three">
    <!--repeat this line for the country name-->
    	<!-- <button class="country-button hover-effect clean"><div class="ta-three-div"><p>Brunei</p></div></button> -->
        <button class="country-button hover-effect clean"><div class="ta-three-div"><p>Malaysia</p></div></button>
    <!-- end of repeat, can delete the bottom-->
        <button class="country-button hover-effect clean"><div class="ta-three-div"><p>Brunei</p></div></button>
    	<button class="country-button hover-effect clean"><div class="ta-three-div"><p>Cambodia</p></div></button>
        <button class="country-button hover-effect clean"><div class="ta-three-div"><p>Indonesia</p></div></button>
        <button class="country-button hover-effect clean"><div class="ta-three-div"><p>Japan</p></div></button>
        <!-- <button class="country-button hover-effect clean"><div class="ta-three-div"><p>Malaysia</p></div></button> -->
        <button class="country-button hover-effect clean"><div class="ta-three-div"><p>Myammar</p></div></button>
        <button class="country-button hover-effect clean"><div class="ta-three-div"><p>Singapore</p></div></button>
		<button class="country-button hover-effect clean"><div class="ta-three-div"><p>Thailand</p></div></button>
        <button class="country-button hover-effect clean"><div class="ta-three-div"><p>Vietnam</p></div></button>
    <!-- end of delete-->   
    </div>		
		
	
</div>



<?php include 'js.php'; ?>

<?php
if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Register Successfully !"; 
        }
        elseif($_GET['type'] == 2)
        {
            $messageType = "Please Register"; 
        }
        elseif($_GET['type'] == 3)
        {
            $messageType = "Incorrect Password";
        }
        echo '
        <script>
            putNoticeJavascript("Notice !! ","'.$messageType.'");  
        </script>
        ';   
        $_SESSION['messageType'] = 0;
    }
    if($_SESSION['messageType'] == 2)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Fail to register!"; 
        }
        elseif($_GET['type'] == 2)
        {
            $messageType = "Phone number has been registered by others !"; 
        }
        elseif($_GET['type'] == 3)
        {
            $messageType = "Email has been registered by others !"; 
        }
        elseif($_GET['type'] == 4)
        {
            $messageType = "Username has been registered by others !"; 
        }
        elseif($_GET['type'] == 5)
        {
            $messageType = "Password length must more than 6 !"; 
        }
        elseif($_GET['type'] == 6)
        {
            $messageType = "Password is different with Retype Passowrd !"; 
        }
        echo '
        <script>
            putNoticeJavascript("Notice !! ","'.$messageType.'");  
        </script>
        ';   
        $_SESSION['messageType'] = 0;
    }
    if($_SESSION['messageType'] == 3)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Unknown Usertype !"; 
        }
        elseif($_GET['type'] == 2)
        {
            $messageType = "Incorrect Password";
        }
        elseif($_GET['type'] == 3)
        {
            $messageType = "Cannot found this username <br> Please Register"; 
        }
        echo '
        <script>
            putNoticeJavascript("Notice !! ","'.$messageType.'");  
        </script>
        ';   
        $_SESSION['messageType'] = 0;
    }
}
?>

</body>
</html>