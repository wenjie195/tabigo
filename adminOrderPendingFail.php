<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Orders.php';
require_once dirname(__FILE__) . '/classes/OrderList.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

// $allTour = getTourPackage($conn, "WHERE display = 'Yes' ");
// $allOrders = getOrders($conn);
// $allOrders = getOrders($conn, "WHERE payment_status = 'PENDING' ");
$allOrders = getOrders($conn, "WHERE payment_status != 'COMPLETED' ");

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>
<!doctype html>
<html>
<head>
	<?php include 'meta.php'; ?>
<!--         <meta property="og:url" content="https://tabigo.holiday/" />
        <link rel="canonical" href="https://tabigo.holiday/" />-->
        <meta property="og:title" content="Order (Pending) | Tabigo" />
        <title>Order (Pending) | Tabigo</title>   
	<?php include 'css.php'; ?>


</head>

<body class="body">
<?php include 'header.php'; ?>


<div class="clear"></div>
<div class="width100 overflow menu-distance same-padding sakura-bg ow-sakura-height">
	<!-- <h1 class="title-h1 raleway red-text ow-no-margin wow fadeIn" data-wow-delay="0.1s">Tour Package <a href="adminAddTour.php" class="pink-link hover-effect underline">(Add)</a></h1>	 -->
    <h1 class="title-h1 raleway red-text ow-no-margin wow fadeIn" data-wow-delay="0.1s">Pending/ <a href="adminOrderCompleted.php" class="pink-link hover-effect underline">Completed</a></h1>	
    <div class="overflow-div">
            <table class="white-table">
                <thead>
                    <th>No.</th>
                    <th>Date</th>
                    <th>Package / Tour Name</th>
                    <!-- <th>Price (RM)</th> -->
                    <th>Price</th>
                    <th>Name</th>
                    <th>Payment Details</th>
                    <th>Status</th>
                    <th>Decision</th>
                    
                </thead>

                <?php
                if($allOrders)
                {
                    for($cnt = 0;$cnt < count($allOrders) ;$cnt++)
                    {
                    ?>    
                        <tr>
                            <td><?php echo ($cnt+1)?></td>
                            <td><?php echo date("Y-m-d",strtotime($allOrders[$cnt]->getDateUpdated()));?></td>

                            <td>
                                <?php 
                                    // echo $orderId = $allOrders[$cnt]->getOrderId();
                                    // echo "<br>";
                                    $orderId = $allOrders[$cnt]->getOrderId();

                                    $conn = connDB();
                                    $orderListRows = getOrderList($conn,"WHERE order_id = ? ", array("order_id") ,array($orderId),"s");
                                    echo $packageName = $orderListRows[0]->getProductName();
                                    // $conn->close();

                                    // if($orderListRows)
                                    // {
                                    //     for($cntA = 0;$cntA < count($orderListRows) ;$cntA++)
                                    //     {
                                    //         // echo $orderListRows[0]->getProductName();
                                    //         echo $orderListRows[$cntA]->getProductName();
                                    //     }
                                    // }

                                ?>
                            </td>

                            <td>RM<?php echo $allOrders[$cnt]->getSubtotal();?></td>
                            <td><?php echo $allOrders[$cnt]->getName();?></td>
                            <td><?php echo $allOrders[$cnt]->getPaymentBankReference();?></td>
                            <td><?php echo $allOrders[$cnt]->getShippingStatus();?></td>

                            <td>
                                <form action="utilities/adminOrderApprovedFunction.php" method="POST">
                                    <button class="transparent-button clean red-link hover-effect" type="submit" name="order_uid" value="<?php echo $allOrders[$cnt]->getOrderId();?>">
                                        <!-- <img src="img/tick.png" class="width100 hover1a" alt="Approve" title="Approve"> -->
                                        Approve
                                    </button>
                                </form>

                                <form action="utilities/adminOrderRejectedFunction.php" method="POST">
                                    <button class="transparent-button clean red-link hover-effect" type="submit" name="order_uid" value="<?php echo $allOrders[$cnt]->getOrderId();?>">
                                        <!-- <img src="img/close.png" class="width100 hover1a" alt="Reject" title="Reject"> -->
                                        Reject
                                    </button>
                                </form>
                            </td>
                            
                            <!-- <td>
                                <a href="adminOrderDecision.php"  class="transparent-button clean red-link hover-effect">
                                    <form>
                                        <button class="transparent-button clean red-link hover-effect" >
                                            Decision
                                        </button>
                                    </form>
                                </a>
                            </td> -->

                        </tr>
                    <?php
                    }
                }
                ?>  

                <!-- <tr>
                    <td>1</td>
                    <td>14/9/2021</td>
                    <td>Half Day Georgetown Heritage tour</td>
                    <td>RM150</td>
                    <td>Available</td>
                    <td><form><a href="adminEditTour.php" class="hover-effect red-link ow-font400"><button class="transparent-button clean orange-link hover-effect">Edit</button></a></form></td>
                    <td><form><button  class="transparent-button clean red-link hover-effect">Delete</button></form></td>
                </tr> -->

            </table>
        </div>
</div>
<div class="clear"></div>
<?php include 'js.php'; ?>

<?php
if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Tour Deleted !"; 
        }
        elseif($_GET['type'] == 2)
        {
            $messageType = "Please Register"; 
        }
        // elseif($_GET['type'] == 3)
        // {
        //     $messageType = "Incorrect Password";
        // }
        echo '
        <script>
            putNoticeJavascript("Notice !! ","'.$messageType.'");  
        </script>
        ';   
        $_SESSION['messageType'] = 0;
    }
}
?>

</body>
</html>