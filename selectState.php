<?php
if (session_id() == "")
{
    session_start();
}
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';
// require_once dirname(__FILE__) . '/sessionLoginChecker.php';

require_once dirname(__FILE__) . '/classes/States.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
    $conn = connDB();

    $country = rewrite($_POST['country']);
    $option = rewrite($_POST['option']);

    // echo $country = rewrite($_POST['country']);
    // echo "<br>";
    // echo $option = rewrite($_POST['option']);

    $itemOption = getStates($conn,"WHERE country = ? AND remark = ? ",array("country","remark"),array($country,$option), "ss");
}

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>
<!doctype html>
<html>
    
<head>
<?php include 'meta.php'; ?>
<meta property="og:url" content="https://tabigo.holiday/" />
<link rel="canonical" href="https://tabigo.holiday/" />
<meta property="og:title" content="Traveler's Choice | Tabigo" />
<title>Traveler's Choice | Tabigo</title>   
<?php include 'css.php'; ?>
</head>

<body class="body">

 <?php include 'header.php'; ?>


<div class="clear"></div>

<div class="width100 overflow menu-distance same-padding sakura-bg ow-sakura-height">
	<h1 class="title-h1 raleway red-text ow-no-margin" style="margin-bottom:10px !important;">Select State</h1>	
    
    <div class="clear"></div>

    <div class="ta-big-three">

        <?php
        $conn = connDB();
        if($itemOption)
        {
            for($cnt = 0;$cnt < count($itemOption) ;$cnt++)
            {
            ?>    

                <?php
                if($option == "Tour" )
                {
                ?>
                    <form action="allTourPackage.php" method="POST" class="country-button">

                <?php
                }
                elseif($option == "Restaurant" )
                {
                ?>
                    <!-- <form action="allRestaurantPackage.php" method="POST" class="country-button"> -->
                    <form action="selectCuisine.php" method="POST" class="country-button">

                <?php
                }
                elseif($option == "Transport" )
                {
                ?>
                    <form action="allTransportationPackage.php" method="POST" class="country-button">

                <?php
                }
                elseif($option == "POI" )
                {
                ?>
                    <form action="allPointOfInterest.php" method="POST" class="country-button">

                <?php
                }
                elseif($option == "Ticket" )
                {
                ?>
                    <form action="allTicketPackage.php" method="POST" class="country-button">

                <?php
                }
                elseif($option == "Wifi" )
                {
                ?>
                    <form action="allWifiPackage.php" method="POST" class="country-button">

                <?php
                }
                ?>

                    <button class="transparent hover-effect clean border0 width100">
                        <div class="ta-three-div">
                            <p><?php echo $itemOption[$cnt]->getState();?></p>
                        </div>
                        <input type="hidden" value="<?php echo $country;?>" name="country" id="country" readonly> 
                        <input type="hidden" value="<?php echo $itemOption[$cnt]->getState();?>" name="state" id="state" readonly> 
                    </button>
                </form>

            <?php
            }
        }
        ?> 

    </div>		
		
	
</div>

<?php include 'js.php'; ?>

</body>
</html>