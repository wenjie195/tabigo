<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

// require_once dirname(__FILE__) . '/classes/Shipping.php';
require_once dirname(__FILE__) . '/classes/Orders.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$uid = $_SESSION['uid'];
$orderUid = $_SESSION['order_uid'];

$conn = connDB();

$userDetails = getUser($conn, "WHERE uid =?",array("uid"),array($uid),"s");
// $userData = $userDetails[0];

// $orderDetails = getOrders($conn," WHERE uid = ? ORDER BY date_created DESC LIMIT 1 ",array("uid"),array($uid),"s");
// $orderDetails = getOrders($conn," WHERE order_id = ?  ",array("order_id"),array($orderUid),"s");
$orderDetails = getOrders($conn," WHERE order_id = ? ORDER BY date_created DESC LIMIT 1 ",array("order_id"),array($orderUid),"s");
$orderID = $orderDetails[0]->getId();
$orderUID = $orderDetails[0]->getOrderId();

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'meta.php'; ?>
<!--         <meta property="og:url" content="https://tabigo.holiday/" />
        <link rel="canonical" href="https://tabigo.holiday/" />-->
        <meta property="og:title" content="Confirm Details| Tabigo" />
        <title>Confirm Details | Tabigo</title>   
	<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'header.php'; ?>
                   
<!-- <div class="width100 overflow menu-distance same-padding min-height padding-bottom50"> -->
<div class="width100 overflow menu-distance same-padding sakura-bg ow-sakura-height">
	<h1 class="title-h1 raleway red-text  wow fadeIn" data-wow-delay="0.1s">Confirm Your Details</h1>	

    <form method="post" action="billplzpost.php">

        <div class="width100 overflow text-center">

            <p class="input-top-p ow-input-top-p">Your Name</p>
            <p class="below-top-p"><?php echo $userDetails[0]->getUsername();?></p>
            <input class="text-center clean border0 fake-input-size" type="hidden" id="name" name="name" value="<?php echo $userDetails[0]->getUsername();?>" readonly>
        
            <div class="clear"></div>

            <div class="width100  text-center-important">
                <p class="input-top-p ow-input-top-p">Contact No.</p>
                <p class="below-top-p"><?php echo $orderDetails[0]->getContactNo();?></p>
                <input class="text-center clean border0 fake-input-size" type="hidden" id="mobile" name="mobile" value="<?php echo $orderDetails[0]->getContactNo();?>" readonly> 
            </div>

            <div class="clear"></div>

            <div class="width100  text-center-important">
                <p class="input-top-p ow-input-top-p">Subtotal</p>
                <p class="below-top-p">RM<?php echo $orderDetails[0]->getSubtotal();?></p>
                <input class="text-center clean border0 fake-input-size" type="hidden" value="RM <?php echo $orderDetails[0]->getSubtotal();?>" readonly> 
            </div>

            <div class="clear"></div>

            <?php  
                $subtotal = $orderDetails[0]->getSubtotal();
                $adjustTotal = ($subtotal * 100);
            ?>

            <!-- <input class="input-name clean" type="hidden" id="email" name="email" value="abc@gmail.com" readonly>  -->
            <input class="input-name clean" type="hidden" id="email" name="email"> 
            <input class="input-name clean" type="hidden" id="amount" name="amount" value="<?php echo $adjustTotal; ?>" readonly> 
            <input class="input-name clean" type="hidden" id="reference_1_label" name="reference_1_label" value="Order UID" readonly> 
            <input class="input-name clean" type="hidden" id="reference_1" name="reference_1" value="<?php echo $orderUID; ?>" readonly>
            <input class="input-name clean" type="hidden" id="reference_2_label" name="reference_2_label" value="Order ID" readonly> 
            <input class="input-name clean" type="hidden" id="reference_2" name="reference_2" value="<?php echo $orderID; ?>" readonly> 

            <div class="clear"></div> 

            <div class="width100 text-center margin-top50">
                <button class="red-btn center-button clean" name="submit">Proceed To Payment</button>
            </div> 

            <div class="clear"></div> 

        </div> 

    </form>

    <!-- </div> -->
</div>



<style>
	.animated.slideUp{
		animation:none !important;}
	.animated{
		animation:none !important;}
	.green-footer{
		display:none;}
</style>

<?php include 'js.php'; ?>

</body>
</html>