<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

// require_once dirname(__FILE__) . '/classes/TourPackage.php';
require_once dirname(__FILE__) . '/classes/Restaurant.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>
<!doctype html>
<html>

<head>
    <?php include 'meta.php'; ?>
    <!-- <meta property="og:url" content="https://tabigo.holiday/" />
    <link rel="canonical" href="https://tabigo.holiday/" /> -->
    <meta property="og:title" content="Edit Restaurant Package | Tabigo" />
    <title>Edit Restaurant Package | Tabigo</title>   
    <?php include 'css.php'; ?>
    <script src="//cdn.ckeditor.com/4.14.0/full/ckeditor.js"></script>
</head>

<body class="body">
<?php include 'headerAdmin.php'; ?>

<div class="clear"></div>

<div class="width100 overflow menu-distance same-padding ow-sakura-height">
	<h1 class="title-h1 raleway red-text ow-no-margin wow fadeIn" data-wow-delay="0.1s">Edit Restaurant Details</a></h1>

    <?php
    if(isset($_POST['package_uid']))
    {
    $conn = connDB();
    $tourDetails = getRestaurant($conn,"WHERE uid = ? ", array("uid") ,array($_POST['package_uid']),"s");
    ?>

        <!-- <form action="utilities/adminTourEditFunction.php" method="POST"> -->
        <form action="utilities/adminRestaurantEditFunction.php" method="POST">

        <input class="tele-input clean" type="hidden" value="<?php echo $tourDetails[0]->getUid(); ?>" name="package_uid" id="package_uid" readonly> 

            <div class="dual-input">
                <p class="input-top-p admin-top-p">Country*</p>
                <input class="tele-input clean" type="text" value="<?php echo $tourDetails[0]->getCountry(); ?>" placeholder="Country" name="country" id="country" required>  
            </div>

            <div class="dual-input second-dual-input">
                <p class="input-top-p admin-top-p">State*</p>
                <input class="tele-input clean" type="text" value="<?php echo $tourDetails[0]->getState(); ?>" placeholder="State" name="state" id="state" required>  
            </div>

            <div class="clean"></div>

            <div class="dual-input">
                <p class="input-top-p admin-top-p">Name*</p>
                <input class="tele-input clean" type="text" value="<?php echo $tourDetails[0]->getName(); ?>" placeholder="Name" name="name" id="name" required>        
            </div>

            <div class="clean"></div>

            <div class="width100 overflow">
                <p class="input-top-p admin-top-p">Photo Link in Google Drive <img src="img/drive-link.jpg" class="tutorial-img"></p>
                <input class="tele-input clean" type="text" value="<?php echo $tourDetails[0]->getPhoto(); ?>" placeholder="Photo" name="photo" id="photo" required>      
            </div>

            <div class="clean"></div>

            <div class="dual-input">
                <p class="input-top-p admin-top-p">Title*</p>
                <input class="tele-input clean" type="text" value="<?php echo $tourDetails[0]->getTitle(); ?>" placeholder="Title" name="title" id="title" required>     
            </div>

            <div class="dual-input second-dual-input">
                <p class="input-top-p admin-top-p">Price*</p>
                <input class="tele-input clean" type="text" value="<?php echo $tourDetails[0]->getPrice(); ?>" placeholder="Price" name="price" id="price" required>         
            </div>

            <div class="clean"></div>

            <div class="width100 overflow">
                <p class="input-top-p admin-top-p">Address*</p>
                <input class="tele-input clean" type="text" value="<?php echo $tourDetails[0]->getAddress(); ?>" placeholder="Address" name="address" id="address" required>      
            </div>

            <div class="clean"></div>

            <div class="width100 overflow">
                <p class="input-top-p admin-top-p">Lat Long*</p>
                <input class="tele-input clean" type="text" value="<?php echo $tourDetails[0]->getLatLong(); ?>" placeholder="Lat Long" name="latlong" id="latlong" required>      
            </div>

            <div class="clean"></div>

            <div class="width100 overflow">
                <p class="input-top-p admin-top-p">Description</p>
                <textarea class="tele-input clean lato input-textarea admin-input editor-input" type="text" placeholder="Description" name="description" id="description"><?php echo $tourDetails[0]->getDescription(); ?></textarea>  	
            </div>   

            <div class="clean"></div>

            <div class="width100 overflow">
                <p class="input-top-p admin-top-p">Opening Hours*</p>
                <input class="tele-input clean" type="text" value="<?php echo $tourDetails[0]->getOpeningHrs(); ?>" placeholder="Opening Hours" name="opening_hrs" id="opening_hrs" required>      
            </div>

            <div class="clean"></div>

            <div class="width100 overflow">
                <p class="input-top-p admin-top-p">Website*</p>
                <input class="tele-input clean" type="text" value="<?php echo $tourDetails[0]->getWebsite(); ?>" placeholder="Website" name="website" id="website" required>      
            </div>

            <div class="clean"></div>

            <div class="width100 overflow">
                <p class="input-top-p admin-top-p">Contact*</p>
                <input class="tele-input clean" type="text" value="<?php echo $tourDetails[0]->getContact(); ?>" placeholder="Contact" name="contact" id="contact" required>    
            </div>

            <div class="clear"></div>  

            <div class="width100 text-center">
                <button class="clean red-btn hover-effect middle-button-size below-forgot margin-bottom30" name="submit">Submit</button>
            </div>

        </form>

    <?php
    }
    ?>

</div>

<div class="clear"></div>

<?php include 'js.php'; ?>

<script>
    CKEDITOR.replace('editor');
</script>

</body>
</html>