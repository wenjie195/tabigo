<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Orders.php';
require_once dirname(__FILE__) . '/classes/OrderList.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

// $allOrders = getOrders($conn, "WHERE payment_status = 'COMPLETED' AND shipping_status = 'REJECTED' ");
$allOrders = getOrders($conn, "WHERE payment_status = 'COMPLETED' AND shipping_status = 'REJECTED' ORDER BY date_created DESC ");

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>
<!doctype html>
<html>
<head>
	<?php include 'meta.php'; ?>
<!--         <meta property="og:url" content="https://tabigo.holiday/" />
        <link rel="canonical" href="https://tabigo.holiday/" />-->
        <meta property="og:title" content="Order (Completed) | Tabigo" />
        <title>Order (Completed) | Tabigo</title>   
	<?php include 'css.php'; ?>


</head>

<body class="body">
<?php include 'headerAdmin.php'; ?>


<div class="clear"></div>
<div class="width100 overflow menu-distance same-padding sakura-bg ow-sakura-height">
    <h1 class="title-h1 raleway red-text ow-no-margin wow fadeIn" data-wow-delay="0.1s">
        <!-- Completed / <a href="adminOrderPending.php" class="pink-link hover-effect underline">Pending</a> -->
        <a href="adminOrderPending.php" class="pink-link hover-effect underline">Pending</a> / <a href="adminOrderCompleted.php" class="pink-link hover-effect underline">Completed</a> / Rejected
    </h1>		
    <div class="overflow-div">
            <table class="white-table">
                <thead>
                    <th>No.</th>
                    <th>Date</th>
                    <th>Order ID</th>
                    <th>Name</th>
                    <th>Amount</th>
                    <th>Payment Details</th>
                    <th>Status</th>
                    <th>Details</th>
                </thead>

                <?php
                if($allOrders)
                {
                    for($cnt = 0;$cnt < count($allOrders) ;$cnt++)
                    {
                    ?>    
                        <tr>
                            <td><?php echo ($cnt+1)?></td>
                            <td><?php echo date("Y-m-d",strtotime($allOrders[$cnt]->getDateUpdated()));?></td>

                            <td>
                                <?php $productUid = $allOrders[$cnt]->getOrderId();?>
                                <?php echo $newstring = substr($productUid, 18, 30);?>
                            </td>

                            <td><?php echo $allOrders[$cnt]->getName();?></td>
                            <td>RM<?php echo $allOrders[$cnt]->getSubtotal();?></td>
                            <td><?php echo $allOrders[$cnt]->getPaymentBankReference();?></td>
                            <td><?php echo $allOrders[$cnt]->getShippingStatus();?></td>

                            <td>
                                <!-- <form action="adminOrdersDetails.php" method="POST"> -->
                                <form action="adminOrdersDetailsRenew.php" method="POST">
                                    <button class="transparent-button clean red-link hover-effect" type="submit" name="order_id" value="<?php echo $allOrders[$cnt]->getOrderId();?>">
                                        Details
                                    </button>
                                </form>
                            </td>

                        </tr>
                    <?php
                    }
                }
                ?>  
            </table>
        </div>
</div>
<div class="clear"></div>
<?php include 'js.php'; ?>

</body>
</html>