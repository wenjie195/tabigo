<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/InterestingPlace.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>
<!doctype html>
<html>

<head>
    <?php include 'meta.php'; ?>
    <!-- <meta property="og:url" content="https://tabigo.holiday/" />
    <link rel="canonical" href="https://tabigo.holiday/" /> -->
    <meta property="og:title" content="Edit POI | Tabigo" />
    <title>Edit POI | Tabigo</title>   
    <?php include 'css.php'; ?>
    <script src="//cdn.ckeditor.com/4.14.0/full/ckeditor.js"></script>
</head>

<body class="body">
<?php include 'headerAdmin.php'; ?>

<div class="clear"></div>

<div class="width100 overflow menu-distance same-padding ow-sakura-height">
	<h1 class="title-h1 raleway red-text ow-no-margin wow fadeIn" data-wow-delay="0.1s">Edit POI Details</a></h1>

    <?php
    if(isset($_POST['item_uid']))
    {
    $conn = connDB();
    $itemDetails = getInterestingPlace($conn,"WHERE uid = ? ", array("uid") ,array($_POST['item_uid']),"s");
    ?>

        <form action="utilities/adminPlaceEditFunction.php" method="POST">

        <input class="tele-input clean" type="hidden" value="<?php echo $itemDetails[0]->getUid(); ?>" name="item_uid" id="item_uid" readonly> 

            <div class="dual-input">
                <p class="input-top-p admin-top-p">Country*</p>
                <input class="tele-input clean" type="text" value="<?php echo $itemDetails[0]->getCountry(); ?>" placeholder="Country" name="country" id="country" required>  
            </div>

            <div class="dual-input second-dual-input">
                <p class="input-top-p admin-top-p">State*</p>
                <input class="tele-input clean" type="text" value="<?php echo $itemDetails[0]->getState(); ?>" placeholder="State" name="state" id="state" required>        
            </div>

            <div class="clean"></div>

            <div class="dual-input">
                <p class="input-top-p admin-top-p">Title*</p>
                <input class="tele-input clean" type="text" value="<?php echo $itemDetails[0]->getTitle(); ?>" placeholder="Title" name="title" id="title" required>  
            </div>

            <div class="clean"></div>

            <div class="form-group publish-border input-div width100 overflow">
                <p class="input-top-p admin-top-p">Description One</p>
                <textarea name="editor_one" id="editor_one" rows="10" cols="80"  class="tele-input clean lato input-textarea admin-input editor-input" ><?php echo $itemDetails[0]->getDescriptionOne(); ?></textarea>
            </div>   

            <div class="clean"></div>

            <div class="form-group publish-border input-div width100 overflow">
                <p class="input-top-p admin-top-p">Description Two</p>
                <textarea name="editor_two" id="editor_two" rows="10" cols="80"  class="tele-input clean lato input-textarea admin-input editor-input" ><?php echo $itemDetails[0]->getDescriptionTwo(); ?></textarea>
            </div>   

            <div class="clean"></div>

            <div class="form-group publish-border input-div width100 overflow">
                <p class="input-top-p admin-top-p">Description Three</p>
                <textarea name="editor_three" id="editor_three" rows="10" cols="80"  class="tele-input clean lato input-textarea admin-input editor-input" ><?php echo $itemDetails[0]->getDescriptionThree(); ?></textarea>
            </div>   

            <div class="clean"></div>

            <div class="width100 overflow">
                <p class="input-top-p admin-top-p">Photo One Link in Google Drive <img src="img/drive-link.jpg" class="tutorial-img"></p>
                <input class="tele-input clean" type="text" value="<?php echo $itemDetails[0]->getPhotoOne(); ?>" placeholder="Photo" name="photo_one" id="photo_one">      
            </div>

            <div class="clean"></div>

            <div class="width100 overflow">
                <p class="input-top-p admin-top-p">Photo Two Link in Google Drive <img src="img/drive-link.jpg" class="tutorial-img"></p>
                <input class="tele-input clean" type="text" value="<?php echo $itemDetails[0]->getPhotoTwo(); ?>" placeholder="Photo" name="photo_two" id="photo_two">      
            </div>

            <div class="clean"></div>

            <div class="width100 overflow">
                <p class="input-top-p admin-top-p">Address</p>
                <textarea class="tele-input clean lato input-textarea admin-input editor-input" type="text" placeholder="Address" name="address" id="address"><?php echo $itemDetails[0]->getAddress(); ?></textarea>  	
            </div> 

            <div class="clean"></div>

            <div class="width100 overflow">
                <p class="input-top-p admin-top-p">Lat Long*</p>
                <input class="tele-input clean" type="text" value="<?php echo $itemDetails[0]->getLatLong(); ?>" placeholder="Lat Long" name="lat_long" id="lat_long" required>  
            </div>

            <div class="clean"></div>

            <div class="width100 overflow">
                <p class="input-top-p admin-top-p">Opening Hours*</p>
                <input class="tele-input clean" type="text" value="<?php echo $itemDetails[0]->getOpeningHrs(); ?>" placeholder="Opening Hours" name="opening_hours" id="opening_hours" required>        
            </div>

            <div class="clean"></div>

            <div class="width100 overflow">
                <p class="input-top-p admin-top-p">Website*</p>
                <input class="tele-input clean" type="text" value="<?php echo $itemDetails[0]->getWebsite(); ?>" placeholder="Website" name="website" id="website" required>        
            </div>

            <div class="clear"></div>  

            <div class="width100 text-center">
                <button class="clean red-btn hover-effect middle-button-size below-forgot margin-bottom30" name="submit">Submit</button>
            </div>

        </form>

    <?php
    }
    ?>

</div>

<div class="clear"></div>

<?php include 'js.php'; ?>

<script>
    CKEDITOR.replace('editor');
</script>

</body>
</html>