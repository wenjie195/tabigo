<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Slider.php';
// require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

$allSlider = getSlider($conn, "WHERE status = 'Show' ");

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>
<!doctype html>
<html>
<head>
	<?php include 'meta.php'; ?>
<!--         <meta property="og:url" content="https://tabigo.holiday/" />
        <link rel="canonical" href="https://tabigo.holiday/" />-->
        <meta property="og:title" content="Ads Banner | Tabigo" />
        <title>Ads Banner | Tabigo</title>   
	<?php include 'css.php'; ?>


</head>

<body class="body">
<?php include 'headerAdmin.php'; ?>


<div class="clear"></div>
<div class="width100 overflow menu-distance same-padding sakura-bg ow-sakura-height">
	<h1 class="title-h1 raleway red-text ow-no-margin wow fadeIn" data-wow-delay="0.1s">Ads Banner <a href="adminAdsAdd.php" class="pink-link hover-effect underline">(Add)</a></h1>	
    <div class="overflow-div">
            <table class="white-table">
                <thead>
                    <th>No.</th>
                    <th>Country</th>
                    <th>Link</th>
                    <th>Update</th>
                    <th>Delete</th>
                </thead>

                <?php
                if($allSlider)
                {
                    for($cnt = 0;$cnt < count($allSlider) ;$cnt++)
                    {
                    ?>    
                        <tr>
                            <td><?php echo ($cnt+1)?></td>
                            <td><?php echo $allSlider[$cnt]->getCountry();?></td>
                            <td><?php echo $allSlider[$cnt]->getLink();?></td>



                            <td>
                                <form action="adminAdsEdit.php" method="POST">
                                    <button class="transparent-button clean green-link hover-effect" type="submit" name="item_uid" value="<?php echo $allSlider[$cnt]->getUid();?>">
                                        Update
                                    </button>
                                </form>
                            </td>

                            <td>
                                <form action="utilities/adminSliderDeleteFunction.php" method="POST" class="claim-css decision">
                                    <button class="transparent-button clean red-link hover-effect" type="submit" name="item_uid" value="<?php echo $allSlider[$cnt]->getUid();?>">
                                        Delete
                                    </button>
                                </form>
                            </td>
                            
                        </tr>
                    <?php
                    }
                }
                ?>  

            </table>
        </div>
</div>

<div class="clear"></div>

<?php include 'js.php'; ?>

<?php
if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Advertisement Updated !"; 
        }
        elseif($_GET['type'] == 2)
        {
            $messageType = "Fail To Update Advertisement !"; 
        }
        elseif($_GET['type'] == 3)
        {
            $messageType = "ERROR !";
        }
        elseif($_GET['type'] == 4)
        {
            $messageType = "Advertisement Added !"; 
        }
        elseif($_GET['type'] == 5)
        {
            $messageType = "Fail To Add Advertisement !"; 
        }
        elseif($_GET['type'] == 6)
        {
            $messageType = "Advertisement Deleted !"; 
        }
        elseif($_GET['type'] == 7)
        {
            $messageType = "Fail To Delete Advertisement !"; 
        }
        echo '
        <script>
            putNoticeJavascript("Notice !! ","'.$messageType.'");  
        </script>
        ';   
        $_SESSION['messageType'] = 0;
    }
}
?>

</body>
</html>