<?php
if (session_id() == "")
{
    session_start();
}
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';
// require_once dirname(__FILE__) . '/sessionLoginChecker.php';

require_once dirname(__FILE__) . '/classes/Countries.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

$allCountry = getCountries($conn);

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>
<!doctype html>
<html>
<head>
	<?php include 'meta.php'; ?>
<!--         <meta property="og:url" content="https://tabigo.holiday/" />
        <link rel="canonical" href="https://tabigo.holiday/" />-->
        <meta property="og:title" content="Traveler's Choice | Tabigo" />
        <title>Traveler's Choice | Tabigo</title>   
	<?php include 'css.php'; ?>


</head>

<body class="body">

<!-- <?php //include 'header.php'; ?> -->
<?php include 'headerBeforeLogin.php'; ?>

<div class="clear"></div>

<div class="width100 overflow menu-distance same-padding ow-sakura-height sakura-bg2 index-padding">
	<h1 class="title-h1 raleway red-text ow-no-margin wow fadeIn" data-wow-delay="0.1s">Welcome to Tabigo</h1>	
    <div class="text-center"><p class="mid-content-p wow fadeIn" data-wow-delay="0.2s">Kindly choose a country.</p></div>
    <div class="clear"></div>

    <div class="big-country-container">
        <?php
        if($allCountry)
        {
            for($cnt = 0;$cnt < count($allCountry) ;$cnt++)
            {
            ?>    
                
                <a href='countryDetails.php?id=<?php echo $allCountry[$cnt]->getId();?>'>
                    <button class="country-button2 hover-effect clean" id="country<?php echo $allCountry[$cnt]->getId();?>">
                        <div class="country2-div"  >
                            <p><?php echo $allCountry[$cnt]->getEnName();?></p>
                        </div>
                    </button>
                </a>
            <?php
            }
        }
        ?>  
    </div>		
		
	
</div>

<?php include 'js.php'; ?>
       <script type="text/javascript">
            // Plugin code
            (function ($) {
                /** Polyfills and prerequisites **/

                // requestAnimationFrame Polyfill
                var lastTime    = 0;
                var vendors     = ['webkit', 'o', 'ms', 'moz', ''];
                var vendorCount = vendors.length;

                for (var x = 0; x < vendorCount && ! window.requestAnimationFrame; ++x) {
                    window.requestAnimationFrame = window[vendors[x] + 'RequestAnimationFrame'];
                    window.cancelAnimationFrame  = window[vendors[x] + 'CancelAnimationFrame'] || window[vendors[x] + 'CancelRequestAnimationFrame'];
                }

                if ( ! window.requestAnimationFrame) {
                    window.requestAnimationFrame = function(callback) {
                        var currTime   = new Date().getTime();
                        var timeToCall = Math.max(0, 16 - (currTime - lastTime));

                        var id   = window.setTimeout(function() { callback(currTime + timeToCall); }, timeToCall);
                        lastTime = currTime + timeToCall;

                        return id;
                    };
                }

                if ( ! window.cancelAnimationFrame) {
                    window.cancelAnimationFrame = function(id) {
                        clearTimeout(id);
                    };
                }

                // Prefixed event check
                $.fn.prefixedEvent = function(type, callback) {
                    for (var x = 0; x < vendorCount; ++x) {
                        if ( ! vendors[x]) {
                            type = type.toLowerCase();
                        }

                        el = (this instanceof jQuery ? this[0] : this);
                        el.addEventListener(vendors[x] + type, callback, false);
                    }

                    return this;
                };

                // Test if element is in viewport
                function elementInViewport(el) {

                    if (el instanceof jQuery) {
                        el = el[0];
                    }

                    var rect = el.getBoundingClientRect();

                    return (
                        rect.top >= 0 &&
                            rect.left >= 0 &&
                            rect.bottom <= (window.innerHeight || document.documentElement.clientHeight) &&
                            rect.right <= (window.innerWidth || document.documentElement.clientWidth)
                        );
                }

                // Random array element
                function randomArrayElem(arr) {
                    return arr[Math.floor(Math.random() * arr.length)];
                }

                // Random integer
                function randomInt(min, max) {
                    return Math.floor(Math.random() * (max - min + 1)) + min;
                }

                /** Actual plugin code **/
                $.fn.sakura = function (event, options) {

                    // Target element
                    var target = this.selector == "" ? $('body') : this;

                    // Defaults for the option object, which gets extended below
                    var defaults = {
                        blowAnimations: ['blow-soft-left', 'blow-medium-left', 'blow-soft-right', 'blow-medium-right'],
                        className: 'sakura',
                        fallSpeed: 1,
                        maxSize: 14,
                        minSize: 10,
                        newOn: 300,
                        swayAnimations: ['sway-0', 'sway-1', 'sway-2', 'sway-3', 'sway-4', 'sway-5', 'sway-6', 'sway-7', 'sway-8']
                    };

                    var options = $.extend({}, defaults, options);

                    // Default or start event
                    if (typeof event === 'undefined' || event === 'start') {

                        // Set the overflow-x CSS property on the target element to prevent horizontal scrollbars
                        target.css({ 'overflow-x': 'hidden' });

                        // Function that inserts new petals into the document
                        var petalCreator = function () {
                            if (target.data('sakura-anim-id')) {
                                setTimeout(function () {
                                    requestAnimationFrame(petalCreator);
                                }, options.newOn);
                            }

                            // Get one random animation of each type and randomize fall time of the petals
                            var blowAnimation = randomArrayElem(options.blowAnimations);
                            var swayAnimation = randomArrayElem(options.swayAnimations);
                            var fallTime = ((document.documentElement.clientHeight * 0.007) + Math.round(Math.random() * 5)) * options.fallSpeed;

                            // Build animation
                            var animations =
                                'fall ' + fallTime + 's linear 0s 1' + ', ' +
                                    blowAnimation + ' ' + (((fallTime > 30 ? fallTime : 30) - 20) + randomInt(0, 20)) + 's linear 0s infinite' + ', ' +
                                    swayAnimation + ' ' + randomInt(2, 4) + 's linear 0s infinite';

                            // Create petal and randomize size
                            var petal  = $('<div class="' + options.className + '" />');
                            var height = randomInt(options.minSize, options.maxSize);
                            var width  = height - Math.floor(randomInt(0, options.minSize) / 3);

                            // Apply Event Listener to remove petals that reach the bottom of the page
                            petal.prefixedEvent('AnimationEnd', function () {
                                if ( ! elementInViewport(this)) {
                                    $(this).remove();
                                }
                            })
                            // Apply Event Listener to remove petals that finish their horizontal float animation
                            .prefixedEvent('AnimationIteration', function (ev) {
                                if (
                                    (
                                        $.inArray(ev.animationName, options.blowAnimations) != -1 ||
                                        $.inArray(ev.animationName, options.swayAnimations) != -1
                                    ) &&
                                    ! elementInViewport(this)
                                ) {
                                    $(this).remove();
                                }
                            })
                            .css({
                                '-webkit-animation': animations,
                                animation: animations,
                                'border-radius': randomInt(options.maxSize, (options.maxSize + Math.floor(Math.random() * 10))) + 'px ' + randomInt(1, Math.floor(width / 4)) + 'px',
                                height: height + 'px',
                                left: (Math.random() * document.documentElement.clientWidth - 100) + 'px',
                                'margin-top': (-(Math.floor(Math.random() * 20) + 15)) + 'px',
                                width: width + 'px'
                            });

                            target.append(petal);
                        };

                        // Finally: Start adding petals
                        target.data('sakura-anim-id', requestAnimationFrame(petalCreator));

                    }
                    // Stop event, which stops the animation loop and removes all current blossoms
                    else if (event === 'stop') {

                        // Cancel animation
                        var animId = target.data('sakura-anim-id');

                        if (animId) {
                            cancelAnimationFrame(animId);
                            target.data('sakura-anim-id', null);
                        }

                        // Remove all current blossoms
                        setTimeout(function() {
                            $('.' + options.className).remove();
                        }, (options.newOn + 50));

                    }
                };
            }(jQuery));

            $(document).ready(function() {
                $('body').sakura();
            });
        </script>
<?php unset($_SESSION['country_id']);unset($_SESSION['country_name']); ?>

<style>
.before-login{
	display:none;}
</style>
<?php
if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Register Successfully !"; 
        }
        elseif($_GET['type'] == 2)
        {
            $messageType = "Please Register"; 
        }
        elseif($_GET['type'] == 3)
        {
            $messageType = "Incorrect Password";
        }
        echo '
        <script>
            putNoticeJavascript("Notice !! ","'.$messageType.'");  
        </script>
        ';   
        $_SESSION['messageType'] = 0;
    }
    elseif($_SESSION['messageType'] == 2)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Fail to register!"; 
        }
        elseif($_GET['type'] == 2)
        {
            $messageType = "Phone number has been registered by others !"; 
        }
        elseif($_GET['type'] == 3)
        {
            $messageType = "Email has been registered by others !"; 
        }
        elseif($_GET['type'] == 4)
        {
            $messageType = "Username has been registered by others !"; 
        }
        elseif($_GET['type'] == 5)
        {
            $messageType = "Password length must more than 6 !"; 
        }
        elseif($_GET['type'] == 6)
        {
            $messageType = "Password is different with Retype Passowrd !"; 
        }
        elseif($_GET['type'] == 7)
        {
            $messageType = "IC Number has been registered by others !"; 
        }
        echo '
        <script>
            putNoticeJavascript("Notice !! ","'.$messageType.'");  
        </script>
        ';   
        $_SESSION['messageType'] = 0;
    }
    elseif($_SESSION['messageType'] == 3)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Unknown Usertype !"; 
        }
        elseif($_GET['type'] == 2)
        {
            $messageType = "Incorrect Password";
        }
        elseif($_GET['type'] == 3)
        {
            $messageType = "Cannot found this username <br> Please Register"; 
        }
        echo '
        <script>
            putNoticeJavascript("Notice !! ","'.$messageType.'");  
        </script>
        ';   
        $_SESSION['messageType'] = 0;
    }
}
?>
</body>
</html>