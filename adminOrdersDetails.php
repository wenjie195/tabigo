<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Orders.php';
require_once dirname(__FILE__) . '/classes/OrderList.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>
<!doctype html>
<html>
<head>
	<?php include 'meta.php'; ?>
<!--         <meta property="og:url" content="https://tabigo.holiday/" />
        <link rel="canonical" href="https://tabigo.holiday/" />-->
        <meta property="og:title" content="Order Details | Tabigo" />
        <title>Order Details | Tabigo</title>   
	<?php include 'css.php'; ?>


</head>

<body class="body">
<?php include 'headerAdmin.php'; ?>

<div class="clear"></div>

<!-- <div class="width100 overflow menu-distance same-padding background-div"> -->
<div class="width100 overflow menu-distance same-padding sakura-bg ow-sakura-height">
    
<h1 class="title-h1 raleway red-text ow-no-margin wow fadeIn" data-wow-delay="0.1s">Order Details</h1>	

    <?php
    if(isset($_POST['order_id']))
    {
    $conn = connDB();
    $productDetails = getOrderList($conn,"WHERE order_id = ? ", array("order_id") ,array($_POST['order_id']),"s");
    ?>

        <div class="overflow-div">
            <table class="white-table">
                 <thead>
                    <th>No.</th>
                    <th>Order ID</th>
                    <th>Package Name</th>
                    <th>Booking Date</th>
                    <th>Unit Price</th>
                    <th>Quantity</th>
                    <th>Total Price</th>
                    <th>Status</th>
                </thead>

                <?php
                if($productDetails)
                {
                    for($cnt = 0;$cnt < count($productDetails) ;$cnt++)
                    {
                    ?>    
                        <tr>
                            <td><?php echo ($cnt+1)?></td>

                            <td>
                                <?php $productUid = $productDetails[$cnt]->getProductUid();?>
                                <?php echo $newstring = substr($productUid, 0, 10);?>
                            </td>

                            <td><?php echo $productDetails[$cnt]->getProductName();?></td>
                            <td><?php echo $productDetails[$cnt]->getServiceDate();?></td>
                            <td>RM<?php echo $productDetails[$cnt]->getOriginalPrice();?></td>
                            <td><?php echo $productDetails[$cnt]->getQuantity();?></td>
                            <td>RM<?php echo $productDetails[$cnt]->getTotalPrice();?></td>

                            <td>
                                <?php 
                                    $status = $productDetails[$cnt]->getStatus();
                                    if($status == 'Sold')
                                    {
                                    ?>

                                        <form action="utilities/adminOrderClaimFunction.php" method="POST">
                                            <button class="transparent-button clean red-link hover-effect" type="submit" name="product_uid" value="<?php echo $productDetails[$cnt]->getId();?>">
                                            <!-- <button class="transparent-button clean red-link hover-effect" type="submit" name="product_uid" value="<?php //echo $productDetails[$cnt]->getProductId();?>"> -->
                                                Claimed
                                            </button>
                                        </form>

                                    <?php
                                    }
                                    elseif($status == 'Claimed')
                                    {
                                        echo "Package Claimed";
                                    }
                                    else
                                    {
                                        echo $status ;
                                    }
                                ?>
                            </td>

                        </tr>
                    <?php
                    }
                }
                ?>  

            </table>
        </div>

    <?php
    }
    ?>
	
</div>

<?php include 'js.php'; ?>
<style>
*{
   
	vertical-align:middle;
	word-break:keep-all !important;
}
a{
	opacity:0.8;
	transition:.15s ease-in-out;
	cursor:pointer;}
a:hover{
	opacity:1;}
.langz{
	display:none;}
</style>

</body>
</html>