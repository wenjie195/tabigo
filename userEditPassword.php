<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

$userDetails = getUser($conn, "WHERE uid =?",array("uid"),array($uid),"s");
// $userDetails = $userData[0]->getFullname();

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html>
<head>
    <?php include 'meta.php'; ?>
    <meta property="og:url" content="https://tabigo.holiday/" />
    <link rel="canonical" href="https://tabigo.holiday/" />
    <meta property="og:title" content="Edit Password | Tabigo" />
    <title>Edit Password | Tabigo</title>   
    <?php include 'css.php'; ?>
</head>

<body class="body">

<?php include 'header.php'; ?>

<div class="width100 overflow menu-distance same-padding min-height padding-bottom50">

    <h1 class="h1-title">Edit Password</h1> 

    <form action="utilities/userEditPasswordFunction.php" method="POST">

        <div class="login-input-div">
            <p class="input-top-text">Current Password</p>
            <input class="clean tele-input" type="text" placeholder="Current Password" id="current_password" name="current_password" required>
        </div>  

        <div class="login-input-div">
            <p class="input-top-text">New Password</p>
            <input class="clean tele-input" type="text" placeholder="New Password" id="new_password" name="new_password" required>
        </div>  

        <div class="login-input-div">
            <p class="input-top-text">Retype New Password</p>
            <input class="clean tele-input" type="text" placeholder="Retype New Password" id="retype_new_password" name="retype_new_password" required>
        </div>  

        <div class="clear"></div>

        <button class="clean red-btn red-btn margin-top30 fix300-btn margin-bottom30" name="submit">Submit</button>

        <div class="clear"></div>
    
    </form>

</div>

<style>
.editprofile-li{
	color:#264a9c;
	background-color:white;}
.editprofile-li .hover1a{
	display:none;}
.editprofile-li .hover1b{
	display:block;}
</style>

<?php include 'js.php'; ?>

<?php
if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Password Updated"; 
        }
        elseif($_GET['type'] == 2)
        {
            $messageType = "Fail To Update Password"; 
        }
        elseif($_GET['type'] == 3)
        {
            $messageType = "password must be same with reenter !!";
        }
        elseif($_GET['type'] == 4)
        {
            $messageType = "password length must be more than 5 !!";
        }
        elseif($_GET['type'] == 5)
        {
            $messageType = "current password is not the same as previous !!";
        }
        echo '
        <script>
            putNoticeJavascript("Notice !! ","'.$messageType.'");  
        </script>
        ';   
        $_SESSION['messageType'] = 0;
    }
}
?>

</body>
</html>