<?php
if (session_id() == "")
{
    session_start();
}
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';
// require_once dirname(__FILE__) . '/sessionLoginChecker.php';

require_once dirname(__FILE__) . '/classes/Countries.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

// if($_SERVER['REQUEST_METHOD'] == 'POST')
// {
//     $conn = connDB();

//     $country = rewrite($_POST['country']);
// }

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>
<!doctype html>
<html>
<head>
	<?php include 'meta.php'; ?>
<!--         <meta property="og:url" content="https://tabigo.holiday/" />
        <link rel="canonical" href="https://tabigo.holiday/" />-->
        <meta property="og:title" content="Traveler's Choice | Tabigo" />
        <title>Traveler's Choice | Tabigo</title>   
	<?php include 'css.php'; ?>


</head>

<body class="body">

 <?php include 'header.php'; ?>


<div class="clear"></div>

<div class="width100 overflow menu-distance same-padding sakura-bg ow-sakura-height">
	<h1 class="title-h1 raleway red-text ow-no-margin wow fadeIn" data-wow-delay="0.1s">Welcome to Tabigo</h1>	
    <div class="text-center"><p class="mid-content-p wow fadeIn" data-wow-delay="0.2s">Kindly choose a state.</p></div>
    <div class="clear"></div>

    <div class="ta-big-three">

        <?php
        if(isset($_GET['id']))
        {
        $conn = connDB();
        $allState = getStates($conn,"WHERE country = ? ",array("country"),array($_GET['id']), "s");
        ?>

            <?php
            if($allState)
            {
                for($cnt = 0;$cnt < count($allState) ;$cnt++)
                {
                ?>    
                    <a href='#'>
                    <!-- <a href='countryDetails.php?id=<?php echo $allCountry[$cnt]->getId();?>'> -->
                        <button class="country-button hover-effect clean">
                            <div class="ta-three-div">
                                <p><?php echo $allState[$cnt]->getState();?></p>
                            </div>
                        </button>
                    </a>
                <?php
                }
            }
            ?> 

        <?php
        }
        ?>

    </div>		
		
	
</div>

<?php include 'js.php'; ?>

<style>
.before-login{
	display:none;}
</style>

</body>
</html>