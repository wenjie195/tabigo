<?php
if (session_id() == "")
{
    session_start();
}
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Countries.php';
require_once dirname(__FILE__) . '/classes/InterestingPlace.php';
require_once dirname(__FILE__) . '/classes/Restaurant.php';
require_once dirname(__FILE__) . '/classes/Ticket.php';
require_once dirname(__FILE__) . '/classes/TourPackage.php';
require_once dirname(__FILE__) . '/classes/Transport.php';
require_once dirname(__FILE__) . '/classes/Wifi.php';
require_once dirname(__FILE__) . '/classes/Session.php';
require_once dirname(__FILE__) . '/classes/Slider.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

// $conn = connDB();

function addCountrySession($conn,$ip_address,$countryName)
{
    if(insertDynamicData($conn,"session",array("ip","country"),
    array($ip_address,$countryName),"ss") === null)
    {
        echo "gg";
    }
    else{    }
    return true;
}

//whether ip is from share internet
if (!empty($_SERVER['HTTP_CLIENT_IP']))   
{
    $ip_address = $_SERVER['HTTP_CLIENT_IP'];
}
//whether ip is from proxy
elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))  
{
    $ip_address = $_SERVER['HTTP_X_FORWARDED_FOR'];
}
//whether ip is from remote address
else
{
    $ip_address = $_SERVER['REMOTE_ADDR'];
}
$ip_address;

$_SESSION['url'] = $_SERVER['REQUEST_URI'];

if(isset($_GET['id']))
{
    $conn = connDB();
    $countryDetails = getCountries($conn,"WHERE id = ? ",array("id"),array($_GET['id']), "s");
    $countryName = $countryDetails[0]->getEnName();

    // $adsDetails = getSlider($conn, "WHERE status = 'Show' ");
    $adsDetails = getSlider($conn,"WHERE country = ? AND status = 'Show' ",array("country"),array($countryName), "s");
    // $adsDetails = getSlider($conn,"WHERE country = ? AND status = 'Show' LIMIT 1 ORDER BY date_created DESC ",array("country"),array($countryName), "s");

    if(addCountrySession($conn,$ip_address,$countryName))
    {}
}

// $conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>
<!doctype html>
<html>

<?php
if($countryDetails)
{
    for($cnt = 0;$cnt < count($countryDetails) ;$cnt++)
    {
    ?>
    <head>
    <?php include 'meta.php'; ?>
    <!-- <meta property="og:url" content="https://tabigo.holiday/" />
    <link rel="canonical" href="https://tabigo.holiday/" /> -->
    <meta property="og:title" content="<?php echo $countryDetails[$cnt]->getEnName();?> | Tabigo" />
    <title><?php echo $countryDetails[$cnt]->getEnName();?> | Tabigo</title>   
    <?php include 'css.php'; ?>
    </head>
    <?php
    }
    ?>
<?php
}
?>

<body class="body">
<?php include 'header.php'; ?>

<div class="clear"></div>

<div class="width100 overflow menu-distance same-padding small2-padding min-height padding-bottom50">

    <div class="category-container">

        <?php
        if($countryDetails)
        {
            for($cnt = 0;$cnt < count($countryDetails) ;$cnt++)
            {
            ?>    

                <form action="selectState.php" method="POST" class="hover-effect top-4 first2">
                    <button class="clean border0 transparent width100 category-font-size hover-effect padding0 tour-color button-css1">
                        <img src="img/white-tour.png" class="category-png new-css-png"> <p class="p11">Tour Package</p>
                        <input class="tele-input clean" type="hidden" value="<?php echo $countryDetails[$cnt]->getEnName();?>" name="country" id="country" readonly> 
                        <input class="tele-input clean" type="hidden" value="Tour" name="option" id="option" readonly> 
                    </button>
                </form>

                <form action="selectState.php" method="POST" class="hover-effect top-4 first2">
                    <button class="clean border0 transparent width100 category-font-size hover-effect padding0 trans-color button-css1">
                        <img src="img/white-vehicle.png" class="category-png new-css-png"> <p class="p11">Transportation</p>
                        <input class="tele-input clean" type="hidden" value="<?php echo $countryDetails[$cnt]->getEnName();?>" name="country" id="country" readonly> 
                        <input class="tele-input clean" type="hidden" value="Transport" name="option" id="option" readonly> 
                    </button>
                </form>

                <a href="https://c121.travelpayouts.com/click?shmarker=297571&promo_id=3770&source_type=link&type=click&trs=137433" target="_blank" >   
                    <div class="hover-effect top-4 flight-color button-css1">
                        <img src="img/white-flight.png" class="category-png new-css-png"> <p class="p11 category-font-size">Hotel & Flight</p>
                    </div> 
                </a> 

                <form action="selectState.php" method="POST" class="hover-effect top-4">
                    <button class="clean border0 transparent width100 category-font-size hover-effect padding0 place-color button-css1">
                        <img src="img/white-place.png" class="category-png new-css-png"> <p class="p11">Place of Interest</p>
                        <input class="tele-input clean" type="hidden" value="<?php echo $countryDetails[$cnt]->getEnName();?>" name="country" id="country" readonly> 
                        <input class="tele-input clean" type="hidden" value="POI" name="option" id="option" readonly> 
                    </button>
                </form>

            <?php
            }
        }
        ?>  

    </div>

    <?php
    if(!$adsDetails)
    {}
    else
    {
    ?>
        <div class="width100 overflow ads-margin">
            <?php $imgName = $adsDetails[0]->getImgName();?>
            <?php $imgLink = $adsDetails[0]->getLink();?>
            <a href="<?php echo $imgLink;?>"><img src="uploadsSlider/<?php echo $imgName;?>" class="width100 hover-effect opacity-hover"></a>
        </div> 
    <?php
    }
    ?>



    <div class="clear"></div>

    <!-- <div class="width100 overflow ads-margin">
    	<a href="https://qlianmeng.asia/tabigo_renew/userDetailsTour.php?id=d5c34474e4a1a9fce87927a0e9b05d8e"><img src="img/sale.jpg" class="width100 hover-effect opacity-hover"></a>
    </div> -->

    <div class="clear"></div>
    
    <div class="second-line red-text">
        <img src="img/package.png" class="gift-size"> Trending
    </div>

    <div class="width100 glider-contain">
        <div class="glider glider1">

        <?php
        $conn = connDB();
        if($countryName)
        {
        $allTour = getTourPackage($conn,"WHERE country = ? AND display = 'Yes' ORDER BY date_created DESC LIMIT 8",array("country"),array($countryName), "s");
        ?>
            <?php
            if($allTour)
            {
                for($cnt = 0;$cnt < count($allTour) ;$cnt++)
                {
                ?> 
                    <a href='userDetailsTour.php?id=<?php echo $allTour[$cnt]->getUid();?>'>
                        <div class="shadow-white-box product-box opacity-hover">
                            <div class=" square miss2">
                                <div class="width100 white-bg image-box-size content2">
                                    <?php 
                                        $linkTour = $allTour[$cnt]->getPhotoOne();
                                        $stringTourA = $linkTour;
                                        $stringTourB = str_replace('https://drive.google.com/file/d/', '', $stringTourA);
                                        $googleLinkTour = str_replace('/view?usp=sharing', '', $stringTourB);
                                    ?>
                                    <img src='https://drive.google.com/uc?export=view&id=<?php echo $googleLinkTour;?>' >
                                </div>
                            </div>
                            <div class="miss3 miss4" id="a<?php echo $allTour[$cnt]->getUid();?>"></div>
                             <style>
                             	#a<?php echo $allTour[$cnt]->getUid();?>{
									background-image:url(https://drive.google.com/uc?export=view&id=<?php echo $googleLinkTour;?>);}
                             </style> 
                            <div class="width100 product-details-div">
                                <p class="width100 text-overflow slider-product-name"><?php echo $allTour[$cnt]->getTitle();?></p>
                                <p class="slider-product-price text-overflow">
                                RM<?php echo $allTour[$cnt]->getPrice();?>
                                </p>
                            </div>
                        </div>
                    </a>
                <?php
                }
            }
            ?>  
        <?php
        }
        ?>

        </div>
    </div>

    <div class="clear"></div>
    <div class="width100 little-margin-top image-button-css1">
        
   

        <?php
        if($countryDetails)
        {
            for($cnt = 0;$cnt < count($countryDetails) ;$cnt++)
            {
            ?>    

                <form action="selectState.php" method="POST" class="image-button-css2">
                    <button class="clean border0 transparent hover-effect padding0">
                        <img src="img/restaurant-button0.png" class="category-png3 opacity-hover button1">
                         <img src="img/restaurant-button1.png" class="category-png3 opacity-hover button2">
                         <img src="img/restaurant-button2.png" class="category-png3 opacity-hover button3">
                         <img src="img/restaurant-button.png" class="category-png3 opacity-hover button4">
                        <input class="tele-input clean" type="hidden" value="<?php echo $countryDetails[$cnt]->getEnName();?>" name="country" id="country" readonly> 
                        <input class="tele-input clean" type="hidden" value="Restaurant" name="option" id="option" readonly> 
                    </button>
                </form>

                <form action="selectState.php" method="POST" class="image-button-css2">
                    <button class="clean border0 transparent hover-effect padding0">
                        <img src="img/ticket-button0.png" class="category-png3 opacity-hover button1">
                        <img src="img/ticket-button1.png" class="category-png3 opacity-hover button2">
                        <img src="img/ticket-button2.png" class="category-png3 opacity-hover button3">
                        <img src="img/ticket-button.png" class="category-png3 opacity-hover button4">
                        <input class="tele-input clean" type="hidden" value="<?php echo $countryDetails[$cnt]->getEnName();?>" name="country" id="country" readonly> 
                        <input class="tele-input clean" type="hidden" value="Ticket" name="option" id="option" readonly> 
                    </button>
                </form>

                <form action="selectState.php" method="POST" class="image-button-css2">
                    <button class="clean border0 transparent hover-effect padding0">
                        <img src="img/wifi-button0.png" class="category-png3 opacity-hover button1">
                        <img src="img/wifi-button1.png" class="category-png3 opacity-hover button2">
                        <img src="img/wifi-button2.png" class="category-png3 opacity-hover button3">
                        <img src="img/wifi-button.png" class="category-png3 opacity-hover button4">
                        <input class="tele-input clean" type="hidden" value="<?php echo $countryDetails[$cnt]->getEnName();?>" name="country" id="country" readonly> 
                        <input class="tele-input clean" type="hidden" value="Wifi" name="option" id="option" readonly> 
                    </button>
                </form>

                <a href="https://www.tokionow.com/easc/online/Travel-Step-1-Single.jsp?aid=BORYITLING" target="_blank"  class="image-button-css2 hover-effect padding0">
                   
                        <img src="img/insurance-button0.png" class="category-png3 opacity-hover button1">
                        <img src="img/insurance-button1.png" class="category-png3 opacity-hover button2">
                        <img src="img/insurance-button2.png" class="category-png3 opacity-hover button3">
                        <img src="img/insurance-button.png" class="category-png3 opacity-hover button4">
                    
                </a>     

                <a href="index.php"  class="image-button-css2 hover-effect padding0">
                    
                        <img src="img/oversea-button0.png" class="category-png3 opacity-hover button1">
                        <img src="img/oversea-button1.png" class="category-png3 opacity-hover button2">
                        <img src="img/oversea-button2.png" class="category-png3 opacity-hover button3">
                        <img src="img/oversea-button.png" class="category-png3 opacity-hover button4">
                  
                </a>   

            <?php
            }
        }
        ?>  

   </div>


</div>

<div class="clear"></div>
<?php include 'js.php'; ?>

</body>
</html>