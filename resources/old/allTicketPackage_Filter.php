<?php
if (session_id() == "")
{
    session_start();
}
// require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

// require_once dirname(__FILE__) . '/classes/Restaurant.php';
require_once dirname(__FILE__) . '/classes/Ticket.php';
require_once dirname(__FILE__) . '/classes/States.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

// $allTour = getTourPackage($conn, "WHERE display = 'Yes' ORDER BY date_created DESC LIMIT 88 ");
$allPackage = getTicket($conn, "WHERE display = 'Yes' ");

$allState = getStates($conn);

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>


<!doctype html>
<html>

<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://tabigo.holiday/" />
<link rel="canonical" href="https://tabigo.holiday/" />-->
<meta property="og:title" content="All Ticket Package | Tabigo" />
<title>All Ticket Package | Tabigo</title>   
<?php include 'css.php'; ?>
</head>


<body class="body">
<?php include 'header.php'; ?>

<div class="width100 overflow menu-distance same-padding min-height padding-bottom50">


     <div class="width100 overflow">

        <?php
        if(isset($_GET['id']))
        {
        $conn = connDB();
        ?>
        	<h1 class="second-line red-text ow-margin-top0 title-css">Ticket Package (<?php echo $_GET['id'];?>)</h1>
        <?php
        }
        ?>
        
            <form action="allTicketPackageSearch.php" method="POST" class="filter-form" >

                <?php
                    $allState = getStates($conn, "WHERE country = ? ",array("country"),array($_GET['id']),"s");
                ?>

                <select class="clean filter-select state-select" id="search_state" name="search_state" required>
                    <!-- <option>Select State</option> -->
                    
                    <?php
                    for ($cnt=0; $cnt <count($allState) ; $cnt++)
                    {
                    ?>
                        <option value="<?php echo $allState[$cnt]->getStateName();?>">
                            <?php echo $allState[$cnt]->getStateName();?>
                        </option>
                    <?php
                    }
                    ?>
                </select>

                <div class="tempo-clear1"></div>
                <button class="clean filter-button hover-effect red-btn">Filter</button>

            </form>

        </div>
    	<!-- <h1 class="line-header margin-bottom50">PingCash Shop</h1> -->
 		<div class="width103">

            <!-- <div class="repeat-four-width-div">
                <img src="img/p16.jpg" class="width100">
                <div class="sold-label">Out of Stock</div>
                <div class="product-description">
                    <p class="product-name-p text-overflow">Razer Hammerhead True Wireless Earbuds</p>
                    <p class="price-p">350.00</p>
                </div>
            </div> -->

        <?php
        if($allPackage)
        {
            for($cnt = 0;$cnt < count($allPackage) ;$cnt++)
            {
            ?>
 		<a href='userDetailsTicket.php?id=<?php echo $allPackage[$cnt]->getUid();?>'>
            <div class="whitebox-redshadow showall-box opacity-hover">
                <div class="square">
                    <div class="width100 white-bg image-box-size content2">
                       

                            <!-- <img src="img/digi.png" > -->

                            <?php 
                                $linkTicket = $allPackage[$cnt]->getPhoto();
                                $stringTicketA = $linkTicket;
                                $stringTicketB = str_replace('https://drive.google.com/file/d/', '', $stringTicketA);
                                $googleLinkTicket = str_replace('/view?usp=sharing', '', $stringTicketB);
                            ?>
                            <img src='https://drive.google.com/uc?export=view&id=<?php echo $googleLinkTicket;?>' >
                    </div>
                </div>
                            <div class="width100 product-details-div">
                                <p class="width100 text-overflow slider-product-name"><?php echo $allPackage[$cnt]->getTitle();?></p>
                                <p class="width100 text-overflow slider-product-name"><?php echo $allPackage[$cnt]->getState();?></p>
                            </div>
                        

            </div>
			</a>
            <?php
            }
            ?>
        <?php
        }
        ?>
                          
        </div>


</div>

<div class="clear"></div>
<?php include 'js.php'; ?>

</body>
</html>