<?php
if (session_id() == "")
{
    session_start();
}
// require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/InterestingPlace.php';
require_once dirname(__FILE__) . '/classes/States.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

// $allTour = getInterestingPlace($conn, "WHERE display = 'Yes' ORDER BY date_created DESC LIMIT 88 ");
$allTour = getInterestingPlace($conn, "WHERE display = 'Yes' ");

$allState = getStates($conn);

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>


<!doctype html>
<html>

<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://tabigo.holiday/" />
<link rel="canonical" href="https://tabigo.holiday/" />-->
<meta property="og:title" content="Place of Interest | Tabigo" />
<title>Place of Interest | Tabigo</title>   
<?php include 'css.php'; ?>
</head>


<body class="body">
<?php include 'header.php'; ?>

<div class="width100 overflow menu-distance same-padding min-height padding-bottom50">


     <div class="width100 overflow">

        <?php
        if(isset($_GET['id']))
        {
        $conn = connDB();
        ?>
        	<h1 class="second-line red-text ow-margin-top0 title-css">Place of Interest (<?php echo $_GET['id'];?>)</h1>
        <?php
        }
        ?>
        
            <form action="allPointOfInterestSearch.php" method="POST" class="filter-form">

                <?php
                    $allState = getStates($conn, "WHERE country = ? ",array("country"),array($_GET['id']),"s");
                ?>
            
                <select class="clean filter-select state-select" id="search_state" name="search_state" required>
                    <!-- <option>Select State</option> -->
                    
                    <?php
                    for ($cnt=0; $cnt <count($allState) ; $cnt++)
                    {
                    ?>
                        <option value="<?php echo $allState[$cnt]->getStateName();?>">
                            <?php echo $allState[$cnt]->getStateName();?>
                        </option>
                    <?php
                    }
                    ?>
                </select>

                <div class="tempo-clear1"></div>
                <button class="clean filter-button hover-effect red-btn">Filter</button>
            </form>

        <!-- <form class="filter-form">
            <select class="clean filter-select state-select">
                <option>Penang</option>
                <option>Selangor</option>
            </select>
           
            <button class="clean filter-button hover-effect red-btn poi-filter">Filter</button>
        </form> -->

        </div>
        <div class="clear"></div>
 		<div class="width103">
            <?php
            if($allTour)
            {
                for($cnt = 0;$cnt < count($allTour) ;$cnt++)
                {
                ?>
			<a href='userDetailsPOI.php?id=<?php echo $allTour[$cnt]->getUid();?>'>
                <div class="whitebox-redshadow showall-box opacity-hover">
                    <div class="square">
                        <div class="width100 white-bg image-box-size content2">
                            
                            <!-- <a href='#'> -->
                                <?php 
                                    $linkTour = $allTour[$cnt]->getPhotoOne();
                                    $stringTourA = $linkTour;
                                    $stringTourB = str_replace('https://drive.google.com/file/d/', '', $stringTourA);
                                    $googleLinkTour = str_replace('/view?usp=sharing', '', $stringTourB);
                                ?>
                                <img src='https://drive.google.com/uc?export=view&id=<?php echo $googleLinkTour;?>' class="width100">
                        </div>
                    </div>
                                <div class="width100 product-details-div">
                                    <p class="width100 text-overflow slider-product-name"><?php echo $allTour[$cnt]->getTitle();?></p>
                                    <p class="slider-product-price text-overflow"><?php echo $allTour[$cnt]->getState();?></p>
                                </div>
                            

                </div>
				</a>
                <?php
                }
                ?>
            <?php
            }
            ?>
        </div>
    </div>


<div class="clear"></div>
<?php include 'js.php'; ?>

</body>
</html>