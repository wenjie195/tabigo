<?php
// require_once dirname(__FILE__) . '/sessionLoginChecker.php';
if (session_id() == "")
{
    session_start();
}
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

$timestamp = time();

include 'selectFilecss.php';
include 'js.php';

require_once('vendor/php-excel-reader/excel_reader2.php');
require_once('vendor/SpreadsheetReader.php');

if (isset($_POST["import"]))
{
  $allowedFileType = ['application/vnd.ms-excel','text/xls','text/xlsx','application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'];

  if(in_array($_FILES["file"]["type"],$allowedFileType))
  {
    // $targetPath = 'uploads/'.$_FILES['file']['name'];
    $targetPath = 'uploads/'.$timestamp.$_FILES['file']['name'];
    move_uploaded_file($_FILES['file']['tmp_name'], $targetPath);
    $Reader = new SpreadsheetReader($targetPath);
    $sheetCount = count($Reader->sheets());
    for($i=0;$i<$sheetCount;$i++)
    {
      $Reader->ChangeSheet($i);
      foreach ($Reader as $Row)
      {

        $location = "";
        if(isset($Row[1])) 
        {
          $location = mysqli_real_escape_string($conn,$Row[0]);
        }
        $name = "";
        if(isset($Row[1])) 
        {
          $name = mysqli_real_escape_string($conn,$Row[1]);
        }
        $photo = "";
        if(isset($Row[1])) 
        {
          $photo = mysqli_real_escape_string($conn,$Row[2]);
        }

        $title = "";
        if(isset($Row[1])) 
        {
          $title = mysqli_real_escape_string($conn,$Row[3]);
        }
        $price = "";
        if(isset($Row[1])) 
        {
          $price = mysqli_real_escape_string($conn,$Row[4]);
        }
        $address = "";
        if(isset($Row[1])) 
        {
          $address = mysqli_real_escape_string($conn,$Row[5]);
        }

        $uid = md5(uniqid());

        if (!empty($location) || !empty($name) || !empty($photo) || !empty($title) || !empty($price) || !empty($address))
        {
          $query = "INSERT INTO restaurant (uid,location,name,photo,title,price,address) 
                    VALUES ('".$uid."','".$location."','".$name."','".$photo."','".$title."','".$price."','".$address."') ";
          $result = mysqli_query($conn, $query);
          if (! empty($result))
          {
            // echo "<script>alert('Excel Uploaded !');window.location='../telemarketing/uploadExcel.php'</script>";       
            echo "Uploaded !";
          }
          else 
          {
            echo "Fail !";
          }
        }
      }
    }
  }
  else
  {
    echo "ERROR !";
  }
}
?>

<!DOCTYPE html>
<html>
<head>
  <?php include 'meta.php'; ?>
  <!-- <meta property="og:url" content="https://qlianmeng.asia/addReferee.php" /> -->
  <meta property="og:title" content="Import Restaurant | Tobigo" />
  <title>Import Restaurant | Tobigo</title>
  <!-- <link rel="canonical" href="https://qlianmeng.asia/addReferee.php" /> -->
  <?php include 'css.php'; ?>
</head>

<body class="body">

<!-- <?php //include 'adminSidebar.php'; ?> -->
<div class="next-to-sidebar"> 

<h1 class="h1-title">Import Excel File</h1>

  <div class="outer-container">
    <form action="" method="post" name="frmExcelImport" id="frmExcelImport" enctype="multipart/form-data">
      <label>Select File</label><br><input type="file" name="file" id="file" accept=".xls,.xlsx"><div class="clear"></div>
      <button type="submit" id="submit" name="import"  class="clean red-btn margin-top30 fix300-btn margin-left0">Submit</button>
      <div id="response" class="<?php if(!empty($type)) { echo $type . " display-block"; } ?>"><?php if(!empty($message)) { echo $message; } ?></div>
    </form>
  </div>

<style>
.import-li{
color:#bf1b37;
background-color:white;}
.import-li .hover1a{
display:none;}
.import-li .hover1b{
display:block;}
</style>

<?php include 'js.php'; ?>
<script  src="https://code.jquery.com/jquery-2.2.0.min.js"></script>
<script type="text/javascript" src="js/modernizr.custom.js"></script>
<script type="text/javascript" src="js/jquery.dlmenu.js"></script>
		<script>
			$(function() {
				$( '#dl-menu' ).dlmenu({
					animationClasses : { classin : 'dl-animate-in-2', classout : 'dl-animate-out-2' }
				});
			});
    </script>
</body>
</html>