-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 09, 2021 at 12:08 PM
-- Server version: 10.4.16-MariaDB
-- PHP Version: 7.4.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `vidatech_tabigo`
--

-- --------------------------------------------------------

--
-- Table structure for table `transportation_long`
--

CREATE TABLE `transportation_long` (
  `id` bigint(20) NOT NULL,
  `uid` varchar(255) DEFAULT NULL,
  `country` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `company_name` text DEFAULT NULL,
  `departure_route` text DEFAULT NULL,
  `description` text DEFAULT NULL,
  `photo_aone` text DEFAULT NULL,
  `photo_atwo` text DEFAULT NULL,
  `photo_athree` text DEFAULT NULL,
  `vehicle_a` text DEFAULT NULL,
  `vehicle_aprice` text DEFAULT NULL,
  `photo_bone` text DEFAULT NULL,
  `photo_btwo` text DEFAULT NULL,
  `photo_bthree` text DEFAULT NULL,
  `vehicle_b` text DEFAULT NULL,
  `vehicle_bprice` text DEFAULT NULL,
  `photo_cone` text DEFAULT NULL,
  `photo_ctwo` text DEFAULT NULL,
  `photo_cthree` text DEFAULT NULL,
  `vehicle_c` text DEFAULT NULL,
  `vehicle_cprice` text DEFAULT NULL,
  `photo_done` text DEFAULT NULL,
  `photo_dtwo` text DEFAULT NULL,
  `photo_dthree` text DEFAULT NULL,
  `vehicle_d` text DEFAULT NULL,
  `vehicle_dprice` text DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `transportation_long`
--
ALTER TABLE `transportation_long`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `transportation_long`
--
ALTER TABLE `transportation_long`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
