<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Countries.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

// $allTour = getTourPackage($conn, "WHERE display = 'Yes' ORDER BY date_created DESC LIMIT 8 ");
// $allPlace = getInterestingPlace($conn, "WHERE display = 'Yes' ORDER BY date_created DESC LIMIT 8 ");

$allCountry = getCountries($conn);

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>
<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<meta property="og:url" content="https://tabigo.holiday/" />
<link rel="canonical" href="https://tabigo.holiday/" />
<meta property="og:title" content="Home | Tabigo" />
<title>Home | Tabigo</title>   
<?php include 'css.php'; ?>

</head>

<body class="body">
<?php include 'header.php'; ?>

<div class="clear"></div>

<div class="width100 overflow menu-distance same-padding min-height padding-bottom50">

  <div class="clear"></div>

  <!-- <div class="ta-big-three"> -->

    <?php
    if($allCountry)
    {
        for($cnt = 0;$cnt < count($allCountry) ;$cnt++)
        {
        ?>    

            <div class="first-line-gps red-text"><img src="img/location.png" class="gps-size"><?php echo $allCountry[$cnt]->getEnName();?></div>
            <div class="ta-big-three">

                <a href='allTourPackage.php?id=<?php echo $allCountry[$cnt]->getEnName();?>'>
                    <button class="country-button hover-effect clean"><div class="ta-three-div"><p>Tour Package</p></div></button>
                </a>

                <a href='allRestaurantPackage.php?id=<?php echo $allCountry[$cnt]->getEnName();?>'>
                    <button class="country-button hover-effect clean"><div class="ta-three-div"><p>Restaurant</p></div></button>
                </a>

                <!-- <button class="country-button hover-effect clean"><div class="ta-three-div"><p>Restaurant</p></div></button> -->

                <!-- <button class="country-button hover-effect clean"><div class="ta-three-div"><p>Transportation</p></div></button> -->

                <a href='allTransportation.php?id=<?php echo $allCountry[$cnt]->getEnName();?>'>
                    <button class="country-button hover-effect clean"><div class="ta-three-div"><p>Transportation</p></div></button>
                </a>

                <a href='allPointOfInterest.php?id=<?php echo $allCountry[$cnt]->getEnName();?>'>
                    <button class="country-button hover-effect clean"><div class="ta-three-div"><p>Place of Interest</p></div></button>
                </a>

                <a href='allTicketPackage.php?id=<?php echo $allCountry[$cnt]->getEnName();?>'>
                    <button class="country-button hover-effect clean"><div class="ta-three-div"><p>Ticket Booking</p></div></button>
                </a>

                <a href='allWifiPackage.php?id=<?php echo $allCountry[$cnt]->getEnName();?>'>
                    <button class="country-button hover-effect clean"><div class="ta-three-div"><p>Wifi</p></div></button>
                </a>

                <!-- <button class="country-button hover-effect clean"><div class="ta-three-div"><p>Ticket Booking</p></div></button>
                <button class="country-button hover-effect clean"><div class="ta-three-div"><p>Wifi</p></div></button> -->

            </div>

        <?php
        }
    }
    ?>  


</div>

<div class="clear"></div>

<?php include 'js.php'; ?>

</body>
</html>