<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

$userDetails = getUser($conn, "WHERE uid =?",array("uid"),array($uid),"s");
// $userDetails = $userData[0]->getFullname();

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html>
<head>
    <?php include 'meta.php'; ?>
    <meta property="og:url" content="https://tabigo.holiday/" />
    <link rel="canonical" href="https://tabigo.holiday/" />
    <meta property="og:title" content="Edit Profile | Tabigo" />
    <title>Edit Profile | Tabigo</title>   
    <?php include 'css.php'; ?>
</head>

<body class="body">

<?php include 'header.php'; ?>

<div class="width100 overflow menu-distance same-padding min-height padding-bottom50">

    <h1 class="h1-title">Edit Profile</h1> 

    <form action="utilities/userEditProfileFunction.php" method="POST">

    <div class="input50-div">
        <p class="input-title-p">Username</p>
        <input class="clean tele-input" type="text" placeholder="Username" value="<?php echo $userDetails[0]->getUsername();?>" id="username" name="username" required>        
    </div> 

    <div class="input50-div second-input50">
        <p class="input-title-p">Fullname (*UNABLE TO EDIT)</p>
        <input class="clean tele-input" type="text" value="<?php echo $userDetails[0]->getFullname();?>" readonly>       
    </div> 

    <div class="clear"></div>

    <div class="input50-div">
        <p class="input-title-p">IC Number</p>
        <input class="clean tele-input"  type="text" placeholder="IC Number" value="<?php echo $userDetails[0]->getIcno();?>" id="icno" name="icno" required>        
    </div> 

    <div class="input50-div second-input50">
        <p class="input-title-p">Phone</p>
        <input class="clean tele-input"  type="text" placeholder="Phone" value="<?php echo $userDetails[0]->getPhone();?>" id="phone" name="phone" required>     
    </div> 

    <div class="clear"></div>

    <div class="width100">
        <p class="input-title-p">Email</p>
        <input class="clean tele-input"  type="email" placeholder="Email" value="<?php echo $userDetails[0]->getEmail();?>" id="email" name="email" required>        
    </div> 

    <div class="clear"></div>


    <div class="width100">
        <p class="input-title-p">Address</p>
        <textarea  type="text" class="clean tele-input textarea-min-height" placeholder="Address" id="address" name="address"><?php echo $userDetails[0]->getAddress();?></textarea> 
    </div> 

    <div class="clear"></div>

    <div class="input50-div">
        <p class="input-title-p">Emergency Contact Name</p>
        <input class="clean tele-input" type="text" placeholder="Emergency People Person" value="<?php echo $userDetails[0]->getEmergencyPpl();?>" id="emergency_ppl" name="emergency_ppl">         
    </div> 

    <div class="input50-div second-input50">
        <p class="input-title-p">Emergency Contact</p>
        <input class="clean tele-input" type="text" placeholder="Emergency Contact" value="<?php echo $userDetails[0]->getEmergencyContact();?>" id="emergency_contact" name="emergency_contact">         
    </div> 

    <div class="clear"></div>

    <button class="clean red-btn red-btn margin-top30 fix300-btn margin-bottom30" name="submit">Submit</button>

    <div class="clear"></div>
    
    </form>

</div>

<style>
.editprofile-li{
	color:#264a9c;
	background-color:white;}
.editprofile-li .hover1a{
	display:none;}
.editprofile-li .hover1b{
	display:block;}
</style>

<?php include 'js.php'; ?>

<?php
if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Profile Updated"; 
        }
        elseif($_GET['type'] == 2)
        {
            $messageType = "Fail To Update Profile"; 
        }
        elseif($_GET['type'] == 3)
        {
            $messageType = "ERROR !!";
        }
        echo '
        <script>
            putNoticeJavascript("Notice !! ","'.$messageType.'");  
        </script>
        ';   
        $_SESSION['messageType'] = 0;
    }
}
?>

</body>
</html>