<?php  
class InterestingPlace {
    /* Member variables */
    var $id,$uid,$country,$state,$title,$descriptionOne,$descriptionTwo,$descriptionThree,$photoOne,$photoTwo,$address,$latlong,$openingHrs,$website,$display,$dateCreated,$dateUpdated;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getUid()
    {
        return $this->uid;
    }

    /**
     * @param mixed $uid
     */
    public function setUid($uid)
    {
        $this->uid = $uid;
    }

    /**
     * @return mixed
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @param mixed $country
     */
    public function setCountry($country)
    {
        $this->country = $country;
    }

    /**
     * @return mixed
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * @param mixed $state
     */
    public function setState($state)
    {
        $this->state = $state;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return mixed
     */
    public function getDescriptionOne()
    {
        return $this->descriptionOne;
    }

    /**
     * @param mixed $descriptionOne
     */
    public function setDescriptionOne($descriptionOne)
    {
        $this->descriptionOne = $descriptionOne;
    }

    /**
     * @return mixed
     */
    public function getDescriptionTwo()
    {
        return $this->descriptionTwo;
    }

    /**
     * @param mixed $descriptionTwo
     */
    public function setDescriptionTwo($descriptionTwo)
    {
        $this->descriptionTwo = $descriptionTwo;
    }

    /**
     * @return mixed
     */
    public function getDescriptionThree()
    {
        return $this->descriptionThree;
    }

    /**
     * @param mixed $descriptionThree
     */
    public function setDescriptionThree($descriptionThree)
    {
        $this->descriptionThree = $descriptionThree;
    }

    /**
     * @return mixed
     */
    public function getPhotoOne()
    {
        return $this->photoOne;
    }

    /**
     * @param mixed $photoOne
     */
    public function setPhotoOne($photoOne)
    {
        $this->photoOne = $photoOne;
    }

    /**
     * @return mixed
     */
    public function getPhotoTwo()
    {
        return $this->photoTwo;
    }

    /**
     * @param mixed $photoTwo
     */
    public function setPhotoTwo($photoTwo)
    {
        $this->photoTwo = $photoTwo;
    }

    /**
     * @return mixed
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @param mixed $address
     */
    public function setAddress($address)
    {
        $this->address = $address;
    }

    /**
     * @return mixed
     */
    public function getLatLong()
    {
        return $this->latlong;
    }

    /**
     * @param mixed $latlong
     */
    public function setLatLong($latlong)
    {
        $this->latlong = $latlong;
    }

    /**
     * @return mixed
     */
    public function getOpeningHrs()
    {
        return $this->openingHrs;
    }

    /**
     * @param mixed $openingHrs
     */
    public function setOpeningHrs($openingHrs)
    {
        $this->openingHrs = $openingHrs;
    }

    /**
     * @return mixed
     */
    public function getWebsite()
    {
        return $this->website;
    }

    /**
     * @param mixed $website
     */
    public function setWebsite($website)
    {
        $this->website = $website;
    }

    /**
     * @return mixed
     */
    public function getDisplay()
    {
        return $this->display;
    }

    /**
     * @param mixed $display
     */
    public function setDisplay($display)
    {
        $this->display = $display;
    }

    /**
     * @return mixed
     */
    public function getDateCreated()
    {
        return $this->dateCreated;
    }

    /**
     * @param mixed $dateCreated
     */
    public function setDateCreated($dateCreated)
    {
        $this->dateCreated = $dateCreated;
    }

    /**
     * @return mixed
     */
    public function getDateUpdated()
    {
        return $this->dateUpdated;
    }

    /**
     * @param mixed $dateUpdated
     */
    public function setDateUpdated($dateUpdated)
    {
        $this->dateUpdated = $dateUpdated;
    }

}

function getInterestingPlace($conn,$whereClause = null,$queryColumns = null,$queryValues = null,$queryTypes = null){
    $dbColumnNames = array("id","uid","country","state","title","description_one","description_two","description_three","photo_one","photo_two","address",
                            "lat_long","opening_hrs","website","display","date_created","date_updated");

    $sql = sqlSelectSimpleBuilder($dbColumnNames,"interesting_place");
    if($whereClause){
        $sql .= $whereClause;
    }

    if($stmt = $conn->prepare($sql)){
        /*
             Binds variables to prepared statement

             i    corresponding variable has type integer
             d    corresponding variable has type double
             s    corresponding variable has type string
             b    corresponding variable is a blob and will be sent in packets
        */

        if($queryColumns&&$queryTypes&&$queryValues){
            $stmt = returnStmtWithDynamicBinding($stmt,$queryValues,$queryTypes);
        }

//        $stmt->bind_param('s',$queryValues[0]);

        /* execute query */
        $stmt->execute();

        /* Store the result (to get properties) */
        $stmt->store_result();

        /* Get the number of rows */
        $num_of_rows = $stmt->num_rows;

        /* Bind the result to variables */
        $stmt->bind_result($id,$uid,$country,$state,$title,$descriptionOne,$descriptionTwo,$descriptionThree,$photoOne,$photoTwo,$address,$latlong,
                            $openingHrs,$website,$display,$dateCreated,$dateUpdated);

        $resultRows = array();
        while ($stmt->fetch()) {
            $class = new InterestingPlace;
            $class->setId($id);
            $class->setUid($uid);
            $class->setCountry($country);
            $class->setState($state);
            $class->setTitle($title);
            $class->setDescriptionOne($descriptionOne);
            $class->setDescriptionTwo($descriptionTwo);
            $class->setDescriptionThree($descriptionThree);
            $class->setPhotoOne($photoOne);
            $class->setPhotoTwo($photoTwo);
            $class->setAddress($address);
            $class->setLatLong($latlong);
            $class->setOpeningHrs($openingHrs);
            $class->setWebsite($website);
            $class->setDisplay($display);
            $class->setDateCreated($dateCreated);
            $class->setDateUpdated($dateUpdated);
          
            array_push($resultRows,$class);
        }

        /* free results */
        $stmt->free_result();

        /* close statement */
        $stmt->close();

        if($num_of_rows <= 0){
            return null;
        }else{
            return $resultRows;
        }
    }else{
//        echo "Prepare Error: ($conn->errno) $conn->error";
        return null;
    }
}
