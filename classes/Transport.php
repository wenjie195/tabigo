<?php  
class Transportation {
    /* Member variables */
    var $id,$uid,$country,$state,$title,$companyName,$route,$description,
        $photoOneA,$photoTwoA,$photoThreeA,$vehicleA,$priceA,$photoOneB,$photoTwoB,$photoThreeB,$vehicleB,$priceB,
        $photoOneC,$photoTwoC,$photoThreeC,$vehicleC,$priceC,$photoOneD,$photoTwoD,$photoThreeD,$vehicleD,$priceD,
        $display,$dateCreated,$dateUpdated;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getUid()
    {
        return $this->uid;
    }

    /**
     * @param mixed $uid
     */
    public function setUid($uid)
    {
        $this->uid = $uid;
    }

    /**
     * @return mixed
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @param mixed $country
     */
    public function setCountry($country)
    {
        $this->country = $country;
    }

    /**
     * @return mixed
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * @param mixed $state
     */
    public function setState($state)
    {
        $this->state = $state;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return mixed
     */
    public function getCompanyName()
    {
        return $this->companyName;
    }

    /**
     * @param mixed $companyName
     */
    public function setCompanyName($companyName)
    {
        $this->companyName = $companyName;
    }

    /**
     * @return mixed
     */
    public function getRoute()
    {
        return $this->route;
    }

    /**
     * @param mixed $route
     */
    public function setRoute($route)
    {
        $this->route = $route;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }



    /** TA */
    /**
     * @return mixed
     */
    public function getPhotoOneA()
    {
        return $this->photoOneA;
    }

    /**
     * @param mixed $photoOneA
     */
    public function setPhotoOneA($photoOneA)
    {
        $this->photoOneA = $photoOneA;
    }

    /**
     * @return mixed
     */
    public function getPhotoTwoA()
    {
        return $this->photoTwoA;
    }

    /**
     * @param mixed $photoTwoA
     */
    public function setPhotoTwoA($photoTwoA)
    {
        $this->photoTwoA = $photoTwoA;
    }

    /**
     * @return mixed
     */
    public function getPhotoThreeA()
    {
        return $this->photoThreeA;
    }

    /**
     * @param mixed $photoThreeA
     */
    public function setPhotoThreeA($photoThreeA)
    {
        $this->photoThreeA = $photoThreeA;
    }

    /**
     * @return mixed
     */
    public function getVehicleA()
    {
        return $this->vehicleA;
    }

    /**
     * @param mixed $vehicleA
     */
    public function setVehicleA($vehicleA)
    {
        $this->vehicleA = $vehicleA;
    }

    /**
     * @return mixed
     */
    public function getPriceA()
    {
        return $this->priceA;
    }

    /**
     * @param mixed $priceA
     */
    public function setPriceA($priceA)
    {
        $this->priceA = $priceA;
    }

    /** TB */
    /**
     * @return mixed
     */
    public function getPhotoOneB()
    {
        return $this->photoOneB;
    }

    /**
     * @param mixed $photoOneB
     */
    public function setPhotoOneB($photoOneB)
    {
        $this->photoOneB = $photoOneB;
    }

    /**
     * @return mixed
     */
    public function getPhotoTwoB()
    {
        return $this->photoTwoB;
    }

    /**
     * @param mixed $photoTwoB
     */
    public function setPhotoTwoB($photoTwoB)
    {
        $this->photoTwoB = $photoTwoB;
    }

    /**
     * @return mixed
     */
    public function getPhotoThreeB()
    {
        return $this->photoThreeB;
    }

    /**
     * @param mixed $photoThreeB
     */
    public function setPhotoThreeB($photoThreeB)
    {
        $this->photoThreeB = $photoThreeB;
    }

    /**
     * @return mixed
     */
    public function getVehicleB()
    {
        return $this->vehicleB;
    }

    /**
     * @param mixed $vehicleB
     */
    public function setVehicleB($vehicleB)
    {
        $this->vehicleB = $vehicleB;
    }

    /**
     * @return mixed
     */
    public function getPriceB()
    {
        return $this->priceB;
    }

    /**
     * @param mixed $priceB
     */
    public function setPriceB($priceB)
    {
        $this->priceB = $priceB;
    }

    /** TC */
    /**
     * @return mixed
     */
    public function getPhotoOneC()
    {
        return $this->photoOneC;
    }

    /**
     * @param mixed $photoOneC
     */
    public function setPhotoOneC($photoOneC)
    {
        $this->photoOneC = $photoOneC;
    }

    /**
     * @return mixed
     */
    public function getPhotoTwoC()
    {
        return $this->photoTwoC;
    }

    /**
     * @param mixed $photoTwoC
     */
    public function setPhotoTwoC($photoTwoC)
    {
        $this->photoTwoC = $photoTwoC;
    }

    /**
     * @return mixed
     */
    public function getPhotoThreeC()
    {
        return $this->photoThreeC;
    }

    /**
     * @param mixed $photoThreeC
     */
    public function setPhotoThreeC($photoThreeC)
    {
        $this->photoThreeC = $photoThreeC;
    }

    /**
     * @return mixed
     */
    public function getVehicleC()
    {
        return $this->vehicleC;
    }

    /**
     * @param mixed $vehicleC
     */
    public function setVehicleC($vehicleC)
    {
        $this->vehicleC = $vehicleC;
    }

    /**
     * @return mixed
     */
    public function getPriceC()
    {
        return $this->priceC;
    }

    /**
     * @param mixed $priceC
     */
    public function setPriceC($priceC)
    {
        $this->priceC = $priceC;
    }

    /** TD */
    /**
     * @return mixed
     */
    public function getPhotoOneD()
    {
        return $this->photoOneD;
    }

    /**
     * @param mixed $photoOneD
     */
    public function setPhotoOneD($photoOneD)
    {
        $this->photoOneD = $photoOneD;
    }

    /**
     * @return mixed
     */
    public function getPhotoTwoD()
    {
        return $this->photoTwoD;
    }

    /**
     * @param mixed $photoTwoD
     */
    public function setPhotoTwoD($photoTwoD)
    {
        $this->photoTwoD = $photoTwoD;
    }

    /**
     * @return mixed
     */
    public function getPhotoThreeD()
    {
        return $this->photoThreeD;
    }

    /**
     * @param mixed $photoThreeD
     */
    public function setPhotoThreeD($photoThreeD)
    {
        $this->photoThreeD = $photoThreeD;
    }

    /**
     * @return mixed
     */
    public function getVehicleD()
    {
        return $this->vehicleD;
    }

    /**
     * @param mixed $vehicleD
     */
    public function setVehicleD($vehicleD)
    {
        $this->vehicleD = $vehicleD;
    }

    /**
     * @return mixed
     */
    public function getPriceD()
    {
        return $this->priceD;
    }

    /**
     * @param mixed $priceD
     */
    public function setPriceD($priceD)
    {
        $this->priceD = $priceD;
    }


    /**
     * @return mixed
     */
    public function getDisplay()
    {
        return $this->display;
    }

    /**
     * @param mixed $display
     */
    public function setDisplay($display)
    {
        $this->display = $display;
    }
    /**
     * @return mixed
     */
    public function getDateCreated()
    {
        return $this->dateCreated;
    }

    /**
     * @param mixed $dateCreated
     */
    public function setDateCreated($dateCreated)
    {
        $this->dateCreated = $dateCreated;
    }

    /**
     * @return mixed
     */
    public function getDateUpdated()
    {
        return $this->dateUpdated;
    }

    /**
     * @param mixed $dateUpdated
     */
    public function setDateUpdated($dateUpdated)
    {
        $this->dateUpdated = $dateUpdated;
    }

}

function getTransportation($conn,$whereClause = null,$queryColumns = null,$queryValues = null,$queryTypes = null){
    $dbColumnNames = array("id","uid","country","state","title","company_name","departure_route","description",
                            "photo_aone","photo_atwo","photo_athree","vehicle_a","price_a","photo_bone","photo_btwo","photo_bthree","vehicle_b","price_b",
                            "photo_cone","photo_ctwo","photo_cthree","vehicle_c","price_c","photo_done","photo_dtwo","photo_dthree","vehicle_d","price_d",
                            "display","date_created","date_updated");

    $sql = sqlSelectSimpleBuilder($dbColumnNames,"transportation");
    if($whereClause){
        $sql .= $whereClause;
    }

    if($stmt = $conn->prepare($sql)){
        /*
             Binds variables to prepared statement

             i    corresponding variable has type integer
             d    corresponding variable has type double
             s    corresponding variable has type string
             b    corresponding variable is a blob and will be sent in packets
        */

        if($queryColumns&&$queryTypes&&$queryValues){
            $stmt = returnStmtWithDynamicBinding($stmt,$queryValues,$queryTypes);
        }

//        $stmt->bind_param('s',$queryValues[0]);

        /* execute query */
        $stmt->execute();

        /* Store the result (to get properties) */
        $stmt->store_result();

        /* Get the number of rows */
        $num_of_rows = $stmt->num_rows;

        /* Bind the result to variables */
        $stmt->bind_result($id,$uid,$country,$state,$title,$companyName,$route,$description,
                            $photoOneA,$photoTwoA,$photoThreeA,$vehicleA,$priceA,$photoOneB,$photoTwoB,$photoThreeB,$vehicleB,$priceB,
                            $photoOneC,$photoTwoC,$photoThreeC,$vehicleC,$priceC,$photoOneD,$photoTwoD,$photoThreeD,$vehicleD,$priceD,
                            $display,$dateCreated,$dateUpdated);

        $resultRows = array();
        while ($stmt->fetch()) {
            $class = new Transportation;
            $class->setId($id);
            $class->setUid($uid);
            $class->setCountry($country);
            $class->setState($state);
            $class->setTitle($title);
            $class->setCompanyName($companyName);
            $class->setRoute($route);
            $class->setDescription($description);

            $class->setPhotoOneA($photoOneA);
            $class->setPhotoTwoA($photoTwoA);
            $class->setPhotoThreeA($photoThreeA);
            $class->setVehicleA($vehicleA);
            $class->setPriceA($priceA);

            $class->setPhotoOneB($photoOneB);
            $class->setPhotoTwoB($photoTwoB);
            $class->setPhotoThreeB($photoThreeB);
            $class->setVehicleB($vehicleB);
            $class->setPriceB($priceB);

            $class->setPhotoOneC($photoOneC);
            $class->setPhotoTwoC($photoTwoC);
            $class->setPhotoThreeC($photoThreeC);
            $class->setVehicleC($vehicleC);
            $class->setPriceC($priceC);

            $class->setPhotoOneD($photoOneD);
            $class->setPhotoTwoD($photoTwoD);
            $class->setPhotoThreeD($photoThreeD);
            $class->setVehicleD($vehicleD);
            $class->setPriceD($priceD);

            $class->setDisplay($display);
            $class->setDateCreated($dateCreated);
            $class->setDateUpdated($dateUpdated);
          
            array_push($resultRows,$class);
        }

        /* free results */
        $stmt->free_result();

        /* close statement */
        $stmt->close();

        if($num_of_rows <= 0){
            return null;
        }else{
            return $resultRows;
        }
    }else{
//        echo "Prepare Error: ($conn->errno) $conn->error";
        return null;
    }
}
