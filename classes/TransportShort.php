<?php  
class Transportation {
    /* Member variables */
    var $id,$uid,$country,$title,$companyName,$route,$description,$photoOne,$photoTwo,$photoThree,$vehicle,$price,$dateCreated,$dateUpdated;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getUid()
    {
        return $this->uid;
    }

    /**
     * @param mixed $uid
     */
    public function setUid($uid)
    {
        $this->uid = $uid;
    }

    /**
     * @param mixed $uid
     */
    public function setUid($uid)
    {
        $this->uid = $uid;
    }

    /**
     * @return mixed
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return mixed
     */
    public function getCompanyName()
    {
        return $this->companyName;
    }

    /**
     * @param mixed $companyName
     */
    public function setCompanyName($companyName)
    {
        $this->companyName = $companyName;
    }

    /**
     * @return mixed
     */
    public function getRoute()
    {
        return $this->route;
    }

    /**
     * @param mixed $route
     */
    public function setRoute($route)
    {
        $this->route = $route;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return mixed
     */
    public function getPhotoOne()
    {
        return $this->photoOne;
    }

    /**
     * @param mixed $photoOne
     */
    public function setPhotoOne($photoOne)
    {
        $this->photoOne = $photoOne;
    }

    /**
     * @return mixed
     */
    public function getPhotoTwo()
    {
        return $this->photoTwo;
    }

    /**
     * @param mixed $photoTwo
     */
    public function setPhotoTwo($photoTwo)
    {
        $this->photoTwo = $photoTwo;
    }

    /**
     * @return mixed
     */
    public function getPhotoThree()
    {
        return $this->photoThree;
    }

    /**
     * @param mixed $photoThree
     */
    public function setPhotoThree($photoThree)
    {
        $this->photoThree = $photoThree;
    }

    /**
     * @return mixed
     */
    public function getVehicle()
    {
        return $this->vehicle;
    }

    /**
     * @param mixed $vehicle
     */
    public function setVehicle($vehicle)
    {
        $this->vehicle = $vehicle;
    }

    /**
     * @return mixed
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param mixed $price
     */
    public function setPrice($price)
    {
        $this->price = $price;
    }

    /**
     * @return mixed
     */
    public function getDateCreated()
    {
        return $this->dateCreated;
    }

    /**
     * @param mixed $dateCreated
     */
    public function setDateCreated($dateCreated)
    {
        $this->dateCreated = $dateCreated;
    }

    /**
     * @return mixed
     */
    public function getDateUpdated()
    {
        return $this->dateUpdated;
    }

    /**
     * @param mixed $dateUpdated
     */
    public function setDateUpdated($dateUpdated)
    {
        $this->dateUpdated = $dateUpdated;
    }

}

function getTransportation($conn,$whereClause = null,$queryColumns = null,$queryValues = null,$queryTypes = null){
    $dbColumnNames = array("id","uid","country","title","company_name","departure_route","description","photo_one","photo_two","photo_three","vehicle","price",
                            "date_created","date_updated");

    $sql = sqlSelectSimpleBuilder($dbColumnNames,"transportation");
    if($whereClause){
        $sql .= $whereClause;
    }

    if($stmt = $conn->prepare($sql)){
        /*
             Binds variables to prepared statement

             i    corresponding variable has type integer
             d    corresponding variable has type double
             s    corresponding variable has type string
             b    corresponding variable is a blob and will be sent in packets
        */

        if($queryColumns&&$queryTypes&&$queryValues){
            $stmt = returnStmtWithDynamicBinding($stmt,$queryValues,$queryTypes);
        }

//        $stmt->bind_param('s',$queryValues[0]);

        /* execute query */
        $stmt->execute();

        /* Store the result (to get properties) */
        $stmt->store_result();

        /* Get the number of rows */
        $num_of_rows = $stmt->num_rows;

        /* Bind the result to variables */
        $stmt->bind_result($id,$uid,$country,$title,$companyName,$route,$description,$photoOne,$photoTwo,$photoThree,$vehicle,$price,$dateCreated,$dateUpdated);

        $resultRows = array();
        while ($stmt->fetch()) {
            $class = new Transportation;
            $class->setId($id);
            $class->setUid($uid);
            $class->setCountry($country);
            $class->setTitle($title);
            $class->setCompanyName($companyName);
            $class->setRoute($route);
            $class->setDescription($description);
            $class->setPhotoOne($photoOne);
            $class->setPhotoTwo($photoTwo);
            $class->setPhotoThree($photoThree);
            $class->setVehicle($vehicle);
            $class->setPrice($price);
            $class->setDateCreated($dateCreated);
            $class->setDateUpdated($dateUpdated);
          
            array_push($resultRows,$class);
        }

        /* free results */
        $stmt->free_result();

        /* close statement */
        $stmt->close();

        if($num_of_rows <= 0){
            return null;
        }else{
            return $resultRows;
        }
    }else{
//        echo "Prepare Error: ($conn->errno) $conn->error";
        return null;
    }
}
