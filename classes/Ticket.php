<?php  
class Ticket {
    /* Member variables */
    var $id,$uid,$country,$state,$photo,$title,$merchantName,$description,$packageA,$packageB,$packageC,$unitA,$unitB,$unitC,$notes,$terms,$display,$dateCreated,$dateUpdated;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getUid()
    {
        return $this->uid;
    }

    /**
     * @param mixed $uid
     */
    public function setUid($uid)
    {
        $this->uid = $uid;
    }

    /**
     * @return mixed
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @param mixed $country
     */
    public function setCountry($country)
    {
        $this->country = $country;
    }

    /**
     * @return mixed
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * @param mixed $state
     */
    public function setState($state)
    {
        $this->state = $state;
    }

    /**
     * @return mixed
     */
    public function getPhoto()
    {
        return $this->photo;
    }

    /**
     * @param mixed $photo
     */
    public function setPhoto($photo)
    {
        $this->photo = $photo;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return mixed
     */
    public function getMerchantName()
    {
        return $this->merchantName;
    }

    /**
     * @param mixed $merchantName
     */
    public function setMerchantName($merchantName)
    {
        $this->merchantName = $merchantName;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return mixed
     */
    public function getPackageA()
    {
        return $this->packageA;
    }

    /**
     * @param mixed $packageA
     */
    public function setPackageA($packageA)
    {
        $this->packageA = $packageA;
    }
    
    /**
     * @return mixed
     */
    public function getPackageB()
    {
        return $this->packageB;
    }

    /**
     * @param mixed $packageB
     */
    public function setPackageB($packageB)
    {
        $this->packageB = $packageB;
    }
    
    /**
     * @return mixed
     */
    public function getPackageC()
    {
        return $this->packageC;
    }

    /**
     * @param mixed $packageC
     */
    public function setPackageC($packageC)
    {
        $this->packageC = $packageC;
    }

    /**
     * @return mixed
     */
    public function getUnitA()
    {
        return $this->unitA;
    }

    /**
     * @param mixed $unitA
     */
    public function setUnitA($unitA)
    {
        $this->unitA = $unitA;
    }

    /**
     * @return mixed
     */
    public function getUnitB()
    {
        return $this->unitB;
    }

    /**
     * @param mixed $unitB
     */
    public function setUnitB($unitB)
    {
        $this->unitB = $unitB;
    }

    /**
     * @return mixed
     */
    public function getUnitC()
    {
        return $this->unitC;
    }

    /**
     * @param mixed $unitC
     */
    public function setUnitC($unitC)
    {
        $this->unitC = $unitC;
    }
    
    /**
     * @return mixed
     */
    public function getNotes()
    {
        return $this->notes;
    }

    /**
     * @param mixed $notes
     */
    public function setNotes($notes)
    {
        $this->notes = $notes;
    }

    /**
     * @return mixed
     */
    public function getTerms()
    {
        return $this->terms;
    }

    /**
     * @param mixed $terms
     */
    public function setTerms($terms)
    {
        $this->terms = $terms;
    }

    /**
     * @return mixed
     */
    public function getDisplay()
    {
        return $this->display;
    }

    /**
     * @param mixed $display
     */
    public function setDisplay($display)
    {
        $this->display = $display;
    }

    /**
     * @return mixed
     */
    public function getDateCreated()
    {
        return $this->dateCreated;
    }

    /**
     * @param mixed $dateCreated
     */
    public function setDateCreated($dateCreated)
    {
        $this->dateCreated = $dateCreated;
    }

    /**
     * @return mixed
     */
    public function getDateUpdated()
    {
        return $this->dateUpdated;
    }

    /**
     * @param mixed $dateUpdated
     */
    public function setDateUpdated($dateUpdated)
    {
        $this->dateUpdated = $dateUpdated;
    }

}

function getTicket($conn,$whereClause = null,$queryColumns = null,$queryValues = null,$queryTypes = null){
    $dbColumnNames = array("id","uid","country","state","photo","title","merchant_name","description","package_a","package_b","package_c","unit_a","unit_b","unit_c",
                                "notes","terms","display","date_created","date_updated");

    $sql = sqlSelectSimpleBuilder($dbColumnNames,"ticket");
    if($whereClause){
        $sql .= $whereClause;
    }

    if($stmt = $conn->prepare($sql)){
        /*
             Binds variables to prepared statement

             i    corresponding variable has type integer
             d    corresponding variable has type double
             s    corresponding variable has type string
             b    corresponding variable is a blob and will be sent in packets
        */

        if($queryColumns&&$queryTypes&&$queryValues){
            $stmt = returnStmtWithDynamicBinding($stmt,$queryValues,$queryTypes);
        }

//        $stmt->bind_param('s',$queryValues[0]);

        /* execute query */
        $stmt->execute();

        /* Store the result (to get properties) */
        $stmt->store_result();

        /* Get the number of rows */
        $num_of_rows = $stmt->num_rows;

        /* Bind the result to variables */
        $stmt->bind_result($id,$uid,$country,$state,$photo,$title,$merchantName,$description,$packageA,$packageB,$packageC,$unitA,$unitB,$unitC,$notes,$terms,$display,
                            $dateCreated,$dateUpdated);

        $resultRows = array();
        while ($stmt->fetch()) {
            $class = new Ticket;
            $class->setId($id);
            $class->setUid($uid);
            $class->setCountry($country);
            $class->setState($state);
            $class->setPhoto($photo);
            $class->setTitle($title);
            $class->setMerchantName($merchantName);
            $class->setDescription($description);
            $class->setPackageA($packageA);
            $class->setPackageB($packageB);
            $class->setPackageC($packageC);
            $class->setUnitA($unitA);
            $class->setUnitB($unitB);
            $class->setUnitC($unitC);
            $class->setNotes($notes);
            $class->setTerms($terms);
            $class->setDisplay($display);
            $class->setDateCreated($dateCreated);
            $class->setDateUpdated($dateUpdated);
          
            array_push($resultRows,$class);
        }

        /* free results */
        $stmt->free_result();

        /* close statement */
        $stmt->close();

        if($num_of_rows <= 0){
            return null;
        }else{
            return $resultRows;
        }
    }else{
//        echo "Prepare Error: ($conn->errno) $conn->error";
        return null;
    }
}
