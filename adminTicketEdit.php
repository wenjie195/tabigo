<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Ticket.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>
<!doctype html>
<html>

<head>
    <?php include 'meta.php'; ?>
    <!-- <meta property="og:url" content="https://tabigo.holiday/" />
    <link rel="canonical" href="https://tabigo.holiday/" /> -->
    <meta property="og:title" content="Edit Ticket Package | Tabigo" />
    <title>Edit Ticket Package | Tabigo</title>   
    <?php include 'css.php'; ?>
    <script src="//cdn.ckeditor.com/4.14.0/full/ckeditor.js"></script>
</head>

<body class="body">
<?php include 'headerAdmin.php'; ?>

<div class="clear"></div>

<div class="width100 overflow menu-distance same-padding ow-sakura-height">
	<h1 class="title-h1 raleway red-text ow-no-margin wow fadeIn" data-wow-delay="0.1s">Edit Ticket Details</a></h1>

    <?php
    if(isset($_POST['item_uid']))
    {
    $conn = connDB();
    $itemDetails = getTicket($conn,"WHERE uid = ? ", array("uid") ,array($_POST['item_uid']),"s");
    ?>

        <!-- <form action="utilities/adminTourEditFunction.php" method="POST"> -->
        <form action="utilities/adminTicketEditFunction.php" method="POST">

        <input class="tele-input clean" type="hidden" value="<?php echo $itemDetails[0]->getUid(); ?>" name="item_uid" id="item_uid" readonly> 

            <div class="dual-input">
                <p class="input-top-p admin-top-p">Country*</p>
                <input class="tele-input clean" type="text" value="<?php echo $itemDetails[0]->getCountry(); ?>" placeholder="Country" name="country" id="country" required>  
            </div>

            <div class="dual-input second-dual-input">
                <p class="input-top-p admin-top-p">State*</p>
                <input class="tele-input clean" type="text" value="<?php echo $itemDetails[0]->getState(); ?>" placeholder="State" name="state" id="state" required>        
            </div>

            <div class="clean"></div>

            <div class="width100 overflow">
                <p class="input-top-p admin-top-p">Photo* <img src="img/drive-link.jpg" class="tutorial-img"></p>
                <input class="tele-input clean" type="text" value="<?php echo $itemDetails[0]->getPhoto(); ?>" placeholder="Photo" name="photo" id="photo">      
            </div>

            <div class="clean"></div>

            <div class="dual-input">
                <p class="input-top-p admin-top-p">Title*</p>
                <input class="tele-input clean" type="text" value="<?php echo $itemDetails[0]->getTitle(); ?>" placeholder="Title" name="title" id="title" required>  
            </div>

            <div class="dual-input second-dual-input">
                <p class="input-top-p admin-top-p">Merchant Name*</p>
                <input class="tele-input clean" type="text" value="<?php echo $itemDetails[0]->getMerchantName(); ?>" placeholder="Merchant Name" name="merchant_name" id="merchant_name" required>        
            </div>

            <div class="clean"></div>

            <div class="form-group publish-border input-div width100 overflow">
                <p class="input-top-p admin-top-p">Description</p>
                <textarea name="editor" id="editor" rows="10" cols="80"  class="tele-input clean lato input-textarea admin-input editor-input" ><?php echo $itemDetails[0]->getDescription(); ?></textarea>
            </div>   

            <div class="clean"></div>

            <div class="dual-input">
                <p class="input-top-p admin-top-p">Unit A*</p>
                <input class="tele-input clean" type="text" value="<?php echo $itemDetails[0]->getUnitA(); ?>" placeholder="Unit A" name="unit_a" id="unit_a" required>  
            </div>
            <div class="dual-input second-dual-input">
                <p class="input-top-p admin-top-p">Price for Package A*</p>
                <input class="tele-input clean" type="text" value="<?php echo $itemDetails[0]->getPackageA(); ?>" placeholder="Package A" name="package_a" id="package_a" required>        
            </div>

            <div class="clean"></div>

            <div class="dual-input">
                <p class="input-top-p admin-top-p">Unit B*</p>
                <input class="tele-input clean" type="text" value="<?php echo $itemDetails[0]->getUnitB(); ?>" placeholder="Unit B" name="unit_b" id="unit_b">  
            </div>
            <div class="dual-input second-dual-input">
                <p class="input-top-p admin-top-p">Price for Package B*</p>
                <input class="tele-input clean" type="text" value="<?php echo $itemDetails[0]->getPackageB(); ?>" placeholder="Package B" name="package_b" id="package_b">        
            </div>

            <div class="clean"></div>

            <div class="dual-input">
                <p class="input-top-p admin-top-p">Unit C*</p>
                <input class="tele-input clean" type="text" value="<?php echo $itemDetails[0]->getUnitC(); ?>" placeholder="Unit C" name="unit_c" id="unit_c">  
            </div>
            <div class="dual-input second-dual-input">
                <p class="input-top-p admin-top-p">Price for Package C*</p>
                <input class="tele-input clean" type="text" value="<?php echo $itemDetails[0]->getPackageC(); ?>" placeholder="Package C" name="package_c" id="package_c">        
            </div>

            <div class="clear"></div>  

            <div class="width100 overflow">
                <p class="input-top-p admin-top-p">Notes</p>
                <textarea class="tele-input clean lato input-textarea admin-input editor-input" type="text" placeholder="Notes" name="notes" id="notes"><?php echo $itemDetails[0]->getNotes(); ?></textarea>  	
            </div>   

            <div class="clear"></div>  

            <div class="width100 overflow">
                <p class="input-top-p admin-top-p">Terms</p>
                <textarea class="tele-input clean lato input-textarea admin-input editor-input" type="text" placeholder="Terms" name="terms" id="terms"><?php echo $itemDetails[0]->getTerms(); ?></textarea>  	
            </div>   

            <div class="clear"></div>  

            <div class="width100 text-center">
                <button class="clean red-btn hover-effect middle-button-size below-forgot margin-bottom30" name="submit">Submit</button>
            </div>

        </form>

    <?php
    }
    ?>

</div>

<div class="clear"></div>

<?php include 'js.php'; ?>

<script>
    CKEDITOR.replace('editor');
</script>

</body>
</html>