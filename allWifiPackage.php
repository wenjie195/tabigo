<?php
if (session_id() == "")
{
    session_start();
}
// require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Wifi.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

// $conn = connDB();

// $_SESSION['url'] = $_SERVER['REQUEST_URI'];
// $allPackage = getWifi($conn, "WHERE display = 'Yes' ");

// $conn->close();

$_SESSION['url'] = $_SERVER['REQUEST_URI'];

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
    $conn = connDB();

    $country = rewrite($_POST['country']);
    $state = rewrite($_POST['state']);

    $allPackage = getWifi($conn, "WHERE country = ? AND state = ? ",array("country","remark"),array($country,$state),"ss");

    $conn->close();
}

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>


<!doctype html>
<html>

<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://tabigo.holiday/" />
<link rel="canonical" href="https://tabigo.holiday/" />-->
<meta property="og:title" content="All Wifi Package | Tabigo" />
<title>All Wifi Package | Tabigo</title>   
<?php include 'css.php'; ?>
</head>


<body class="body">
<?php include 'header.php'; ?>

<div class="width100 overflow menu-distance same-padding min-height padding-bottom50">


     <div class="width100 overflow">

        <h1 class="second-line red-text ow-margin-top0 title-css" style="margin-bottom:10px !important;">Wifi Package (<?php echo $state;?>, <?php echo $country;?>)</h1>

		</div>
        <div class="clear"></div>
    	<!-- <h1 class="line-header margin-bottom50">PingCash Shop</h1> -->
 		<div class="width103">

            <!-- <div class="repeat-four-width-div">
                <img src="img/p16.jpg" class="width100">
                <div class="sold-label">Out of Stock</div>
                <div class="product-description">
                    <p class="product-name-p text-overflow">Razer Hammerhead True Wireless Earbuds</p>
                    <p class="price-p">350.00</p>
                </div>
            </div> -->

        <?php
        if($allPackage)
        {
            for($cnt = 0;$cnt < count($allPackage) ;$cnt++)
            {
            ?>
			<a href='userDetailsWifi.php?id=<?php echo $allPackage[$cnt]->getUid();?>'>
            <div class="whitebox-redshadow showall-box opacity-hover">
<!--                <div class="square">
                    <div class="width100 white-bg image-box-size content2">
                        

                            <img src="img/digi.png" >
                            </div>
                        
                    </div>-->
                            <div class="width100 product-details-div">
                                <p class="width100 text-overflow slider-product-name"><?php echo $allPackage[$cnt]->getTitle();?></p>
                                <p class="width100 slider-product-price text-overflow">RM<?php echo $allPackage[$cnt]->getPrice();?></p>

                </div>
            </div>
			</a>
            <?php
            }
            ?>
        <?php
        }
        ?>
                          
        </div>
    </div>


<div class="clear"></div>
<?php include 'js.php'; ?>

</body>
</html>