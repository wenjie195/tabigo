<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Transport.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

// $allTour = getTourPackage($conn, "WHERE display = 'Yes' ");
$allTransport = getTransportation($conn, "WHERE display = 'Yes' ORDER BY date_created DESC");

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>
<!doctype html>
<html>
<head>
	<?php include 'meta.php'; ?>
<!--         <meta property="og:url" content="https://tabigo.holiday/" />
        <link rel="canonical" href="https://tabigo.holiday/" />-->
        <meta property="og:title" content="Transportation | Tabigo" />
        <title>Transportation | Tabigo</title>   
	<?php include 'css.php'; ?>


</head>

<body class="body">
<?php include 'headerAdmin.php'; ?>


<div class="clear"></div>
<div class="width100 overflow menu-distance same-padding sakura-bg ow-sakura-height">
    <h1 class="title-h1 raleway red-text ow-no-margin">Transportation
        <!-- <a href="adminTourAdd.php" class="pink-link hover-effect underline">(Add 1 by 1)</a> | <a href="uploadExcelTransportation.php" class="pink-link hover-effect underline">(Add by Excel)</a> -->
        <!--<a href="#" class="pink-link hover-effect underline">(Add 1 by 1)</a> |--> <a href="uploadExcelTransportation.php" class="pink-link hover-effect underline add-link">(Add by Excel)</a>
    </h1>	
    <div class="overflow-div">
            <table class="white-table">
                <thead>
                    <th>No.</th>
                    <th>Country</th>
                    <th>State</th>
                    <th>Title</th>
                    <th>Company Name</th>
                    <th>Route</th>
                    <th>Vehicle Type / Price&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                    <!-- <th>Price</th> -->
                    <th>Edit</th>
                    <th>Delete</th>
                </thead>

                <?php
                if($allTransport)
                {
                    for($cnt = 0;$cnt < count($allTransport) ;$cnt++)
                    {
                    ?>    
                        <tr>
                            <td><?php echo ($cnt+1)?></td>
                            <!-- <td> - </td> -->
                            <td><?php echo $allTransport[$cnt]->getCountry();?></td>
                            <td><?php echo $allTransport[$cnt]->getState();?></td>
                            <td><?php echo $allTransport[$cnt]->getTitle();?></td>
                            <td><?php echo $allTransport[$cnt]->getCompanyName();?></td>
                            <td><?php echo $allTransport[$cnt]->getRoute();?></td>

                            <td>
                                <?php echo $allTransport[$cnt]->getVehicleA();?> | RM <?php echo $allTransport[$cnt]->getPriceA();?><br>
                                <?php echo $allTransport[$cnt]->getVehicleB();?> | RM <?php echo $allTransport[$cnt]->getPriceB();?><br>
                                <?php echo $allTransport[$cnt]->getVehicleC();?> | RM <?php echo $allTransport[$cnt]->getPriceC();?><br>
                                <?php echo $allTransport[$cnt]->getVehicleD();?> | RM <?php echo $allTransport[$cnt]->getPriceC();?>
                            </td>

                            <!-- <td>
                                RM <?php echo $allTransport[$cnt]->getPriceA();?><br>
                                RM <?php echo $allTransport[$cnt]->getPriceB();?><br>
                                RM <?php echo $allTransport[$cnt]->getPriceC();?><br>
                                RM <?php echo $allTransport[$cnt]->getPriceD();?>
                            </td> -->
                            
                            <td>
                                <form action="adminTransportationEdit.php" method="POST">
                                <!-- <form action="#" method="POST"> -->
                                    <button class="transparent-button clean orange-link hover-effect" type="submit" name="package_uid" value="<?php echo $allTransport[$cnt]->getUid();?>">
                                        Edit
                                    </button>
                                </form>
                            </td>

                            <td>
                                <form method="POST" action="utilities/adminTransportationDeleteFunction.php" class="hover1">
                                    <button class="transparent-button clean red-link hover-effect" type="submit" name="package_uid" value="<?php echo $allTransport[$cnt]->getUid();?>">
                                        Delete
                                    </button>
                                </form>
                            </td>

                        </tr>
                    <?php
                    }
                }
                ?>  

                <!-- <tr>
                    <td>1</td>
                    <td>14/9/2021</td>
                    <td>Half Day Georgetown Heritage tour</td>
                    <td>RM150</td>
                    <td>Available</td>
                    <td><form><a href="adminEditTour.php" class="hover-effect red-link ow-font400"><button class="transparent-button clean orange-link hover-effect">Edit</button></a></form></td>
                    <td><form><button  class="transparent-button clean red-link hover-effect">Delete</button></form></td>
                </tr> -->

            </table>
        </div>
</div>
<div class="clear"></div>
<?php include 'js.php'; ?>

<?php
if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Transportation Deleted !"; 
        }
        elseif($_GET['type'] == 2)
        {
            $messageType = "New Tour Package Added !"; 
        }
        elseif($_GET['type'] == 3)
        {
            $messageType = "Transportation Details Updated !";
        }
        elseif($_GET['type'] == 4)
        {
            $messageType = "Fail To Updated !";
        }
        elseif($_GET['type'] == 5)
        {
            $messageType = "ERROR !";
        }
        echo '
        <script>
            putNoticeJavascript("Notice !! ","'.$messageType.'");  
        </script>
        ';   
        $_SESSION['messageType'] = 0;
    }
}
?>

</body>
</html>