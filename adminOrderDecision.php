<?php
// require_once dirname(__FILE__) . '/sessionLoginChecker.php';
if (session_id() == "")
{
    session_start();
}
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>
<!doctype html>
<html>
<head>
	<?php include 'meta.php'; ?>
<!--         <meta property="og:url" content="https://tabigo.holiday/" />
        <link rel="canonical" href="https://tabigo.holiday/" />-->
        <meta property="og:title" content="Half Day Georgetown Heritage tour | Tabigo" />
        <title>Half Day Georgetown Heritage tour | Tabigo</title>   
	<?php include 'css.php'; ?>


</head>

<body class="body">
<?php include 'header.php'; ?>


<div class="clear"></div>
<div class="width100 overflow menu-distance same-padding background-div">
            
                <div class="cover-gap article-content min-height2 blog-content-div1 with-price-height">
                    <div class="print-area" id="printarea">
                        <div class="clear"></div>

                        <h1 class="red-h1 slab darkpink-text red-text">User Details</h1>
						<p class="article-p">
                        <b>Username</b><br>
                        Ng Yi Ling<br>
                        user@gmail.com<br>
                        0165334201<br>
                        Malaysia
                        </p>
                        <div class="clear"></div>

                        <h1 class="red-h1 slab darkpink-text red-text">Half Day Georgetown Heritage tour</h1>
						<table class="article-info-table">
                        	<tr>
                            	<td class="info-td1"><img src="img/location.png" class="info-img"></td>
                                <td class="info-td2">Penang, Malaysia</td>
                            </tr>
                        	<tr>
                            	<td class="info-td1"><img src="img/company.png" class="info-img"></td>
                                <td class="info-td2">Golden Adventure tours & travel</td>
                            </tr>                            
                        	<tr>
                            	<td class="info-td1"><img src="img/money.png" class="info-img"></td>
                                <td class="info-td2">RM150</td>
                            </tr>                             
                        </table>
                         

                        
                      
                        <div class="test" data-allowfullscreen="true">
                            <div class="article-css-control"  data-allowfullscreen="true">
                                <p class="article-p">Visit most of the famous Heritage sightseeing spot at Georgetown area, start from morning 9am to 2pm ,Transportation, lunch and all the entrance fee are included.</p> 
                                <p class="article-p">Schedule</p> 
                                <table>
                                	<tr>
                                    	<td>09:00</td>
                                        <td>Depart Hotel</td>
                                    </tr>
                                	<tr>
                                    	<td>09:30 - 11:00</td>
                                        <td>Georgetown Heritage Tour<br>
                                            ->St. George Church<br> 
                                            ->Kuan Yin Temple or The Goddess of Mercy Temple (Penangss Oldest Chinese Temple) <br> 
                                            ->Sri Mahamariamman Indian Temple <br> 
                                            ->Kapitan Keling Mosque <br> 
                                            ->Yap Kongsi Temple <br> 
                                            ->Khoo Kongsi <br> 
                                            ->Lebuh Aceh Mosque <br> 
                                            ->Penang Street Art <br> 
                                        </td>
                                    </tr>
                                	<tr>
                                    	<td>11:40 - 11:55</td>
                                        <td>Fort Cornwallis (built by the British East India Company in the late 18th century) </td>
                                    </tr>                                    
                                	<tr>
                                    	<td>12:05 - 12:30</td>
                                        <td>Houses on water stilts</td>
                                    </tr>      
                                	<tr>
                                    	<td>12:45 - 13:30</td>
                                        <td>Lunch: Local City restaurant </td>
                                    </tr>                                     
                                	<tr>
                                    	<td>13:45 - 14:15</td>
                                        <td>Shopping(•Sourviner and Latex shops)</td>
                                    </tr>                                	
                                    <tr>
                                    	<td>14:30</td>
                                        <td>Arrive Hotel</td>
                                    </tr>                                                                                                  
                                </table>  
                                <img src="img/p1.jpg">
                                <p class="article-p text-center">Sri Mahamariamman Indian Temple</p>
                                <img src="img/p2.jpg">
                                <p class="article-p text-center">St. George Church</p>
                                <img src="img/p3.jpg">
                                <p class="article-p text-center">Penang Street Art</p>
                                <img src="img/p4.jpg">
                                <p class="article-p text-center">Fort Cornwallis</p>                                
                                <p class="article-p text-center">* The above itinerary is subject to change depending on the weather and traffic condition on that day.<br>
*The photos stated above are for internal use only, not for advertising. </p>
                                
                            </div>
                        </div>
                        
                        <div class="hide-print">
                            <script async src="https://static.addtoany.com/menu/page.js"></script>
                            <!-- AddToAny END -->

                            <div class="clear"></div>

                            <div class="width100 overflow padding-bottom50">
                                <!-- AddToAny BEGIN -->
                                <div class="border-div"></div>
                                <h3 class="dark-blue-text share-h3">Share:</h3>
                                <div class="a2a_kit a2a_kit_size_32 a2a_default_style">
                                <a class="a2a_button_copy_link"></a>
                                <a class="a2a_button_facebook"></a>
                                <a class="a2a_button_twitter"></a>
                                <a class="a2a_button_linkedin"></a>
                                <a class="a2a_button_blogger"></a>
                                <a class="a2a_button_facebook_messenger"></a>
                                <a class="a2a_button_whatsapp"></a>
                                <a class="a2a_button_wechat"></a>
                                <a class="a2a_button_line"></a>
                                <a class="a2a_button_telegram"></a>
                                <!--<a class="a2a_button_print"></a>-->
                                </div> 
                            </div>



                           
                        </div>
                    </div>  
                </div>
           
	
</div>
<div class="price-div width100 same-padding">
   
   <button class="purchase-div hover-effect clean border0 decs-btn">Refund</button>
   <button class="purchase-div hover-effect clean ow-green-btn border0 decs-btn second-decs-btn">Approve</button>
</div>


<?php include 'js.php'; ?>
<style>
*{
   
	vertical-align:middle;
	word-break:keep-all !important;
}
a{
	opacity:0.8;
	transition:.15s ease-in-out;
	cursor:pointer;}
a:hover{
	opacity:1;}
.langz{
	display:none;}
.footer-div{
	display:none;}
</style>

</body>
</html>