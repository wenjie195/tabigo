<?php
// require_once dirname(__FILE__) . '/sessionLoginChecker.php';
if (session_id() == "")
{
    session_start();
}
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

$timestamp = time();

include 'selectFilecss.php';
include 'js.php';

require_once('vendor/php-excel-reader/excel_reader2.php');
require_once('vendor/SpreadsheetReader.php');

if (isset($_POST["import"]))
{
  $allowedFileType = ['application/vnd.ms-excel','text/xls','text/xlsx','application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'];

  if(in_array($_FILES["file"]["type"],$allowedFileType))
  {
    // $targetPath = 'uploads/'.$_FILES['file']['name'];
    $targetPath = 'uploads/'.$timestamp.$_FILES['file']['name'];
    move_uploaded_file($_FILES['file']['tmp_name'], $targetPath);
    $Reader = new SpreadsheetReader($targetPath);
    $sheetCount = count($Reader->sheets());
    for($i=0;$i<$sheetCount;$i++)
    {
      $Reader->ChangeSheet($i);
      foreach ($Reader as $Row)
      {

        $country = "";
        if(isset($Row[0])) 
        {
          $country = mysqli_real_escape_string($conn,$Row[0]);
        }
        $title = "";
        if(isset($Row[0])) 
        {
          $title = mysqli_real_escape_string($conn,$Row[1]);
        }
        $companyName = "";
        if(isset($Row[0])) 
        {
          $companyName = mysqli_real_escape_string($conn,$Row[2]);
        }
        $route = "";
        if(isset($Row[0])) 
        {
          $route = mysqli_real_escape_string($conn,$Row[3]);
        }
        $description = "";
        if(isset($Row[0])) 
        {
          $description = mysqli_real_escape_string($conn,$Row[4]);
        }
        $photoOne = "";
        if(isset($Row[0])) 
        {
          $photoOne = mysqli_real_escape_string($conn,$Row[5]);
        }
        $photoTwo = "";
        if(isset($Row[0])) 
        {
          $photoTwo = mysqli_real_escape_string($conn,$Row[6]);
        }
        $photoThree = "";
        if(isset($Row[0])) 
        {
          $photoThree = mysqli_real_escape_string($conn,$Row[7]);
        }
        $vehicle = "";
        if(isset($Row[0])) 
        {
          $vehicle = mysqli_real_escape_string($conn,$Row[8]);
        }
        $price = "";
        if(isset($Row[0])) 
        {
          $price = mysqli_real_escape_string($conn,$Row[9]);
        }

        $uid = md5(uniqid());

        if (!empty($country) || !empty($title) || !empty($companyName) || !empty($route) || !empty($description) || !empty($photoOne) || !empty($photoTwo) || !empty($photoThree) || !empty($vehicle) || !empty($price))
        {
          $query = "INSERT INTO transportation (uid,country,title,company_name,departure_route,description,photo_one,photo_two,photo_three,vehicle,price) 
                    VALUES ('".$uid."','".$country."','".$title."','".$companyName."','".$route."','".$description."','".$photoOne."','".$photoTwp."','".$photoThree."','".$vehicle."','".$price."') ";

          $result = mysqli_query($conn, $query);
          if (! empty($result))
          {
            // echo "<script>alert('Excel Uploaded !');window.location='../telemarketing/uploadExcel.php'</script>";       
            echo "Uploaded !";
          }
          else 
          {
            echo "Fail !";
          }
        }
      }
    }
  }
  else
  {
    echo "ERROR !";
  }
}
?>

<!DOCTYPE html>
<html>
<head>
  <?php include 'meta.php'; ?>
  <!-- <meta property="og:url" content="https://qlianmeng.asia/addReferee.php" /> -->
  <meta property="og:title" content="Import Transportation | Tobigo" />
  <title>Import Transportation | Tobigo</title>
  <!-- <link rel="canonical" href="https://qlianmeng.asia/addReferee.php" /> -->
  <?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'header.php'; ?>
<div class="clear"></div>
<div class="width100 overflow menu-distance same-padding ow-sakura-height">
	<h1 class="title-h1 raleway red-text wow fadeIn" data-wow-delay="0.1s">Import Excel File for Transportation</a></h1>

  <div class="outer-container text-center ">
    <form action="" method="post" name="frmExcelImport" id="frmExcelImport" enctype="multipart/form-data">
      <label>Select File</label><br><input type="file" name="file" id="file" accept=".xls,.xlsx"><div class="clear"></div>
      <button type="submit" id="submit" name="import"  class="clean red-btn margin-top30 fix300-btn margin-left0">Submit</button>
      <div id="response" class="<?php if(!empty($type)) { echo $type . " display-block"; } ?>"><?php if(!empty($message)) { echo $message; } ?></div>
    </form>
  </div>
</div>
<div class="clear"></div>


<?php include 'js.php'; ?>
<style>
.footer-div{
	display:none;}
</style>
<div class="clear"></div>	
<div class="footer-div" style="display:block !important;">
	<p class="footer-p wow fadeIn" data-wow-delay="0.1s">&copy;<span id="year"></span> Tabigo, All Rights Reserved.</p>
</div>
</body>
</html>