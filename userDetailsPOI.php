<?php
if (session_id() == "")
{
    session_start();
}
// require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Countries.php';
require_once dirname(__FILE__) . '/classes/InterestingPlace.php';
require_once dirname(__FILE__) . '/classes/TourPackage.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$uid = $_SESSION['uid'];
// if($_SESSION['uid'] != "")
// {
//     $uid = $_SESSION['uid'];
// }
// else
// {
//     $uid = NULL;
// }

$_SESSION['url'] = $_SERVER['REQUEST_URI'];

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>
<!doctype html>
<html>

<?php 
    // Program to display URL of current page. 
    if(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on') 
    $link = "https"; 
    else
    $link = "http"; 

    // Here append the common URL characters. 
    $link .= "://"; 

    // Append the host(domain name, ip) to the URL. 
    $link .= $_SERVER['HTTP_HOST']; 

    // Append the requested resource location to the URL 
    $link .= $_SERVER['REQUEST_URI']; 

    // Print the link 
    // echo $link; 
?>

<?php
if(isset($_GET['id']))
{
$conn = connDB();
$TourDetails = getInterestingPlace($conn,"WHERE uid = ? AND display = 'YES' ",array("uid"),array($_GET['id']), "s");
// $TourDetails = getTourPackage($conn,"WHERE uid = ? AND display = 'YES' ",array("uid"),array($_GET['id']), "s");
?>

    <?php
    if($TourDetails)
    {
        for($cnt = 0;$cnt < count($TourDetails) ;$cnt++)
        {
        ?>

            <head>
            <?php include 'meta.php'; ?>
<!--            <meta property="og:url" content="https://tabigo.holiday/" />
            <link rel="canonical" href="https://tabigo.holiday/" />-->
            <meta property="og:title" content="<?php echo $TourDetails[$cnt]->getTitle();?> | Tabigo" />
            <title><?php echo $TourDetails[$cnt]->getTitle();?> | Tabigo</title>   
            <?php include 'css.php'; ?>
            </head>
            <body class="body">
            <?php include 'header.php'; ?>

        <?php
        }
        ?>
    <?php
    }
    ?>

    <div class="clear"></div>

    <?php
    if($TourDetails)
    {
        for($cnt = 0;$cnt < count($TourDetails) ;$cnt++)
        {
        ?>
        
            <div class="width100 overflow same-padding background-div">

                <div class="cover-gap article-content min-height2 blog-content-div1 with-price-height  ow-menu-distance">
                    <div class="print-area" id="printarea">

                        <div class="clear"></div>

                        <?php $tourUid = $TourDetails[$cnt]->getUid();?>
                        <?php $tourState = $TourDetails[$cnt]->getState();?>

                        <h1 class="red-h1 slab darkpink-text red-text"><?php echo $TourDetails[$cnt]->getTitle();?></h1>
                        <table class="article-info-table">
                            <tr>
                                <td class="info-td1"><img src="img/location.png" class="info-img"></td>
                                <td class="info-td2"><?php echo $TourDetails[$cnt]->getState();?>, <?php echo $TourDetails[$cnt]->getCountry();?></td>
                            </tr>
                        
                            <tr>
                                <td class="info-td1"><img src="img/compass.png" class="info-img"></td>
                                <td class="info-td2"><?php echo $TourDetails[$cnt]->getLatLong();?></td>
                            </tr>           
                            
                            <tr>
                                <td class="info-td1"><img src="img/company.png" class="info-img"></td>
                                <td class="info-td2"><?php echo $TourDetails[$cnt]->getAddress();?></td>
                            </tr>    

                            <tr>
                                <td class="info-td1"><img src="img/clock.png" class="info-img"></td>
                                <td class="info-td2"><?php echo $TourDetails[$cnt]->getOpeningHrs();?></td>
                            </tr>  
                            <tr>
                                <td class="info-td1"><img src="img/globe.png" class="info-img"></td>
                                <td class="info-td2">
                                    <a href='<?php echo $TourDetails[$cnt]->getWebsite();?>' target="_blank">
                                        <?php echo $TourDetails[$cnt]->getWebsite();?>
                                    </a>
                                </td>
                            </tr>  

                        </table>

                        <div class="test" data-allowfullscreen="true">
                            <div class="article-css-control"  data-allowfullscreen="true">
                                <p class="article-p"><?php echo $TourDetails[$cnt]->getDescriptionOne();?></p> 
                                <p class="article-p"><?php echo $TourDetails[$cnt]->getDescriptionTwo();?></p> 
                                <p class="article-p"><?php echo $TourDetails[$cnt]->getDescriptionThree();?></p> 
                            </div>
                        </div>
                        

                        <div class="test" data-allowfullscreen="true">
                            <div class="article-css-control"  data-allowfullscreen="true">
                                <?php 
                                $linkTour = $TourDetails[$cnt]->getPhotoOne();
                                $stringTourA = $linkTour;
                                $stringTourB = str_replace('https://drive.google.com/file/d/', '', $stringTourA);
                                $googleLinkTour = str_replace('/view?usp=sharing', '', $stringTourB);
                                ?>
                                <img src='https://drive.google.com/uc?export=view&id=<?php echo $googleLinkTour;?>' >

                                <?php 
                                $linkTour2 = $TourDetails[$cnt]->getPhotoTwo();
                                $stringTourA2 = $linkTour2;
                                $stringTourB2 = str_replace('https://drive.google.com/file/d/', '', $stringTourA2);
                                $googleLinkTour2 = str_replace('/view?usp=sharing', '', $stringTourB2);
                                ?>
                                <img src='https://drive.google.com/uc?export=view&id=<?php echo $googleLinkTour2;?>' >
                                                              
<!--                                <p class="article-p text-center">
                                    * The above itinerary is subject to change depending on the weather and traffic condition on that day.<br>
                                    *The photos stated above are for internal use only, not for advertising.
                                </p>
                                -->
                            </div>
                        </div>

                        <div class="hide-print">
                            <script async src="https://static.addtoany.com/menu/page.js"></script>
                            <!-- AddToAny END -->

                            <div class="clear"></div>

                            <div class="width100 overflow padding-bottom50">
                                <!-- AddToAny BEGIN -->
                                <div class="border-div"></div>
                                <h3 class="dark-blue-text share-h3">Share:</h3>
                                <div class="a2a_kit a2a_kit_size_32 a2a_default_style">
                                <a class="a2a_button_copy_link"></a>
                                <a class="a2a_button_facebook"></a>
                                <a class="a2a_button_twitter"></a>
                                <a class="a2a_button_linkedin"></a>
                                <a class="a2a_button_blogger"></a>
                                <a class="a2a_button_facebook_messenger"></a>
                                <a class="a2a_button_whatsapp"></a>
                                <a class="a2a_button_wechat"></a>
                                <a class="a2a_button_line"></a>
                                <a class="a2a_button_telegram"></a>
                                <!--<a class="a2a_button_print"></a>-->
                                </div> 
                            </div>
                        </div>

                        <div class="clear"></div>

                        <div class="border-div"></div>
                            <h3 class="dark-blue-text share-h3">Recommended Place of Interest</h3>
                            <div class="width100 glider-contain">
                                <div class="glider glider1">

                                    <?php
                                    $conn = connDB();
                                    $recommend = getInterestingPlace($conn," WHERE uid != '$tourUid' AND state = '$tourState' AND display = 'YES' ORDER BY date_created DESC LIMIT 8 ");
                                    if($recommend)
                                    {   
                                        for($cntAA = 0;$cntAA < count($recommend) ;$cntAA++)
                                        {
                                        ?>
                                            <a href='userDetailsPOI.php?id=<?php echo $recommend[$cnt]->getUid();?>'>
                                                <div class="shadow-white-box product-box opacity-hover">
                                                    <div class=" square">
                                                        <div class="width100 white-bg image-box-size content2">
                                                        <?php $tourImage = $recommend[$cntAA]->getPhotoOne();?>
                                                            <?php 
                                                                $stringTourA = $tourImage;
                                                                $stringTourB = str_replace('https://drive.google.com/file/d/', '', $stringTourA);
                                                                $googleLinkTour = str_replace('/view?usp=sharing', '', $stringTourB);
                                                            ?>
                                                            <img src='https://drive.google.com/uc?export=view&id=<?php echo $googleLinkTour;?>' >
                                                        </div>
                                                    </div>

                                                    <div class="width100 product-details-div">
                                                        <p class="width100 text-overflow slider-product-name"><?php echo $recommend[$cntAA]->getTitle();?></p>
                                                        <p class="width100 slider-product-price text-overflow"><?php echo $recommend[$cntAA]->getState();?></p>
                                                    </div>
                                                </div>
                                            </a>
                                        <?php
                                        }
                                        ?>
                                    <?php
                                    }
                                    ?>

                                </div>
                            </div>
                        </div>

                    </div>
                </div>

            </div>

            <!-- <div class="price-div width100 same-padding">
            </div> -->

        <?php
        }
        ?>
    <?php
    }
    ?>

<?php
}
?>

<?php include 'js.php'; ?>
<style>
*{
   
	vertical-align:middle;
	word-break:keep-all !important;
}
a{
	opacity:0.8;
	transition:.15s ease-in-out;
	cursor:pointer;}
a:hover{
	opacity:1;}
.langz{
	display:none;}
.footer-div{
	display:none;}
</style>

</body>
</html>