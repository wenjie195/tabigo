<?php
// require_once dirname(__FILE__) . '/sessionLoginChecker.php';
if (session_id() == "")
{
    session_start();
}
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/InterestingPlace.php';
require_once dirname(__FILE__) . '/classes/TourPackage.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

$allTour = getTourPackage($conn, "WHERE display = 'Yes' ORDER BY date_created DESC LIMIT 8 ");
$allPlace = getInterestingPlace($conn, "WHERE display = 'Yes' ORDER BY date_created DESC LIMIT 8 ");

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>
<!doctype html>
<html>
<head>
	<?php include 'meta.php'; ?>
<!--         <meta property="og:url" content="https://tabigo.holiday/" />
        <link rel="canonical" href="https://tabigo.holiday/" />-->
        <meta property="og:title" content="Home | Tabigo" />
        <title>Home | Tabigo</title>   
	<?php include 'css.php'; ?>


</head>

<body class="body">
<?php include 'header.php'; ?>


<div class="clear"></div>
<div class="width100 overflow menu-distance same-padding min-height padding-bottom50">
	<!-- Change the Country Name -->
	<div class="first-line-gps red-text"><img src="img/location.png" class="gps-size"> Malaysia</div>
    <div class="clear"></div>
    <div class="second-line red-text"><img src="img/package.png" class="gift-size"> Tour Package <a href="#" class="red-text hover-effect pink-link underline">(View More)</a></div>
    <div class="width100 glider-contain">
        <div class="glider glider1">
    		<!-- Repeat this -->

          <?php
          if($allTour)
          {
              for($cnt = 0;$cnt < count($allTour) ;$cnt++)
              {
              ?>    
                  <!-- <a href='userTourDetails.php'> -->
                  <!-- <a href='#'> -->
                  <a href='userTourDetails_Renew.php?id=<?php echo $allTour[$cnt]->getUid();?>'>
                    <div class="shadow-white-box product-box opacity-hover">
                      <div class=" square">
                        <div class="width100 white-bg image-box-size content2">

                        <?php 
                          $linkTour = $allTour[$cnt]->getPhotoOne();
                          $stringTourA = $linkTour;
                          $stringTourB = str_replace('https://drive.google.com/file/d/', '', $stringTourA);
                          $googleLinkTour = str_replace('/view?usp=sharing', '', $stringTourB);
                        ?>
                          <!-- <img src="img/st george church.jpg" > -->
                          <img src='https://drive.google.com/uc?export=view&id=<?php echo $googleLinkTour;?>' >
                        </div>
                      </div>
                      <div class="width100 product-details-div">
                        <p class="width100 text-overflow slider-product-name"><?php echo $allTour[$cnt]->getTitle();?></p>
                        <p class="slider-product-price text-overflow">
                          RM<?php echo $allTour[$cnt]->getPrice();?>
                        </p>
                      </div>
                    </div>
                  </a>

              <?php
              }
          }
          ?>  
    
          <!-- End of Delete -->          
        </div>
    </div>
	
    <div class="clear"></div>
    <div class="second-line red-text"><img src="img/restaurant.png" class="gift-size"> Restaurant <a href="#" class="red-text hover-effect pink-link underline">(View More)</a></div>
    <div class="width100 glider-contain">
        <div class="glider glider2">
    		<!-- Repeat this -->
          <a href='#'>
            <div class="shadow-white-box product-box opacity-hover">
              <div class=" square">
                  <div class="width100 white-bg image-box-size content2">
                      
                        <img src="img/haixian.jpg" >
                  </div>
              </div>


              <div class="width100 product-details-div">



                <p class="width100 text-overflow slider-product-name">Zhonghua gourmet restaurant</p>
                <p class="slider-product-price text-overflow">
                  RM30
                </p>
              </div>
            </div>
          </a>
			<!-- End of Repeat, can remove bottom -->
          <a href='#'>
            <div class="shadow-white-box product-box opacity-hover">
              <div class=" square">
                  <div class="width100 white-bg image-box-size content2">
                      
                        <img src="img/new.jpg" >
                  </div>
              </div>


              <div class="width100 product-details-div">


                <p class="width100 text-overflow slider-product-name">New WK Restaurant Luyang (新旺角中餐点心)</p>
                <p class="slider-product-price text-overflow">
                  RM37
                </p>
              </div>
            </div>
          </a>

            
          <!-- End of Delete -->          
        </div>
    </div>
	    
    <div class="clear"></div>
    <div class="second-line red-text"><img src="img/bus.png" class="gift-size"> Transportation <a href="#" class="red-text hover-effect pink-link underline">(View More)</a></div>
    <div class="width100 glider-contain">
        <div class="glider glider3">
    		<!-- Repeat this -->
          <a href='#'>
            <div class="shadow-white-box product-box opacity-hover">
              <div class=" square">
                  <div class="width100 white-bg image-box-size content2">
                      
                        <img src="img/capital.jpg" >
                  </div>
              </div>


              <div class="width100 product-details-div">



                <p class="width100 text-overflow slider-product-name">Penang International Airport to City Hotel or V.V</p>
                <p class="slider-product-price text-overflow">
                 Private Charter
                </p>
              </div>
            </div>
          </a>
			<!-- End of Repeat, can remove bottom -->
                      <a href='#'>
            <div class="shadow-white-box product-box opacity-hover">
              <div class=" square">
                  <div class="width100 white-bg image-box-size content2">
                      
                        <img src="img/capital.jpg" >
                  </div>
              </div>


              <div class="width100 product-details-div">


                <p class="width100 text-overflow slider-product-name">Penang International Airport to Beach Hotel or V.V</p>
                <p class="slider-product-price text-overflow">
                  Private Charter
                </p>
              </div>
            </div>
          </a>
                    <a href='#'>
            <div class="shadow-white-box product-box opacity-hover">
              <div class=" square">
                  <div class="width100 white-bg image-box-size content2">
                      
                        <img src="img/capital.jpg" >
                  </div>
              </div>


              <div class="width100 product-details-div">



                <p class="width100 text-overflow slider-product-name">Penang International Airport to Cruise Terminal or V.V</p>
                <p class="slider-product-price text-overflow">
                  Private Charter
                </p>
              </div>
            </div>
          </a>
                    <a href='#'>
            <div class="shadow-white-box product-box opacity-hover">
              <div class=" square">
                  <div class="width100 white-bg image-box-size content2">
                      
                        <img src="img/capital.jpg" >
                  </div>
              </div>


              <div class="width100 product-details-div">



                <p class="width100 text-overflow slider-product-name">Butterworth Railway Station to City Hotel or V.V</p>
                <p class="slider-product-price text-overflow">
                  Private Charter
                </p>
              </div>
            </div>
          </a>
                    <a href='#'>
            <div class="shadow-white-box product-box opacity-hover">
              <div class=" square">
                  <div class="width100 white-bg image-box-size content2">
                      
                        <img src="img/capital.jpg" >
                  </div>
              </div>


              <div class="width100 product-details-div">



                <p class="width100 text-overflow slider-product-name">Butterworth Railway Station to Beach Hotel or V.V</p>
                <p class="slider-product-price text-overflow">
                  Private Charter
                </p>
              </div>
            </div>
          </a>
            
          <!-- End of Delete -->          
        </div>
    </div>    
    
	    
    <div class="clear"></div>
    <div class="second-line red-text"><img src="img/place.png" class="gift-size"> Place of Interest <a href="#" class="red-text hover-effect pink-link underline">(View More)</a></div>
    <div class="width100 glider-contain">
        <div class="glider glider4">

                <?php
                if($allPlace)
                {
                    for($cnt = 0;$cnt < count($allPlace) ;$cnt++)
                    {
                    ?>   

                        <a href='#'>
                          <div class="shadow-white-box product-box opacity-hover">
                            <div class=" square">
                              <div class="width100 white-bg image-box-size content2">
                                <!-- <img src="img/gurney plaza.jpg" > -->

                                <?php 
                                  $linkPlace = $allPlace[$cnt]->getPhotoOne();
                                  $stringPlaceA = $linkPlace;
                                  $stringPlaceB = str_replace('https://drive.google.com/file/d/', '', $stringPlaceA);
                                  $googleLinkPlace = str_replace('/view?usp=sharing', '', $stringPlaceB);
                                ?>
                                <img src='https://drive.google.com/uc?export=view&id=<?php echo $googleLinkPlace;?>' >

                              </div>
                            </div>
                            <div class="width100 product-details-div">
                              <p class="width100 text-overflow slider-product-name"><?php echo $allPlace[$cnt]->getTitle();?></p>
                              <p class="slider-product-price text-overflow">
                                <?php echo $allPlace[$cnt]->getState();?>
                              </p>
                            </div>
                          </div>
                        </a>

                    <?php
                    }
                }
                ?>  
   
        </div>
    </div>    
        
 	    
    <div class="clear"></div>
    <div class="second-line red-text"><img src="img/ticket.png" class="gift-size"> Ticket Booking <a href="#" class="red-text hover-effect pink-link underline">(View More)</a></div>
    <div class="width100 glider-contain">
        <div class="glider glider5">
    		<!-- Repeat this -->
          <a href='#'>
            <div class="shadow-white-box product-box opacity-hover">
              <div class=" square">
                  <div class="width100 white-bg image-box-size content2">
                      
                        <img src="img/escape.png" >
                  </div>
              </div>


              <div class="width100 product-details-div">



                <p class="width100 text-overflow slider-product-name">Escape Penang</p>
                <p class="slider-product-price text-overflow">
                 Penang
                </p>
              </div>
            </div>
          </a>
			<!-- End of Repeat, can remove bottom -->
 
          <!-- End of Delete -->          
        </div>
    </div>       
    
    <div class="clear"></div>
    <div class="second-line red-text"><img src="img/wifi.png" class="gift-size"> Wifi <a href="#" class="red-text hover-effect pink-link underline">(View More)</a></div>
    <div class="width100 glider-contain">
        <div class="glider glider6">
    		<!-- Repeat this -->
          <a href='#'>
            <div class="shadow-white-box product-box opacity-hover">
              <div class=" square">
                  <div class="width100 white-bg image-box-size content2">
                      
                        <img src="img/digi.png" >
                  </div>
              </div>


              <div class="width100 product-details-div">



                <p class="width100 text-overflow slider-product-name">Digi</p>
                <p class="slider-product-price text-overflow">
                 RM57
                </p>
              </div>
            </div>
          </a>
			<!-- End of Repeat, can remove bottom -->
 
          <!-- End of Delete -->          
        </div>
    </div>      
    <div class="clear"></div>
    <div class="second-line red-text"><img src="img/airplane.png" class="gift-size"> Hotel & Flight <a href="#" class="red-text hover-effect pink-link underline">(View More)</a></div>
    <div class="width100">    
    	<a href="#"><img src="img/hotel-flight.jpg" class="width100 opacity-hover"></a>
    </div>
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
</div>


<div class="clear"></div>
<?php include 'js.php'; ?>


</body>
</html>