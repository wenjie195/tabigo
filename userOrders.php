<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Orders.php';
require_once dirname(__FILE__) . '/classes/OrderList.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

// $userDetails = getUser($conn, "WHERE uid =?",array("uid"),array($uid),"s");
// $userData = $userDetails[0];

// $products = getOrderList($conn);
// $products = getOrderList($conn, "WHERE user_uid = ? ORDER BY date_created DESC ",array("user_uid"),array($uid),"s");
// $products = getOrderList($conn, "WHERE user_uid = ? AND status = 'Redeem' ORDER BY date_created DESC ",array("user_uid"),array($uid),"s");

// $allOrders = getOrders($conn, "WHERE payment_status = 'COMPLETED' AND shipping_status = 'PENDING' ");
$products = getOrders($conn, "WHERE uid = ? ORDER BY date_created DESC ",array("uid"),array($uid),"s");

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>
<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<meta property="og:url" content="https://tabigo.holiday/" />
<link rel="canonical" href="https://tabigo.holiday/" />
<meta property="og:title" content="Order History | Tabigo" />
<title>Order History | Tabigo</title>   
<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'header.php'; ?>


<div class="clear"></div>
<div class="width100 overflow menu-distance same-padding sakura-bg ow-sakura-height">
	<h1 class="title-h1 raleway red-text ow-no-margin wow fadeIn" data-wow-delay="0.1s">Order History</h1>	
        <div class="overflow-div">
            <table class="white-table">
                <thead>
                    <th>No.</th>
                    <th>Date</th>
                    <th>Order ID</th>
                    <th>Amount</th>
                    <th>Payment Details</th>
                    <th>Status</th>
                    <th>Details</th>
                </thead>

                <tbody>
                    <?php
                        if($products)
                        {
                            for($cnt = 0;$cnt < count($products) ;$cnt++)
                            {
                            ?>
                                <tr>
                                    <td><?php echo ($cnt+1)?></td>
                                    <td><?php echo $date = date("Y-m-d",strtotime($products[0]->getDateUpdated()));?></td>

                                    <td>
                                        <?php $productUid = $products[$cnt]->getOrderId();?>
                                        <?php echo $newstring = substr($productUid, 18, 30);?>
                                    </td>

                                    <td>RM<?php echo $products[$cnt]->getSubtotal();?></td>
                                    <td><?php echo $products[$cnt]->getPaymentBankReference();?></td>
                                    <td><?php echo $products[$cnt]->getShippingStatus();?></td>


                                    <td>
                                        <form action="userOrdersDetails.php" method="POST">
                                            <button class="transparent-button clean red-link hover-effect" type="submit" name="order_id" value="<?php echo $products[$cnt]->getOrderId();?>">
                                                Details
                                            </button>
                                        </form>
                                    </td>

                                </tr>
                            <?php
                            }
                        }
                    ?>                                 
                </tbody>

            </table>
        </div>
</div>

<div class="clear"></div>
<?php include 'js.php'; ?>

</body>
</html>