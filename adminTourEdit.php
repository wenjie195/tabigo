<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/TourPackage.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>
<!doctype html>
<html>

<head>
    <?php include 'meta.php'; ?>
    <!-- <meta property="og:url" content="https://tabigo.holiday/" />
    <link rel="canonical" href="https://tabigo.holiday/" /> -->
    <meta property="og:title" content="Edit Tour Package | Tabigo" />
    <title>Edit Tour Package | Tabigo</title>   
    <?php include 'css.php'; ?>
    <script src="//cdn.ckeditor.com/4.14.0/full/ckeditor.js"></script>
</head>

<body class="body">
<?php include 'headerAdmin.php'; ?>

<div class="clear"></div>

<div class="width100 overflow menu-distance same-padding ow-sakura-height">
	<h1 class="title-h1 raleway red-text ow-no-margin wow fadeIn" data-wow-delay="0.1s">Edit Tour Package</a></h1>

    <?php
    if(isset($_POST['package_uid']))
    {
    $conn = connDB();
    $tourDetails = getTourPackage($conn,"WHERE uid = ? ", array("uid") ,array($_POST['package_uid']),"s");
    ?>

        <form action="utilities/adminTourEditFunction.php" method="POST">

        <input class="tele-input clean" type="hidden" value="<?php echo $tourDetails[0]->getUid(); ?>" name="package_uid" id="package_uid" readonly> 

            <div class="dual-input">
                <p class="input-top-p admin-top-p">Country*</p>
                <!-- <select class="tele-input clean"><option>Malaysia</option></select> -->
                <input class="tele-input clean" type="text" value="<?php echo $tourDetails[0]->getCountry(); ?>" placeholder="Country" name="country" id="country" required>  
            </div>

            <div class="dual-input second-dual-input">
                <p class="input-top-p admin-top-p">State*</p>
                <!-- <select class="tele-input clean"><option>Penang</option></select>   -->
                <input class="tele-input clean" type="text" value="<?php echo $tourDetails[0]->getState(); ?>" placeholder="State" name="state" id="state" required>        
            </div>

            <div class="clean"></div>

            <div class="dual-input">
                <p class="input-top-p admin-top-p">Merchant Name*</p>
                <input class="tele-input clean" type="text" value="<?php echo $tourDetails[0]->getMerchantName(); ?>" placeholder="Merchant Name" name="merchant_name" id="merchant_name" required>  
            </div>

            <div class="dual-input second-dual-input">
                <p class="input-top-p admin-top-p">Price*</p>
                <input class="tele-input clean" type="text" value="<?php echo $tourDetails[0]->getPrice(); ?>" placeholder="Price" name="price" id="price" required>         
            </div>

            <div class="clean"></div>

            <div class="width100 overflow">
                <p class="input-top-p admin-top-p">Term and Condition (Less than 100 words)*</p>
                <input class="tele-input clean" type="text" value="<?php echo $tourDetails[0]->getTerms(); ?>" placeholder="Conditions" name="terms" id="terms" required>      
            </div>       

            <div class="width100 overflow">
                <p class="input-top-p admin-top-p">Title*</p>
                <input class="tele-input clean" type="text" value="<?php echo $tourDetails[0]->getTitle(); ?>" placeholder="Title" name="title" id="title" required>      
            </div>

            <div class="clear"></div>

            <div class="width100 overflow">
                <p class="input-top-p admin-top-p">Photo 1 Link in Google Drive <img src="img/drive-link.jpg" class="tutorial-img"></p>
                <input class="tele-input clean" type="text" value="<?php echo $tourDetails[0]->getPhotoOne(); ?>" placeholder="Photo 1 Link in Google Drive" name="photo_one" id="photo_one" required>    
            </div>      

            <div class="clear"></div>

            <div class="width100 overflow">
                <p class="input-top-p admin-top-p">Photo 2 Link in Google Drive <img src="img/drive-link.jpg" class="tutorial-img"></p>
                <input class="tele-input clean" type="text" value="<?php echo $tourDetails[0]->getPhotoTwo(); ?>" placeholder="Photo 2 Link in Google Drive" name="photo_two" id="photo_two">    
            </div>      

            <div class="clear"></div>

            <div class="width100 overflow">
                <p class="input-top-p admin-top-p">External Link (If Any) <img src="img/drive-link.jpg" class="tutorial-img"></p>
                <input class="tele-input clean" type="text" value="<?php echo $tourDetails[0]->getLinks(); ?>" placeholder="Link" name="google_link" id="google_link">    
            </div>        

            <div class="clear"></div>

            <div class="width100 overflow">
                <p class="input-top-p admin-top-p">Description</p>
                <textarea class="tele-input clean lato input-textarea admin-input editor-input" type="text" placeholder="Description" name="description" id="description"><?php echo $tourDetails[0]->getDescription(); ?></textarea>  	
            </div>   

            <div class="form-group publish-border input-div width100 overflow">
                <p class="input-top-p admin-top-p">Main Content (Avoid "' and don't copy paste image inside, click the icon for upload image/video/link tutorial)
                    <img src="img/refer.png" class="refer-png pointer opacity-hover open-refertexteditor" alt="Click Me" title="Click Me">
                </p>
                <textarea name="editor" id="editor" rows="10" cols="80"  class="tele-input clean lato blue-text input-textarea admin-input editor-input" ><?php echo $tourDetails[0]->getParagraphOne(); ?></textarea>
            </div>    

            <div class="clear"></div>  

            <div class="width100 text-center">
                <button class="clean red-btn hover-effect middle-button-size below-forgot margin-bottom30" name="submit">Submit</button>
            </div>

        </form>

    <?php
    }
    ?>

</div>

<div class="clear"></div>

<?php include 'js.php'; ?>

<script>
    CKEDITOR.replace('editor');
</script>

</body>
</html>