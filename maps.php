<?php
if (session_id() == "")
{
    session_start();
}
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

// require_once dirname(__FILE__) . '/classes/Article.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
// require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$conn = connDB();

// $articles = getArticles($conn, " WHERE display = 'Yes' ORDER BY date_created DESC LIMIT 4");

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<meta property="og:url" content="https://vincaps.com/" />
<link rel="canonical" href="https://vincaps.com/" />
<meta property="og:title" content="Maps" />
<title>Maps</title>
<meta property="og:description" content="We are experts in strategic business development and solutions, investor relationship service and various capital raising exercises that would help you get funded and realize your business dream." />
<meta name="description" content="We are experts in strategic business development and solutions, investor relationship service and various capital raising exercises that would help you get funded and realize your business dream." />
<meta name="keywords" content="Get Funded, Fundraising, ECF, Equity Crowd Funding, Angel Investor, Venture Capital, Business Funding, Accelerator, IPO, Company Valuation, Private Equity, Entrepreneurship, PitchDeck, Pitching, Investor, Business Proposal, Initial Public Offering, fundraising company in malaysia, fundraising company in penang, strategic business partner, Equity Crowdfuning, Family Office, Government Grants, fundraising consulting firm, 融资, 筹资, 投资, 投资商,">

<?php include 'css.php'; ?>
</head>

<body class="body">

<div class="clear"></div>  

<div class="width100">
<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3972.7687684224506!2d100.26568191471223!3d5.298736596155999!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x304abffb10295c85%3A0x1c90f81946e8fde5!2sSummerSkye%20Residences!5e0!3m2!1sen!2smy!4v1617605578556!5m2!1sen!2smy"></iframe>
</div>


<div id="googleMap" style="width:100%;height:400px;"></div>

<script>
function myMap() {
var mapProp= {
  center:new google.maps.LatLng(51.508742,-0.120850),
  zoom:5,
};
var map = new google.maps.Map(document.getElementById("googleMap"),mapProp);
}
</script>

<script src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY&callback=myMap"></script>

<?php include 'js.php'; ?>

</body>
</html>