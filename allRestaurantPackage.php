<?php
if (session_id() == "")
{
    session_start();
}
// require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Restaurant.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

// $conn = connDB();

// $_SESSION['url'] = $_SERVER['REQUEST_URI'];
// // $allTour = getRestaurant($conn, "WHERE display = 'Yes' ");

// $conn->close();

$_SESSION['url'] = $_SERVER['REQUEST_URI'];

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
    $conn = connDB();

    $country = rewrite($_POST['country']);
    $state = rewrite($_POST['state']);
    $cuisine = rewrite($_POST['cuisine']);

    // echo $country = rewrite($_POST['country']);
    // echo "<br>";
    // echo $state = rewrite($_POST['state']);
    // echo "<br>";
    // echo $cuisine = rewrite($_POST['cuisine']);

    // $allTour = getRestaurant($conn, "WHERE country = ? AND state = ? ",array("country","remark"),array($country,$state),"ss");
    $allTour = getRestaurant($conn, "WHERE country = ? AND state = ? AND cuisine = ? ",array("country","remark","cuisine"),array($country,$state,$cuisine),"sss");

    $conn->close();
}

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>


<!doctype html>
<html>

<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://tabigo.holiday/" />
<link rel="canonical" href="https://tabigo.holiday/" />-->
<meta property="og:title" content="All Restaurant Package | Tabigo" />
<title>All Restaurant Package | Tabigo</title>   
<?php include 'css.php'; ?>
</head>


<body class="body">
<?php include 'header.php'; ?>

<div class="width100 overflow menu-distance same-padding min-height padding-bottom50">


    <div class="width100 overflow">

    <h1 class="second-line red-text ow-margin-top0 title-css" style="margin-bottom:10px !important;">Restaurant Package (<?php echo $state;?>, <?php echo $country;?>)</h1>


        <!-- <form class="filter-form">
            <select class="clean filter-select state-select">
                <option>Penang</option>
                <option>Selangor</option>
            </select>

            <select class="clean filter-select price-filter">
                <option>Lowest price first</option>
                <option>Highest price first</option>
            </select> 

            <div class="tempo-clear1"></div>
            <button class="clean filter-button hover-effect red-btn">Filter</button>
        </form> -->

        </div>
		<div class="clear"></div>
    	<!-- <h1 class="line-header margin-bottom50">PingCash Shop</h1> -->
 		<div class="width103">

            <!-- <div class="repeat-four-width-div">
                <img src="img/p16.jpg" class="width100">
                <div class="sold-label">Out of Stock</div>
                <div class="product-description">
                    <p class="product-name-p text-overflow">Razer Hammerhead True Wireless Earbuds</p>
                    <p class="price-p">350.00</p>
                </div>
            </div> -->

        <?php
        if($allTour)
        {
            for($cnt = 0;$cnt < count($allTour) ;$cnt++)
            {
            ?>
			<a href='userDetailsRestaurant.php?id=<?php echo $allTour[$cnt]->getUid();?>'>
            <div class="whitebox-redshadow showall-box opacity-hover">
                <div class="square miss2">
                    <div class="width100 white-bg image-box-size content2">
                        <!-- <a href='userTourDetails.php?id=<?php //echo $allTour[$cnt]->getUid();?>'> -->
                        
                            <?php 
                                $linkTour = $allTour[$cnt]->getPhoto();
                                $stringTourA = $linkTour;
                                $stringTourB = str_replace('https://drive.google.com/file/d/', '', $stringTourA);
                                $googleLinkTour = str_replace('/view?usp=sharing', '', $stringTourB);
                            ?>
                            <img src='https://drive.google.com/uc?export=view&id=<?php echo $googleLinkTour;?>' class="width100">
                    </div>
                </div>
                            <div class="miss3" id="a<?php echo $allTour[$cnt]->getUid();?>"></div>
                             <style>
                             	#a<?php echo $allTour[$cnt]->getUid();?>{
									background-image:url(https://drive.google.com/uc?export=view&id=<?php echo $googleLinkTour;?>);}
                             </style>                
                            <div class="width100 product-details-div">
                                <p class="width100 text-overflow slider-product-name"><?php echo $allTour[$cnt]->getName();?></p>

                                <p class="width100 slider-product-price text-overflow">
                                    <?php 
                                    $country = $allTour[$cnt]->getCountry();
                                    if($country == "Malaysia")
                                    {
                                        echo "RM";

                                    }
                                    elseif($country == "Japan")
                                    {
                                        echo "JPY";
                                    }
                                    else
                                    {}
                                    ?>
                                    <?php echo $allTour[$cnt]->getPrice();?>
                                </p>
                            </div>
                        

            </div>
			</a>
            <?php
            }
            ?>
        <?php
        }
        ?>
                          
        </div>
    </div>


<div class="clear"></div>
<?php include 'js.php'; ?>

</body>
</html>