<?php
if (session_id() == "")
{
    session_start();
}
// require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Countries.php';
require_once dirname(__FILE__) . '/classes/InterestingPlace.php';
require_once dirname(__FILE__) . '/classes/Restaurant.php';
require_once dirname(__FILE__) . '/classes/Ticket.php';
require_once dirname(__FILE__) . '/classes/TourPackage.php';
require_once dirname(__FILE__) . '/classes/Transport.php';
require_once dirname(__FILE__) . '/classes/Wifi.php';
// require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/classes/Session.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

// $conn = connDB();

function addCountrySession($conn,$ip_address,$countryName)
{
    if(insertDynamicData($conn,"session",array("ip","country"),
    array($ip_address,$countryName),"ss") === null)
    {
        echo "gg";
    }
    else{    }
    return true;
}

// $uid = $_SESSION['uid'];

//whether ip is from share internet
if (!empty($_SERVER['HTTP_CLIENT_IP']))   
{
    $ip_address = $_SERVER['HTTP_CLIENT_IP'];
}
//whether ip is from proxy
elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))  
{
    $ip_address = $_SERVER['HTTP_X_FORWARDED_FOR'];
}
//whether ip is from remote address
else
{
    $ip_address = $_SERVER['REMOTE_ADDR'];
}
$ip_address;

$_SESSION['url'] = $_SERVER['REQUEST_URI'];
// $allTour = getTourPackage($conn, "WHERE display = 'Yes' ORDER BY date_created DESC LIMIT 8 ");
// $allPlace = getInterestingPlace($conn, "WHERE display = 'Yes' ORDER BY date_created DESC LIMIT 8 ");

if(isset($_GET['id']))
{
    // echo $_GET['id'];
    $conn = connDB();
    // echo $_SESSION['country_id'] = $_GET['id'];

    // $_SESSION['country_id'] = $_GET['id'];
    $countryDetails = getCountries($conn,"WHERE id = ? ",array("id"),array($_GET['id']), "s");
    // $_SESSION['country_name'] = $countryName = $countryDetails[0]->getEnName();
    $countryName = $countryDetails[0]->getEnName();

    if(addCountrySession($conn,$ip_address,$countryName))
    {}
}

// $conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>
<!doctype html>
<html>


    <?php
    if($countryDetails)
    {
      for($cnt = 0;$cnt < count($countryDetails) ;$cnt++)
      {
      ?>
        <head>
        <?php include 'meta.php'; ?>
        <!-- <meta property="og:url" content="https://tabigo.holiday/" />
        <link rel="canonical" href="https://tabigo.holiday/" /> -->
        <meta property="og:title" content="<?php echo $countryDetails[$cnt]->getEnName();?> | Tabigo" />
        <title><?php echo $countryDetails[$cnt]->getEnName();?> | Tabigo</title>   
        <?php include 'css.php'; ?>
        </head>
      <?php
      }
      ?>
    <?php
    }
    ?>

<body class="body">
<?php include 'header.php'; ?>

<div class="clear"></div>
<div class="width100 overflow menu-distance same-padding min-height padding-bottom50">


    <div class="category-container">



            <?php
            if($countryDetails)
            {
                for($cnt = 0;$cnt < count($countryDetails) ;$cnt++)
                {
                ?>    

                    <!-- <a href='allTourPackage.php?id=<?php //echo $countryDetails[$cnt]->getEnName();?>'>
                        <div class="four-div1 whitebox-redshadow category-div hover-effect">
                            <img src="img/tour.png" class="category-png"> Tour Package
                        </div>
                    </a> -->

                    <form action="selectState.php" method="POST" class="four-div1 whitebox-redshadow category-div hover-effect">
                        <button class="clean border0 transparent width100 category-font-size hover-effect">
                            <img src="img/tour.png" class="category-png"> Tour Package
                            <input class="tele-input clean" type="hidden" value="<?php echo $countryDetails[$cnt]->getEnName();?>" name="country" id="country" readonly> 
                            <input class="tele-input clean" type="hidden" value="Tour" name="option" id="option" readonly> 
                        </button>
                    </form>

                    <!-- <a href='allRestaurantPackage.php?id=<?php //echo $countryDetails[$cnt]->getEnName();?>'>
                        <div class="four-div1 whitebox-redshadow category-div hover-effect">
                            <img src="img/restaurant2.png" class="category-png"> Restaurant
                        </div>
                    </a> -->

                    <form action="selectState.php" method="POST" class="four-div1 whitebox-redshadow category-div hover-effect">
                        <button class="clean border0 transparent width100 category-font-size hover-effect">
                            <img src="img/restaurant2.png" class="category-png"> Restaurant
                            <input class="tele-input clean" type="hidden" value="<?php echo $countryDetails[$cnt]->getEnName();?>" name="country" id="country" readonly> 
                            <input class="tele-input clean" type="hidden" value="Restaurant" name="option" id="option" readonly> 
                        </button>
                    </form>

                    <!-- <a href='allTransportation.php?id=<?php //echo $countryDetails[$cnt]->getEnName();?>'>    
                        <div class="four-div1 whitebox-redshadow category-div hover-effect">
                            <img src="img/transportation.png" class="category-png"> Transportation
                        </div>
                    </a> -->

                    <form action="selectState.php" method="POST" class="four-div1 whitebox-redshadow category-div hover-effect">
                        <button class="clean border0 transparent width100 category-font-size hover-effect">
                            <img src="img/transportation.png" class="category-png"> Transportation
                            <input class="tele-input clean" type="hidden" value="<?php echo $countryDetails[$cnt]->getEnName();?>" name="country" id="country" readonly> 
                            <input class="tele-input clean" type="hidden" value="Transport" name="option" id="option" readonly> 
                        </button>
                    </form>

                    <!-- <a href='allPointOfInterest.php?id=<?php //echo $countryDetails[$cnt]->getEnName();?>'>   
                        <div class="four-div1 whitebox-redshadow category-div hover-effect">
                            <img src="img/place2.png" class="category-png"> Place of Interest
                        </div> 
                    </a> -->

                    <form action="selectState.php" method="POST" class="four-div1 whitebox-redshadow category-div hover-effect">
                        <button class="clean border0 transparent width100 category-font-size hover-effect">
                            <img src="img/place2.png" class="category-png"> Place of Interest
                            <input class="tele-input clean" type="hidden" value="<?php echo $countryDetails[$cnt]->getEnName();?>" name="country" id="country" readonly> 
                            <input class="tele-input clean" type="hidden" value="POI" name="option" id="option" readonly> 
                        </button>
                    </form>

                    <!-- <a href='allTicketPackage.php?id=<?php //echo $countryDetails[$cnt]->getEnName();?>'>   
                        <div class="four-div1 whitebox-redshadow category-div hover-effect">
                            <img src="img/ticket2.png" class="category-png"> Ticket Booking
                        </div>
                    </a> -->

                    <form action="selectState.php" method="POST" class="four-div1 whitebox-redshadow category-div hover-effect">
                        <button class="clean border0 transparent width100 category-font-size hover-effect">
                            <img src="img/ticket2.png" class="category-png"> Ticket Booking
                            <input class="tele-input clean" type="hidden" value="<?php echo $countryDetails[$cnt]->getEnName();?>" name="country" id="country" readonly> 
                            <input class="tele-input clean" type="hidden" value="Ticket" name="option" id="option" readonly> 
                        </button>
                    </form>

                    <!-- <a href='allWifiPackage.php?id=<?php //echo $countryDetails[$cnt]->getEnName();?>'>   
                        <div class="four-div1 whitebox-redshadow category-div hover-effect">
                            <img src="img/wifi2.png" class="category-png"> Wifi
                        </div>
                    </a> -->

                    <form action="selectState.php" method="POST" class="four-div1 whitebox-redshadow category-div hover-effect">
                        <button class="clean border0 transparent width100 category-font-size hover-effect">
                            <img src="img/wifi2.png" class="category-png"> Wifi
                            <input class="tele-input clean" type="hidden" value="<?php echo $countryDetails[$cnt]->getEnName();?>" name="country" id="country" readonly> 
                            <input class="tele-input clean" type="hidden" value="Wifi" name="option" id="option" readonly> 
                        </button>
                    </form>

                    <!-- <a href='#'>         -->
                    <a href="https://c121.travelpayouts.com/click?shmarker=297571&promo_id=3770&source_type=link&type=click&trs=137433" target="_blank">
                        <div class="four-div1 whitebox-redshadow category-div hover-effect">
                            <img src="img/flight.png" class="category-png"> Hotel & Flight
                        </div>
                    </a>    
                    <!-- <a href='#'>          -->
                    <a href="https://www.tokionow.com/easc/online/Travel-Step-1-Single.jsp?aid=BORYITLING" target="_blank">
                        <div class="four-div1 whitebox-redshadow category-div hover-effect">
                            <img src="img/insurance.png" class="category-png"> Insurance
                        </div>
                    </a>                
                    <a href="index.php">
                        <div class="four-div1 whitebox-redshadow category-div hover-effect">
                            <img src="img/oversea.png" class="category-png"> Oversea Tour
                        </div>
                    </a>   

                <?php
                }
            }
            ?>  


        
    </div>

    <div class="clear"></div>
    
    <div class="second-line red-text">
        <img src="img/package.png" class="gift-size"> Trending
        <!-- <a href="#" class="red-text hover-effect pink-link underline">(View More)</a>-->
    </div>
    
    <div class="width100 glider-contain">
        <div class="glider glider1">

        <?php
        // if(isset($_GET['id']))
        // {
        $conn = connDB();
        // $countryDetails = getCountries($conn,"WHERE id = ? ",array("id"),array($_GET['id']), "s");
        // $countryName = $countryDetails[0]->getEnName();

        if($countryName)
        {
        $allTour = getTourPackage($conn,"WHERE country = ? AND display = 'Yes' ORDER BY date_created DESC LIMIT 8",array("country"),array($countryName), "s");
        ?>

            <?php
            if($allTour)
            {
            for($cnt = 0;$cnt < count($allTour) ;$cnt++)
            {
            ?> 

                <a href='userDetailsTour.php?id=<?php echo $allTour[$cnt]->getUid();?>'>
                    <div class="shadow-white-box product-box opacity-hover">
                        <div class=" square">
                            <div class="width100 white-bg image-box-size content2">
                                <?php 
                                $linkTour = $allTour[$cnt]->getPhotoOne();
                                $stringTourA = $linkTour;
                                $stringTourB = str_replace('https://drive.google.com/file/d/', '', $stringTourA);
                                $googleLinkTour = str_replace('/view?usp=sharing', '', $stringTourB);
                                ?>
                                <!-- <img src="img/st george church.jpg" > -->
                                <img src='https://drive.google.com/uc?export=view&id=<?php echo $googleLinkTour;?>' >
                            </div>
                        </div>
                        <div class="width100 product-details-div">
                            <p class="width100 text-overflow slider-product-name"><?php echo $allTour[$cnt]->getTitle();?></p>
                            <p class="slider-product-price text-overflow">
                            RM<?php echo $allTour[$cnt]->getPrice();?>
                            </p>
                        </div>
                    </div>
                </a>

            <?php
            }
            }
            ?>  

        <?php
        }
        ?>
         
        </div>
    </div>
	
    <div class="clear"></div>
    
</div>

<div class="clear"></div>
<?php include 'js.php'; ?>

</body>
</html>