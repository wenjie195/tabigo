<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Orders.php';
require_once dirname(__FILE__) . '/classes/OrderList.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>
<!doctype html>
<html>

<head>
<?php include 'meta.php'; ?>
<meta property="og:url" content="https://tabigo.holiday/" />
<link rel="canonical" href="https://tabigo.holiday/" />
<meta property="og:title" content="Order Details | Tabigo" />
<title>Order Details | Tabigo</title>   
<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'header.php'; ?>

<div class="clear"></div>

<!-- <div class="width100 overflow menu-distance same-padding sakura-bg with-price-height min-height2"> -->
<div class="width100 overflow menu-distance same-padding sakura-bg ow-sakura-height">
    
    <h1 class="title-h1 raleway red-text ow-no-margin follow-dual-align" >Order Details</h1>	

    <?php
    if(isset($_POST['order_id']))
    {
    $conn = connDB();
    $orderDetails = getOrders($conn,"WHERE order_id = ? ", array("order_id") ,array($_POST['order_id']),"s");
    $userUid = $orderDetails[0]->getUid();
    $userDetails = getUser($conn,"WHERE uid = ? ", array("uid") ,array($userUid),"s");
    ?>

    <div class="dual-input det-div">
    	<p class="input-top-p">Full Name</p>
        <p class="user-details1"><?php echo $orderDetails[0]->getName(); ?></p>
    </div>
	<div class="dual-input second-dual-input det-div">
    	<p class="input-top-p">IC Number</p>
        <p class="user-details1"><?php echo $userDetails[0]->getIcNo(); ?></p>
    </div>  
    <div class="clear"></div>
	<div class="dual-input det-div">
    	<p class="input-top-p">Phone Number</p>
        <p class="user-details1"><?php echo $orderDetails[0]->getContactNo(); ?></p>
    </div>
	<div class="dual-input second-dual-input det-div">
    	<p class="input-top-p">Country</p>
        <p class="user-details1"><?php echo $userDetails[0]->getCountry(); ?></p>
    </div>      
    <div class="clear"></div>
	<div class="width100 det-div">
    	<p class="input-top-p">Email</p>
        <p class="user-details1"><?php echo $userDetails[0]->getEmail(); ?></p>
    </div>    

    <?php
    }
    ?>

      
    <?php
    if(isset($_POST['order_id']))
    {
    $conn = connDB();
    $productDetails = getOrderList($conn,"WHERE order_id = ? ", array("order_id") ,array($_POST['order_id']),"s");
    ?>

        <div class="overflow-div">
            <table class="white-table">
                 <thead>
                    <th>No.</th>
                    <th>Package Name</th>
                    <th>Booking Date</th>
                    <th>Unit Price</th>
                    <th>Quantity</th>
                    <th>Total Price</th>
                    <th>Status</th>
                </thead>

                <?php
                if($productDetails)
                {
                    for($cnt = 0;$cnt < count($productDetails) ;$cnt++)
                    {
                    ?>    
                        <tr>
                            <td><?php echo ($cnt+1)?></td>
                            <td><?php echo $productDetails[$cnt]->getProductName();?></td>
                            <td><?php echo $productDetails[$cnt]->getServiceDate();?></td>
                            <td>RM<?php echo $productDetails[$cnt]->getOriginalPrice();?></td>
                            <td><?php echo $productDetails[$cnt]->getQuantity();?></td>
                            <td>RM<?php echo $productDetails[$cnt]->getTotalPrice();?></td>
                            <td><?php echo $productDetails[$cnt]->getStatus();?></td>
                        </tr>
                    <?php
                    }
                }
                ?>  

            </table>
        </div>
 
    <?php
    }
    ?>
	
</div>


<?php include 'js.php'; ?>

<!-- <style>
*{
   
	vertical-align:middle;
	word-break:keep-all !important;
}
a{
	opacity:0.8;
	transition:.15s ease-in-out;
	cursor:pointer;}
a:hover{
	opacity:1;}
.langz{
	display:none;}
.footer-div{
	display:none;}
</style> -->

</body>
</html>